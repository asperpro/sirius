<?php

require_once 'init.php';

$soapClientUrl = $configData['soap/domain'] . '/api/soap/?wsdl';
$soapOptions = array(
	'keep_alive' => true,
	'encoding'      =>'UTF-8',
	'compression'   => SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP | SOAP_COMPRESSION_DEFLATE,
);

$endPoint = '/api/nomenclature';
$client     = new Zend_Rest_Client($configData['crm/domain']);
$client->getHttpClient()->setConfig(array('timeout' => 30));
$clientSOAP = new SoapClient($soapClientUrl, $soapOptions);

$productModel           = new Catalog_Model_Product();
$productAttributesModel = new Catalog_Model_Product_Attributes();
$productCategoriesModel = new Catalog_Model_Category_Products();
$productMediaModel = new Catalog_Model_Product_Medias();

$session = $clientSOAP->login($configData['soap/login'], $configData['soap/password']);
// Get completed products
if ($products = $productModel->getAllByStatus('completed', $configData['cron/downloader/limit'], array(2, 3, 4, 5, 8, 10, 11, 12))) {
	foreach ($products as $key => $product) {
		echo $product['name'] . "\n\r";
		// Send request to Platon
		$data = array(
			'name' => $product['name'],
			'sku' => $product['sku'],
			'price' => $product['price'],
			'alias' => $product['alias'],
			'supplier_id' => $product['supplier_id'],
		);
		$response = $client->restPost($endPoint, $data, 'UTF-8');
		if (200 == $response->getStatus()) {
			// Send request to Magento
			$responseData = $response->getBody();
			$responseData = json_decode($responseData);

			$productCategories = $productCategoriesModel->getByProdId($product['id']);
			$productAttributes = $productAttributesModel->getByProdId($product['id']);

			$singleAttribData = array();
			$multiAttribData = array();

			foreach ($productAttributes as $attribute) {
				if ($attribute['type'] == 'multiselect') {
					$multiAttribData[$attribute['code']][] = $attribute['value'];
				} else {
					$singleAttribData[$attribute['code']] = $attribute['value'];
				}
			}
			$productAttributes = array(
				'single_data' => $singleAttribData,
				'multi_data'  => $multiAttribData
			);
			try {
				$result = $clientSOAP->call($session, 'catalog_product.create', array('simple', $product['set_id'], $responseData->code, array(
					'status'      			=> '1',
					'visibility'  			=> '4',
					'name'        			=> $product['name'],
					'weight'        			=> $product['weight'],
					'price'        			=> $product['price'],
					'description' 			=> $product['description'],
					'short_description' 		=> $product['description'],
					'additional_attributes' 		=> $productAttributes,
					'categories'  			=> $productCategories,
					'websites'    			=> array(1),
				)));
				
				// Set product media attributes
				$mediaImageMode = array('image', 'small_image', 'thumbnail');
				if ($productMedia = $productMediaModel->getByProdId($product['id'])) {
					foreach ($productMedia as $position => $media) {
						$fileContent = base64_encode(file_get_contents(MEDIA_PATH . $media['filename']));
						$file = array(
							'content' => $fileContent,
							'mime' => $media['mime_type'],
						);
						$clientSOAP->call($session, 'catalog_product_attribute_media.create', array($result, array(
							'file' => $file,
							'position' => $position,
							'types' => ($media['is_main']) ? $mediaImageMode : '',
							'exclude' => 0
						)));
					}	
				}
				
			} catch (SoapFault $e) {
				echo $e->getMessage();
			}

			// Update product status
			$values = array(
				'status' => 'archived',
			);
			$productModel->update($values, $product['id']);
		}
	}
}

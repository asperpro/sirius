<?php

set_time_limit(0);

ini_set('display_errors', 1);
ini_set('default_socket_timeout', 600);
error_reporting(E_ALL);

$time = microtime(true);
$memory = memory_get_usage();
 
// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

define('MEDIA_PATH', APPLICATION_PATH . '/../media/product/');

set_include_path(
APPLICATION_PATH.'/../library'.PATH_SEPARATOR.
APPLICATION_PATH.'/../library/Zend'.PATH_SEPARATOR
);

require_once 'Zend/Application.php';

// Create application, bootstrap, and run
$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);
$application->bootstrap();

// Get config
$config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', 'production');
$configModel = new System_Model_Config();
$configData = $configModel->getAllConfigs();

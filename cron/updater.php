<?php

require_once 'init.php';

// Init models
$catalogCategoryModel  = new Catalog_Model_Category();
$attributeSetModel     = new Attribute_Model_Attribute_Set();
$attributeModel        = new Attribute_Model_Attribute();
$attributeOptionsModel = new Attribute_Model_Attribute_Options();
$supplierModel		   = new Reference_Model_Supplier();

// Get suppliers
$endPoint = '/api/supplier';
$client = new Zend_Rest_Client($config->erp->domain);
$response = $client->restGet($endPoint);
if (200 == $response->getStatus()) {
	// Delete all suppliers
	$supplierModel->deleteAll();
	// Set all suppliers
	$responseData = Zend_Json::decode($response->getBody());
	foreach ($responseData['data'] as $id => $value) {
		$data = array(
			'id' => $id,
			'name' => $value,
		);
		$supplierModel->add($data);
	}
}

// Send request to Magento SOAP Api
$soapClientUrl = $config->soap->client_domain . '/api/soap/?wsdl';
$soapOptions = array(
	'keep_alive' => true,
	'encoding'      =>'UTF-8',
    'compression'   => SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP | SOAP_COMPRESSION_DEFLATE,
);
$client = new SoapClient($soapClientUrl, $soapOptions);
$session = $client->login('rudakov', 'llk78n3crke3dkldlcgqv18qwt237d9t');

// Clear category table
$catalogCategoryModel->deleteAll();

// Get all categories
$apiResponses = array();
$apiResponses[] = $client->call($session, 'catalog_category.tree');
getCategories($apiResponses);
unset($apiResponses);

// Clear attribute tables
$attributeSetModel->deleteAll();
$attributeModel->deleteAll();
$attributeOptionsModel->deleteAll();

// Get all attributes sets
$apiResponses = $client->call($session, "catalog_product_attribute_set.list");
foreach ($apiResponses as $set) {
    $setList = array(
        'id' => $set['set_id'],
        'name' => $set['name']
    );
    $attributeSetModel->add($setList);
    
    // Get set attributes
    if ($attributes = $client->call($session, "product.listOfAdditionalAttributes", array(
        'simple',
        $set['set_id']
    ))) {
    	foreach ($attributes as $attribute) {
    		
		    $attributeInfo = $client->call($session, 'product_attribute.info', $attribute['attribute_id']);
		    if ($attributeInfo['is_user_defined'] == '1') {
		        $attributesList = array(
		            'id'   => $attribute['attribute_id'],
		            'code' => $attribute['code'],
		            'type' => $attribute['type'],
		            'name' => $attributeInfo['frontend_label'][0]['label'],
		        );
		        $attributeModel->replace($attributesList);
		        $attributeModel->setAttributeToSet($set['set_id'], $attribute['attribute_id']);
		
		        if (in_array($attributesList['type'], array('select', 'multiselect'))) {
		        	if (isset($attributeInfo['options'])) {
		            	foreach ($attributeInfo['options'] as $option) {
		                    $optionList = array(
		                        'id'           => $option['value'],
		                        'attribute_id' => $attributesList['id'],
		                        'name'         => $option['label']
		                    );
		                    $attributeOptionsModel->replace($optionList);
		                }
		        	}
		        }
		    }
		}
    }
}

$client->endSession($session);

function getCategories($apiCatalogs)
{
	global $catalogCategoryModel;
	global $crumbs;
	
    foreach ($apiCatalogs as $catalog) {
    	if (2 >= $catalog['level']) {
    		$crumbs = array();
    	}
    	$crumbs = array_slice($crumbs, 0, $catalog['level'] - 2);
    	$crumbs[] = $catalog['name'];
        $catalogList = array(
            'id'        => $catalog['category_id'],
            'parent_id' => $catalog['parent_id'],
            'level'     => $catalog['level'],
            'name'      => $catalog['name'],
            'full_name' => implode(' / ', $crumbs),
            'is_active' => (int)$catalog['is_active']
        );
        $catalogCategoryModel->add($catalogList);

        if (!empty($catalog['children'])) {
            getCategories($catalog['children']);
        }
    }
}
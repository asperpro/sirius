<?php

require_once 'init.php';

// Loading models
$productModel = new Catalog_Model_Product();
$interfaceModel = new Interface_Model_Interface();

// Get pending products
if ($products = $productModel->getQueue(10)) {
	foreach ($products as $product) {
		// Get interfaces by supplier
		if ($interfaces = $interfaceModel->getBySupplierId($product['supplier_id'])) {
			foreach ($interfaces as $interface) {
				echo $product['name'], "\n\r";
				$resourceModel = $interfaceModel->getResourceModel($interface['name']);
				$resourceModel->setProduct($product);
				$resourceModel->setInterface($interface);
				$resourceModel->search();
				$resourceModel->save();
				unset($resourceModel);
			}
		}
		$productModel->updateQueue($product['id']);
		sleep(rand(1,2));
	}
}
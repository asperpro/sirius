<?php

class Platon_Acl
{
    protected $_acl = null;

    public function __construct()
    {
		$this->storeAcl();
		$this->setPermissions();
		
		return $this->getAcl();
    }
    
    public function getAcl()
    {
        return $this->_acl;
    }
    
    public function storeAcl()
    {
        if (null !== $this->_acl) {
            return $this->_acl;
        }
        $acl = new Zend_Acl();
        $this->_acl = $acl;
        
        return $this->_acl;
    }
    
    public function setPermissions()
    {
        $acl = $this->getAcl();
        // get resources
        $resourceModel = new Auth_Model_Resource();
   		$resources = $resourceModel->fetchAll();
        foreach ($resources as $resource) {
        	$resourceData = $this->toArray($resource->resource);
        	$resourceItem = $resourceData['module'] . '_' . $resourceData['controller'];
            if (false == $this->getAcl()->has($resourceItem)) {
                $this->getAcl()->add(new Zend_Acl_Resource($resourceItem));
            }
        }
        // get roles
        $roleModel = new Auth_Model_Role();
   		$roles = $roleModel->fetchAll();
        foreach ($roles as $role) {
            $this->getAcl()->addRole(new Zend_Acl_Role('role_' . $role->id));
        }
        // get permissions
        $permissionModel = new Auth_Model_Permission();
        $permissions = $permissionModel->getAllPermissions();
        
        foreach ($permissions as $permission) {
        	$role = 'role_' . $permission->role_id;
        	$resourceData = $this->toArray($permission->resource);
        	$resourceItem = $resourceData['module'] . '_' . $resourceData['controller'];
            $this->getAcl()->allow($role, $resourceItem, $resourceData['action']);
        }
    }
    
    protected function toArray($resource)
    {
    	$items = array(
    		'module' => 'default',
    		'controller' => 'index',
    		'action' => 'index'
    	);
    	if ($data = explode('_', $resource)) {
    		$i = 0;
    		foreach ($items as $key => $value) {
    			if (!empty($data[$i])) {
    				$items[$key] = $data[$i];
    			}
    			$i ++;
    		}
    	}
    	
    	return $items;
    }
}
<?php

class Platon_Filters_Render_Date extends Bvb_Grid_Filters_Render_Table_Date
{
	public function render()
	{
		$view = $this->getView();
		// add helper path
        $view->addHelperPath('ZendX/JQuery/View/Helper/', 'ZendX_JQuery_View_Helper');
        
        $this->removeAttribute('id');
        $this->setAttribute('style', 'margin-left:5px;width:85px!important;');

		return "<span style=\"float:left;margin-right:5px;\">" . $this->__('От') . ":</span>" .
		        $view->datePicker(
		                $this->getFieldName() . '[from]',
		                $this->getDefaultValue('from'),
		                array(
		                    'dateFormat' => 'dd.mm.yy',
		                ),
		                array_merge($this->getAttributes(), array('id' => 'filter_' . $this->getFieldName() . '_from'))
		            ) .
		        "<span style=\"float:left;margin-right:5px;\">" . $this->__('До') . ":</span>" .
		        $view->datePicker(
		                $this->getFieldName() . '[to]',
		                $this->getDefaultValue('to'),
		                array(
		                    'dateFormat' => 'dd.mm.yy',
		                ),
		                array_merge($this->getAttributes(), array('id' => 'filter_' . $this->getFieldName() . '_to'))
		        );
	}
}
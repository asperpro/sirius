<?php

class Platon_Form_Decorator_ViewHelper extends Zend_Form_Decorator_ViewHelper 
{ 
     public function render($content) 
     {
         $element = $this->getElement();
         if ($element->isRequired()) {
             $class  = $element->getAttrib('class'); // append to current attrib
             $element->setAttrib('class', $class . ' form-control');
         }
         return parent::render($content);
     }
 }
<?php

class Platon_Form_DisplayGroup extends Zend_Form_DisplayGroup {

    /*public function loadDefaultDecorators() {
        if ($this->loadDefaultDecoratorsIsDisabled()) {
            return;
        }

        $this->setDecorators(
            array(
                'ViewHelper',
                array(array('td' => 'HtmlTag'), array('tag' => 'td', 'style' => 'width: 360px;')),
                array('Label', array('tag' => 'td', 'requiredSuffix' => ' *')),
                array(array('tr' => 'HtmlTag'), array('tag' => 'tr')),
                array(array('tbody' => 'HtmlTag'), array('tag' => 'tbody')),
                array(array('table' => 'HtmlTag'), array('tag' => 'table', 'class' => 'tiny')),
                array(array('separator' => 'HtmlTag'), array('tag' => 'div', 'class' => 'separator'))
            )
        );
    }*/

    public function setLabel($label) {
        return $this->setAttrib('label', (string) $label);
    }

    public function getLabel() {
        return $this->getAttrib('label');
    }

    public function setRequired($flag) {
        $this->_required = (bool) $flag;
        return $this;
    }

    public function isRequired() {
    }

}
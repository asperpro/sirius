<?php

class Platon_Mail
{
    // templates name
    const SEND_ORDER = 'order';
    const SEND_SYSTEM_REQUEST_ORDER = 'request-order';
    const SEND_SYSTEM_CALLBACK = 'callback';
    const SEND_SYSTEM_REVIEW = 'review';
	const SEND_COUPON_NEWYEAR2014 = 'newyear2014';
	const SEND_WAITLIST = 'waitlist';
 
    protected $_viewContent;
    protected $templateVariables = array();
    protected $templateName;
    protected $_mail;
    protected $_subject;
    protected $recipient;
 
    public function __construct()
    {
        $this->_mail = new Zend_Mail('UTF-8');
        $this->_viewContent = new Zend_View();
    }
 
    /**
     * Set variables for use in the templates
     *
     * @param string $name  The name of the variable to be stored
     * @param mixed  $value The value of the variable
     */
    public function __set($name, $value)
    {
        $this->templateVariables[$name] = $value;
    }
 
    /**
     * Set the template file to use
     *
     * @param string $filename Template filename
     */
    public function setTemplate($filename)
    {
        $this->templateName = $filename;
    }
    
    public function setSubject($subject)
    {
        $this->_subject = $subject;
    }
 
    /**
     * Set the recipient address for the email message
     *
     * @param string $email Email address
     */
    public function setRecipient($email)
    {
        $this->recipient = $email;
    }
    
    public function getSubTemplateContent($subTemplateName, $vars)
    {
    	$config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini');
    	$templatePath = $config->production->email->templatePath;
    	
    	$viewContent = realpath($templatePath) . DIRECTORY_SEPARATOR . $this->templateName . '/' . $subTemplateName . '.tpl';
        $stringMail = file_get_contents($viewContent);
        
        foreach ($vars as $key => $value) {
        	$stringMail = str_replace('{{' . $key . '}}', $value, $stringMail);
        }
        
        return $stringMail;
    }

    /**
     * Send email
     *
     * @todo Add from name
     */
    public function send()
    {
        //$config = Zend_Registry::get('config');
 
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini');
 
        $templatePath = $config->production->email->templatePath;
        $templateVars = $config->production->resources->mail->toArray();

        foreach ($templateVars as $key => $value) {
            if (!array_key_exists($key, $this->templateVariables)) {
                $this->{$key} = $value;
            }
        }

        $viewContent = realpath($templatePath) . DIRECTORY_SEPARATOR . $this->templateName . '/index.tpl';
        $html = file_get_contents($viewContent);
        foreach ($this->templateVariables as $key => $value) {
        	if (!is_array($value)) {
        		$html = str_replace('{{' . $key . '}}', $value, $html);	
        	}
        }

        $this->_mail->addTo($this->recipient);
        $this->_mail->setSubject($this->_subject);
        $this->_mail->setBodyHtml($html);
        $this->_mail->send();
    }
}
<?php

class Platon_View_Helper_LayerNavigation
{
	const LIMIT_FILTER_ITEM = 8;
    public $view;
    
    protected $_filters = array();
	protected $_sorts = array();
	protected $_request = null;

    public function layerNavigation($params = null)
    {
    	$this->_request = new Zend_Controller_Request_Http();
    	$this->getQueryParams();
    	
    	if (!empty($this->_sorts['availiable'])) {
    		$params['availiable'] = true;
    	}    	
    	// Get all filterable product options
    	$productModel = new Category_Model_Product();
    	$cache = Zend_Registry::get('cache');
    	
    	// Get options
    	$cacheId = null;
    	$cacheFacilityId = null;
    	$cacheTags = array();
    	if (isset($params['category_id']) && !isset($params['availiable'])) {
    		$cacheId = $this->view->lang . '_options_category_' . $params['category_id'];
    		$cacheFacilityId = $this->view->lang . '_facilities_category_' . $params['category_id'];
    		if (1 == count($this->_filters)) {
    			$hash = md5(serialize($this->_filters));
    			$cacheId .= '_' . $hash;
    			$cacheFacilityId .= '_' . $hash;
    		}
    		$cacheTags = array('category' . $params['category_id']);
    	}
    	if (isset($params['brend_id']) && !isset($params['availiable'])) {
    		$cacheId = $this->view->lang . '_options_brend_' . $params['brend_id'];
    		$cacheFacilityId = $this->view->lang . '_facilities_brend_' . $params['brend_id'];
    		if (1 == count($this->_filters)) {
    			$hash = md5(serialize($this->_filters));
    			$cacheId .= '_' . $hash;
    			$cacheFacilityId .= '_' . $hash;
    		}
    		$cacheTags = array('brend' . $params['brend_id']);
    	}
    	if ($cacheId) {
    		if (!$options = $cache->load($cacheId)) {
	    		$options = $productModel->getFacetFilters($params, $this->_filters, $this->view->lang);
	    		$cache->save($options, $cacheId, $cacheTags);
	    	}
	    	if (!$facilities = $cache->load($cacheFacilityId)) {
	    		$facilities = $productModel->getFacetFacilityFilters($params, $this->_filters, $this->view->lang);
	    		$cache->save($facilities, $cacheFacilityId, $cacheTags);
	    	}
    	} else {
    		$options = $productModel->getFacetFilters($params, $this->_filters, $this->view->lang);
    		$facilities = $productModel->getFacetFacilityFilters($params, $this->_filters, $this->view->lang);
    	}
    	
    	// Get brends
    	$cacheId = null;
    	if (isset($params['category_id']) || isset($params['query'])) {
    		if (isset($params['category_id']) && !$this->_filters && !isset($params['availiable'])) {
    			$cacheId = $this->view->lang . '_brends_category_' . $params['category_id'];
    			if (!$brends = $cache->load($cacheId)) {
		    		$brends = $productModel->getFacetBrends($params, $this->_filters);
		    		$cache->save($brends, $cacheId);
		    	}
    		} else {
    			$brends = $productModel->getFacetBrends($params, $this->_filters);		
    		}
    	}
    	
    	// Get collections
    	$cacheId = null;
    	if (isset($params['brend_id'])) {
    		if (isset($params['brend_id']) && !$this->_filters && !isset($params['availiable'])) {
    			$cacheId = $this->view->lang . '_collections_brend_' . $params['brend_id'];
    			if (!$collections = $cache->load($cacheId)) {
		    		$collections = $productModel->getFacetCollections($params, $this->_filters);
		    		$cache->save($collections, $cacheId);
		    	}
    		} else {
    			$collections = $productModel->getFacetCollections($params, $this->_filters);
    		}
    	}
		
    	// Get options
    	$optionValues = array();
		foreach ($options as $key => $_option) {
			if (isset($optionValues[$_option['option_id']])) {
				$optionValues[$_option['option_id']]['values'][$_option['value_id']]['name'] = $_option['value_name'];
				$optionValues[$_option['option_id']]['values'][$_option['value_id']]['total'] = $_option['total'];
			} else {
				$optionValues[$_option['option_id']] = array();
				$optionValues[$_option['option_id']]['name'] = $_option['option_name'];
				$optionValues[$_option['option_id']]['values'][$_option['value_id']]['name'] = $_option['value_name'];
				$optionValues[$_option['option_id']]['values'][$_option['value_id']]['total'] = $_option['total'];
			}
		}
    	$filtersXhtml = '';
    	// Options
    	foreach ($optionValues as $oid => $_option) {
    		if (isset($params['exclude_option_id']) &&
    			$oid == $params['exclude_option_id']) {
    			continue;
    		}
    		$optionTitle = Platon_Text::toUcFirst($_option['name']);
    		$filtersXhtml .= '<div class="filter-item">';
    		$filtersXhtml .= '<a name="option_' . $oid . '"></a>';
    		$filtersXhtml .= '<h4 class="opener">' . $optionTitle . '</h4>';
    		if ($_option['values']) {
    			$filtersXhtml .= '<div class="slide">';
    			$i = 0;
    			foreach ($_option['values'] as $vid => $_value) {
    				if (self::LIMIT_FILTER_ITEM == $i) {
						$filtersXhtml .= '</div><div class="slide" style="display:none;">';
					}
    				$isChecked = '';
    				if (isset($this->_filters['options']) && isset($this->_filters['options'][$oid])) {
    					if (in_array($vid, $this->_filters['options'][$oid])) {
    						$isChecked = 'checked="checked"';
    					}
    				}
					$valueTitle = Platon_Text::toUcFirst($_value['name']);
    				$filtersXhtml .= '<div class="row">';
					$filtersXhtml .= '<input type="checkbox" ' . $isChecked . ' value="Y" id="f_' . $oid . '_' . $vid . '" name="f_' . $oid . '_' . $vid . '" onclick="ga(\'send\', \'event\', \'Category\', \'Filter\', \'' . $optionTitle . ': ' . $valueTitle . '\');" class="check">';
					$filtersXhtml .= '<label onclick="ga(\'send\', \'event\', \'Category\', \'View\', \'' . $optionTitle . ': ' . $valueTitle . '\');">';
					if (!$isChecked) {
						$filtersXhtml .= '<a href="' . $this->_buildFilterUrl('f_' . $oid . '_' . $vid) . '">';	
					}
					$filtersXhtml .= $valueTitle;
					if (!$isChecked) {
						$filtersXhtml .= '</a>';	
					}
					$filtersXhtml .= '<span>(' . $_value['total'] . ')</span></label>';
	        		$filtersXhtml .= '</div>';
	        		$i ++;
    			}
    			$filtersXhtml .= '</div>';
    			if (count($_option['values']) > self::LIMIT_FILTER_ITEM) {
					$filtersXhtml .= '<a class="link-all" href="#option_' . $oid . '"><span>Все варианты</span></a>';	
				}
    		}
    		$filtersXhtml .= '</div>';
    	}
    	
    	$requestUri = $this->_request->getRequestUri();
		$activeFilterXhtml = $this->_getActiveBoxHtml();
		$priceFilterXhtml = $this->_getPriceBoxHtml();
		$brendFilterXhtml = (isset($brends)) ? $this->_getBrendBoxHtml($brends) : '';
		$facilityFilterXhtml = $this->_getFacilityBoxHtml($facilities, $params);
		$collectionFilterXhtml = (isset($collections)) ? $this->_getCollectionBoxHtml($collections) : '';
		$formInputs = $this->_getFormInputsHtml($params);
    	
		$this->_appendScript($params);
		
    	$xhtml = '<div class="layered-navigation">'
    			. '<div class="filter-active">'
    			. '<h3>Подбор по параметрам</h3>'
				. $activeFilterXhtml
				. '<form id="filter-form" name="filter" method="get" action="' . $requestUri . '">'
				. $formInputs
				. $priceFilterXhtml
				. $brendFilterXhtml
				. $collectionFilterXhtml
				. $facilityFilterXhtml
				. $filtersXhtml
				. '</form>'
				. '</div>'
				. '</div>';    	
    	
    	return $xhtml;
    }
    
    protected function _getFacilityBoxHtml($facilities, $params)
    {
    	$facilityXhtml = '';
    	if ($facilities) {
	    	$facilityXhtml .= '<div class="filter-item">';
			$facilityXhtml .= '<a name="facilities"></a>';
			$facilityXhtml .= '<h4 class="opener">' . $this->view->translate('Функции') . '</h4>';
			$facilityXhtml .= '<div class="slide">';
			foreach ($facilities as $key => $_facility) {
				if (isset($params['exclude_facility_id']) &&
	    			$_facility['id'] == $params['exclude_facility_id']) {
	    			continue;
	    		}
				if (self::LIMIT_FILTER_ITEM == $key) {
					$facilityXhtml .= '</div><div class="slide" style="display:none;">';
				}
				$isChecked = '';
				if (!empty($this->_filters['facilities']) && in_array($_facility['id'], $this->_filters['facilities'])) {
					$isChecked = 'checked="checked"';
				}
				$facilityXhtml .= '<div class="row">';
				$facilityXhtml .= '<input type="checkbox" ' . $isChecked . ' value="Y" id="f_f_' . $_facility['id'] . '" name="f_f_' . $_facility['id'] . '" onclick="ga(\'send\', \'event\', \'Category\', \'Filter\', \'Facility: ' . $_facility['name'] . '\');" class="check">';
				$facilityXhtml .= '<label onclick="ga(\'send\', \'event\', \'Category\', \'View\', \'Facility: ' . $_facility['name'] . '\');">';
				if (!$isChecked) {
					$facilityXhtml .= '<a rel="nofollow" href="' . $this->_buildFilterUrl('f_f_' . $_facility['id']) . '">';	
				}
				$facilityXhtml .= $_facility['name'];
				if (!$isChecked) {
					$facilityXhtml .= '</a>';	
				}
				$facilityXhtml .= '<span>(';
				$facilityXhtml .= $_facility['total'];
				$facilityXhtml .= ')</span></label>';
	    		$facilityXhtml .= '</div>';
			}
			$facilityXhtml .= '</div>';
			if (count($facilities) > self::LIMIT_FILTER_ITEM) {
				$facilityXhtml .= '<a class="link-all" href="#facilities"><span>Все функции</span></a>';	
			}
			$facilityXhtml .= '</noindex>';
			$facilityXhtml .= '</div>';	
    	}
		
		return $facilityXhtml;
    }
    
    protected function _getBrendBoxHtml($brends)
    {
    	$brendXhtml = '';
    	if ($brends) {
    		$brendXhtml .= '<div class="filter-item">';
			$brendXhtml .= '<a name="brends"></a>';
			$brendXhtml .= '<noindex>';
			$brendXhtml .= '<h4 class="opener">' . $this->view->translate('Бренды') . '</h4>';
			$brendXhtml .= '<div class="slide">';
			foreach ($brends as $key => $_brend) {
				if (self::LIMIT_FILTER_ITEM == $key) {
					$brendXhtml .= '</div><div class="slide" style="display:none;">';
				}
				$isChecked = '';
				if (!empty($this->_filters['brends']) && in_array($_brend['id'], $this->_filters['brends'])) {
					$isChecked = 'checked="checked"';
				}
				$brendXhtml .= '<div class="row">';
				$brendXhtml .= '<input type="checkbox" ' . $isChecked . ' value="Y" id="f_b_' . $_brend['id'] . '" name="f_b_' . $_brend['id'] . '" onclick="ga(\'send\', \'event\', \'Category\', \'Filter\', \'Brend: ' . $_brend['name'] . '\');" class="check">';
				$brendXhtml .= '<label onclick="_ga(\'send\', \'event\', \'Category\', \'View\', \'Brend: ' . $_brend['name'] . '\');">';
				if (!$isChecked) {
					$brendXhtml .= '<a rel="nofollow" href="' . $this->_buildFilterUrl('f_b_' . $_brend['id']) . '">';	
				}
				$brendXhtml .= $_brend['name'];
				if (!$isChecked) {
					$brendXhtml .= '</a>';	
				}
				$brendXhtml .= '<span>(';
				if (!empty($this->_filters['brends']) && !$isChecked) {
					$brendXhtml .= '+';
				}
				$brendXhtml .= $_brend['total'];
				$brendXhtml .= ')</span></label>';
	    		$brendXhtml .= '</div>';
			}
			$brendXhtml .= '</div>';
			if (count($brends) > self::LIMIT_FILTER_ITEM) {
				$brendXhtml .= '<a class="link-all" href="#brends"><span>Все варианты</span></a>';	
			}
			$brendXhtml .= '</noindex>';
			$brendXhtml .= '</div>';	
    	}
		
		return $brendXhtml;
    }
    
    protected function _getCollectionBoxHtml($collections)
    {
		$collectionXhtml = '<div class="filter-item">';
		$collectionXhtml .= '<a name="collections"></a>';
		$collectionXhtml .= '<noindex>';
		$collectionXhtml .= '<h4 class="opener">' . $this->view->translate('Коллекции') . '</h4>';
		$collectionXhtml .= '<div class="slide">';
		foreach ($collections as $key => $_collection) {
			if (self::LIMIT_FILTER_ITEM == $key) {
				$collectionXhtml .= '</div><div class="slide" style="display:none;">';
			}
			$isChecked = '';
			if (!empty($this->_filters['collections']) && in_array($_collection['id'], $this->_filters['collections'])) {
				$isChecked = 'checked="checked"';
			}
			$collectionXhtml .= '<div class="row">';
			$collectionXhtml .= '<input type="checkbox" ' . $isChecked . ' value="Y" id="f_c_' . $_collection['id'] . '" name="f_c_' . $_collection['id'] . '" onclick="ga(\'send\', \'event\', \'Category\', \'Filter\', \'Brend: ' . $_collection['name'] . '\');" class="check">';
			$collectionXhtml .= '<label onclick="ga(\'send\', \'event\', \'Category\', \'View\', \'Collection: ' . $_collection['name'] . '\');">';
			if (!$isChecked) {
				$collectionXhtml .= '<a rel="nofollow" href="' . $this->_buildFilterUrl('f_c_' . $_collection['id']) . '">';	
			}
			$collectionXhtml .= $_collection['name'];
			if (!$isChecked) {
				$collectionXhtml .= '</a>';	
			}
			$collectionXhtml .= '<span>(';
			if (!empty($this->_filters['collections']) && !$isChecked) {
				$collectionXhtml .= '+';
			}
			$collectionXhtml .= $_collection['total'];
			$collectionXhtml .= ')</span></label>';
    		$collectionXhtml .= '</div>';
		}
		$collectionXhtml .= '</div>';
		if (count($collections) > self::LIMIT_FILTER_ITEM) {
			$collectionXhtml .= '<a class="link-all" href="#collections"><span>Все варианты</span></a>';	
		}
		$collectionXhtml .= '</noindex>';
		$collectionXhtml .= '</div>';
		
		return $collectionXhtml;
    }
    
    protected function _getFormInputsHtml($params = array())
    {
    	$fileds = array(
    		'price',
    		'date',
    		'name',
    		'views',
    		'availiable',
    	);
    	$xhtml = '<input type="hidden" name="view" value="">'
			   . '<input type="hidden" name="query" value="' . (isset($params['query']) ? $params['query'] : '') . '">'
			   . '<input type="hidden" name="bid" value="">';
    	foreach ($fileds as $_field) {
    		$value = (isset($this->_sorts[$_field])) ? $this->_sorts[$_field]: '';
    		$xhtml .= '<input type="hidden" name="s_' . $_field . '" value="' . $value . '">';
    	}
    	
    	return $xhtml;
    }
    
    protected function _getPriceBoxHtml()
    {
    	$startAmount = (isset($this->_filters['start_amount'])) ? $this->_filters['start_amount'] : '';
    	$finishAmount = (isset($this->_filters['finish_amount'])) ? $this->_filters['finish_amount'] : '';
    	
    	$xhtml = '<div class="filter-item">'
    			. '<h4 class="opener">' . $this->view->translate('Цена') . '</h4>'
    			. '<div class="slide filter-price">'
    			. '<span>от</span><input value="' . $startAmount . '" type="text" name="f_start_amount" class="text price" onclick="ga(\'send\', \'event\', \'Category\', \'Filter\', \'цена: от\');">'
    			. '<span>до</span><input value="' . $finishAmount . '" type="text" name="f_finish_amount" class="text price" onclick="ga(\'send\', \'event\', \'Category\', \'Filter\', \'цена: до\');">'
    			. '<span>грн.</span>&nbsp;<input type="submit" class="submit" value="ОК">'
    			. '</div>'
    			. '</div>';
    	
    	return $xhtml;
    }
    
    protected function _appendScript($params = array())
    {
    	$redirectUrl = $_SERVER['REDIRECT_URL'];
    	$redirectUrl .= (isset($params['query'])) ? '?query=' . $params['query'] : '';
    	$js = 'jQuery("#link-clear").click(function(){'
				. 'window.location.href = "' . $redirectUrl . '";'
				. '});';
		$this->view->jQuery()->addOnload($js);
    }
    
    protected function _getActiveBoxHtml()
    {
    	$xhtml = '<div class="filter-active-box">';
    	
    	if ($this->_filters) {
    		
    		$optionModel = new Category_Model_Product_Option();
    		$optionValueModel = new Category_Model_Product_Option_Value();
    		$brendModel = new Category_Model_Brend();
    		$collectionModel = new Category_Model_Collection();
    		$facilityModel = new Category_Model_Product_Facility();
    		
    		// Price html
    		$priceXhtml = '';
    		if (!empty($this->_filters['start_amount'])) {
	    		$priceXhtml .= 'от ' . $this->_filters['start_amount'];
	    	}
	    	if (!empty($this->_filters['finish_amount'])) {
	    		$priceXhtml .= ($priceXhtml) ? ' ' : '';
	    		$priceXhtml .= 'до ' . $this->_filters['finish_amount'];
	    	}
    		
    		$xhtml .= '<ul>';
    		if ($priceXhtml) {
    			$xhtml .= '<li>' . $this->view->translate('Цена') . '</li>';
    			$xhtml .= '<li><a href="#"><img src="/public/img/_.gif" width="9" height="9" alt="_">' . $priceXhtml . ' грн.</a></li>';
    		}
    		if (isset($this->_filters['brends'])) {
    			$xhtml .= '<li>' . Platon_Text::toUcFirst($this->view->translate('Бренды')) . '</li>';
	    		foreach ($this->_filters['brends'] as $_bid) {
	    			$brend = $brendModel->find($_bid);
	    			$xhtml .= '<li><a href="' . $this->_buildFilterUrl('f_b_' . $_bid) . '"><img src="/public/img/_.gif" width="9" height="9" alt="_">' . Platon_Text::toUcFirst($brend['name']) . '</a></li>';
	    		}	
    		}
    		if (isset($this->_filters['collections'])) {
    			$xhtml .= '<li>' . Platon_Text::toUcFirst($this->view->translate('Коллекции')) . '</li>';
	    		foreach ($this->_filters['collections'] as $_cid) {
	    			$collection = $collectionModel->find($_cid);
	    			$xhtml .= '<li><a href="' . $this->_buildFilterUrl('f_c_' . $_cid) . '"><img src="/public/img/_.gif" width="9" height="9" alt="_">' . Platon_Text::toUcFirst($collection['name']) . '</a></li>';
	    		}	
    		}
    		if (isset($this->_filters['facilities'])) {
    			$xhtml .= '<li>' . Platon_Text::toUcFirst($this->view->translate('Функции')) . '</li>';
	    		foreach ($this->_filters['facilities'] as $_fid) {
	    			$facility = $facilityModel->find($_fid);
	    			$facilityTitle = ('ua' == $this->view->lang) ? $facility['name_ua'] : $facility['name'];
	    			$xhtml .= '<li><a href="' . $this->_buildFilterUrl('f_f_' . $_fid) . '"><img src="/public/img/_.gif" width="9" height="9" alt="_">' . Platon_Text::toUcFirst($facilityTitle) . '</a></li>';
	    		}	
    		}
    		if (isset($this->_filters['options'])) {
	    		foreach ($this->_filters['options'] as $oid => $_filter) {
	    			$option = $optionModel->find($oid);
	    			$xhtml .= '<li>' . Platon_Text::toUcFirst($option['name']) . '</li>';
	    			foreach ($_filter as $_value) {
	    				$optionValue = $optionValueModel->find($_value);
	    				$optionTitle = ('ua' == $this->view->lang) ? $optionValue['value_ua'] : $optionValue['value'];
	    				$xhtml .= '<li><a href="' . $this->_buildFilterUrl('f_' . $oid . '_' . $_value) . '"><img src="/public/img/_.gif" width="9" height="9" alt="_">' . Platon_Text::toUcFirst($optionTitle) . '</a></li>';
	    			}
	    		}	
    		}
    		$xhtml .= '<li class="reset_filter"><a href="javascript:;" id="link-clear" class="switch"><img src="/public/img/_.gif" width="9" height="9" alt="_">' . $this->view->translate('Сбросить фильтр') . '</a></li>';
    		$xhtml .= '</ul>';
    	}
    	$xhtml .= '</div>';
    	
    	return $xhtml;
    }

	protected function _buildFilterUrl($value)
	{
		$value = $value . '=Y';
		$search = array('&'.$value, $value);
		$replace = '';
		
		$requestUri = $this->_request->getRequestUri();
		$url = str_replace($search, $replace, $requestUri);
		
		if ($requestUri == $url) {
			return $this->formFilterUrl($value);
		}
		
		return $url;
	}
	
	public function formFilterUrl($value)
	{
		$requestUri = $this->_request->getRequestUri();
		
		$pos = strpos($requestUri, '?');
		$requestUri .= ($pos === false) ? '?' : '&';
		$requestUri .= $value;
		
		return $requestUri;
	}
	
	public function getQueryParams()
	{
		$filters = array();
		$sorts = array();
		
		$httpRequest = new Zend_Controller_Request_Http();
		
		$data = $httpRequest->getParams();
		foreach ($data as $key => $value) {
			if (preg_match('/^f_[0-9]+_[0-9]+$/', $key)) {
				$params = explode('_', $key);
				$filters['options'][$params[1]][] = $params[2];
				continue;
			}
			if (preg_match('/^f_f_[0-9]+$/', $key)) {
				$params = explode('_', $key);
				$filters['facilities'][] = $params[2];
				continue;
			}
			if (preg_match('/^f_b_[0-9]+$/', $key)) {
				$params = explode('_', $key);
				$filters['brends'][] = $params[2];
				continue;
			}
			if (preg_match('/^f_c_[0-9]+$/', $key)) {
				$params = explode('_', $key);
				$filters['collections'][] = $params[2];
				continue;
			}
			if (preg_match('/^s_[a-z]+$/', $key)) {
				$params = explode('_', $key);
				$sorts[$params[1]] = $value;
				continue;
			}
			if (preg_match('/^f_[a-z]+_[a-z]+$/', $key)) {
				$params = explode('_', $key);
				if ('Y' == $value) {
					$filters[$params[1]][] = $params[2];
				} else {
					$filters[$params[1] . '_' . $params[2]] = $value;
				}
			}
		}

		$this->_filters = $filters;
		$this->_sorts = $sorts;
	}

    public function setView(Zend_View_Interface $view)
    {
        $this->view = $view;
    }
}
<?php

class Zend_View_Helper_TopicList {
	
	public $view;
	
	public function TopicList($articleTopics = array())
	{
		$xhtml = '';
		$articleTopicIds = array();
		if ($articleTopics) {
			foreach ($articleTopics as $_topic) {
				$articleTopicIds[] = $_topic['id'];
			}
		}
		
		$articleModel = new Default_Model_Article();
		$data = $articleModel->getTopicList($this->view->lang);
		
		$articleTypes = array(
			'articles' => 'Обзоры и статьи',
			'advice' => 'Советы',
			'news' => 'Новости',
		);
		
		$xhtml .= '<div id="topic-list-block">';
		if ($data) {
			foreach ($data as $type => $topics) {
				$xhtml .= '<ul class="topic-list-container">';
				if ($topics) {
					$xhtml .= '<li>';
					$xhtml .= '<a class="article-type-title" href="/blog/' . $type . '/">' . $this->view->translate($articleTypes[$type]) . '</a>';
					$xhtml .= '<ul class="topic-list-item">';
		 			foreach ($topics as $topic) {
		 				if (!in_array($topic['id'], $articleTopicIds)) {
		 					$xhtml .= '<li><noindex><a rel="nofollow" href="/blog/' . $type . '/' . $topic['alias'] . '/">' . $topic['title'] .'</a></noindex>';
		 					$xhtml .= '<span class="counter">(' . $topic['total'] . ')</span></li>';	
		 				} else {
		 					$xhtml .= '<li><a href="/blog/' . $type . '/' . $topic['alias'] . '/">' . $topic['title'] .'</a>';
		 					$xhtml .= '<span class="counter">(' . $topic['total'] . ')</span></li>';
		 				}
		 			}
		 			$xhtml .= '</ul>';
					$xhtml .= '</li>';	
				}
				$xhtml .= '</ul>';
			}
		}
		$xhtml .= '</div>';
		
		return $xhtml;
	}
	
	public function setView(Zend_View_Interface $view)
	{	
		$this->view = $view;
	}
}
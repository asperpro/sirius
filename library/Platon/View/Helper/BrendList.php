<?php

class Platon_View_Helper_BrendList
{
    public $view;

    public function brendList($categoryId, $isNofollow = false, $limitColItem = null)
    {
    	$xhtml = '<ul>';
    	
    	$cache = Zend_Registry::get('cache');
    	
    	$brendModel = new Category_Model_Brend();
    	$cacheId = 'brendlist_' . $categoryId;
    	if ($isNofollow) {
    		$cacheId .= '_nofollow';
    	}
    	if (!$data = $cache->load($cacheId)) {
    		$data = $brendModel->getAllByCatagory($categoryId);
    		$cache->save($data, $cacheId);
    	}
    	
    	if ($data) {
    		foreach ($data as $key => $value) {
    			$xhtml .= '<li>';
    			if ($isNofollow) {
    				$xhtml .= '<noindex>';
    			}
    			$xhtml .= '<a' . (($isNofollow) ? ' rel="nofollow"' : '') . ' href="/' . $value['slug'] . '/">' . $value['name'] . '</a>';
    			if ($isNofollow) {
    				$xhtml .= '</noindex>';
    			}
    			$xhtml .= '</li>';
    			if ($limitColItem && $key == $limitColItem) {
    				$xhtml .= '</ul><ul>';
    			}
    		}
    	}
    	
    	$xhtml .= '<ul>';
    	
    	return $xhtml;
    }
    
    public function setView(Zend_View_Interface $view)
    {
        $this->view = $view;
    }
}
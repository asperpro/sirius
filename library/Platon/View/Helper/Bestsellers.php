<?php

class Platon_View_Helper_Bestsellers
{
    public $view;

    public function bestsellers($params)
    {
    	$xhtml = '';
    	$cacheId = null;
    	$cacheTags = array();
    	$cache = Zend_Registry::get('cache');
    	
    	if (isset($params['category_id'])) {
    		$cacheId = 'bestsellers_category_' . $params['category_id'];
    		$cacheTags = array('category' . $params['category_id']);
    	}
    	if (isset($params['brend_id'])) {
    		$cacheId = 'bestsellers_brend_' . $params['brend_id'];
    		$cacheTags = array('brend' . $params['brend_id']);
    	}
    	if (isset($params['collection_id'])) {
    		$cacheId = 'bestsellers_collection_' . $params['collection_id'];
    		$cacheTags = array('collection' . $params['collection_id']);
    	}
    	if (!$products = $cache->load($cacheId)) {
    		$categoryModel = new Category_Model_Category();
    		$products = $categoryModel->getCategoryBestsellers($params);
    		$cache->save($products, $cacheId, $cacheTags);
    	}
    	
    	if ($products) {
    		$xhtml .= '<div id="bestseller-block" class="sidebar-item">';
    		$xhtml .= '<h2>' . $this->view->translate('Лидеры продаж') . '</h2>';
	    	$xhtml .= '<table class="bestsellers">';
	    	foreach ($products as $key => $value) {
	    		
	    		$rateWidth = ($value['review_rate']) ? $value['review_rate'] * 19 : 0;
	    		$xhtml .= '<tr>'
	    				. '<td class="image">'
	    				. '<a href="/' . $value['slug'] . '.html"><img width="60px" src="http://watch4you.com.ua/public/files/product/thumbnail/' . $value['image'] . '" /></a>'
	    				. '</td>'
	    				. '<td style="vertical-align:top;">'
	    				. '<p><b>' . ($key + 1) . '.</b> <a href="/' . $value['slug'] . '.html">' . $value['name'] . '</a></p>'
	    				. '<div class="price">'
	    				. '<div class="uah">' . number_format(round($value['price_uah']), 0, ',', ' ') . ' <span>грн.</span></div>'
	    				. '</div>'
	    				. '<div class="rating">'
						. '<div class="value"><div style="width:' . $rateWidth . 'px;"></div></div>'
						. '</div>'
						. '</div>'
	    				. '</td>'
	    				. '</tr>';
	    	}
	    	$xhtml .= '</table>';	
	    	$xhtml .= '</div>';	
    	}
    	
    	return $xhtml;
    }
    
    public function setView(Zend_View_Interface $view)
    {
        $this->view = $view;
    }
}
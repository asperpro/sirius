<?php
class Platon_View_Helper_FormMultiCheckbox extends Zend_View_Helper_FormMultiCheckbox
{
    public function formMultiCheckbox($name, $value = null, $attribs = null,
                                      $options = null, $listsep = "<br />\n")
    {
        // zend_form_element attrib has higher precedence
        if (isset($attribs['listsep'])) {
            $listsep = $attribs['listsep'];
        }

        // Store original separator for later if changed
        $origSep = $listsep;

        // Don't allow whitespace as a seperator
        $listsep = trim($listsep);

        // Force a separator if empty
        if (empty($listsep)) {
            $listsep = $attribs['listsep'] = "<br />\n";
        }

        $string  = $this->formRadio($name, $value, $attribs, $options, $listsep);
        $checkboxes = explode($listsep, $string);

        $html = '';
        // Your code
        $col = round(count($checkboxes)/20);
        if ($col > 0) {
            $columns = array_chunk($checkboxes, count($checkboxes)/3); //columns
        } else {
            $columns = array($checkboxes);
        }

        foreach ($columns as $columnOfCheckboxes) {
            $html .= '<div style="float:left; padding-right: 5px">';

            $html .= implode($origSep, $columnOfCheckboxes);

            $html .= '</div>';
        }
            $html .= '<div class="clearfix">';

        return $html;
    }
}
<?php

class Platon_Text
{
	static public function toUcFirst($string, $encoding = 'UTF-8')
	{
	    $strlen = mb_strlen($string, $encoding);
	    $firstChar = mb_substr($string, 0, 1, $encoding);
	    $then = mb_substr($string, 1, $strlen - 1, $encoding);
	    
	    return mb_strtoupper($firstChar, $encoding) . $then;
	}
	
	static public function toLowerCase($string, $encoding = 'UTF-8')
	{
	    if (function_exists('mb_strtoupper') && function_exists('mb_substr') && !empty($string)) { 
	        $string = mb_strtolower($string, $encoding); 
	    } else {
	        $string = strtolower($string); 
	    }
	    
	    return $string; 
	}
	
	static public function truncate($input, $numWords, $padding = '')
	{
		$input = strip_tags($input);
		$output = strtok($input, " \n");
    	while(--$numWords > 0) $output .= " " . strtok(" \n"); 
    	if ($output != $input) {
    		$output .= $padding; 
    	}
    	
    	return $output;
	}
	
	static public function transliterate($text)
	{
		preg_match_all('/./u', $text, $text);
		$text = $text[0];
		$simplePairs = array('а' => 'a', 'л' => 'l' , 'у' => 'u' , 'б' => 'b' , 'м' => 'm' , 'т' => 't' , 'в' => 'v' , 'н' => 'n' , 'ы' => 'y' , 'г' => 'g' , 'о' => 'o' , 'ф' => 'f' , 'д' => 'd' , 'п' => 'p' , 'и' => 'i' , 'р' => 'r' , 'А' => 'A' , 'Л' => 'L' , 'У' => 'U' , 'Б' => 'B' , 'М' => 'M' , 'Т' => 'T' , 'В' => 'V' , 'Н' => 'N' , 'Ы' => 'Y' , 'Г' => 'G' , 'О' => 'O' , 'Ф' => 'F' , 'Д' => 'D' , 'П' => 'P' , 'И' => 'I' , 'Р' => 'R' , );
		$complexPairs = array( 'з' => 'z' , 'ц' => 'c' , 'к' => 'k' , 'ж' => 'zh' , 'ч' => 'ch' , 'х' => 'kh' , 'е' => 'e' , 'с' => 's' , 'ё' => 'jo' , 'э' => 'eh' , 'ш' => 'sh' , 'й' => 'jj' , 'щ' => 'shh' , 'ю' => 'ju' , 'я' => 'ja' , 'З' => 'Z' , 'Ц' => 'C' , 'К' => 'K' , 'Ж' => 'ZH' , 'Ч' => 'CH' , 'Х' => 'KH' , 'Е' => 'E' , 'С' => 'S' , 'Ё' => 'JO' , 'Э' => 'EH' , 'Ш' => 'SH' , 'Й' => 'JJ' , 'Щ' => 'SHH' , 'Ю' => 'JU' , 'Я' => 'JA' , 'Ь' => "" , 'Ъ' => "" , 'ъ' => "" , 'ь' => "" , );
		$specialSymbols = array( "_" => "-", "'" => "", "`" => "", "^" => "", " " => "-", '.' => '', ',' => '', ':' => '', '"' => '', "'" => '', '<' => '', '>' => '', '«' => '', '»' => '', ' ' => '-', );
		$translitLatSymbols = array( 'a','l','u','b','m','t','v','n','y','g','o', 'f','d','p','i','r','z','c','k','e','s', 'A','L','U','B','M','T','V','N','Y','G','O', 'F','D','P','I','R','Z','C','K','E','S', );
		$simplePairsFlip = array_flip($simplePairs);
		$complexPairsFlip = array_flip($complexPairs);
		$specialSymbolsFlip = array_flip($specialSymbols);
		$charsToTranslit = array_merge(array_keys($simplePairs),array_keys($complexPairs));
		$translitTable = array();
		foreach($simplePairs as $key => $val) $translitTable[$key] = $simplePairs[$key];
		foreach($complexPairs as $key => $val) $translitTable[$key] = $complexPairs[$key];
		foreach($specialSymbols as $key => $val) $translitTable[$key] = $specialSymbols[$key];
		$result = "";
		$nonTranslitArea = false;
		foreach($text as $char) {
			if(in_array($char,array_keys($specialSymbols))) {
				$result.= $translitTable[$char];
			} elseif(in_array($char,$charsToTranslit)) {
				if($nonTranslitArea) {
					$result.= ""; $nonTranslitArea = false;
				} $result.= $translitTable[$char]; 
			} else {
				if(!$nonTranslitArea && in_array($char,$translitLatSymbols)) {
					$result.= ""; $nonTranslitArea = true;
				}
				$result.= $char;
			}
		}
		
		return strtolower(preg_replace("/[-]{2,}/", '-', $result));
	}
}
<?php

class Platon_Controller_Plugin_Language extends Zend_Controller_Plugin_Abstract
{
	const DEFAULT_LANG = 'ru';
	
	static public $_locales = array(
		'ru' => 'ru_RU',
	    'uk' => 'uk_UA',
	);
	
	public function __construct()
	{
    	$lang = null;
    	if (isset($_SERVER['HTTP_HOST'])) {
    		if ($serverHttpHost = $_SERVER['HTTP_HOST']) {
	        	$urlParts = explode('.', $serverHttpHost);
	        	$lang = $urlParts[0];
	        }
    	}
        $curLang = ($lang == 'ua') ? 'uk' : $lang;
        if(!array_key_exists($curLang, self::$_locales)) {
            $curLang = self::DEFAULT_LANG;
        }
        $langFilePath = APPLICATION_PATH . '/languages/'. $curLang . '/LC_MESSAGES/' . $curLang . '.mo';
        if ($curLang != self::DEFAULT_LANG) {
	        if(!file_exists($langFilePath)) {
	            $langFilePath = APPLICATION_PATH . '/languages/' . self::DEFAULT_LANG . '/LC_MESSAGES/' . self::DEFAULT_LANG . '.mo';
	            $curLang = self::DEFAULT_LANG;
	        }
        }
		// set up translation adapter
		$translate = new Zend_Translate('gettext', $langFilePath, $curLang);
		// set default locale
		$translate->setLocale(self::$_locales[$curLang]);
		Zend_Registry::set('Zend_Translate', $translate);
       	Zend_Registry::set('Zend_Translate_Lang', ($curLang == 'uk') ? 'ua' : $curLang);
	}
}
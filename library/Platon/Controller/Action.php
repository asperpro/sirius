<?php

class Platon_Controller_Action extends Zend_Controller_Action
{
	protected $_cache = null;
	
    protected function _initDefaultGrid($id = 'grid')
	{
		$view = new Zend_View();
        $view->setEncoding('ISO-8859-1');
        $config = new Zend_Config_Ini('./application/configs/grid.ini', 'production');
        $grid = Bvb_Grid::factory('Table', $config, $id);

        $grid->setEscapeOutput(false);
        $grid->saveParamsInSession(true);
        $grid->setUseKeyEventsOnFilters(true);
        $grid->setExport(array());
        $grid->setView($view);
        $grid->setRecordsPerPage(40);
    	$grid->setPaginationInterval(array(40 => 40, 60 => 60, 80 => 80));
    	$grid->addFiltersRenderDir('Platon/Filters/Render/', 'Platon_Filters_Render');

        return $grid;
	}
	
	/*public function preDispatch()
	{
        $module = $this->_request->getModuleName();
        $controller = $this->_request->getControllerName();
        
        $this->view->path = $module . '/' . $controller;
        $this->_cache = Zend_Registry::get('cache');
	}*/
	
	public function postDispatch()
    {
    	$title = (isset($this->view->title)) ? $this->view->title : ''; 
    	$this->view->headTitle($title);
    }
}
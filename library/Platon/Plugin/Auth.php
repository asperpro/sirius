<?php

class Platon_Plugin_Auth extends Zend_Controller_Plugin_Abstract
{
	public function preDispatch(Zend_Controller_Request_Abstract $request)
	{
		$this->_request = $request;
        $auth = Zend_Auth::getInstance();
        
        $module = $this->_request->getModuleName();
        $controller = $this->_request->getControllerName();
        $action = $this->_request->getActionName();
        
        if ('auth' != $module) {
            if (!$auth->hasIdentity()) {
            	$this->_request->setModuleName('auth');
	        	$this->_request->setControllerName('index');
	        	$this->_request->setActionName('index');
            }
        }
	}
}
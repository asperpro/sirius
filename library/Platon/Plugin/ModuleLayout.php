<?php

class Platon_Plugin_ModuleLayout extends Zend_Controller_Plugin_Abstract 
{ 
    public function preDispatch(Zend_Controller_Request_Abstract $request) 
    { 
        $module = $request->getModuleName(); 
        if ($module == 'admin') { 
            Zend_Layout::getMvcInstance()->setLayout('admin');
        }
    } 
}
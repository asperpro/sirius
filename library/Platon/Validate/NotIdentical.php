<?php

class Platon_Validate_NotIdentical extends Zend_Validate_Abstract
{
    const NO_CONTEXT     = "noContext"; 
    const NO_CONTEXT_KEY = "noContextKey"; 
    const IDENTICAL  = "identical"; 

    protected $_token = ''; 

    protected $_messageTemplates = array( 
        self::NO_CONTEXT     => 'No context was provided to the validator', 
        self::NO_CONTEXT_KEY => 'No matching field was found in the context against which to validate', 
        self::IDENTICAL  => 'The values are matched', 
    );
    
    public function __construct($options = null) 
    {
        if (is_string($options)) { 
            $this->_token = $options; 
        } elseif (is_array($options) && isset($options['token'])) { 
            $this->_token = $options['token']; 
        } 
    }

    public function isValid($value, $context = null)
    {
        $this->_setValue($value);
        
        if (!is_array($context)) { 
            $this->_error(self::NO_CONTEXT); 
            return false; 
        }
        if (!isset($context[$this->_token])) { 
            $this->_error(self::NO_CONTEXT_KEY); 
            return false; 
        } 
        if ($value == $context[$this->_token]) {
            $this->_error(self::IDENTICAL); 
            return false;
        }
        
        return true;
    }
}
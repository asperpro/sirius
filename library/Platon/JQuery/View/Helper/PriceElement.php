<?php

class Platon_JQuery_View_Helper_PriceElement extends Platon_JQuery_View_Helper_GridElement
{
	protected $_gridMode = 'price';
	
    public function priceElement($name, $value = null, $attribs = null)
    {
    	return parent::gridElement($name, $value, $attribs);
    }
    
    public function _renderFoot($value)
    {
    	$output = array();

    	$total = 0;
    	$nomenclature = (isset($value['nomenclature'])) ? $value['nomenclature'] : $value;
    	if ($nomenclature) {
    		foreach ($nomenclature as $val) {
	    		$total += $val['total'];
	    	}
    	}
    	
    	$currency = 'грн.';
    	if ($currencyId = $this->view->form->getValue('currency_id')) {
    		$currency = $this->_currencies[$currencyId];
    	}
    	
		$output[] = '<tfoot style="display:block;">';
		$output[] = '<tr class="total" style="display:block;">';
		$output[] = '<td style="display:block;"><div style="float:left;">Всего:</div><div style="float:right;" class="base_total"><span class="v">' . number_format($total, 2, '.', '') . '</span> <span class="grid_currency">' . $currency .  '</span></div></td>';
		$output[] = '</tr>';
		$output[] = '</tfoot>';
    	
    	return implode('', $output);
    }
}
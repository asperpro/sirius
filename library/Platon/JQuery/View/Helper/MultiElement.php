<?php

require_once 'ZendX/JQuery/View/Helper/UiWidget.php';

class Platon_JQuery_View_Helper_MultiElement extends ZendX_JQuery_View_Helper_UiWidget
{
    /**
     * Рисуем элемент
     *
     * @param string $id Id HTML-элемента
     * @param string $value Значение элемента
     * @param array $params Массив конфигурации из секции options
     * @return string
     */
    public function multiElement($id, $value = null, array $params = array()) {
        /**
         * Определяем форму с элементами группы
         * Добавляем форму в JS
         */
		$jsFormLabelVar = $id . 'SubFormLabels';
        if(isset($params['renderform'])) {
            $this->jquery->addJavascript('var ' . $jsFormLabelVar . ' = ' 
                                                . ZendX_JQuery::encodeJson($params['labels']) . ';');
        }
        $jsFormVar = $id . '_subform';
        if(isset($params['renderform'])) {
            $this->jquery->addJavascript('var ' . $jsFormVar . ' = ' 
                                                . ZendX_JQuery::encodeJson($params['renderform']) . ';');
        }
        /**
         * Определяем кнопку удаления группы
         */
        $icon_delete = $this->view->formButton($id . '_delete', 'Удалить');;
        /**
         * Формируем кнопку добавления новой группы и вешаем на нее JS
         */
        $button_id = $id . '_add';
        $button = $this->view->formButton($button_id, 'Добавить');
        $jquery_handler = ZendX_JQuery_View_Helper_JQuery::getJQueryHandler();
        $js = array();
        $js[] = sprintf('%s("#%s").next("ul").find("> li").prepend(%s("%s").click(function(){
            	%s(this).parent("li").remove();
                return false;
            }));',
        $jquery_handler, $button_id, $jquery_handler, addslashes($icon_delete), $jquery_handler);
        $js[] = sprintf('%s("#%s").click(function(){
            	var itr = %s(this).next("ul").find("> li").length-1;
            	var Tmpl = %s.replace(/name=\"%s\[\]\[/g,"name=\"%s["+itr+"][");
            	var li = %s(this).next("ul").find("li:last").clone(true).insertBefore(%s(this)
            					.next("ul").find("li:last")).prepend(Tmpl).show();
				if (typeof multiElementCallback == "function") {
					multiElementCallback(li);
				}
        	});',
        $jquery_handler, $button_id, $jquery_handler, $jsFormVar, $id, $id, $jquery_handler, $jquery_handler);
        $this->jquery->addOnLoad(join(PHP_EOL,$js));
        /**
         * Отрисовываем переданные группы и шаблон
         */
        $xhtml = array();
        $xhtml[] = '<ul>';
        $attribs = array();
        foreach ($params as $k=>$v) if(in_array($k,array('class','style'))) $attribs[$k] = $v;
        /**
         * Устанавливаем пришедшие значения
         */
		$valCount = count($value);
		for ($i = 0; $i < $valCount; $i++) {
			$form = $params['forms'][$i];
			$form->setElementsBelongTo($id . '['.$i.']');
        	$xhtml[] = '<li' . $this->_htmlAttribs($attribs) . '>' . $form->render() . '</li>';
		}
		
        /**
         * Отрисовываем "хвост" списка
         */
        if(isset($attribs['style'])) $attribs['style'] .= ';display:none'; else $attribs['style'] = 'display:none'; 
        $xhtml[] = '<li' . $this->_htmlAttribs($attribs) . '></li>';
        $xhtml[] = '</ul>';
		
        return $button . join(PHP_EOL,$xhtml);
    }
}
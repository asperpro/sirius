<?php

class Platon_JQuery_View_Helper_SimpleElement extends Platon_JQuery_View_Helper_GridElement
{
	protected $_gridMode = 'simple';
	
    public function simpleElement($name, $value = null, $attribs = null)
    {
    	return parent::gridElement($name, $value, $attribs);
    }
}
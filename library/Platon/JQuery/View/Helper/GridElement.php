<?php

require_once 'ZendX/JQuery/View/Helper/UiWidget.php';

class Platon_JQuery_View_Helper_GridElement extends ZendX_JQuery_View_Helper_UiWidget
{
	protected $_fields = array();
	protected $_name = null;
	protected $_upload = null;
	protected $_width = null;
	protected $_gridMode = 'simple';
	protected $_priceMode = 'IP';
	protected $_currencies = array();
	
    public function gridElement($name, $value = null, $attribs = null)
    {
    	$output = array();
    	
    	$this->_name = $name;
    	if (isset($attribs['fields'])) {
			$this->_fields['id'] = array(
				'title' => '№',
				'type' => 'plain',
				'style' => array(
					'width' => '42px',
					'text-align' => 'right',
				)
			);
    		$this->_fields = array_merge($this->_fields, $attribs['fields']);
    	}
    	
    	//$currencyModel = new Reference_Model_Currency();
    	//$this->_currencies = $currencyModel->getAllCurrencies(false, true);
    	$this->_currencies = array(0 => '0');

    	if (isset($attribs['upload'])) {
    		$this->_upload = $attribs['upload'];
    	}
    	if (isset($attribs['width'])) {
    		$this->_width = $attribs['width'];
    	}    	
    	if (isset($attribs['price_mode'])) {
    		$this->_priceMode = $attribs['price_mode'];
    	}
    	
    	$this->_setJavascript();
    	
    	$output[] = $this->_renderActions();
    	$output[] = $this->_openTable();
    	$output[] = $this->_renderHead();
    	$output[] = $this->_renderBody($value);
    	$output[] = $this->_renderFoot($value);
    	$output[] = $this->_closeTable();
    	
    	return implode('', $output);
    }
    
    protected function _openTable()
    {
    	return '<table class="zend_form_grid" ' . (($this->_width) ? 'style="' . $this->_width . '"':'') . '>';
    }
    
    protected function _closeTable()
    {
    	return '</table>';
    }
    
    protected function _renderHead()
    {
    	$output = array();
    	
    	$output[] = '<thead>';
    	$output[] = '<tr>';
    	
    	foreach ($this->_fields as $column => $attribs) {
    		
    		$fieldStyles = array();
    		if (!empty($attribs['style'])) {
    			foreach ($attribs['style'] as $style => $value) {
    				$fieldStyles[] = $style . ':' . $value; 
    			}
    		}
    		$inlineStyles = implode(';', $fieldStyles);
    		if ('id' == $column) {
    			$output[] = '<th style="' . $inlineStyles . '" id="zfg_head_' . $column . '">';
    		} else {
    			$output[] = '<th style="' . $inlineStyles . '" onclick="grid.sort(\'' . $column . '\')" id="zfg_head_' . $column . '">';	
    		}
    		$output[] = '<span></span><div style="float:' . $attribs['style']['text-align'] . ';">' . $attribs['title'] . '</div>';
    		$output[] = '</th>';
    	}
    	$output[] = '</tr>';
    	$output[] = '</thead>';
    	
    	return implode('', $output);
    }
    
    protected function _renderBody($value)
    {
    	$nomenclature = (isset($value['nomenclature'])) ? $value['nomenclature'] : $value;
    	if ($nomenclature) {
    		foreach ($nomenclature as $key => $val) {
	    		$nomenclature[$key]['id'] = $key + 1;
	    	}
    	}
    	
    	$js = array();
    	$js[] = "var grid = new Grid();";
    	$js[] = "var currencies = " . Zend_Json::encode($this->_currencies) . ";";
    	$js[] = "grid.data = " . (($value) ? Zend_Json::encode($nomenclature) : '[]') . ";";
    	$js[] = "grid.fields = " . Zend_Json::encode($this->_fields) . ";";
    	$js[] = "grid.name = '" . $this->_name . "';";
    	$js[] = "grid.mode = '" . $this->_gridMode . "';";
    	$js[] = "grid.price_mode = '" . $this->_priceMode . "';";
		$js[] = "jQuery(document).ready(function(){
					grid.render();
				});";
	
		$this->jquery->addJavascript(implode('', $js));
    	
    	return '<tbody></tbody>';
    }
    
    protected function _renderActions()
    {
    	$output = array();
    	$output[] = '<table class="zend_form_grid_actions">';
		$output[] = '<tr>';
    	$output[] = '<td>';
    	$output[] = '<a href="javascript:;" onclick="grid.addRow();">Добавить</a>';
    	$output[] = '<a href="javascript:;" onclick="grid.deleteRow();">Удалить</a>';
    	$output[] = '</td>';
    	if ($this->_upload) {
	    	$output[] = '<td align="right">';
	    	$output[] = '<div id = "upload_container"></div>';
	    	$output[] = '</td>';	
    	}
    	$output[] = '</tr>';
    	$output[] = '</table>';
    	
    	return implode('', $output);
    }
    
    protected function _renderFoot($value)
    {
    }
    
    protected function _setJavascript()
    {
    	$js = array();
    	$js[] = "var uploadifySwfFile = '/public/swf/uploadify.swf';
				var uploadPhpFile    = '/reference/nomenclature/upload';
				var uploadFolder      = '/media/uploads/';
				var uploadCancelImg  = '/public/img/bullet_cross.png';
				var uploadBrowseButton = '/public/img/upload.png';";
    	
    	$jsOnLoad = "jQuery('#upload_container').uploadify({
    					'height'   : 13,
						'uploader' : uploadifySwfFile,
						'script'   : uploadPhpFile,
						'folder'   : uploadFolder,
						'cancelImg': uploadCancelImg,
						'auto'     : true,
						'multi'	   : false,
						'fileExt'  : '*.xls',
						'fileDesc' : 'Microsoft Excel (.xls)',
						'displayData': 'speed',
						'buttonImg': uploadBrowseButton,
						'onCancel' : function (event, queueID, fileObj, data) {
							alert('Загрузка файла отменена');
						},
						'onError' : function (event, queueID, fileObj, errorObj) {
							alert('Error during file upload. Maybe the file is too big ? Size: ' +  fileObj.size + ' Error:' +  errorObj.info());
						},
						'onComplete' : function (event, queueID, fileObj, response, data) {
							if (response == '' || response == 0 || response == '0') {
								alert('Error during with the upload');
							} else {
								grid.fileUploadCallback(response);
							}
						}
					});";
    	
    	$this->jquery->addJavascript(implode('', $js));
    	$this->jquery->addOnLoad($jsOnLoad);
    	$this->jquery->addStylesheet('/public/css/uploadify.css');
    	$this->jquery->addJavascriptFile('/public/js/swfobject.js');
    	$this->jquery->addJavascriptFile('/public/js/jquery.uploadify.v2.1.4.min.js');
    	$this->jquery->addJavascriptFile('/public/js/grid_element.js');
    }
}
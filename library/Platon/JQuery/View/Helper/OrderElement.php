<?php

class Platon_JQuery_View_Helper_OrderElement extends Platon_JQuery_View_Helper_GridElement
{
	protected $_gridMode = 'price';

    public function orderElement($name, $value = null, $attribs = null)
    {
    	return parent::gridElement($name, $value, $attribs);
    }
    
    protected function _renderBody($value)
    {
    	$nomenclature = (isset($value['nomenclature'])) ? $value['nomenclature'] : null;
    	if ($nomenclature) {
    		foreach ($nomenclature as $key => $val) {
	    		$nomenclature[$key]['id'] = $key + 1;
	    	}
    	}
    	$js = array();
    	$js[] = "var grid = new Grid();";
    	$js[] = "grid.data = " . (($nomenclature) ? Zend_Json::encode($nomenclature) : '[]') . ";";
    	$js[] = "grid.fields = " . Zend_Json::encode($this->_fields) . ";";
    	$js[] = "grid.name = '" . $this->_name . "';";
    	$js[] = "grid.mode = '" . $this->_gridMode . "';";
    	$js[] = "grid.price_mode = '" . $this->_priceMode . "';";
    	$js[] = "grid.prepaymentTotal = '" . (float)$value['prepayment_total'] . "';";
		$js[] = "jQuery(document).ready(function(){grid.render();});";
	
		$this->jquery->addJavascript(implode('', $js));
    	
    	return '<tbody></tbody>';
    }
    
    public function _renderFoot($value)
    {
    	$output = array();
    	
    	$total = 0;
    	$nomenclature = (isset($value['nomenclature'])) ? $value['nomenclature'] : null;
    	if ($nomenclature) {
    		foreach ($nomenclature as $val) {
    			$total += $val['total'];
    		}
    	}
    	$currency = 'грн.';
    	
		$output[] = '<tfoot style="display:block;">';
		$output[] = '<tr class="total" style="display:block;">';
		$output[] = '<td style="display:block;"><div style="float:left;">Всего:</div><div style="float:right;" class="base_total"><span class="v">' . number_format($total, 2, '.', '') . '</span> <span class="grid_currency">' . $currency .  '</div></span></td>';
		$output[] = '<td style="display:block;"><div style="float:left;">Оплачено:</div><div style="float:right;" class="prepayment_total"><span class="v">' . number_format($value['prepayment_total'], 2, '.', '') . '</span> <span class="grid_currency">' . $currency .  '</span></div></td>';
		$output[] = '<td style="display:block;"><div style="float:left;">Итого к оплате:</div><div style="float:right;" class="balance_total"><span class="v">' . number_format($value['balance_total'], 2, '.', '') . '</span> <span class="grid_currency">' . $currency .  '</span></div></td>';
		$output[] = '</tr>';
		$output[] = '</tfoot>';
    	
    	return implode('', $output);
    }
}
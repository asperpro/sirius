<?php

require_once 'Zend/Form/Element/Xhtml.php';

class Platon_JQuery_Form_Element_Grid extends Zend_Form_Element_Xhtml
{
	public $helper = 'gridElement';

	public function isValid ($value, $context = null)
    {
        return parent::isValid($value, $context);
    }

    public function getValue()
    {
        if (is_array($this->_value)) {
			$value = $this->_value;
            $this->setValue($value);
        }

        return parent::getValue();
    }
}

<?php

class Platon_JQuery_Form_Element_Grid_Simple extends Platon_JQuery_Form_Element_Grid
{
	public $helper = 'simpleElement';

	public function isValid ($value, $context = null)
    {
    	if ($value) {
    		$nomenclatureModel = new Reference_Model_Nomenclature();
	    	foreach ($value as $key => $_value) {
	    		try {
	    			if (isset($_value['name'])) {
	    				if (!$_value['name']) {
	    					throw new Exception('Введите наименование номенклатуры');	
	    				}
		    			if (!$item = $nomenclatureModel->findByName($_value['name'])) {
			    			throw new Exception('Не удалось найти номенклатуру "' . $_value['name'] . '"');
			    		}	
			    	}
		    		if (isset($_value['count']) && !(int)$_value['count']) {
			    		throw new Exception('Количество товара не может быть нулевым');
			    	}
		    		$value[$key]['id'] = $item['id'];
	    		} catch (Exception $e) {
	    			$this->addError($e->getMessage());
	    			if ($this->getErrorMessages()) {
			    		return false;	
			    	}
	    		}
			}	
    	}
    	$this->setValue($value);
    	
        return parent::isValid($value, $context);
    }
}
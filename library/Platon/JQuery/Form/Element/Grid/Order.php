<?php

class Platon_JQuery_Form_Element_Grid_Order extends Platon_JQuery_Form_Element_Grid
{
	public $helper = 'orderElement';

	public function isValid ($value, $context = null)
    {
    	if ($value) {
	    	$nomenclatureModel = new Reference_Model_Nomenclature();
	    	foreach ($value as $key => $_value) {
	    		try {
	    			if (empty($_value['name'])) {
		    			throw new Exception('Введите наименование номенклатуры');
			    	}
					if (!$item = $nomenclatureModel->findByName($_value['name'])) {
		    			throw new Exception('Не удалось найти номенклатуру "' . $_value['name'] . '"');
		    		}
		    		if (!(int)$_value['count']) {
			    		throw new Exception('Количество товара не может быть нулевым');
			    	}
			    	if (!(float)$_value['price']) {
			    		throw new Exception('Цена товара не может быть нулевой');
			    	}
			    	if (!(float)$_value['total']) {
			    		throw new Exception('Сумма товара не может быть нулевой');
			    	}
		    		$value[$key]['id'] = $item['id'];
	    		} catch (Exception $e) {
	    			$this->addError($e->getMessage());
	    			if ($this->getErrorMessages()) {
			    		return false;	
			    	}
	    		}
			}	
    	}
    	$this->setValue($value);
    	
        return parent::isValid($value, $context);
    }
}

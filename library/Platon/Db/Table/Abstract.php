<?php

class Platon_Db_Table_Abstract extends Zend_Db_Table_Abstract
{
//	protected function _setupDatabaseAdapter() 
//	{
//	    $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
//		$resource = $bootstrap->getPluginResource('multidb');
//		$this->_db = $resource->getDb();
//	}
	
	public function find($id)
	{
		$row = parent::find($id)->current();
		if ($row !== null) {
			return $row->toArray();
		}
		
		return $row;
	}

	public function truncate($table) {

		$sql = 'TRUNCATE TABLE ' . $table . ';';
		return $this->_db->query($sql);
	}
	
	public function replace($table, $data) {

		// columns submitted for insert
		$dataColumns = array_keys($data);

		// generate SQL statement
		$cols = '';
		$vals = '';
		foreach ($dataColumns as $col) {
			$cols .= $this->getAdapter()->quoteIdentifier($col) . ',';
			$vals .=	(@get_class($data[$col]) == 'Zend_Db_Expr')
						? $data[$col]->__toString()
						: $this->getAdapter()->quoteInto('?', $data[$col]);
			$vals .= ',';
		}
		$cols = rtrim($cols, ',');
		$vals = rtrim($vals, ',');
		$sql = 'REPLACE INTO ' . $table . ' (' . $cols . ') VALUES (' . $vals . ');';

		return $this->_db->query($sql);

	}
}
<?php

class Sirius_Resource_Dwtoys extends Sirius_Resource
{
	public function search()
	{
		$revision = array();
		$keyword = trim($this->getProduct()->sku);
		$requestUrl = $this->getInterface()->url . "/Search?text=" . $keyword;
		$response = $this->getResponse($requestUrl);
		$sku = '';
		if ($response && 200 == $response->getStatus()) {
			if ($content = $this->compressContent($response->getBody())) {
				if (preg_match('/<h2 class="catalog_item_title"><a href="(.*?)">.*?<\/a><\/h2>/', $content, $matches)) {
					// Load product page
					$encodedUrlParams = array();
					$parts = explode('/', $matches[1]);
					foreach ($parts as $part) {
						$encodedUrlParams[] = urlencode($part);
					}
					$matches[1] = implode('/', $encodedUrlParams);
					$requestUrl = $this->getInterface()->url . $matches[1];
					$response = $this->getResponse($requestUrl);
					if ($response && 200 == $response->getStatus()) {
						$content = $this->compressContent($response->getBody());
						$revision['request_url'] = $requestUrl;

						// Get revision name
						if (preg_match('/<div class="product_title.*?>.*?<h1>(.*?)<\/h1>/is', $content, $matches)) {
							$revision['name'] = $matches[1];
						} else {
							$revision['name'] = $this->getProduct()->name;
						}
						// Get revision attributes
						$revision['attributes'] = array();
						if (preg_match_all('/<td align="right" width=".*?">(.*?)<\/td>.*?<td>(.*?)<\/td>/s', $content, $matches)) {
							//attributes is name of attribute
							$attributes = str_replace("</strong>", '', $matches[1]);
							$values = $matches[2];
							foreach ($attributes as $key => $attribute) {
								$revision['attributes'][$this->formatAttributeName($attribute)] = $this->escape($values[$key]);
							}
						}

						// Get revision sku
						if (preg_match('/>Арт\. .*? (.*?)<\/div>/is', $content, $matches)) {
							$sku = $matches[1];
						}

						// Get revision price value
						if (preg_match('/catalog_item_price">(.*?) <span>/is', $content, $matches)) {
							$price = $matches[1];
							$revision['price'] = $price;
						}

						// Get revision category
						$revision['category'] = '';
						if (preg_match('/"breadcrumbs">(.*?)<\/div>/is', $content, $matches)) {
							if (preg_match_all('/<a.*?>(.*?)<\/a>.*?\//is', $matches[1], $matches)) {
								$revision['category'] = $this->escape(implode('/', $matches[1]));
							}
						}


						// Get revision weight value
						$revision['weight'] = 0;

						// Get revision images
						$revision['media'] = array();
						if (preg_match('/<div class="catalog_item_img">(.*?)<\/div>/is', $content, $matches)) {
							if (preg_match_all('/<a .*? href="(.*?)".*?>/is', $matches[1], $matches)) {
								if (!empty($matches[1])) {
									foreach ($matches[1] as $image) {
										$revision['media'][] = $this->getInterface()->url . $image;
									}
								}
							}
						}
						if (preg_match('/<div class="videobig" .*? src="(.*?)"/is', $content, $matches)) {
							$revision['media'][] = $matches[1];
						}

						// Get revision description value
						$revision['description'] = '';
						if (preg_match('/class="product_description_text">.*?p>(.*?)<\/p>/is', $content, $matches)) {
							$revision['description'] = $this->escape($matches[1], true);
						}
						if (preg_match('/class="product_tech_row">.*?<b>Дополнительно<\/b>.*?<td.*?>(.*?)<\/td>/is', $content, $matches)) {
							$revision['description'] = $revision['description'] . "<br />" . $matches[1];
						}
					}
				}
			}
		}
		
		if (!$sku) {
			$revision = array();
		}
		$this->setRevision($revision);
	}

	protected function compressContent($value)
	{
		$value = trim($value);
		$value = preg_replace("#[\n\r\t]#", "", $value);
		$value = preg_replace('#>[\s]+<#', '><', $value);
		$value = preg_replace('#[\s]+#', ' ', $value);
		$value = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $value);
		$value = preg_replace('#<link(.*?)/>#is', '', $value);

		return $value;
	}
}
<?php

class Sirius_Resource_Babyhit extends Sirius_Resource
{
	public function search()
	{
		$revision = array();
		
		$keyword = trim($this->getProduct()->sku);
		$keyword = urlencode($keyword);
		$requestUrl = $this->getInterface()->url . '/search/?q=' . $keyword;
		$response = $this->getResponse($requestUrl);
		
		// Load product page
		if ($response &&
			200 == $response->getStatus()) {
			if ($content = $this->compressContent($response->getBody())) {
				$revision['request_url'] = $requestUrl;
				// Get revision name
				$revision['name'] = '';
				if (preg_match('/<h1 itemprop=\"name\">(.*?)<\/h1>/is', $content, $matches)) {
					$revision['name'] = $this->escape($matches[1]);
				}
				
				// Get revision price value
				$revision['price'] = '';
				if (preg_match('/<span itemprop="price"><meta itemprop="priceCurrency" content="UAH"\>(.*?)<\/span>/is', $content, $matches)) {
					$revision['price'] = $this->escape($matches[1]);
				}
				
				// Get revision weight value
				$revision['weight'] = 0;
				
				// Get revision category
				$revision['category'] = '';
				if (preg_match_all('/<span itemprop=\"title\">(.*?)<\/span>/is', $content, $matches)) {
					array_shift($matches[1]);
					array_shift($matches[1]);
					$revision['category'] = $this->escape(implode('/', $matches[1]));
				}
				
				// Get revision description value
				$revision['description'] = '';
				if (preg_match('/<div id=\"tab-1\".*?>(.*?)<\/div>/s', $content, $matches)) {
					$revision['description'] = trim($this->escape($matches[1], true), '<br>');
				}
				// Get revision images
				$revision['media'] = array();
				if (preg_match_all('/<a href="\/upload\/(.+?)" data-lightbox=\"zoomer\".*?>/is', $content, $matches)) {
					if (!empty($matches[1])) {
						foreach ($matches[1] as $image) {
							$revision['media'][] = $this->getInterface()->url . '/upload/' . $image;
						}
					}
				}
				
				// Get revision attributes
				$revision['attributes'] = array();
				if (preg_match_all('/<tr><td>(.*?)<\/td><td>(.*?)<\/td><\/tr>/is', $content, $matches)) {
					$attributes = $matches[1];
					$values = $matches[2];
					foreach ($attributes as $key => $attribute) {
						$attribute = $this->escape($attribute, true);
						$attribute = str_replace('?', '', $attribute);
						$revision['attributes'][$attribute] = $this->escape($values[$key]);	
					}
				}
				if (preg_match('/<span itemprop=\"brand\">(.*?)<\/span>/is', $content, $matches)) {
					$revision['attributes']['Производитель'] = $this->escape($matches[1]);
				}
			}
		}
		
		if (false == strpos($content, 'Артикул: ' , $this->getProduct()->sku)) {
			$revision = array();
		}
		
		$this->setRevision($revision);
	}
	
	protected function compressContent($value)
	{
		$value = trim($value);
		$value = preg_replace("#[\n\r\t]#", "", $value);
	    $value = preg_replace('#>[\s]+<#', '><', $value);
	    $value = preg_replace('#[\s]+#', ' ', $value);
		$value = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $value);
		
		return $value;
	}
}
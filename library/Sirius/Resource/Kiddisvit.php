<?php

class Sirius_Resource_Kiddisvit extends Sirius_Resource
{
	public function search()
	{
		$revision = array();
		if (is_numeric($this->getProduct()->sku)) $this->getProduct()->sku = '0' . $this->getProduct()->sku;
		$keyword = trim($this->getProduct()->sku);

		$requestUrl = $this->getInterface()->url . '/brands/search';
		$requestData = array('search_key_www' => $keyword);

		$response = $this->getResponse($requestUrl, 'post', $requestData);

		if ($response && 200 == $response->getStatus()) {
			if ($content = $this->compressContent($response->getBody())) {
				$content = iconv('windows-1251', 'utf-8', $content);

				if (preg_match('/<div class="tov_name_b_new2"><a href="(.*?)".*?>.*?<\/a><\/div>/', $content, $matches)) {
					
					// Load product page
					$requestUrl = $this->getInterface()->url . '/' . $matches[1];
					$response = $this->getResponse($requestUrl);
					$revision['request_url'] = $requestUrl;

					if ($response && 200 == $response->getStatus()) {
						$content = $this->compressContent($response->getBody());
						$content = iconv('windows-1251', 'utf-8', $content);

						// Get revision weight value
						$revision['weight'] = 0;

						// Get revision name
						$revision['name'] = '';
						if (preg_match('/<div class="content">.*?<div class="blue fs20 pb17">(.*?)<\/div>/', $content, $matches)) {
							$revision['name'] = $this->escape($matches[1]);
						}

						// Get revision description value
						$revision['description'] = '';
						if (preg_match('/<div style="display: block;" id="d1" class="div_tovar_navi">.*?<!--.*?-->.*?<div class="marker">.*?Отзывы \(.*?\)(.*?)<div id="dv3" class="div_tovar_sert">/is', $content, $matches)) {
							$revision['description'] = $this->escape($matches[1], true);
						}

						// Get revision price value
						$revision['price'] = 0;
						if (preg_match('/<div class="razd-param">.*?<span class="bold">(.*?)<\/span>.*?<div class="old_price">/', $content, $matches)) {
							$price = $this->formatPrice($matches[1]);
							$revision['price'] = $this->escape($price);
						}

						// Get revision attributes
						$revision['attributes'] = array();
						if (preg_match_all('/<div class="pb7"><span class="dgrey block float-l w80">(.*?)<\/span><span class="block float-l">(.*?)<\/span>/is', $content, $matches)) {
							$attributes = $matches[1];
							$values = $matches[2];
							foreach ($attributes as $key => $attribute) {
								$revision['attributes'][$this->formatAttributeName($attribute)] = $this->escape($values[$key]);
							}

							if (isset($revision['attributes']['Бренд'])) {
								$revision['attributes']['Производитель'] = $this->escape($revision['attributes']['Бренд']);
								unset($revision['attributes']['Бренд']);
							}

							unset($revision['attributes']['Артикул']);
						}

						// Get revision category
						$revision['category'] = '';
						if (isset($revision['attributes']['Категория'])) {
							$revision['category'] = $this->escape($revision['attributes']['Категория']);
							unset($revision['attributes']['Категория']);
						}

						// Get revision images
						$revision['media'] = array();
						if (preg_match_all('/<table.*?tab_img_tov_new.*?<a.*?href="(.*?)".*?<img/is', $content, $matches)) {
							if (!empty($matches[1])) {
								foreach ($matches[1] as $image) {
									$revision['media'][] = $image;
								}
							}
						}
					}
				}
			}
		}

		$this->setRevision($revision);
	}
}
<?php

class Sirius_Resource_Microlabtoys extends Sirius_Resource
{
	public function search()
	{
		$revision = array();
		
		$keyword = trim($this->getProduct()->sku);
		$keyword = urlencode($keyword);
		$requestUrl = $this->getInterface()->url . '/search/?q=' . $keyword;
		$response = $this->getResponse($requestUrl);
		
		// Load search page
		if ($response &&
			200 == $response->getStatus()) {
			if ($content = $this->compressContent($response->getBody())) {

				if (preg_match('/<a class="search_page-catalog-item-name" href="(.*?)">.*?<\/a>/is', $content, $matches)) {
					if (!empty($matches[1])) {
						$requestUrl = $this->getInterface()->url . $matches[1];
						$response = $this->getResponse($requestUrl);
						
						// Load product page
						if ($response &&  200 == $response->getStatus()) {
    				        if ($content = $this->compressContent($response->getBody())) {
    				            $revision['request_url'] = $requestUrl;
    				            // Get revision name
    				            $revision['name'] = '';
    				            if (preg_match('/<h1>(.*?)<\/h1>/is', $content, $matches)) {
    				                $revision['name'] = $this->escape(html_entity_decode($matches[1]));
    				            }
    				            
    				            // Get revision price value
    				            $revision['price'] = '';
    				            if (preg_match('/<div class="catalog-element-price"><div class="crossed_price"><\/div><div class="price">(.*?)<\/div><\/div>/is', $content, $matches)) {
    				                $revision['price'] = $this->formatPrice($this->escape($matches[1]));
    				            }
    				            
    				            // Get revision weight value
    				            $revision['weight'] = 0;
    				            
    				            // Get revision category
    				            $revision['category'] = '';
    				            if (preg_match_all('/<span itemprop=\"title\">(.*?)<\/span>/is', $content, $matches)) {
    				                array_shift($matches[1]);
    				                array_shift($matches[1]);
    				                $revision['category'] = $this->escape(implode('/', $matches[1]));
    				            }
    				            
    				            // Get revision description value
    				            $revision['description'] = '';
    				            if (preg_match('/<div class="catalog-element-prevtext"><div class="scroll_detail_previevtext"><div>(.*?)<\/div>/s', $content, $matches)) {
    				                $revision['description'] = trim($this->escape($matches[1], true));
    				            }
    				            // Get revision images
    				            $revision['media'] = array();
    				            if (preg_match_all('/<a rel="nofollow" class="image" href="[0-9]" data-bigimagesrc="(.*?)">/is', $content, $matches)) {
    				                if (!empty($matches[1])) {
    				                    foreach ($matches[1] as $image) {
    				                        $revision['media'][] = $this->getInterface()->url . $image;
    				                    }
    				                }
    				            }
    				            
    				            // Get revision attributes
    				            $revision['attributes'] = array();
    				            if (preg_match_all('/<div class="catalog-element-gruped_prop .*? clearfix"><b>(.*?)<\/b><span>(.*?)<\/span><\/div>/is', $content, $matches)) {
    				                $attributes = $matches[1];
    				                $values = $matches[2];
    				                foreach ($attributes as $key => $attribute) {
    				                    $attribute = $this->escape($attribute, true);
    				                    $revision['attributes'][$attribute] = $this->escape(html_entity_decode($values[$key]));
    				                }
    				            }
    				        }
						}
					}
				}
				
			}
		}
		$this->setRevision($revision);
	}
	
	protected function compressContent($value)
	{
		$value = trim($value);
		$value = preg_replace("#[\n\r\t]#", "", $value);
	    	$value = preg_replace('#>[\s]+<#', '><', $value);
	    	$value = preg_replace('#[\s]+#', ' ', $value);
		$value = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $value);
		
		return $value;
	}
}
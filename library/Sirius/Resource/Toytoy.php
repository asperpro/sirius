<?php

class Sirius_Resource_Toytoy extends Sirius_Resource
{
	public function search()
	{
		$revision = array();
		
		$keyword = trim($this->getProduct()->sku);
		$requestUrl = $this->getInterface()->url;
		$response = $this->getResponse($requestUrl, 'post', array('serach' => $keyword));

		if ($response &&
			200 == $response->getStatus()) {
			if ($content = $this->compressContent($response->getBody())) {
				if (preg_match('/<li><h3><a href="(.*?)">.*?<\/a><\/h3><\/li>/', $content, $matches)) {
					// Load product page
					$requestUrl = $matches[1];
					$response = $this->getResponse($requestUrl);
					if ($response && 200 == $response->getStatus()) {
						$content = $this->compressContent($response->getBody());

						$revision['request_url'] = $requestUrl;
						// Get revision name
						$revision['name'] = '';
						if (preg_match('/<h1>(.*?)<\/h1>/', $content, $matches)) {
							$revision['name'] = $this->escape($matches[1]);
						}
						
						// Get revision price value
						$revision['price'] = '';
						if (preg_match('/<div class="curent_price">(.*?)<\/div>/', $content, $matches)) {
							$revision['price'] = $this->escape($matches[1]);
						}
		
						// Get revision category
						$revision['category'] = '';
						if (preg_match('/<div class="bredcrumb"><ul>.*?<li><a href="http:\/\/toytoyukraine.com\/catalog\/.*?">(.*?)<\/a><\/li><li>&rsaquo;<\/li>.*?<\/ul><\/div>/', $content, $matches)) {
							$revision['category'] = $this->escape($matches[1]);
						}
		
						// Get revision weight value
						$revision['weight'] = 0;
		
						// Get revision images
						$revision['media'] = array();
						if (preg_match_all('/<a href="(.*?)" rel="large_image" title=".*?">/is', $content, $matches)) {
							foreach ($matches[1] as $image) {
								$url = $this->getInterface()->url . $image;
								if (filter_var($url, FILTER_VALIDATE_URL)) {
									$revision['media'][] = $url;	
								}
							}
						}
						
						// Get revision attributes
						$revision['attributes'] = array();
						if (preg_match('/<p class="h2"><b>(.*?)<\/b>.*?<\/p>/is', $content, $matches)) {
							$revision['attributes']['Производитель'] = $this->escape($matches[1]);
						}
		
						// Get revision description value
						$revision['description'] = '';
						if (preg_match('/<div class="description_text">(.*?)<\/div>/is', $content, $matches)) {
							$revision['description'] = $this->escape($matches[1], true);
							if (preg_match('/https:\/\/www.youtube.com\/embed\/(.*?)\?rel=0&amp;controls=0/', $content, $matches)) {
								$revision['attributes']['Видеообзор'] = 'https://www.youtube.com/v/' . $matches[1];
							}
						}
					}
				}
			}
		}

		$this->setRevision($revision);
	}

	protected function compressContent($value)
	{
		$value = trim($value);
		$value = preg_replace("#[\n\r\t]#", "", $value);
		$value = preg_replace('#>[\s]+<#', '><', $value);
		$value = preg_replace('#[\s]+#', ' ', $value);
		$value = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $value);
		$value = preg_replace('#<link(.*?)/>#is', '', $value);

		return $value;
	}
}
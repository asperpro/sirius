<?php

class Sirius_Resource_Neplaksa extends Sirius_Resource
{
    public function search()
    {
        $revision = array();
        $keyword = trim($this->getProduct()->sku);
        $keyword = urlencode($keyword);
        $searchRequestUrl = $this->getInterface()->url . '/index.php?keywords=' . $keyword;
        $response = $this->getResponse($searchRequestUrl);

        // Load search page
        if ($response &&
            200 == $response->getStatus()
        ) {
            if ($content = $this->compressContent($response->getBody())) {
                $requestUrl = '';
                preg_match_all('/<div class=\"product_name\"><a href=\"(.*?)\"><span.*?/', $content, $matches);

                if (!empty($matches[1])) {
		$revisionFound = false;
                    foreach ($matches[1] as $_match) {
		    if ($revisionFound) continue;
                        $requestUrl = $_match;
                        if ($requestUrl) {
                            $response = $this->getResponse($requestUrl);
                            // Load product page
                            if ($response && 200 == $response->getStatus()) {
                                if ($content = $this->compressContent($response->getBody())) {
			      $revision = array();
                                    $revision['request_url'] = $requestUrl;
                                    // Get revision name
                                    $revision['name'] = '';
                                    if (preg_match('/<h1 class=\"product_name\"><span>(.*?)<\/span><\/h1>/is', $content, $matches)) {
                                        $revision['name'] = $this->escape(html_entity_decode($matches[1]));
                                        $revision['name'] .= ' (' . trim($this->getProduct()->sku) . ')';
                                    }

                                    // Get revision price value
                                    $revision['price'] = 0;

                                    // Get revision weight value
                                    $revision['weight'] = 0;

                                    // Get revision category
                                    $revision['category'] = '';
                                    preg_match_all('/<i itemscope itemtype=\"http:\/\/data-vocabulary.org\/Breadcrumb\"><a itemprop=\"url\" href=\".*?\" class=\"headerNavigation\"><span itemprop=\"title\">(.*?)<\/span><\/a><\/i>/', $content, $matches);

                                    if (!empty($matches[1])) {
                                        array_shift($matches[1]);
                                        $revision['category'] = implode('/', $matches[1]);
                                    }

                                    // Get revision description value
                                    $revision['description'] = '';
                                    if (preg_match_all('/<P>(.*?)<\/P>/', $content, $matches)) {
                                        $revision['description'] = trim($this->escape(implode('<br>', $matches[1])), '<br>');
                                    }
                                    // Get revision attributes
                                    $revision['attributes'] = array();

                                    preg_match_all('/<TD>(.*?)<\/TD>/', $content, $matches);
                                    if (!empty($matches[1])) {
                                        $attributes = $matches[1];
                                        foreach ($attributes as $key => $_attribute) {
                                            if ($key%2 != 0) continue;
                                            $attribute = str_replace(':', '', $this->escape($_attribute, true));
				    $attributes[$key+1] = str_replace('&nbsp;', ' ', trim($attributes[$key+1], '.'));
				    if ('Материал' == $attribute) {
					$attributeArray = explode(', ', $this->escape($attributes[$key+1]));
					foreach ($attributeArray as $i => $_attributeArray) {
						$attributeArray[$i] = mb_convert_case($_attributeArray, MB_CASE_TITLE, 'UTF-8');	
					}
					$attributeArray = $attributeArray[0];
				    }
                                            $values = ('Материал' == $attribute) ? $attributeArray : $attributes[$key+1];
                                            $revision['attributes'][$this->formatAttributeName($attribute)] = $values;
                                        }
                                    }

                                    if (!empty($revision['attributes']['Артикул'])) {
                                        $revision['attributes']['Артикул'] = trim($revision['attributes']['Артикул'], '.');
                                    }

                                    // Get brand
                                    if (preg_match('/>Другие товары от (.*?)</', $content, $matches)) {
                                        $revision['attributes'][$this->formatAttributeName('Бренд')] = $this->escape($matches[1]);
                                    }

                                    // Get revision images
                                    $revision['media'] = array();
                                    if (preg_match_all('/<img class=\"zoom ch_im\" data-zoom-image=\"(.*?)\"/is', $content, $matches)) {
                                        if (!empty($matches[1])) {
                                            foreach ($matches[1] as $image) {
                                                $revision['media'][] = $this->getInterface()->url . '/' . $image;
                                            }
                                            $revision['media'] = array_unique($revision['media']);
                                        }
                                    }
                                }
                            }
                        }
                        if (!empty($revision['attributes']['Артикул']) && $revision['attributes']['Артикул'] == trim($this->getProduct()->sku)) {

                            unset($revision['attributes']['Артикул']);
                            $revision['is_moderated'] = true;
                            $this->setRevision($revision);
		        $revisionFound = true;
                        }
                    }
                }
            }
        }
    }

    protected function compressContent($value)
    {
        $value = trim($value);
        $value = preg_replace("#[\n\r\t]#", "", $value);
        $value = preg_replace('#>[\s]+<#', '><', $value);
        $value = preg_replace('#[\s]+#u', ' ', $value);
        $value = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $value);

        return $value;
    }

    protected function mbucfirst($string, $encoding)
    {
        $strlen = mb_strlen($string, $encoding);
        $firstChar = mb_substr($string, 0, 1, $encoding);
        $then = mb_substr($string, 1, $strlen - 1, $encoding);
        
        return mb_strtoupper($firstChar, $encoding) . $then;
    }
}
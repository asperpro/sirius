<?php

class Sirius_Resource_Mdi extends Sirius_Resource
{
	public function search()
	{
		$revision = array();

		$keyword = trim($this->getProduct()->sku);
		$keyword = urlencode($keyword);
		$requestUrl = $this->getInterface()->url . '/search/index.php?q=' . $keyword;
		$response = $this->getResponse($requestUrl);
		
		if ($response &&
			200 == $response->getStatus()) {
			if ($content = $this->compressContent($response->getBody())) {
				if (preg_match_all('/<div class="sect_img"><a href="(.*?)"><img src=".*?"><\/a><\/div><div class="cat\-item\-info">(.*?)<\/div>/', $content, $matches)) {
					$requestUrl = '';
					foreach ($matches[1] as $key => $link) {
						if ($this->getProduct()->sku == trim($matches[2][$key])) {
							$requestUrl = $this->getInterface()->url . $link;
							break;
						}
					}
					// Load product page
					$response = $this->getResponse($requestUrl);
					$revision['request_url'] = $requestUrl;
					if ($response &&
						200 == $response->getStatus()) {							
						$content = $this->compressContent($response->getBody());
						// Get revision name
						$revision['name'] = '';
						if (preg_match('/<h1><span>(.*)<\/span><\/h1>/', $content, $matches)) {
							$matches[1] = html_entity_decode($matches[1]);
							$revision['name'] = $this->escape($matches[1]);
						}
						
						// Get revision price value
						$revision['price'] = 0;
						if (preg_match('/<div class="price".*?>(.*?)<\/div>/', $content, $matches)) {
							$revision['price'] = $this->formatPrice($matches[1]);
						}
						
						// Get revision weight value
						$revision['weight'] = 0;
						if (preg_match('/<span class="text">Вес товара: (.*) г<\/span>/s', $content, $matches)) {
							$revision['weight'] = $this->formatWeight($matches[1]);
						}
						
						// Get revision category
						$revision['category'] = '';
						if (preg_match("/<a href=\"\/\" title=\"Главная страница\">Главная страница<\/a><\/li><li><a.*?>(.*?)<\/a><\/li>/ims", $content, $matches)) {
							$revision['category'] = $matches[1];
						}
						
						// Get revision description value
						$revision['description'] = '';
						if (preg_match('/<div class="bx_item_section_name_gray".*?>Полное описание<\/div><p>(.*?)<\/p>/s', $content, $matches)) {
							$revision['description'] = $this->escape($matches[1], true);
						}
						// Get revision images
						$revision['media'] = array();
						if (preg_match('/<a class="bx_bigimages_aligner">.*?<img id=".*?" src="(.*?)" alt=".*?" title=".*?">/', $content, $matches)) {
							$matches[1] = strstr($matches[1], '?', true);
							$revision['media'][] = 'http:' . $matches[1];
						}
						if (preg_match_all('/<a class="cnt ra" href="(.*?)" rel="group">/', $content, $matches)) {
							if (!empty($matches[1])) {
								foreach ($matches[1] as $image) {
									$image = strstr($image, '?', true);
									$revision['media'][] = 'http:' . $image;
								}
							}
						}
						// Get revision attributes
						$revision['attributes'] = array();
						$revision['attributes']['Производитель'] = 'МДИ';
						if ('Lucy&Leo' == $revision['category']) {
							$revision['attributes']['Производитель'] = 'Lucy&Leo';
						}
					}
				}
			}
		}

		$this->setRevision($revision);
	}
	
	protected function compressContent($value)
	{
		$value = trim($value);
		$value = preg_replace("#[\n\r\t]#", "", $value);
	    	$value = preg_replace('#>[\s]+<#', '><', $value);
	    	$value = preg_replace('#[\s]+#', ' ', $value);
		$value = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $value);
		
		return $value;
	}
}
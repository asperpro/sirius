<?php

class Sirius_Resource_Kinder extends Sirius_Resource
{
	public function search()
	{
		$revision = array();
		$keyword = trim($this->getProduct()->sku);

		// Load product page
		$requestUrl = $this->getInterface()->url . '/search?query=' . $keyword;
		$response = $this->getResponse($requestUrl);

		if ($response && 200 == $response->getStatus()) {
			if ($content = $this->compressContent($response->getBody())) {

				if (preg_match('/<ul class="goods-tiles">.*?<div class="description"><a href="(.*?)".*?<\/a>/', $content, $matches)) {

					// Load product page
					$requestUrl = $this->getInterface()->url . $matches[1];
					$response = $this->getResponse($requestUrl);

					if ($response && 200 == $response->getStatus()) {
						$content = $this->compressContent($response->getBody());

						$revision['request_url'] = $requestUrl;

						// Get revision name
						$revision['name'] = '';
						if (preg_match('/<h1 class="title" itemprop="name">(.*?)<\/h1>/', $content, $matches)) {
							$revision['name'] = $this->escape($matches[1]);
						}

						// Get revision price value
						$revision['price'] = 0;
						if (preg_match('/<span class="number" itemprop="price">(.*?)<\/span>/', $content, $matches)) {
							$revision['price'] = $this->formatPrice($this->escape($matches[1]));
						}

						// Get revision description value
						$revision['description'] = '';
						if (preg_match('/<h2>О товаре<\/h2>(.*?)<\/div>/', $content, $matches)) {
							$revision['description'] = $this->escape($matches[1], true);
						}

						// Get revision attributes
						$revision['attributes'] = array();

						preg_match('/<div.*?id="characteristics">.*?<ul class="characteristics-list">(.*?)<\/ul>/', $content, $matches);
						if (count($matches) > 1) {
							$attributesContent = $matches[1];

							if (preg_match_all('/<li><span class="key">(.*?):<\/span><span class="value">(.*?)<\/span><\/li>/', $attributesContent, $matches)) {
								$attributes = $matches[1];
								$values = $matches[2];
								foreach ($attributes as $key => $attribute) {
									if (($this->formatAttributeName($attribute)) == '') continue;
									$revision['attributes'][$this->formatAttributeName($attribute)] = $this->escape($values[$key]);
								}
							}
						}

						// Get revision category
						$revision['category'] = '';
						preg_match('/<ul.*?class="breadcrumbs">(.*?)<\/ul>/', $content, $matches);
						if (count($matches) > 1) {
							$categoriesContent = $matches[1];
							if (preg_match_all('/<li.*?><a.*?>(.*?)<\/a><\/li>/', $categoriesContent, $matches)) {
								array_shift($matches[1]);
								array_pop($matches[1]);
								$revision['category'] = $this->escape(implode('/', $matches[1]));
							}
						}

						// Get revision weight value
						$revision['weight'] = 0;
						if (isset($revision['attributes']['Вес изделия'])) {
							$revision['weight'] = $this->formatWeight($revision['attributes']['Вес изделия']);
							unset($revision['attributes']['Вес изделия']);
						}

						// Get revision producer
					
						if (isset($revision['attributes']['Бренд'])) {
							$revision['attributes']['Производитель'] = $revision['attributes']['Бренд'];
							unset($revision['attributes']['Бренд']);
						}

						// Get revision images
						$revision['media'] = array();

						preg_match('/<ul class="product-photos.*?>(.*?)<\/ul>/', $content, $matches);
						if (count($matches) > 1) {
							$mediaContent = $matches[1];

							if (preg_match_all('/<li.*?><img src="(.*?)".*?<\/li>/is', $mediaContent, $matches)) {
								if (!empty($matches[1])) {
									foreach ($matches[1] as $image) {
										$revision['media'][] = $this->getInterface()->url . $image;
									}
								}
							}
						}
					}
				}
			}

		}

		$this->setRevision($revision);
	}

	protected function compressContent($value)
	{
		$value = trim($value);
		$value = preg_replace("#[\n\r\t]#", "", $value);
		$value = preg_replace('#>[\s]+<#', '><', $value);
		$value = preg_replace('#[\s]+#', ' ', $value);
		$value = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $value);
		//$value = preg_replace('#<header(.*?)>(.*?)</header>#is', '', $value);
		$value = preg_replace('#<link(.*?)/>#is', '', $value);

		return $value;
	}
}
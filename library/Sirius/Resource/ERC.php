<?php

class Sirius_Resource_ERC extends Sirius_Resource
{
    const XMLSRC   = 'https://api.erc.ua/api/erc/wares.ru.xml';
    const XMLLOCAL = 'media/erc.xml';
    public function search()
    {
        $start = microtime(true);
        $mem_start = memory_get_usage();

        $keyword = trim($this->getProduct()->sku);
        $xmlUrl = $this->getInterface()->url;
        $revision = $this->ercXmlParse($keyword, $xmlUrl);

        $response = $this->getResponse($revision['request_url']);

        // Load product page
        if ($response && 200 == $response->getStatus()) {
            if ($content = $this->compressContent($response->getBody())) {

                // Get revision category
                $revision['category'] = '';
                preg_match('/<ul class="breadcrumbs.*?">(.*?)<\/ul>/is', $content, $matches);
                preg_match_all('/<li>\s*<a.*?>(.*?)<\/a>\s*<\/li>/is', $matches[1], $matches);
                if (!empty($matches[1])) {

                    array_shift($matches[1]);
                    array_pop($matches[1]);
                    $category = implode('/', $matches[1]);
                    $revision['category'] = $this->escape(strip_tags($category));
                }
            }
        }

        $this->setRevision($revision);
    }

    protected function ercXmlParse($sku)
    {
        libxml_use_internal_errors(false);
        $this->receivefile();
        $ercXmlReader = new XMLReader;
        $ercXmlReader->open(self::XMLLOCAL);
        $doc = new DOMDocument;
        $revision = [];

        while ($ercXmlReader->read() && $ercXmlReader->name !== 'ware');

        // now that we're at the right depth, hop to the next <ware/> until the end of the tree
        while ($ercXmlReader->name === 'ware') {

            //$node = new SimpleXMLElement($erc_xmlReader->readOuterXML());
            $node = simplexml_import_dom($doc->importNode($ercXmlReader->expand(), true));
            if ($node->sku->code == $sku) {

                $revision['request_url'] = $this->escape((string) $node->url);
                $revision['name'] = $this->escape((string) $node->title);
                $revision['price'] = $this->escape((string) $node->ercRrcPrice);
                $revision['attributes']['Производитель'] = $this->escape((string) $node->vendor);
                if($node->description){
                    $revision['description'] = $this->escape((string)$node->description);
                }
                foreach ($node->groups->group as $group) {

                    foreach ($group->items->item as $item) {

                        $revisionValue = [];
                        if ($item->values) {
                            if ($item->values->option) {
                                $revisionValue[] = $item->values->option->value . '(' . $item->values->option->help . ')';
                            }

                            foreach ($item->values->value as $value) {

                                $revisionValue[] = $value;
                            }
                        }

                        if ($item->value) {

                            $revisionValue[] = $item->value;
                        }
                        $revisionValue = implode(', ', $revisionValue);
                        $revision['attributes'][(string) $item->title] = $this->escape(html_entity_decode($revisionValue));
                    }
                }
                foreach ($node->images->src as $image) {
                    $revision['media'][] = $this->escape(html_entity_decode((string) $image));
                }
                break;
            }

            // go to next <ware />
            $ercXmlReader->next('ware');
        }
        return $revision;
    }

    protected function receivefile()
    {
        //check file
        if (file_exists(self::XMLLOCAL)) {

            //check last file change
            if (filemtime(self::XMLLOCAL) < (time()-60*60*24)) {

                //call funthion that recive and fixing xml from remote resource
                $this->filterContent();
            }
        } else {
            $this->filterContent();
        }

    }

    protected function compressContent($value)
    {
        $value = trim($value);
        $value = preg_replace("#[\n\r\t]#", "", $value);
        $value = preg_replace('#>[\s]+<#', '><', $value);
        $value = preg_replace('#[\s]+#u', ' ', $value);
        $value = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $value);

        return $value;
    }

    protected function filterContent()
    {
        //register of filter, that removing invalid chars
        stream_filter_register("removeInvalid", "removeInvalidCharFilter");
        $souce = fopen(self::XMLSRC, 'r');
        $target = fopen(self::XMLLOCAL, 'w');

        //appendind filter to the source stream
        stream_filter_append($souce, "removeInvalid");

        //reading the remote xml and save to the local xml via filtering
        stream_copy_to_stream($souce, $target);
        fclose($souce);
        fclose($target);
    }
}

class removeInvalidCharFilter extends php_user_filter
{
    protected static $pattern = '/([\x09\x0A\x0D\x20-\x7E]|[\xC2-\xDF][\x80-\xBF]|\xE0[\xA0-\xBF][\x80-\xBF]|[\xE1-\xEC\xEE\xEF][\x80-\xBF]{2}|\xED[\x80-\x9F][\x80-\xBF]|\xF0[\x90-\xBF][\x80-\xBF]{2}|[\xF1-\xF3][\x80-\xBF]{3}|\xF4[\x80-\x8F][\x80-\xBF]{2})|./x';

    public function filter($in, $out, &$consumed, $closing)
    {
        while ($bucket = stream_bucket_make_writeable($in)) {

            //remove the invalid chars in the stream via regular expresion
            $bucket->data = preg_replace(self::$pattern, '$1', $bucket->data);
            $consumed += $bucket->datalen;
            stream_bucket_append($out, $bucket);
        }
        return PSFS_PASS_ON;
    }
}

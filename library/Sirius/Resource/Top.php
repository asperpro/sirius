<?php

class Sirius_Resource_Top extends Sirius_Resource
{
	public function search()
	{
		$revision = array();
		$keyword = trim($this->getProduct()->sku);

		// Load product page
		$requestUrl = $this->getInterface()->url . '/product.php?id_product=' . $keyword;
		$response = $this->getResponse($requestUrl);

		if ($response && 200 == $response->getStatus()) {
			if ($content = $this->compressContent($response->getBody())) {

				$revision['request_url'] = $requestUrl;

				// Get revision weight value
				$revision['weight'] = 0;

				// Get revision price value
				$revision['price'] = 0;

				// Get revision name
				$revision['name'] = '';
				if (preg_match('/<div id="content_left">.*?<h1>(.*?)<\/h1>/', $content, $matches)) {
					$revision['name'] = $this->escape($matches[1]);
				}

				// Get revision category
				$revision['category'] = '';
				preg_match('/<ul id="breadcramb">(.*?)<\/ul>/', $content, $matches);
				if (count($matches) > 1) {
					$categoriesContent = $matches[1];
					if (preg_match_all('/<li><a.*?>(.*?)<\/a><\/li>/', $categoriesContent, $matches)) {
						$revision['category'] = $this->escape(implode('/', $matches[1]));
					}
				}

				// Get revision description value
				$revision['description'] = '';
				if (preg_match('/<div id="product_block_right">.*?<h3>Описание:<\/h3>(.*?)<\/div>/', $content, $matches)) {
					$revision['description'] = $this->escape($matches[1], true);
				}

				// Get revision attributes
				$revision['attributes'] = array();

				preg_match('/<ul id="character">(.*?)<\/ul>/', $content, $matches);
				if (count($matches) > 1) {
					$attributesContent = $matches[1];

					if (preg_match_all('/<li>(.*?):(.*?)<\/li>/', $attributesContent, $matches)) {
						$attributes = $matches[1];
						$values = $matches[2];
						foreach ($attributes as $key => $attribute) {
							$revision['attributes'][$this->formatAttributeName($attribute)] = $this->escape($values[$key]);
						}
					}
				}

				// Get revision images
				$revision['media'] = array();

				preg_match('/<div id="product_small_img">(.*?)<\/div>/', $content, $matches);
				if (count($matches) > 1) {
					$mediaContent = $matches[1];

					if (preg_match_all('/<a href="(.*?)".*?<\/a>/is', $mediaContent, $matches)) {
						if (!empty($matches[1])) {
							foreach ($matches[1] as $image) {
								$revision['media'][] = $image;
							}
						}
					}
				}
			}

		}

		$this->setRevision($revision);
	}
}
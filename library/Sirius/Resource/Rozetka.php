<?php

class Sirius_Resource_Rozetka extends Sirius_Resource
{
	public function search()
	{
		$revision = array();

		$keyword = '(' . $this->getProduct()->sku . ')';
		$keyword = urlencode($keyword);
		$requestUrl = $this->getInterface()->url . '/search/?section=/&text=' . $keyword;
		$response = $this->getCurlResponse($requestUrl);
		
		if ($response &&
			200 == $response->getStatus()) {
			if ($content = $this->compressContent($response->getBody())) {
				if (preg_match('/<div class="search-container">.*?<div id="block_with_search".*?<div class="g-i-list-middle-part"><div class="g-i-list-title">.*?<a.*?href="(.*?)".*?<\/a><\/div>/', $content, $matches)) {
					// Load product page
					$requestUrl = $matches[1];
					$response = $this->getResponse($requestUrl);
					$revision['request_url'] = $requestUrl;
					
					if ($response &&
						200 == $response->getStatus()) {							
						$content = $this->compressContent($response->getBody());

						// Get revision name
						$revision['name'] = '';
						if (preg_match('/<h1 class="detail-title" itemprop="name">(.*)<\/h1>/', $content, $matches)) {
							$revision['name'] = $this->escape($matches[1]);
						}

						// Get revision price value
						$revision['price'] = 0;
						if (preg_match('/<span itemprop="price"><meta itemprop="priceCurrency" content="UAH">(.*?)<\/span>/', $content, $matches)) {
							$revision['price'] = $this->escape($matches[1]);
						}

						// Get revision category and producer
						$revision['category'] = '';
						if (preg_match_all('/<li class="breadcrumbs-i".*?><a.*?><span class="breadcrumbs-title" itemprop="title">(.*?)<\/span>/', $content, $matches)) {
							$revision['category'] = array_slice($matches[1], 1, count($matches[1]) - 2);
							$revision['category'] = $this->escape(implode('/', $revision['category']));
							$producer = array_pop($matches[1]);
						}

						// Get revision description value
						$revision['description'] = '';
						if (preg_match('/<div class="b-rich-text text-description-content box-hide" id="short_text">(.*?)<\/div>/s', $content, $matches)) {
							$revision['description'] = $this->escape($matches[1], true);
						}

						// Get revision images
						$revision['media'] = array();
						if (preg_match_all('/<div class="detail-tab-i-img"><a href="(.*?)".*?<\/a><\/div>/is', $content, $matches)) {
							if (!empty($matches[1])) {
								foreach ($matches[1] as $image) {
									$revision['media'][] = $image;
								}
							}
						}

						// Get revision attributes
						$revision['attributes'] = array();
						$response = $this->getResponse($requestUrl . 'tab=characteristics/');
						if ($response &&
							200 == $response->getStatus()) {
							if ($content = $this->compressContent($response->getBody())) {

								if (preg_match_all('/<div class="pp-characteristics-tab-i"><dt.*?>(.*?)<\/dt><dd.*?>(.*?)<\/dd>/s', $content, $matches)) {

									$attributes = $matches[1];
									$values     = $matches[2];

									foreach ($attributes as $key => $attribute) {
										$revision['attributes'][$attribute] = $this->escape($values[$key]);
									}
								}

							}
						}

						if (isset($producer)) $revision['attributes']['Производитель'] = $producer;

					}
				}
			}
		}

		$this->setRevision($revision);
	}

	protected function compressContent($value)
	{
		$value = trim($value);
		//$value = preg_replace("#[\n\r\t]#", "", $value);
		//$value = preg_replace('#>[\s]+<#', '><', $value);
		//$value = preg_replace('#[\s]+#', ' ', $value);
		//$value = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $value);
		//$value = preg_replace('#<link(.*?)/>#is', '', $value);

		return $value;
	}
}
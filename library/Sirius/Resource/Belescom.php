<?php

class Sirius_Resource_Belescom extends Sirius_Resource
{
	const INDEX_URL = 'index.php?lang=ua';
	const ACCOUNT_USERNAME = 'sales@kingdomtoys.com.ua';
	const ACCOUNT_PASSWORD = 'kingdomtoys';
	
	public function search()
	{
		$revision = array();
		
		$this->_login();

		$keyword = iconv('utf-8', 'windows-1251', $this->getProduct()->name);
		$requestUrl = $this->getInterface()->url . '/' . self::INDEX_URL;
		// Find product by keyword
		$response = $this->getCurlResponse($requestUrl, 'post', array('find' => $keyword));
		// Get search results
		$response = $this->getCurlResponse($requestUrl . '&section=2');
		if ($response &&
			200 == $response->getStatus()) {
			if ($content = $response->getBody()) {
				if (preg_match('/<div class="goods_small_card full" id="row_.*?"><a href="(.*?)">/', $content, $matches)) {
					// Load product page
					$requestUrl = $this->getInterface()->url . '/' . $matches[1];
					$response = $this->getCurlResponse($requestUrl);
					$revision['request_url'] = $requestUrl;
					
					if ($response &&
						200 == $response->getStatus()) {							
						$content = $this->compressContent($response->getBody());
						// Get revision name
						$revision['name'] = $this->getProduct()->name;
						// Get revision price value
						$revision['price'] = 0;
						if (preg_match('/<div class="price">(.*?)<\/div>/', $content, $matches)) {
							$revision['price'] = $this->formatPrice($matches[1]);
						}
						// Get revision category
						$revision['category'] = '';
						/*if (preg_match('/<div class="breadcrumbs">(.*?)<\/div>/is', $content, $matches)) {
							if (preg_match_all('/<a href="\/catalog\/.*">(.*?)<\/a>/is', $matches[1], $matches)) {
								$revision['category'] = $this->escape(implode('/', $matches[1]));
							}
						}*/
						// Get revision description value
						$revision['description'] = '';
						if (preg_match('/<div class="content_text"><span style="color:#4b82a8;">(.*?)<\/div>/is', $content, $matches)) {
							$revision['description'] = $this->escape(iconv('windows-1251', 'utf-8', $matches[1]), true);
							$revision['description'] = trim(str_replace('Опис:', '', $revision['description']));
						}
						// Get revision images
						$revision['media'] = array();
						if (preg_match('/<div class="goods_card"><img src="(.*?)"*?>/is', $content, $matches)) {
							$revision['media'][] = $this->getInterface()->url . '/' . $matches[1];
						}
						
						// Get revision attributes
						$revision['attributes'] = array();
						if (preg_match('/<div class="material">(.*?)<\/div>/is', $content, $matches)) {
							$revision['attributes']['Материал'] = $this->escape(iconv('windows-1251', 'utf-8', $matches[1]), true);
						}
						if (preg_match('/<div class="brand"><a.*?title="(.*?)"/is', $content, $matches)) {
							$revision['attributes']['Производитель'] = $this->escape(iconv('windows-1251', 'utf-8', $matches[1]), true);
						}
					}
				}
			}
		}
		$this->setRevision($revision);
	}
	
	private function _login()
	{
		$loginUrl = $this->getInterface()->url . '/' . self::INDEX_URL;
		$data = array(
			'login' => iconv('utf-8', 'windows-1251', self::ACCOUNT_USERNAME),
			'pswd' => iconv('utf-8', 'windows-1251', self::ACCOUNT_PASSWORD),
		);
		$response = $this->getCurlResponse($loginUrl, 'post', $data);
		$headers = $response->getHeaders();
		$this->setPhpSessionId($headers['Set-cookie'][0]);
	}
	
	public function formatPrice($value)
	{
		$value = parent::formatPrice($value);
		$rules = array(
			array(
				'percent' => 40,
				's_value' => 0,
				'f_value' => 100
			),
			array(
				'percent' => 35,
				's_value' => 100,
				'f_value' => 200
			),
			array(
				'percent' => 30,
				's_value' => 200,
				'f_value' => 300
			),
			array(
				'percent' => 25,
				's_value' => 300,
				'f_value' => 400
			),
			array(
				'percent' => 20,
				's_value' => 400,
				'f_value' => 800
			),
			array(
				'percent' => 25,
				's_value' => 800,
				'f_value' => 1100
			),
			array(
				'percent' => 18,
				's_value' => 1100,
				'f_value' => 2500
			),
			array(
				'percent' => 15,
				's_value' => 2500,
				'f_value' => 3500
			),
			array(
				'percent' => 8,
				's_value' => 3500,
				'f_value' => 1000000
			),
			
		);
		foreach ($rules as $rule) {
			if ($value >= $rule['s_value'] &&
				$value < $rule['f_value']) {
				$value += ($value*($rule['percent']/100));
				return $value;
			}
		}
		
		return $value;
	}
}
<?php

class Sirius_Resource_Bville extends Sirius_Resource
{
	public function search()
	{
		$revision = array();
		
		$keyword = trim($this->getProduct()->sku);
		$keyword = urlencode($keyword);
		$requestUrl = $this->getInterface()->url . '/ui-autocomplete.php?limit=11&cID=5&term=' . $keyword;
		$response = $this->getResponse($requestUrl);
		
		if ($response &&
			200 == $response->getStatus()) {
			if ($content = $this->compressContent($response->getBody())) {
				$content = json_decode($response->getBody(), 1);
				if (isset($content[0]) && $content[0]['product_code'] == $keyword) {
					// Load product page
					$requestUrl = $this->getInterface()->url . $content[0]['url'];
					$response = $this->getResponse($requestUrl);
					$revision['request_url'] = $requestUrl;
					
					if ($response &&
						200 == $response->getStatus()) {							
						$content = $this->compressContent($response->getBody());
						// Get revision name
						$revision['name'] = '';
						if (preg_match_all('/<input name=\"message_subject\" type=\"hidden\" value=\"(.*?)\">/is', $content, $matches)) {
							$revision['name'] = $this->escape($matches[1][0]);
						}
						
						// Get revision price value
						$revision['price'] = '';
						
						// Get revision weight value
						$revision['weight'] = 0;
						
						// Get revision category
						$revision['category'] = '';
						if (preg_match('/<td class=\"cbt\" align=\"left\" style=\"padding-left: 11px;\">(.*?)<\/td>/is', $content, $matches)) {
							if (preg_match_all('/<a href=\"gamecat_[0-9]*.html\">(.*?)<\/a>/is', $matches[1], $links)) {
								$links[1] = array_unique($links[1]);
								if (3 == count($links[1])) array_pop($links[1]);
								$revision['category'] = $this->escape(implode('/', $links[1]));
							}	
						}
						
						// Get revision description value
						$revision['description'] = '';
						if (preg_match('/<div class="textdesc">(.*?)<\/div>/s', $content, $matches)) {
							$revision['description'] = $this->escape($matches[1], true);
						}
						// Get revision images
						$revision['media'] = array();
						if (preg_match_all('/<a href=\"pictures\/(.*?)\" media=\"gallery\" class=\"pirobox_gall\".*?>/', $content, $matches)) {
							if (!empty($matches[1])) {
								foreach ($matches[1] as $image) {
									$revision['media'][] = $this->getInterface()->url . '/pictures/' . $image;
								}
							}
						} elseif (preg_match('/<div class="bigimg-wrap" data-img-desc="top">.*?<imgid.*?src="(.*)"alt=".*">/is', $content, $matches)) {
							if (!empty($matches[1])) {
								$revision['media'][] = $this->getInterface()->url . $matches[1];	
							}
						}
						
						// Get revision attributes
						$revision['attributes'] = array();
						if (preg_match_all('/>\s(.+?):\s<span class=\"titletext\">(.*?)<\/span>/is', $content, $matches)) {
							$attributes = $matches[1];
							$attributes[0] = "Бренд";
							$values = $matches[2];
							foreach ($attributes as $key => $attribute) {
								$attribute = $this->escape($attribute, true);
								$attribute = str_replace('?', '', $attribute);
								$revision['attributes'][$attribute] = $this->escape($values[$key]);	
							}
						}
					}
				}
			}
		}

		$this->setRevision($revision);
	}
	
	protected function compressContent($value)
	{
		$value = trim($value);
		$value = iconv('cp1251', 'utf-8', $value);
		$value = preg_replace("#[\n\r\t]#", "", $value);
	    $value = preg_replace('#>[\s]+<#', '><', $value);
	    $value = preg_replace('#[\s]+#', ' ', $value);
		$value = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $value);
		
		return $value;
	}
}
<?php

class Sirius_Resource_Dreammakers extends Sirius_Resource
{
	public function search()
	{
		$revision = array();
		$keyword = urlencode(trim($this->getProduct()->sku));

		$requestUrl = $this->getInterface()->url . '/?lan=rus&page=company&searching=' . $keyword;
		$response = $this->getResponse($requestUrl);
		if ($response && 200 == $response->getStatus()) {
			if ($content = $this->compressContent($response->getBody())) {
				$content = iconv('windows-1251', 'utf-8', $content);

				if (preg_match("/<div class='zzz'>.*?<div class='tovaro fl'>.*?<a href='(.*?)'>.*?<\/a>/", $content, $matches)) {

					// Load product page
					$requestUrl = $this->getInterface()->url . $matches[1];
					$response = $this->getResponse($requestUrl);
					$revision['request_url'] = $requestUrl;

					if ($response && 200 == $response->getStatus()) {
						$content = $this->compressContent($response->getBody());
						$content = iconv('windows-1251', 'utf-8', $content);

						// Get revision weight value
						$revision['weight'] = 0;

						// Get revision price value
						$revision['price'] = 0;

						// Get revision name
						$revision['name'] = '';
						if (preg_match("/<div class='zzz'>.*?<div class='fl kolo'>.*?<h2>(.*?)<\/h2>/", $content, $matches)) {
							$revision['name'] = $this->escape($matches[1]);
						}

						// Get revision description value
						$revision['description'] = '';
						if (preg_match('/<div class=\'fl kolo\'>(.*?)<\/div>/', $content, $matches)) {
							$revision['description'] = $this->escape($matches[1], true);
						}

						// Get revision attributes
						$sku = '';
						$revision['attributes'] = array();
						preg_match("/<div class='fl esto'>.*?<table.*?>(.*?)<\/table>/", $content, $matches);
						if (count($matches) > 1) {
							$attributesContent = $matches[1];

							if (preg_match_all("/<tr><td.*?>(.*?)<\/td><td.*?>(.*?)<\/td><\/tr>/is", $attributesContent, $matches)) {
								$attributes = $matches[1];
								$values = $matches[2];
								foreach ($attributes as $key => $attribute) {
									$revision['attributes'][$this->formatAttributeName($attribute)] = $this->escape($values[$key]);
								}
								$sku = (isset($revision['attributes']['Артикул'])) ? $revision['attributes']['Артикул'] : '';
								unset($revision['attributes']['Артикул']);
							}
						}

						// Get revision producer
						if (isset($revision['attributes']['Бренд'])) {
							preg_match('/Все товары "(.*?)"/', $revision['attributes']['Бренд'], $matches);
							$revision['attributes']['Производитель'] = $this->escape($matches[1]);
							unset($revision['attributes']['Бренд']);
						}

						// Get revision category
						$revision['category'] = '';
						if (isset($revision['attributes']['Категория'])) {
							preg_match('/Все товары "(.*?)"/', $revision['attributes']['Категория'], $matches);
							$revision['category'] = $this->escape($matches[1]);
							unset($revision['attributes']['Категория']);
						}

						// Get revision images
						$revision['media'] = array();
						if (preg_match_all("/<a class=zoom rel=group href='(.*?)' title=''><img/is", $content, $matches)) {
							if (!empty($matches[1])) {
								foreach ($matches[1] as $image) {
									$revision['media'][] = $this->getInterface()->url . $image;
								}
							}
						}
						if ($sku != $keyword) {
							$revision = array();
						}
					}
				}
			}

		}
		$this->setRevision($revision);
	}
}
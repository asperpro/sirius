<?php

class Sirius_Resource_Distributions extends Sirius_Resource
{
	public function search(){

        $revision = array();
        
		$keyword = trim($this->getProduct()->name);
		$keyword = urlencode($keyword);
		$requestUrl = $this->getInterface()->url . '/search?utf8=%E2%9C%93&search=' . $keyword;
        $response = $this->getResponse($requestUrl);
		
		////Load search page
		if($response && 200 == $response->getStatus()){

			if($content = $this->compressContent($response->getBody())){

				$requestUrl = '';
				preg_match_all("/<a class='product-title' href='(.*?)'>/",$content,$matches);

				if(!empty($matches[1])){

					$revisionFound = false;
					foreach($matches[1] as $_match){
						
						if($revisionFound) continue;
						$requestUrl = $this->getInterface()->url . $_match;
						if($requestUrl){

							//Load product page
							$response = $this->getResponse($requestUrl);
							if($response && 200 == $response->getStatus()){

								if($content = $this->compressContent($response->getBody())){

									$revision['request_url'] = $requestUrl;
									//Get revision name
									$revision['name'] = '';
									if(preg_match("/<div class='large-9 small-12 columns'><h1>(.*?)<\/h1><div/",$content,$matches)){
										$revision['name'] = $this->escape(html_entity_decode($matches[1]));
									}

									// Get revision price value
									$revision['price'] = '';
									if(preg_match("/<div class='price retail'><span>(.*?)<\/span>/",$content,$matches)){
										$revision['price'] = $this->formatPrice($this->escape($matches[1]));
									}


									//Get revision category
									$revision['category'] = '';
									if(preg_match("/xs-hide-current'>(.*?)<\/ul>/",$content,$matches)){

										preg_match_all("/<li><a.*?\">(.*?)<\/a><\/li>/",$matches[1],$matches);
										if(!empty($matches[1])){

											array_shift($matches[1]);
    				                		$revision['category'] = implode('/', $matches[1]);
										}
									}

									// Get revision description value
									$revision['description'] = '';
									if(preg_match("/<div class='desc-mini row without-dash'><p>(.*?)<\/p>/",$content,$matches)){
										$revision['description'] = trim($this->escape($matches[1]), '<br>');
									}
									
									// Get revision images
									$revision['media'] = array();
									if(preg_match_all("/<a class='thumbnail' href='(.*?)'>/s",$content,$matches)){
										
										if(!empty($matches[1])){

											foreach($matches[1] as $image){

												$revision['media'][] = $this->getInterface()->url . $image;
											}
										}
									}elseif(preg_match("/id='main-picture'>\s*<img alt=\".*?\"\s+src=\"(.*?)\".*?'>/s",$content,$matches)){
										$revision['media'][] = $this->getInterface()->url . $matches[1];
									}
									
									// Get revision attributes
									$revision['attributes'] = array();
									if(preg_match("/<ul class=\"short_props\">\s*(.*?)\s*<\/ul>/s",$content,$matches)){

										if(preg_match_all("/<li>(.*?):(.*?)<\/li>/s", $matches[1], $matches)){

											$attributes = $matches[1];
    				                		$values = $matches[2];
											foreach($attributes as $key => $attribute){
												
												$attribute = $this->escape($attribute, true);
												$revision['attributes'][$attribute] = $this->escape(html_entity_decode($values[$key]));
											}
										}
									}
									if(preg_match("/brand'>\s*<a .*?><img alt=\"(.*?)\" src/s",$content,$matches)){
										$revision['attributes']['Производитель'] = trim($this->escape($matches[1]));
									}
									
									// Get additional features
									if(preg_match("/id='descriptions-tab'>\s*<div.*?'>(.*?)<ul/s",$content,$matches)){
										$revision['description'] .= trim($this->escape($matches[1]), '<br>');
									}
									if(preg_match("/<ul class=\"features\">\s*(.*?)\s*<\/ul>.*?<h5>(.*?)<\/h5>.*?<ul class=\"kit\">\s*(.*?)\s*<\/ul>.*?<h5>(.*?)<\/h5>.*?<ul class=\"addons\">\s*(.*?)\s*<\/ul>/s",$content,$matches)){
										$revision['description'] .= '. ' . $this->escape(html_entity_decode($matches[1])) .
																	'. ' . $this->escape(html_entity_decode($matches[2])) .
																	': ' . $this->escape(html_entity_decode($matches[3])) .
																	'. ' . $this->escape(html_entity_decode($matches[4])) .
																	': ' . $this->escape(html_entity_decode($matches[5]));
									}
								}
							}
						}
					}
				}
			}
		}

		$this->setRevision($revision);
    }

    protected function compressContent($value)
	{
		$value = trim($value);
		$value = preg_replace("#[\n\r\t]#", "", $value);
	    	$value = preg_replace('#>[\s]+<#', '><', $value);
	    	$value = preg_replace('#[\s]+#u', ' ', $value);
		$value = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $value);
		
		return $value;
	}
}
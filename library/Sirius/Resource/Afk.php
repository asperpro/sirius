<?php

class Sirius_Resource_Afk extends Sirius_Resource
{
	public function search()
	{
		$revision = array();
		$keyword = trim($this->getProduct()->sku);

		// Load product page
		$requestUrl = $this->getInterface()->url . '/catalog?text=' . $keyword;
		$response = $this->getResponse($requestUrl);

		if ($response && 200 == $response->getStatus()) {
			if ($content = $this->compressContent($response->getBody())) {

				if (preg_match('/<div class="catalog_group">.*?<div class="catalog_item_title.*?"><a href="(.*?)".*?<\/a>/', $content, $matches)) {

					// Load product page
					$requestUrl = $this->getInterface()->url . $matches[1];
					$response = $this->getResponse($requestUrl);

					if ($response && 200 == $response->getStatus()) {
						$content = $this->compressContent($response->getBody());

						$revision['request_url'] = $requestUrl;

						// Get revision name
						$revision['name'] = '';
						if (preg_match('/<div id="divCatalog">.*?<h1 itemprop="name" >(.*?)<\/h1>/', $content, $matches)) {
							$revision['name'] = $this->escape($matches[1]);
						}

						// Get revision price value
						$revision['price'] = 0;
						if (preg_match('/<div itemprop="price".*?>(.*?)#8372<\/div>/', $content, $matches)) {
							$revision['price'] = $this->formatPrice($this->escape($matches[1]));
						}

						// Get revision description value
						$revision['description'] = '';
						if (preg_match('/<div class="description_text"><div itemprop="description".*?>(.*?)<\/div>/', $content, $matches)) {
							$revision['description'] = $this->escape($matches[1], true);
						}

						// Get revision category
						$revision['category'] = '';

						preg_match('/<ul.*?class="breadcrumbs">(.*?)<\/div>/', $content, $matches);
						if (count($matches) > 1) {
							$categoriesContent = $matches[1];
							if (preg_match_all('/<a.*?>(.*?)<\/a>/', $categoriesContent, $matches)) {
								array_shift($matches[1]);
								$revision['category'] = $this->escape(implode('/', $matches[1]));
							}
						}

						// Get revision images
						$revision['media'] = array();

						preg_match('/<div.*?class="descr_gallery_div"><ul class="ul_gal">(.*?)<\/ul><\/div>/', $content, $matches);
						if (count($matches) > 1) {
							$mediaContent = $matches[1];

							if (preg_match_all('/<li>.*?<img.*?data-large="(.*?)".*?<\/li>/is', $mediaContent, $matches)) {
								if (!empty($matches[1])) {
									foreach ($matches[1] as $image) {
										$revision['media'][] =  str_replace('products/', 'products/large/', $this->getInterface()->url . $image);
									}
								}
							}
						}

						// Get revision attributes
						$revision['attributes'] = array();

						preg_match('/<div class="description_text"><h6 id="5">Характеристики<\/h6>(.*?)<\/div>/', $content, $matches);
						if (count($matches) > 1) {
							$attributesContent = $matches[1];

							if (preg_match_all('/<tr><td.*?>(.*?)<\/td><td>(.*?)<\/td><\/tr>/', $attributesContent, $matches)) {
								$attributes = $matches[1];
								$values = $matches[2];
								foreach ($attributes as $key => $attribute) {
									if (($this->formatAttributeName($attribute)) == '') continue;
									$revision['attributes'][$this->formatAttributeName($attribute)] = $this->escape($values[$key]);
								}
							}
						}

						preg_match('/<div class="description_text"><h6 id="5">Характеристики<\/h6>.*?<div class="description_text_column06.*?>(.*?)<\/div>/', $content, $matches);
						if (count($matches) > 1) {
							$attributesContent = $matches[1];

							if (preg_match_all('/<tr><td.*?>(.*?)<\/td><td>(.*?)<\/td><\/tr>/', $attributesContent, $matches)) {
								$attributes = $matches[1];
								$values = $matches[2];
								foreach ($attributes as $key => $attribute) {
									if (($this->formatAttributeName($attribute)) == '') continue;
									$revision['attributes'][$this->formatAttributeName($attribute)] = $this->escape($values[$key]);
								}
							}
						}

						unset ($revision['attributes']['Высота, см.']);
						unset ($revision['attributes']['Глубина, см.']);
						unset ($revision['attributes']['Ширина, см.']);

						// Get revision weight value
						$revision['weight'] = 0;
						if (isset($revision['attributes']['Вес, кг'])) {
							$revision['weight'] = $this->formatWeight($revision['attributes']['Вес, кг']);
							unset($revision['attributes']['Вес, кг']);
						}

					}
				}
			}

		}

		$this->setRevision($revision);
	}
}
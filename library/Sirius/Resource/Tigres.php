<?php

class Sirius_Resource_Tigres extends Sirius_Resource
{
	public function search()
	{
		$revision = array();
		$keyword = urlencode(trim($this->getProduct()->sku));
		
		// Load product page
		$requestUrl = $this->getInterface()->url . '/catalogue/?search=' . $keyword;
		$response = $this->getResponse($requestUrl);
		if ($response && 200 == $response->getStatus()) {
			if ($content = $this->compressContent($response->getBody())) {
				if (preg_match('/<section id="items-list">.*?<li>.*?<h4 class="title"><a.*?href="(.*?)".*?<\/a><\/h4>/', $content, $matches)) {

					// Load product page
					$requestUrl = $this->getInterface()->url . $matches[1];
					$response = $this->getResponse($requestUrl);
					if ($response && 200 == $response->getStatus()) {
						$content = $this->compressContent($response->getBody());

						$revision['request_url'] = $requestUrl;

						// Get revision weight value
						$revision['weight'] = 0;

						// Get revision name
						$revision['name'] = '';
						if (preg_match('/<h1 itemprop="name">(.*?)<\/h1>/', $content, $matches)) {
							$revision['name'] = $this->escape($matches[1]);
						}

						// Get revision category
						$revision['category'] = '';
						preg_match('/<ul id="breadcrumbs">(.*?)<\/ul>/', $content, $matches);
						if (count($matches) > 1) {
							$categoriesContent = $matches[1];
							if (preg_match_all('/<li.*?><a.*?>(.*?)<\/a><\/li>/', $categoriesContent, $matches)) {
								array_shift($matches[1]);
								array_shift($matches[1]);
								$revision['category'] = $this->escape(implode('/', $matches[1]));
							}
						}

						// Get revision price value
						$revision['price'] = 0;
						if (preg_match('/<em.*?id="price-value".*?>(.*?)<\/em>/', $content, $matches)) {
							$revision['price'] = $this->formatPrice($this->escape($matches[1]));
						}

						// Get revision description value
						$revision['description'] = '';
						if (preg_match('/<div style="height: auto;" id="node-info" itemprop="description">(.*?)<\/div>/', $content, $matches)) {
							$tagPosition = strpos($matches[1], 'Теги:');
							if (false !== $tagPosition) {
								$matches[1] = substr($matches[1], 0, $tagPosition);
							}
							$revision['description'] = $this->escape($matches[1], true);
						}

						// Get revision attributes
						$revision['attributes'] = array();

						preg_match('/<div class="description catalog-node"><dl>(.*?)<\/dl>/', $content, $matches);
						if (count($matches) > 1) {
							$attributesContent = $matches[1];

							if (preg_match_all('/<dt>(.*?)<\/dt><dd>(.*?)<\/dd>/', $attributesContent, $matches)) {
								$attributes = $matches[1];
								$values = $matches[2];
								foreach ($attributes as $key => $attribute) {
									if (($this->formatAttributeName($attribute)) == '') continue;
									$revision['attributes'][$this->formatAttributeName($attribute)] = $this->escape($values[$key]);
								}
							}
						}
						// Get revision images
						$revision['media'] = array();
						if (preg_match_all('/<li class="photo-li"><a href="(.*?)" rel="lightbox".*?>.*?<\/a>.*?<\/li>/is', $content, $matches)) {
							foreach ($matches[1] as $media) {
								$revision['media'][] = $this->getInterface()->url . str_replace('/big-', '/original-', $media);
							}
						} elseif (preg_match('/<img itemprop="image" src="(.*?)" alt=".*">/is', $content, $matches)) {
							$revision['media'][] = $this->getInterface()->url . str_replace('/item-', '/original-', $matches[1]);
						}
					}
				}
			}

		}
		$this->setRevision($revision);
	}

	protected function compressContent($value)
	{
		$value = trim($value);
		$value = preg_replace("#[\n\r\t]#", "", $value);
		$value = preg_replace('#>[\s]+<#', '><', $value);
		$value = preg_replace('#[\s]+#', ' ', $value);
		$value = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $value);
		//$value = preg_replace('#<header(.*?)>(.*?)</header>#is', '', $value);
		$value = preg_replace('#<link(.*?)/>#is', '', $value);

		return $value;
	}
}
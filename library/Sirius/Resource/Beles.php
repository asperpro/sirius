﻿<?php

class Sirius_Resource_Beles extends Sirius_Resource
{
	const LOGIN_URL = 'default/login';
	const ACCOUNT_USERNAME = 'kingdomtoys';
	const ACCOUNT_PASSWORD = 'kingdomtoys';
	const ACCOUNT_CITY = 'Харьков';
	
	public function search()
	{
		$revision = array();
		
		$this->_login();

		$keyword = $this->getProduct()->sku;
		$keyword = urlencode($keyword);
		$requestUrl = $this->getInterface()->url . '/catalog/0/1/12/name/1?q=' . $keyword;
		$response = $this->getCurlResponse($requestUrl);
		
		if ($response &&
			200 == $response->getStatus()) {
			if ($content = $this->compressContent($response->getBody())) {
				if (preg_match('/<ul class="products">.*?<li>.*?<a class="name" href="(.*?)">.*<\/a>.*?<\/li>.*?<\/ul>/', $content, $matches)) {
					// Load product page
					$requestUrl = $this->getInterface()->url . $matches[1];
					$response = $this->getCurlResponse($requestUrl);
					$revision['request_url'] = $requestUrl;
					
					if ($response &&
						200 == $response->getStatus()) {							
						$content = $this->compressContent($response->getBody());
						
						// Get revision name
						$revision['name'] = '';
						if (preg_match('/<h1>(.*)<\/h1>/', $content, $matches)) {
							$revision['name'] = $this->escape($matches[1]);
						}
						
						// Get revision price value
						$revision['price'] = 0;
						if (preg_match('/<span class="price">(.*?)<\/span>/', $content, $matches)) {
							$revision['price'] = $this->escape($matches[1], true);
						}
						
						// Get revision category
						$revision['category'] = '';
						if (preg_match('/<div class="breadcrumbs">(.*?)<\/div>/is', $content, $matches)) {
							if (preg_match_all('/<a href="\/catalog\/.*">(.*?)<\/a>/is', $matches[1], $matches)) {
								$revision['category'] = $this->escape(implode('/', $matches[1]));
							}
						}
						// Get revision description value
						$revision['description'] = '';
						if (preg_match('/<\/style><\/head><body><p>(.*)<\/p><\/body>/is', $content, $matches)) {
							$revision['description'] = $this->escape($matches[1], true);
						}
						// Get revision images
						$revision['media'] = array();
						if (preg_match('/<a class="nyroModal" href="(.*)">zoom<\/a>/is', $content, $matches)) {
							$revision['media'][] = $this->getInterface()->url . $matches[1];
						}
						
						// Get revision attributes
						$revision['attributes'] = array();
						if (preg_match_all('/<dt>(.*?)<\/dt><dd>(.*?)<\/dd>/s', $content, $matches)) {
							$attributes = $matches[1];
							$values = $matches[2];
							foreach ($attributes as $key => $attribute) {
								$attribute = $this->escape($attribute, true);
								$attribute = str_replace(':', '', $attribute);
								$revision['attributes'][$attribute] = $this->escape($values[$key]);	
							}
							unset($revision['attributes']['Код']);
						}
					}
				}
			}
		}
		
		$this->setRevision($revision);
	}
	
	private function _login()
	{
		$loginUrl = $this->getInterface()->url . '/' . self::LOGIN_URL;
		$data = array(
			'fdata[username]' => self::ACCOUNT_USERNAME,
			'fdata[password]' => self::ACCOUNT_PASSWORD,
			'fdata[city]' => self::ACCOUNT_CITY,
			'fdata[_csrf_token]' => $this->_getToken(),
		);
		$this->getCurlResponse($loginUrl, 'post', $data);
	}
	
	private function _getToken()
	{
		$loginUrl = $this->getInterface()->url . '/' . self::LOGIN_URL;
		if ($response = $this->getCurlResponse($loginUrl)) {
			if (200 == $response->getStatus()) {
				$content = $response->getBody();
				$content = $this->compressContent($content);
				if (preg_match('/<input type="hidden" name="fdata\[_csrf_token\]" value="(.*)" id="fdata__csrf_token" \/>/is', $content, $match)) {
					return trim($match[1]);
				}
			}
		}
		
		return null;
	}
	
	public function formatPrice($value)
	{
		$value = parent::formatPrice($value);
		$rules = array(
			array(
				'percent' => 40,
				's_value' => 0,
				'f_value' => 100
			),
			array(
				'percent' => 30,
				's_value' => 100,
				'f_value' => 200
			),
			array(
				'percent' => 25,
				's_value' => 200,
				'f_value' => 300
			),
			array(
				'percent' => 20,
				's_value' => 300,
				'f_value' => 400
			),
			array(
				'percent' => 20,
				's_value' => 400,
				'f_value' => 800
			),
			array(
				'percent' => 20,
				's_value' => 800,
				'f_value' => 1100
			),
			array(
				'percent' => 18,
				's_value' => 1100,
				'f_value' => 2500
			),
			array(
				'percent' => 15,
				's_value' => 2500,
				'f_value' => 3500
			),
			array(
				'percent' => 8,
				's_value' => 3500,
				'f_value' => 1000000
			),
			
		);
		foreach ($rules as $rule) {
			if ($value >= $rule['s_value'] &&
				$value < $rule['f_value']) {
				$value += ($value*($rule['percent']/100));
				return $value;
			}
		}
		
		return $value;
	}
}
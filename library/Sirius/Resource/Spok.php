<?php

class Sirius_Resource_Spok extends Sirius_Resource
{
	public function search()
	{
		$revision = array();
		$maxCoincidence = 0;

		$keyword = $this->getProduct()->sku;
		$keyword = urlencode($keyword);
		$requestUrl = $this->getInterface()->url . '/search/?text=' . $keyword;
		$response = $this->getResponse($requestUrl);
		if ($response &&
			200 == $response->getStatus()) {
			if ($content = $this->compressContent($response->getBody())) {
				if (preg_match_all('/<header class="g-i-title"><a href="(.*?)">(.*?)<\/a><\/header>/', $content, $matches)) {

					$maxElementLink = $matches[1][0];
					foreach ($matches[2] as $key => $term) {
						$coincidence = $this->_checkTitleIdentity($term, $this->getProduct()->name . ' ' . $this->getProduct()->sku);
						if ($maxCoincidence < $coincidence) {
							$maxCoincidence = $coincidence;
							$maxElementLink = $matches[1][$key];
						}
					}

					// Load product page
					$requestUrl = $maxElementLink;
					$response = $this->getResponse($requestUrl);
					$revision['request_url'] = $requestUrl;
					
					if ($response &&
						200 == $response->getStatus()) {
						$content = $this->compressContent($response->getBody());
					}
				} else {
					$revision['request_url'] = $requestUrl;
				}

				// Get revision name
				$revision['name'] = '';
				if (preg_match('/<div class="pp-page-title".*?<span itemprop="name">(.*?)<\/span>.*?<\/div>/', $content, $matches)) {
					$revision['name'] = $this->escape($matches[1]);
				}

				// Get revision price value
				$revision['price'] = 0;
				if (preg_match('/<div itemprop="price" class=".*?">(.*?)<\/div>/', $content, $matches)) {
					$price = $this->formatPrice($matches[1]);
					$revision['price'] = $this->escape($price);
				}

				// Get revision category and producer
				$revision['category'] = '';
				if (preg_match_all('/<li class="breadcrumbs-item".*?><a.*?><span itemprop="title">(.*?)<\/span>/', $content, $matches)) {
					$revision['category'] = array_slice($matches[1], 1, count($matches[1]) - 2);
					$revision['category'] = $this->escape(implode('/', $revision['category']));
					$producer = array_pop($matches[1]);
				}

				// Get revision weight value
				$revision['weight'] = 0;
				if (preg_match('/<table class="pp-characteristics-table">.*?<tr><th>Вес: <\/th><td>(.*) кг<\/td><\/tr>/s', $content, $matches)) {
					$revision['weight'] = $this->formatWeight($matches[1]);
				}

				// Get revision images
				$revision['media'] = array();
				if (preg_match_all('/<div class="pp-image-preview-i" name="image_small"><a href="(.*?)".*?<\/a><\/div>/is', $content, $matches)) {
					if (!empty($matches[1])) {
						foreach ($matches[1] as $image) {
							$revision['media'][] = $image;
						}
					}
				}
				if (preg_match_all('/<div id="image_large"><div class=""><a href="(.*?)".*?<\/a><\/div>/is', $content, $matches)) {
					if (!empty($matches[1])) {
						foreach ($matches[1] as $image) {
							$revision['media'][] = $image;
						}
					}
				}

				// Get revision description value
				$revision['description'] = '';

				if (preg_match('/<p itemprop="description">(.*?)<\/p>/s', $content, $matches)) {
					$revision['description'] .= $this->escape($matches[1], true);
				}
				if (preg_match('/<div class="grid-box desc-in-all"><h2.*?<\/h2>(.*?)<\/div>/s', $content, $matches)) {
					$revision['description'] .= $this->escape($matches[1], true);
				}

				// Get revision attributes

				$revision['attributes'] = array();
				preg_match('/<table class="pp-characteristics-table">(.*?)<\/table>/', $content, $matches);
				if (count($matches) > 1) {
					$attributesContent = $matches[1];

					if (preg_match_all('/<tr><th>(.*?)<\/th><td>(.*?)<\/td><\/tr>/s', $attributesContent, $matches)) {

						$attributes = $matches[1];
						$values     = $matches[2];

						foreach ($attributes as $key => $attribute) {
							$revision['attributes'][$this->formatAttributeName($attribute)] = $this->escape($values[$key]);
						}
					}
				}

				if (isset($producer)) $revision['attributes']['Производитель'] = $producer;
			}


		}
		if (0 == $maxCoincidence || !$this->checkIdentity($revision['name'], '(' . $keyword . ')')) $revision = array();
		$this->setRevision($revision);
	}

	protected function compressContent($value)
	{
		$value = trim($value);
		$value = preg_replace("#[\n\r\t]#", "", $value);
		$value = preg_replace('#>[\s]+<#', '><', $value);
		$value = preg_replace('#[\s]+#', ' ', $value);
		$value = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $value);
		$value = preg_replace('#<link(.*?)/>#is', '', $value);

		return $value;
	}

	protected function escape($value, $allowableTags = null)
	{
		$value = parent::escape($value, $allowableTags);
		$value = str_replace('Читать описание полностью', '', $value);

		return $value;
	}
}
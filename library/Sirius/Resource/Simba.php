<?php

class Sirius_Resource_Simba extends Sirius_Resource
{
	public function search()
	{
		$revision = array();
		$keyword = trim(urlencode($this->getProduct()->sku));

		// Load product page
		$requestUrl = $this->getInterface()->url . '/search.shtml?s=' . $keyword;
		$response = $this->getResponse($requestUrl);

		if ($response && 200 == $response->getStatus()) {
			if ($content = $this->compressContent($response->getBody())) {

				if (preg_match('/<div id="content_main">.*?<div class="search_uri"><a href="(.*?)".*?<\/a>/', $content, $matches)) {

					// Load product page
					$requestUrl = $this->getInterface()->url . $matches[1];
					$response = $this->getResponse($requestUrl);

					if ($response && 200 == $response->getStatus()) {
						$content = $this->compressContent($response->getBody());

						$revision['request_url'] = $requestUrl;

						// Get revision name
						$revision['name'] = '';
						if (preg_match('/<table id="table_main_content">.*?<div class="product_box_title_text".*?<strong>(.*?)<\/strong>/', $content, $matches)) {
							$revision['name'] = $this->escape($matches[1]);
						}

						// Get revision price value
						$revision['price'] = 0;

						// Get revision description value
						$revision['description'] = '';
						if (preg_match('/<table id="table_main_content">.*?<strong>Описание<\/strong>.*?<p>(.*?)<\/p>/', $content, $matches)) {
							$revision['description'] = $this->escape($matches[1], true);
						}

						// Get revision category
						$revision['category'] = '';

						preg_match('/<span id="navigation_breadcrumb_spacer">(.*?)<\/span>/', $content, $matches);
						if (count($matches) > 1) {
							$categoriesContent = $matches[1];
							if (preg_match_all('/<a.*?>(.*?)<\/a>/', $categoriesContent, $matches)) {
								array_shift($matches[1]);
								array_shift($matches[1]);
								array_pop($matches[1]);
								$revision['category'] = $this->escape(implode('/', $matches[1]));
							}
						}

						// Get revision images
						$revision['media'] = array();
						if (preg_match('/<img id="product_main_image" src=".*?p\=(.*?)\.jpg/', $content, $matches)) {
							$revision['media'][] = $this->getInterface()->url . $this->escape($matches[1], true) . '.jpg';
						}

						// Get revision attributes
						$revision['attributes'] = array();

						preg_match('/<div style="margin-left: 13px; margin-right: 35px;">.*?<strong>Описание<\/strong>.*?<br \/>(.*?)<\/table>/', $content, $matches);
						if (count($matches) > 1) {
							$attributesContent = $matches[1];

							if (preg_match_all('/<div><strong>(.*?)<\/strong>:&nbsp;&nbsp;(.*?)<\/div>/', $attributesContent, $matches)) {
								$attributes = $matches[1];
								$values = $matches[2];
								foreach ($attributes as $key => $attribute) {
									if (($this->formatAttributeName($attribute)) == '') continue;
									$revision['attributes'][$this->formatAttributeName($attribute)] = $this->escape($values[$key]);
								}
							}
						}

						// Get revision weight value
						$revision['weight'] = 0;
						if (isset($revision['attributes']['Вес:'])) {
							$revision['weight'] = $this->formatWeight($revision['attributes']['Вес:']);
							unset($revision['attributes']['Вес:']);
						}

						// Get revision producer
						if (preg_match('/<tr id="row_main_content">.*?<div class="secondary_navigation_cell_active"><a.*?>(.*?)<\/a><\/div>/', $content, $matches)) {
							$revision['attributes']['Производитель'] = $this->escape($matches[1]);
						}

					}
				}
			}

		}
		
		$this->setRevision($revision);
	}
}
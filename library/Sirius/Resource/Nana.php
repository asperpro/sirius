<?php

class Sirius_Resource_Nana extends Sirius_Resource
{
	public function search()
	{
		$revision = array();
		
		$keyword = $this->getProduct()->sku;
		$keyword = urlencode($keyword);
		$requestUrl = $this->getInterface()->url . '/detskiye-tovary/?q=' . $keyword;
		$response = $this->getResponse($requestUrl);
		
		if ($response &&
			200 == $response->getStatus()) {
			if ($content = $this->compressContent($response->getBody())) {
				if (preg_match('/<\/div><a href="(.*?)" class="thumb".*?>/', $content, $matches)) {
					// Load product page
					$requestUrl = $this->getInterface()->url . $matches[1];
					$response = $this->getResponse($requestUrl);
					$revision['request_url'] = $requestUrl;

					if ($response &&
						200 == $response->getStatus()) {							
						$content = $this->compressContent($response->getBody());
						// Get revision name
						$revision['name'] = '';
						if (preg_match('/<h1>(.*)<\/h1>/', $content, $matches)) {
							$revision['name'] = $this->escape($matches[1]);
						}
						
						// Get revision price value
						$revision['price'] = 0;
						if (preg_match('/<div class="price".*?>(.*?)<\/div>/', $content, $matches)) {
							$revision['price'] = $this->formatPrice($matches[1]);
						}
						
						// Get revision weight value
						$revision['weight'] = 0;
						if (preg_match('/<span class="text">Вес товара: (.*) г<\/span>/s', $content, $matches)) {
							$revision['weight'] = $this->formatWeight($matches[1]);
						}
						
						// Get revision category
						$revision['category'] = '';
						if (preg_match_all('/<span class="drop"><a class="number" href=".*?><span>(.*?)<\/span>/', $content, $matches)) {
							$revision['category'] = $this->escape(implode('/', $matches[1]));
						}
						
						// Get revision description value
						$revision['description'] = '';
						if (preg_match('/<div class="detail_text">(.*?)<\/div>/s', $content, $matches)) {
							$revision['description'] = $this->escape($matches[1], true);
						}
						// Get revision images
						$revision['media'] = array();
						if (preg_match_all("/<a href=\"\/upload\/(.*?)\" rel=\"item_slider\" class=\"fancy\">/", $content, $matches)) {
							if (!empty($matches[1])) {
								foreach ($matches[1] as $image) {
									$revision['media'][] = $this->getInterface()->url . '/upload/' . $image;
								}
							}
						}
						
						// Get revision attributes
						$revision['attributes'] = array();
						if (preg_match_all('/<td class="char_name">(.*?)<\/td><td class="char_value">(.*?)<\/td>/s', $content, $matches)) {
							$attributes = $matches[1];
							$values = $matches[2];
							foreach ($attributes as $key => $attribute) {
								if (false !== strpos($attribute, 'hint')) {
									preg_match("/<span class=\"whint\"><div class=\"hint\"><span class=\"icon\"><i>\?<\/i><\/span><div class=\"tooltip\">.*?<\/div><\/div>(.*?)<\/span>/", $attribute, $attribute);
									$attribute = $attribute[1];
								}
								
								$attribute = $this->escape($attribute, true);
								$attribute = str_replace('?', '', $attribute);
								$revision['attributes'][$attribute] = $this->escape($values[$key]);	
							}
						}
						// Get revision producer
						$revision['attributes']['Производитель'] = 'Na-Na';
					}
				}
			}
		}

		$this->setRevision($revision);
	}

	protected function formatWeight($value)
	{
		$value = parent::formatWeight($value);
		$value = $value / 1000;
		
		return $value;
	}
}
<?php

class Sirius_Resource_Chicco extends Sirius_Resource
{
	public function search()
	{
		$revision = array();
		
		$keyword = trim($this->getProduct()->sku);
		$requestUrl = $this->getInterface()->url . '/goodsearch.html';
		$response = $this->getResponse($requestUrl, 'post', array('search_text' => $keyword));
		// Load search page
		if ($response &&
			200 == $response->getStatus()) {
			if ($content = $this->compressContent($response->getBody())) {

					  $revision['request_url'] = '';
					  if (preg_match('/<div class="fb-like" data-href="(.*?)" data-layout="button_count" data-action="like" data-show-faces="true" data-share="false"><\/div>/is', $content, $matches)) {
					      $revision['request_url'] = $this->getInterface()->url . $matches[1];
					  }
    				            // Get revision name
    				            $revision['name'] = '';
    				            if (preg_match('/<h1 class="fn">(.*?)<\/h1>/is', $content, $matches)) {
    				                $revision['name'] = $this->escape(html_entity_decode($matches[1]));
    				            }
    				            
    				            // Get revision price value
    				            $revision['price'] = '';
    				            if (preg_match('/<b style="font-weight: 700;font-size:18px; color:#cc0000" class="price">(.*?)<\/b>/is', $content, $matches)) {
    				                $revision['price'] = $this->formatPrice($this->escape($matches[1]));
    				            }
    				            
    				            // Get revision weight value
    				            $revision['weight'] = 0;
    				            
    				            // Get revision category
    				            $revision['category'] = '';
    				            if (preg_match_all('/<span itemprop=\"title\">(.*?)<\/span>/is', $content, $matches)) {
    				                array_shift($matches[1]);
    				                $revision['category'] = $this->escape(implode('/', $matches[1]));
    				            }
    				            
    				            // Get revision description value
    				            $revision['description'] = '';
    				            if (preg_match_all('/<div class="pi_panel">(.*?)<\/div>/is', $content, $matches)) {
					      foreach($matches[1] as $match) {
						$revision['description'] .= trim($this->escape($match, true));
					      }
    				            }
    				            // Get revision images
    				            $revision['media'] = array();
					  if (preg_match('/<meta property="og:image" content="(.*?)" \/>/is', $content, $match)) {
					      if (!empty($match[1])) {
    				                     $revision['media'][] = $match[1];
    				                }
    				            }
    				            if (preg_match_all('/<a href="photos\/(.*?)" rel="screenshoots" class="dop_img_a">/is', $content, $matches)) {
					      if (!empty($matches[1])) {
    				                    foreach ($matches[1] as $image) {
    				                        $revision['media'][] = $this->getInterface()->url . '/photos/' . $image;
    				                    }
    				                }
    				            }
    				            
    				            // Get revision attributes
    				            $revision['attributes'] = array();
    				            $revision['attributes']['Производитель'] = 'Chicco';
				
			}
		}
		$this->setRevision($revision);
	}
	
	protected function compressContent($value)
	{
		$value = trim($value);
		$value = preg_replace("#[\n\r\t]#", "", $value);
	    	$value = preg_replace('#>[\s]+<#', '><', $value);
	    	$value = preg_replace('#[\s]+#', ' ', $value);
		$value = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $value);
		$value = iconv('windows-1251', 'utf-8', $value);
		
		return $value;
	}

	protected function escape($value, $allowTags = false)
	{
		$value = trim($value);
		$allowableTags = null;
		if ($allowTags) {
			$allowableTags = array('<p>', '</p>', '<br>', '</br>', '<ul>', '</ul>', '<li>', '</li>','<h4>','</h4>');
			$allowableTags = implode('', $allowableTags);
		}
		$value = str_replace('<p></p>', '', $value);
		$value = strip_tags($value, $allowableTags);
		
		return $value;
	}
}
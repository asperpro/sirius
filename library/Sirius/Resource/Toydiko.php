<?php

class Sirius_Resource_Toydiko extends Sirius_Resource
{
	public function search()
	{
		$revision = array();

		$keyword = trim($this->getProduct()->sku);
		$requestUrl = $this->getInterface()->url . "/ua/catalogsearch/result/?q=" . $keyword;
		$response = $this->getResponse($requestUrl);
		$sku = '';
		if ($response &&
			200 == $response->getStatus()) {
			if ($content = $this->compressContent($response->getBody())) {
				if (preg_match('/<h2 class="product-name"><a href="(.*?)">.*?<\/a><\/h2>/', $content, $matches)) {

					// Load product page
					$requestUrl = $matches[1];
					preg_match('/(.*\.html)/', $requestUrl, $requestUrl);
					$requestUrl = $requestUrl[1];
					$response = $this->getResponse($requestUrl);
					if ($response && 200 == $response->getStatus()) {
						$content = $this->compressContent($response->getBody());
						$revision['request_url'] = $requestUrl;

						// Get revision name
						$revision['name'] = $this->getProduct()->name;

						// Get revision attributes
						$revision['attributes'] = array();
						if (preg_match_all('/<p><strong>(.*?)<\/strong>: (.*?)<\/p>/is', $content, $matches)) {
							$attributes = str_replace("</strong>", '', $matches[1]);
							$values = $matches[2];
							foreach ($attributes as $key => $attribute) {
								$revision['attributes'][$this->formatAttributeName($attribute)] = $this->escape($values[$key]);
							}
						}
                        if (preg_match('/<a class=\"fade-on-hover\".*?><img src=\".*?\" alt=\"(.*?)\" \/>/is', $content, $matches)) {
                            $revision['attributes']['Производитель'] = $this->escape($matches[1]);
                        }

						// Get revision sku
						if (preg_match('/Артикул: <\/span>.*?<span class="value">(.*?)<\/span>/is', $content, $matches)) {
							$sku = $matches[1];
						}

						// Get revision price value
						if (preg_match('/<span class="price">(.*?)<\/span>/is', $content, $matches)) {
							$price = str_replace(',', '.', $matches[1]);
							$price = str_replace('&nbsp;грн', '', htmlentities($price, null, 'utf-8'));
							$revision['price'] = $price;
						}

						// Get revision category
						$revision['category'] = '';

						// Get revision weight value
						$revision['weight'] = 0;

						// Get revision images
						$revision['media'] = array();
						if (preg_match_all('/<div class="item" >.*?<a href=".*?img src="(.*?)".*?<\/a>.*?<\/div>/is', $content, $matches)) {
							if (!empty($matches[1])) {
								foreach ($matches[1] as $image) {
								    if (strpos($image, 'wysiwyg')) continue;
									$revision['media'][] = $image;
								}
							}
						}

						// Get revision description value
						$revision['description'] = '';
						if (preg_match('/<div class="std">(.*?)<\/div>/is', $content, $matches)) {
							$revision['description'] = $this->escape($matches[1], true);
						}
					}
				}
			}
		}
		
		if (!$sku) {
			$revision = array();
		}
		$this->setRevision($revision);
	}

	protected function compressContent($value)
	{
		$value = trim($value);
		$value = preg_replace("#[\n\r\t]#", "", $value);
		$value = preg_replace('#>[\s]+<#', '><', $value);
		$value = preg_replace('#[\s]+#', ' ', $value);
		$value = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $value);
		$value = preg_replace('#<link(.*?)/>#is', '', $value);

		return $value;
	}
}
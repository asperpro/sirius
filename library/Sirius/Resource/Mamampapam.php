<?php

class Sirius_Resource_Mamampapam extends Sirius_Resource
{
	public function search()
	{
		$revision = array();
		
		$keyword = trim($this->getProduct()->name);
		$keyword = urlencode($keyword);
		$requestUrl = $this->getInterface()->url . '/k_' . $keyword;
		$response = $this->getResponse($requestUrl);

		// Load search page
		if ($response &&
			200 == $response->getStatus()) {
			if ($content = $this->compressContent($response->getBody())) {
				if (preg_match('/<div class="category-products-container">.*?<a itemprop="url" class="pop-product-name-link" href="(.*?)">.*?><\/div>/is', $content, $matches)) {
					if (!empty($matches[1])) {
						$requestUrl = $this->getInterface()->url . $matches[1];
						$response = $this->getResponse($requestUrl);
						
						// Load product page
						if ($response &&  200 == $response->getStatus()) {
    				        if ($content = $this->compressContent($response->getBody())) {
    				            $revision['request_url'] = $requestUrl;
    				            // Get revision name
    				            $revision['name'] = '';
    				            if (preg_match('/<h1 itemprop="name" .*?>(.*?)<\/h1>/is', $content, $matches)) {
    				                $revision['name'] = $this->escape(html_entity_decode($matches[1]));
    				            }
								
    				            // Get revision price value
    				            $revision['price'] = '';
    				            if (preg_match('/<span itemprop="price">(.*?)<\/span>/is', $content, $matches)) {
    				                $revision['price'] = $this->formatPrice($this->escape($matches[1]));
    				            }
    				            
    				            // Get revision weight value
    				            $revision['weight'] = 0;
    				            
    				            // Get revision category
    				            $revision['category'] = '';
    				            preg_match_all('/<a.*?href=".*?"><span itemprop="name">(.*?)<\/span><\/a>/is', $content, $matches);
					  			if (!empty($matches[1])) {
    				                array_shift($matches[1]);
    				                $revision['category'] = implode('/', $matches[1]);
    				            }
    				            
    				            // Get revision description value
    				            $revision['description'] = '';
    				            if (preg_match('/<div id="all-info".*?wysiwyg-area">(.*?)<\/div>/s', $content, $matches)) {
    				                $revision['description'] = trim($this->escape($matches[1]), '<br>');
								}
    				            // Get revision images
    				            $revision['media'] = array();
    				            if (preg_match_all('/<li><div class="product-attribute-block"><a href="(.*?)".*?<\/li>/is', $content, $matches)) {
    				                if (!empty($matches[1])) {
    				                    foreach ($matches[1] as $image) {
    				                        $revision['media'][] = $this->getInterface()->url . $image;
    				                    }
									}
    				            } elseif (preg_match('/<div class="product-main-image-block"><a href="(.*?)"/is', $content, $matches)) {
									$revision['media'][] = str_replace('/uploads', $this->getInterface()->url .'/uploads', $matches[1]);
					  			}

    				            // Get revision attributes
    				            $revision['attributes'] = array();
    				            if (preg_match_all('/<td>(.*?)<\/td>[\s]*<td>(.*?)<\/td>/is', $content, $matches)) {
									
    				                $attributes = $matches[1];
    				                $values = $matches[2];
    				                foreach ($attributes as $key => $attribute) {
    				                    $attribute = $this->escape($attribute, true);
    				                    $revision['attributes'][$attribute] = $this->escape(html_entity_decode($values[$key]));
									}
								}
								if(preg_match('/Производитель:\s*<a.*?>(.*?)<\/a>/', $content, $matches)){
									$revision['attributes']['Производитель'] = $this->escape(html_entity_decode($matches[1]));
								}
    				        }
						}
					}
				}
				
			}
		}
		$this->setRevision($revision);
	}
	
	protected function compressContent($value)
	{
		$value = trim($value);
		$value = preg_replace("#[\n\r\t]#", "", $value);
	    	$value = preg_replace('#>[\s]+<#', '><', $value);
	    	$value = preg_replace('#[\s]+#u', ' ', $value);
		$value = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $value);
		
		return $value;
	}
}
<?php

class Sirius_Soap extends Platon_Db_Table_Abstract
{
	protected $session;
	protected $client;
	protected $configs;

	function __construct() {

		$configModel = new System_Model_Config();
		$configData = $configModel->getAllConfigs();

		$soapClientUrl = $configData['soap/domain'] . '/api/soap/?wsdl';
		$soapOptions = array(
			'keep_alive' => true,
			'encoding'      =>'UTF-8',
			'compression'   => SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP | SOAP_COMPRESSION_DEFLATE,
		);
		$this->client = new SoapClient($soapClientUrl, $soapOptions);
		$this->session = $this->client->login($configData['soap/login'], $configData['soap/password']);
		$this->configs = $configData;
	}
}
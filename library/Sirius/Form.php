<?php

class Sirius_Form extends Zend_Form
{
	protected $_buttons = array();
	
    public function loadDefaultDecorators()
	{
		parent::loadDefaultDecorators();
		
		return;
		
		$this->setDecorators(array(
			'FormElements', 
			array('HtmlTag', array('tag' => 'table')), 
			'Form'
		)); 

		$this->setElementDecorators(array( 
			'ViewHelper', 
			'Errors', 
			array('decorator' => array('td' => 'HtmlTag'), 'options' => array('tag' => 'td')), 
			array('Label', array('tag' => 'td')), 
			array('decorator' => array('tr' => 'HtmlTag'), 'options' => array('tag' => 'tr')), 
		));
		// Get submit buttons
		if ($elements = $this->getElements()) {
			foreach ($elements as $name => $value) {
				if ($value instanceof Zend_Form_Element_Submit) {
					$this->_buttons[] = $name;
				}
			}
		}
		
		if ($this->_buttons) {
			foreach ($this->_buttons as $key => $value) {
				$this->$value->setDecorators(array( 
					array( 
						'decorator' => 'ViewHelper', 
						'options' => array('helper' => 'formSubmit')), 
					array( 
						'decorator' => array('td' => 'HtmlTag'), 
						'options' => array('tag' => 'td', 'colspan' => 2)), 
					array( 
						'decorator' => array('tr' => 'HtmlTag'), 
						'options' => array('tag' => 'tr')), 
				));
			}
		}
	}
	
	public function setElements(array $elements)
    {
        return $this->addElements($elements);
    }
}
<?php

abstract class Sirius_Resource
{
	const MAX_IMAGE_WIDTH = 1000;
	private $_product;
	private $_interface;
	private $_revision;
	static protected $userAgents = array(
		'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.6) Gecko/20070725 Firefox/2.0.0.6',
		'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:5.0) Gecko/20100101 Firefox/5.0',
		'Mozilla/5.0 (Windows NT 6.1.1; rv:5.0) Gecko/20100101 Firefox/5.0',
		'Mozilla/5.0 (X11; U; Linux i586; de; rv:5.0) Gecko/20100101 Firefox/5.0',
		'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.1 (KHTML, like Gecko) Ubuntu/11.04 Chromium/14.0.825.0 Chrome/14.0.825.0 Safari/535.1',
		'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/14.0.824.0 Safari/535.1',
		'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.7; rv:5.0) Gecko/20100101 Firefox/5.0',
		'Mozilla/5.0 (Macintosh; PPC MacOS X; rv:5.0) Gecko/20110615 Firefox/5.0',
		'Mozilla/5.0 (Windows; U; MSIE 9.0; WIndows NT 9.0; en-US))',
		'Mozilla/5.0 (compatible; MSIE 8.0; Windows NT 5.2; Trident/4.0; Media Center PC 4.0; SLCC1; .NET CLR 3.0.04320)',
		'Mozilla/5.0 (Windows; U; MSIE 7.0; Windows NT 6.0; en-US)',
		'Mozilla/5.0 (compatible; Konqueror/4.5; FreeBSD) KHTML/4.5.4 (like Gecko)',
		'Opera/9.80 (Windows NT 6.1; U; es-ES) Presto/2.9.181 Version/12.00',
		'Opera/9.80 (X11; Linux x86_64; U; fr) Presto/2.9.168 Version/11.50',
		'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_8; de-at) AppleWebKit/533.21.1 (KHTML, like Gecko) Version/5.0.5 Safari/533.21.1',
		'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_7; da-dk) AppleWebKit/533.21.1 (KHTML, like Gecko) Version/5.0.5 Safari/533.21.1',
		'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:42.0) Gecko/20100101 Firefox/42.0',
		'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.86 Safari/537.36',
		'Mozilla/5.0 (iPad; CPU OS 9_1 like Mac OS X) AppleWebKit/600.1.4 (KHTML, like Gecko) CriOS/46.0.2490.85 Mobile/13B143 Safari/600.1.4',
	);
	private $_phpSessionId;


	public function setPhpSessionId($seesionId)
	{
		$this->_phpSessionId = $seesionId;
	}

	public function getPhpSessionId()
	{
		return $this->_phpSessionId;
	}
	
	/**
	 * Set interface data
	 *
	 * @param array $data
	 */
	public function setInterface($data)
	{
		$data['url'] = trim($data['url'], '/');
		$this->_interface = (object)$data;
	}
	
	/**
	 * Get interface data
	 */
	public function getInterface()
	{
		return $this->_interface;
	}
	
	/**
	 * Set product data
	 *
	 * @param array $data
	 */
	public function setProduct($data)
	{
		$this->_product = (object)$data;
	}
	
	/**
	 * Get product data
	 */
	public function getProduct()
	{
		return $this->_product;
	}

	/**
	 * Set revision data
	 *
	 * @param array $data
	 */
	public function setRevision($data)
	{
		$this->_revision = (object)$data;
	}
	
	/**
	 * Get revision data
	 */
	public function getRevision()
	{
		return $this->_revision;
	}
	
	private function _getResponseAdapter($requestUrl, $method = 'get', $data = null, $headers = array())
	{
	    $lenght = count(self::$userAgents) - 1;
	    $index = rand(0, $lenght);
		
	    $client = new Zend_Http_Client();
	    $client->setUri($requestUrl);
	    if ($headers) {
		$client->setHeaders($headers);
	    }
	    $client->setConfig(array(
	        'maxredirects' => 5,
	        'timeout'      => 60,
	        'useragent'	   => self::$userAgents[$index],
	        'strictredirects' => true,
	    ));
	    
		$client->setMethod(Zend_Http_Client::GET);
	    if ($method == 'post') {
	    	$client->setMethod(Zend_Http_Client::POST);
	    }
		if ($data) {
			$client->setParameterPost($data);
		}
		
		return $client;
	}
	
	protected function getCurlResponse($requestUrl, $method = 'get', $data = null, $headers = array())
	{
		$client = $this->_getResponseAdapter($requestUrl, $method, $data, $headers);
		
		$adapter = new Zend_Http_Client_Adapter_Curl();
		$client->setAdapter($adapter);
	    	$adapter->setConfig(array(
		    'curloptions' => array(
		        CURLOPT_RETURNTRANSFER => 1,
		        CURLOPT_FOLLOWLOCATION => true,
		        CURLOPT_SSL_VERIFYPEER => false,
		        CURLOPT_RETURNTRANSFER => true,
		        CURLOPT_COOKIE => $this->getPhpSessionId(),
		        CURLOPT_COOKIEJAR => "cookies.txt",
		        CURLOPT_COOKIEFILE => "cookies.txt",
		        CURLOPT_COOKIESESSION => true,
		    )
		));
	    $response = $client->request();

	    return $response;
	}
	
	/**
	 * Send request to url and return response code and body
	 *
	 * @param string $requestUrl
	 * @param string $method
	 * @param mixed $data
	 * @return object
	 */
	protected function getResponse($requestUrl, $method = 'get', $data = null, $headers = array())
	{
	    $client = $this->_getResponseAdapter($requestUrl, $method, $data, $headers);
	    $response = $client->request();

	    return $response;
	}

	/**
	 * Formatting price value
	 *
	 * @param string $value
	 * @return float
	 */
	protected function formatPrice($value)
	{
		$value = preg_replace('/[^0-9.,]/', '', $value);
		$value = (float)str_replace(array(',', ' '), array('.', ''), $value);
		$value = round($value);
		
		return $value;
	}
	
	/**
	 * Format revision weight
	 *
	 * @param string $value
	 * @return float
	 */
	protected function formatWeight($value)
	{
		$value = preg_replace('/[^0-9]/', '', $value);
		
		return (float)$value;
	}
	
	protected function formatAttributeName($value)
	{
		$search = array(':', '&nbsp;');
		$replace = array('');
		$value = $this->escape($value);
		$value = str_replace($search, $replace, $value);
		$value = trim($value);
		
		return $value;
	}
	
	protected function getMediaData($file)
	{
		$data = array();

		$parts = pathinfo($file);
		if (!isset($parts['extension'])) return false;
		$data['name'] = preg_replace('/[^\da-z]/i', '', $parts['filename']);
		$data['name'] = md5($data['name'] . uniqid() . mktime()) . '.';
		
		switch ($parts['extension']) {
			case 'png':
				$data['name'] .= 'png';
				$data['mime_type'] = 'image/png';
				break;
			case 'jpg':
			case 'jpeg':
			default:
				$data['name'] .= 'jpg';
				$data['mime_type'] = 'image/jpeg';
		}
		
		return $data;
	}
	
	protected function uploadMedia($filename, $media)
	{
		$path = '';
		$mediaPath = APPLICATION_PATH . '/../media/product/';
		$chars = str_split(substr($filename, 0, 2));

		foreach ($chars as $char) {
			$path .= $char . '/';
			if (!is_dir($mediaPath . $path)) {
				mkdir($mediaPath . $path);
			}
		}

		$filePath = $mediaPath . $path . $filename;
		$mediaResponse = $this->getResponse($media);
		if (200 != $mediaResponse->getStatus()) {
			return false;
		}
		file_put_contents($filePath, $mediaResponse->getBody());
		// Trim the image
		$imagick = new Imagick($filePath);
		$imageWidth = ($imagick->getImageWidth() > self::MAX_IMAGE_WIDTH) ? self::MAX_IMAGE_WIDTH : $imagick->getImageWidth();
		$imageHeight = ($imagick->getImageHeight() > self::MAX_IMAGE_WIDTH) ? self::MAX_IMAGE_WIDTH : $imagick->getImageWidth();
		$imagick->trimImage(0);
		if ($imagick->getImageWidth() > self::MAX_IMAGE_WIDTH || $imagick->getImageHeight() > self::MAX_IMAGE_WIDTH) {
			$imagick->resizeImage(self::MAX_IMAGE_WIDTH, self::MAX_IMAGE_WIDTH, imagick::FILTER_LANCZOS, 1, true);
		}
		$imagick->writeImage($filePath);
		
		return $path . $filename;
	}
	
	protected function getMediaMimeType($file)
	{
		$parts = pathinfo($file);
		
		switch ($parts['extension']) {
			case 'png':
				$mimeType = 'image/png';
				break;
			case 'jpg':
			case 'jpeg':
			default:
				$mimeType = 'image/jpeg';
		}
		
		return $mimeType;
	}
	
	/**
	 * Compress html content
	 *
	 * @param string $value
	 * @return string
	 */
	protected function compressContent($value)
	{
		$value = trim($value);
		$value = preg_replace("#[\n\r\t]#", "", $value);
	    $value = preg_replace('#>[\s]+<#', '><', $value);
	    $value = preg_replace('#[\s]+#', ' ', $value);
		$value = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $value);
		$value = preg_replace('#<header(.*?)>(.*?)</header>#is', '', $value);
		$value = preg_replace('#<link(.*?)/>#is', '', $value);
		
		return $value;
	}
	
	/**
	 * Cleanup variables
	 *
	 * @param string $value
	 * @param boolean $allowTags
	 * @return string
	 */
	protected function escape($value, $allowTags = false)
	{
		$value = trim($value);
		$allowableTags = null;
		if ($allowTags) {
			$allowableTags = array('<p>', '</p>', '<br>', '</br>', '<ul>', '</ul>', '<li>', '</li>');
			$allowableTags = implode('', $allowableTags);
		}
		$value = str_replace('<p></p>', '', $value);
		$value = strip_tags($value, $allowableTags);
		
		return $value;
	}
	
	protected function checkIdentity($value, $identity)
	{
		if (false === strpos($value, $identity)) {
			return false;
		}

		return true;
	}

	protected function setRevisionCategory()
	{
		if ($category = $this->getRevision()->category) {
			$intefaceCategoryModel = new Interface_Model_Interface_Category();
			$categories = $intefaceCategoryModel->findByAlias($category, $this->getInterface()->id);
			$this->getRevision()->category = $categories;
		}
	}
	
	protected function setRevisionAttributeSet()
	{
		$setId = 4;
		$attributeSetCategoryModel = new Attribute_Model_Attribute_Set_Category();
		if ($categories = $this->getRevision()->category) {
			foreach ($categories as $category) {
				if ($attributeSetId = $attributeSetCategoryModel->findByCategoryId($category)) {
					$setId = $attributeSetId;
				}
			}
		}
		$this->getRevision()->setId = $setId;
	}
	
	protected function setRevisionAttributes()
	{
		if ($attributes = $this->getRevision()->attributes) {
			$this->getRevision()->attributes = array();
			// Check attributes by name
			$iAttributeModel = new Interface_Model_Interface_Attribute();
			$attributeModel = new Attribute_Model_Attribute();
			$iAttributes = $iAttributeModel->getAttributeBySet($this->getRevision()->setId, $this->getInterface()->id);

			foreach ($attributes as $_alias => $_value) {
				if (!$attribute = $attributeModel->getByName($_alias, $this->getRevision()->setId)) {
					if ($iAttributes) {
						foreach ($iAttributes as $key => $_attribute) {
							if ($_alias == $_attribute['alias']) {
								$attribute = $_attribute;
								unset($iAttributes[$key]);
								break;
							}
						}	
					}
				}
				if ($attribute) {
					$this->getRevision()->attributes[$attribute['id']] = $this->getRevisionAttributeValue($_value, $attribute);
					unset($attributes[$_alias]);	
				}
			}
		}
		if ($attributes) {
			$this->getRevision()->description .= ($this->getRevision()->description) ? '<br/><br/>' : '';
			foreach ($attributes as $attribute => $value) {
				$this->getRevision()->description .= $attribute . ': ' . $value . '<br/>';
			}
		}
	}
	
	protected function getRevisionAttributeValue($alias, $attribute)
	{
		if (!in_array($attribute['type'], array('select', 'multiselect'))) {
			return $alias;
		}
		
		$result = array();
		$alias = explode(',', $alias);
		
		$iAttributeValueModel = new Interface_Model_Interface_Attribute_Value();
		$attributeValueModel = new Attribute_Model_Attribute_Options();
		if (is_array($alias)) {
			foreach ($alias as $_alias) {
				$_alias = trim($_alias);
				if (!$optionValue = $attributeValueModel->getOptionByName($_alias, $attribute['id'])) {
					$optionValue = $iAttributeValueModel->getValueByAlias($_alias, $this->getRevision()->setId, $attribute['id'], $this->getInterface()->id);
				}
				$result[] = $optionValue;
			}
		}

		return $result;
	}
	
	public function save()
	{
		if (empty($this->getRevision()->request_url)) return;

		$this->setRevisionCategory();
		$this->setRevisionAttributeSet();
		$this->setRevisionAttributes();
		
		// Init models
		$revisionModel = new Catalog_Model_Revision();
		$revisionCategoryModel = new Catalog_Model_Revision_Category();
		$revisionAttributeModel = new Catalog_Model_Revision_Attribute();
		$revisionMediaModel = new Catalog_Model_Revision_Media();
		$mediaModel = new Catalog_Model_Media();
		$productModel = new Catalog_Model_Product();
		
		// Save revision
		$values = array(
			'product_id' => $this->getProduct()->id,
			'interface_id' => $this->getInterface()->id,
			'set_id' => $this->getRevision()->setId,
			'weight' => (isset($this->getRevision()->weight)) ? $this->getRevision()->weight : 0,
			'name' => (isset($this->getRevision()->name)) ? $this->getRevision()->name : '',
			'price' => (isset($this->getRevision()->price)) ? $this->formatPrice($this->getRevision()->price) : '',
			'description' => (isset($this->getRevision()->description)) ? $this->getRevision()->description : '',
			'user_id' => $revisionModel->getRevisionUser(),
			'request_url' => $this->getRevision()->request_url,
		);
		$revision = $values;
		$revisionId = $revisionModel->add($values);
		
		// Save revision categories
		if ($this->getRevision()->category) {
			foreach ($this->getRevision()->category as $category) {
				$values = array(
					'category_id' => $category,
					'revision_id' => $revisionId,
				);
				$revisionCategoryModel->add($values);
			}
		}
		
		// Save revision attributes
		if ($this->getRevision()->attributes) {
			foreach ($this->getRevision()->attributes as $attribute => $value) {
				$values = (!is_array($value)) ? (array)$value : $value;
				foreach ($values as $_value) {
					$values = array(
						'revision_id' => $revisionId,
						'attribute_id' => $attribute,
						'value' => $_value,
					);
					$revisionAttributeModel->add($values);
				}
			}
		}

		// Save revision media
		if ($this->getRevision()->media) {
			foreach ($this->getRevision()->media as $key => $media) {
				
				// Get media data
				if ($mediaData = $this->getMediaData($media)) {
					// Upload media file
					$filename = $this->uploadMedia($mediaData['name'], $media);
				
					// Add media file
					$values = array(
						'filename' => $filename,
						'mime_type' => $mediaData['mime_type'],
					);
					$mediaId = $mediaModel->add($values);
					$values = array(
						'media_id' => $mediaId,
						'revision_id' => $revisionId,
						'is_main' => (!$key) ? 1 : 0,
					);
					$revisionMediaModel->add($values);
				}
			}
		}

		if (!empty($this->getRevision()->is_moderated)) {
			$revision['categories'] = $revisionCategoryModel->getByRevisionId($revisionId);
			$revision['media']      = $revisionMediaModel->getByRevisionId($revisionId);
			$revision['attributes'] = $this->_getRevisionAttributes($this->getRevision()->setId, $revisionId);

			$productData = array(
				'status' => 'completed',
				'set_id' => $revision['set_id'],
				'name' => $revision['name'],
				'price' => $revision['price'],
				'weight' => $revision['weight'],
				'description' => trim($revision['description']),
			);

			$productModel->update($productData, $this->getProduct()->id);
			unset($productData);

			// Log accept revision event
			$eventValues = array(
				'event' => System_Model_Log::EVENT_ACCEPT_REVISION,
				'entity_id' => $this->getProduct()->id,
			);
			$logEventModel = new System_Model_Log();
			$logEventModel->add($eventValues);

			$productAttributesModel = new Catalog_Model_Product_Attributes();
			$productCategoriesModel = new Catalog_Model_Category_Products();
			$productMediasModel     = new Catalog_Model_Product_Medias();

			// Save all categories
			if ($revision['categories']) {
				foreach ($revision['categories'] as  $category) {
					$values = array(
						'product_id'  => $this->getProduct()->id,
						'category_id' => $category['category_id'],
					);
					$productCategoriesModel->add($values);
				}
			}

			// Save all attributes
			if ($revision['attributes']) {
				foreach($revision['attributes'] as $attribute) {
					if (!empty($attribute['value'])) {
						$options = is_array($attribute['value']) ? $attribute['value'] : array($attribute['value']);
						foreach ($options as $option) {
							$values = array(
								'product_id' => $this->getProduct()->id,
								'attribute_id' => $attribute['attribute_id'],
								'value' => $option,
							);
							$productAttributesModel->add($values);
						}
					}
				}
			}

			// Save all media
			if ($revision['media']) {
				foreach ($revision['media'] as $media) {
					$values = array(
						'product_id' => $this->getProduct()->id,
						'media_id'   => $media['media_id'],
						'is_main'    => $media['is_main'],
					);
					$productMediasModel->add($values);
				}
			}

			$revisionModel->deleteById($revisionId);

		} else {
			// Update product status
			$values = array(
				'status' => 'prepared',
			);
			$productModel->update($values, $this->getProduct()->id);
		}
	}

	protected function _getRevisionAttributes($setId, $revisionId)
	{
		$revisionModel          = new Catalog_Model_Revision();
		$revisionAttributeModel = new Catalog_Model_Revision_Attribute();
		$attributesModel        = new Attribute_Model_Attribute();
		$attributeValueModel    = new Attribute_Model_Attribute_Options();
		$attributeSetAttributesModel = new Attribute_Model_Attribute_Set_Attributes();

		$revision = $revisionModel->find($revisionId);
		$attributes = $attributeSetAttributesModel->getBySet($setId);
		$revisionAttributes = $attributes;

		foreach ($attributes as $num => $attribute) {
			if ($revisionAttribute = $revisionAttributeModel->getByAttribIdAndRevisionId($revisionId, $attribute['attribute_id'])) {
				$revisionAttributes[$num]['value'] = (count($revisionAttribute) == 1) ? array_shift($revisionAttribute) : $revisionAttribute;
			}
		}
		unset($attributes);
		
		return $revisionAttributes;
	}

	protected function _checkTitleIdentity($term, $name)
	{
		$identityCounter = 0;
		$term = Platon_Text::toLowerCase($term);
		$termParts = explode(' ', $name);

		foreach ($termParts as $part) {
			$part = preg_quote(Platon_Text::toLowerCase($part));
			$pattern = '/' . $part . '/';
			if (preg_match($pattern, $term)) {
				$identityCounter ++;
			}
		}
		
		return $identityCounter;
	}
}
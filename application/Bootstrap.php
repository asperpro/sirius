<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    protected function _initView()
    {
        $view = new Zend_View();

        $view->doctype('XHTML1_STRICT');
        $view->setEncoding('UTF-8');
		//$view->headMeta()->appendHttpEquiv('Content-Type', 'text/html;charset=utf-8');
        $viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper('viewRenderer');

        $view->headLink()->appendStylesheet('/public/css/bootstrap.min.css');
        $view->headLink()->appendStylesheet('/public/css/bootstrap-responsive.min.css');
        $view->headLink()->appendStylesheet('/public/css/jquery.fancybox.css');
        $view->headLink()->appendStylesheet('/public/css/jquery.fancybox-thumbs.css');
        $view->headLink()->appendStylesheet('/public/css/sirius.css');
        $view->headLink()->appendStylesheet('/public/sb_admin/bower_components/metisMenu/dist/metisMenu.min.css');
        $view->headLink()->appendStylesheet('/public/sb_admin/dist/css/timeline.css');
        $view->headLink()->appendStylesheet('/public/sb_admin/dist/css/sb-admin-2.css');
        $view->headLink()->appendStylesheet('/public/sb_admin/bower_components/morrisjs/morris.css');
        $view->headLink()->appendStylesheet('/public/sb_admin/bower_components/font-awesome/css/font-awesome.min.css');

        $view->headScript()->appendFile('https://www.google.com/jsapi');
        $view->headScript()->appendFile('/public/js/jquery.min.js');
        $view->headScript()->appendFile('/public/js/jquery-ui-1.9.2.custom.js');
        $view->headScript()->appendFile('/public/js/bootstrap.min.js');
        $view->headScript()->appendFile('/public/js/tinymce/tinymce.min.js');
        $view->headScript()->appendFile('/public/js/jquery.fancybox.js');
        $view->headScript()->appendFile('/public/js/jquery.fancybox-thumbs.js');
        $view->headScript()->appendFile('/public/js/jquery.numeric.js');
        $view->headScript()->appendFile('/public/js/jquery.validate.js');
        $view->headScript()->appendFile('/public/js/jquery.mousewheel-3.0.6.pack.js');

        $view->headScript()->appendFile('/public/js/core.js');
        $view->headScript()->appendFile('/public/sb_admin/bower_components/metisMenu/dist/metisMenu.min.js');
        $view->headScript()->appendFile('/public/sb_admin/bower_components/raphael/raphael-min.js');
        $view->headScript()->appendFile('/public/sb_admin/bower_components/morrisjs/morris.min.js');
        $view->headScript()->appendFile('/public/sb_admin/dist/js/sb-admin-2.js');

        // add helper path
        ZendX_JQuery_View_Helper_JQuery::enableNoConflictMode();
        $view->addHelperPath('ZendX/JQuery/View/Helper/', 'ZendX_JQuery_View_Helper');
        $view->addHelperPath('Platon/JQuery/View/Helper', 'Platon_JQuery_View_Helper');
        $view->addHelperPath('ZendX/JQuery/View/Helper/JQuery', 'ZendX_JQuery_View_Helper_JQuery');

        // Add it to the ViewRenderer
        $viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper('ViewRenderer');
        $viewRenderer->setView($view);


        // Init auth
        if (Zend_Auth::getInstance()->hasIdentity()) {
            $view->user = Zend_Auth::getInstance()->getStorage()->read();
        }

        return $view;
    }

    protected function _initFrontControllerOutput()
    {
        $this->bootstrap('FrontController');
        $frontController = $this->getResource('FrontController');

        $response = new Zend_Controller_Response_Http;
        $response->setHeader('Content-Type', 'text/html; charset=UTF-8', true);
        $frontController->setResponse($response);

        $frontController->setParam('useDefaultControllerAlways', false);

        $autoloader = Zend_Loader_Autoloader::getInstance();
        $autoloader->suppressNotFoundWarnings(false);
        $autoloader->setFallbackAutoloader(true);

        $frontController->registerPlugin(new Platon_Plugin_Auth());

        return $frontController;
    }

    protected function _initMessenger()
    {
        $flashMsgHelper = Zend_Controller_Action_HelperBroker::getStaticHelper('FlashMessenger');
        $messages = $flashMsgHelper->getMessages();

        /// Assign the messages
        $this->view->messages = $messages;
    }

    protected function _initPlugins()
    {
        $this->bootstrap('frontController');
        $frontController = $this->getResource('frontController');
        $frontController->registerPlugin(new Platon_Plugin_ModuleLayout());
    }
    
    	protected function _initTranslation()
	{
		$data = array(
			'Submit' => 'Отправить',
			'Apply Filter' => 'Применить фильтр',
			'Clear Order' => 'Сбросить сотрировку',
			'Clear Filters' => 'Сбросить фильтры',
			'Clear Filters and Order' => 'Сбросить фильтры и сортировку',
			'Select All' => 'Выбрать все',
			'Select Visible' => 'Выбрать видимые',
			'Unselect All' => 'Снять все',
			'items selected' => 'записей выбрано',
			'Actions' => 'Действия',
			'First' => 'Первая',
			'Last' => 'Последняя',
			'Next' => 'Следующая',
			'Previous' => 'Предыдущая',
			'Page' => 'Страница',
			'No records found' => 'Данные не найдены',
			'No records selected' => 'Нет выбранных записей',
			'All' => 'Все',
			'From' => 'От',
			'To' => 'До',
			"Value is required and can't be empty" => "Значение является обязательным и не может быть пустым",
		);
		// set up translation adapter
		$translate = new Zend_Translate('array', $data, 'ru');
		// set default locale
		$translate->setLocale('ru');
		Zend_Registry::set('Zend_Translate', $translate);
	}
}
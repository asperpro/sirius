<?php

class Attribute_Model_UpdateAttribute extends Sirius_Soap
{
	public function update($data)
	{
		$attributeModel             = new Attribute_Model_Attribute();
		$attributeSetAttributeModel = new Attribute_Model_Attribute_Set_Attributes();
		$attributeOptionsModel      = new Attribute_Model_Attribute_Options();

		// Get set attributes
		if((int)$data['attribute_id']) {
			$attributes = array($data);
			$previousAttributes[] = $attributeModel->find($data['attribute_id']);

			$attributeModel->delete('id =' . $data['attribute_id']);
			$attributeSetAttributeModel->deleteAttribute($data['attribute_id'], $data['set']);
		} else {
			$previousAttributes = $attributeModel->getAll(true);

			$attributes = $this->client->call($this->session, "product.listOfAdditionalAttributes", array(
				'simple',
				$data['set']
			));
			$attributeModel->deleteBySet($data['set']);
			$attributeSetAttributeModel->delete('set_id =' . $data['set']);
		}

		foreach ($attributes as $attribute) {

			$attributeInfo = $this->client->call($this->session, 'product_attribute.info', $attribute['attribute_id']);
			if (isset($attributeInfo['is_user_defined']) && ($attributeInfo['is_user_defined'] == '1')) {
				$attributesList = array(
					'id'   => $attributeInfo['attribute_id'],
					'code' => $attributeInfo['attribute_code'],
					'type' => $attributeInfo['frontend_input'],
					'name' => $attributeInfo['frontend_label'][0]['label'],
				);

				foreach ($previousAttributes as $key => $previous) {
					if ($previous['id'] == $attributesList['id']) {
						$attributesList['is_active'] = $previous['is_active'];
						$attributesList['sort_order'] = $previous['sort_order'];
					}
				}

				$attributeModel->replace($attributesList);
				$attributeModel->setAttributeToSet($data['set'], $attribute['attribute_id']);

				if (in_array($attributesList['type'], array('select', 'multiselect'))) {
					if (isset($attributeInfo['options'])) {
						foreach ($attributeInfo['options'] as $option) {
							$optionList = array(
								'id'           => $option['value'],
								'attribute_id' => $attributesList['id'],
								'name'         => $option['label']
							);
							$attributeOptionsModel->replace($optionList);
						}
					}
				}
			}
		}
	}

}
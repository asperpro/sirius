<?php

class Attribute_Model_Attribute_UpdateSet extends Sirius_Soap
{
	public function update()
	{
		$attributeSetModel = new Attribute_Model_Attribute_Set();

		// Clear attribute tables
		$attributeSetModel->deleteAll();

		$sets = $this->client->call($this->session, "catalog_product_attribute_set.list");
		foreach ($sets as $set) {
			$setList = array(
				'id' => $set['set_id'],
				'name' => $set['name']
			);
			$attributeSetModel->add($setList);
		}
	}

}
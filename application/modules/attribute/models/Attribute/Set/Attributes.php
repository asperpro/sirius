<?php

class Attribute_Model_Attribute_Set_Attributes extends Platon_Db_Table_Abstract
{
	protected $_name = 'attribute_set_attributes';

	public function getBySet($id)
	{
		$select = $this->select()
			->from(array('asa' => $this->_name), array('attribute_id'))
			->joinLeft(array('a' => 'attributes'), 'a.id = asa.attribute_id', array())
			->where('set_id = ?', $id)
			->where('is_active = ?', 1)
			->order('a.sort_order DESC');
		return $this->fetchAll($select)->toArray();
	}

	public function deleteAttribute($attribute, $set)
	{
		try {
			$this->getAdapter()->beginTransaction();
			$where = array();
			$where[] = $this->getAdapter()->quoteInto('attribute_id = ?', $attribute);
			$where[] = $this->getAdapter()->quoteInto('set_id = ?', $set);

			if (null === parent::delete($where)) {
				throw new Platon_Exception('Не удалось удалить аттрибут из связки');
			}
			$this->getAdapter()->commit();
		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}


}
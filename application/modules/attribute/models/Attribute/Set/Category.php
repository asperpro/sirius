<?php

class Attribute_Model_Attribute_Set_Category extends Platon_Db_Table_Abstract
{
	protected $_name = 'attribute_set_categories';

	public function getAll()
	{
		$select = $this->select()
			->setIntegrityCheck(false)
			->from(array('asc' => $this->_name), array('id', 'attribute_set_id', 'category_id', 'created_date'))
			->order('asc.created_date DESC');

		return $select;
	}

	public function findById($id)
	{
		$select = $this->select()
			->setIntegrityCheck(false)
			->from(array('asc' => $this->_name))
			->joinInner(array('c' => 'categories'), 'c.id = asc.category_id', array('categories' => 'full_name'))
			->where('asc.id = ?', $id);
		if ($result = $this->fetchAll($select)) {
			$result = $result->toArray();
		}
		return $result[0];
	}

	public function update(array $data, $id)
	{
		try {
			$this->getAdapter()->beginTransaction();
			$where = $this->getAdapter()->quoteInto('id = ?', $id);

			if (null === parent::update($data, $where)) {
				throw new Platon_Exception('Не удалось обновить набор атрибутов');
			}

			$this->getAdapter()->commit();
		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}

	public function add(array $data)
	{
		$data['created_date'] = new Zend_Db_Expr('NOW()');
		try {
			$this->getAdapter()->beginTransaction();
			if (!$id = $this->insert($data)) {
				throw new Platon_Exception('Не удалось добавить Набор атрибутов');
			}
			$this->getAdapter()->commit();
			return $id;
		} catch (Platon_Exception_Form $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}

	public function findByCategoryId($categoryId)
	{
		$select = $this->select()
						->from(array('asc' => $this->_name), array('attribute_set_id'))
						->where('category_id = ?', $categoryId)
						->limit(1);
		if ($result = $this->fetchRow($select)) {
			$result = $result->attribute_set_id;
		}
		
		return $result;
	}
}
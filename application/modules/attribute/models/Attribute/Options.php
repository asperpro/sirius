<?php

class Attribute_Model_Attribute_Options extends Platon_Db_Table_Abstract
{
	protected $_name = 'attribute_options';

	public function getOptions($id)
	{
		$data = array();
		$select = $this->select()
						->from($this->_name, array('id', 'name'))
						->where('attribute_id = ?', $id)
						->order('name');
		if ($result = $this->fetchAll($select)) {
			foreach ($result as $value) {
				$data[$value->id] = $value->name;
			}
		}

		return $data;
	}

	public function add(array $data)
	{
		try {
			if (!$id = $this->insert($data)) {
				throw new Platon_Exception('Не удалось добавить значения атрибутов');
			}

			return $id;
		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}

	public function replace(array $data)
	{
		try {
			if (!parent::replace($this->_name, $data)) {
				throw new Platon_Exception('Не удалось добавить значения атрибутов');
			}

		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}

	public function deleteAll() {

		try{
			if (! $this->truncate($this->_name)) {
				throw new Platon_Exception('Не удалось очистить таблицу значений атрибутов');
			}

		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}
	
	public function getOptionByName($name, $attributeId)
	{
		$select = $this->select()
						->from($this->_name, array('id'))
						->where('attribute_id = ?', $attributeId)
						->where('name = ?', $name);
		if ($result = $this->fetchRow($select)) {
			$result = $result->id;
		}

		return $result;
	}
}
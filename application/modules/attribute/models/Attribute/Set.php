<?php

class Attribute_Model_Attribute_Set extends Platon_Db_Table_Abstract
{
	protected $_name = 'attribute_sets';

	public function getAll($returnType = false)
	{
		$select = $this->select()
			->from(array('as' => $this->_name), array('id', 'name'));
		if ($returnType) {
			return $this->fetchAll($select)->toArray();
			}

		return $select;
	}

	public function getAllSets()
	{
		$data = array();
		$select = $this->select()
						->from($this->_name, array('id', 'name'))
						->order('name');
		if ($result = $this->fetchAll($select)) {
			foreach ($result as $value) {
				$data[$value->id] = $value->name;
			}
		}

		return $data;
	}

	public function add(array $data)
	{
		$data['imported_date'] = new Zend_Db_Expr('NOW()');
		try {
			if (!$id = $this->insert($data)) {
				throw new Platon_Exception('Не удалось добавить наборы атрибутов');
			}
			return $id;
		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}

	public function replace(array $data)
	{
		try {

			if (!parent::replace($this->_name, $data)) {
				throw new Platon_Exception('Не удалось добавить наборы атрибутов');
			}

		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}

	public function deleteAll() {

		try {
			if (!$this->truncate($this->_name)) {
				throw new Platon_Exception('Не удалось очистить таблицу наборов атрибутов');
			}
		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}
}
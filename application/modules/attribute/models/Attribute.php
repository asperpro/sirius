<?php

class Attribute_Model_Attribute extends Platon_Db_Table_Abstract
{
	protected $_name = 'attributes';
	protected $_tables = array(
		'set_attribute' => 'attribute_set_attributes',
	);

	public function getAll($returnType = false)
	{
		$select = $this->select()
			->from(array('a' => $this->_name), array('id', 'name', 'is_active', 'sort_order'));
		if ($returnType) {
			if ($result = $this->fetchAll($select)) {
				return $result->toArray();
			}
		}

		return $select;
	}

	public function add(array $data)
	{
		try {

			if (!$id = $this->insert($data)) {
				throw new Platon_Exception('Не удалось добавить атрибуты');
			}

			return $id;
		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}

	public function update(array $data, $id)
	{
		try {
			$this->getAdapter()->beginTransaction();
			$where = $this->getAdapter()->quoteInto('id = ?', $id);

			if (null === parent::update($data, $where)) {
				throw new Platon_Exception('Не удалось обновить конфигурацию');
			}

			$this->getAdapter()->commit();
		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}

	public function replace(array $data)
	{
		$data['imported_date'] = new Zend_Db_Expr('NOW()');
		try {

			if (!parent::replace($this->_name, $data)) {
				throw new Platon_Exception('Не удалось добавить атрибуты');
			}
			
		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}
	
	public function setAttributeToSet($setId, $attributeId)
	{
		$data = array(
			'set_id' => $setId,
			'attribute_id' => $attributeId,
		);
		try {
			if (!parent::replace($this->_tables['set_attribute'], $data)) {
				throw new Platon_Exception('Не удалось добавить связь набор-атрибут');
			}
		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}

	public function deleteBySet($set) {

		$sql = "DELETE a.* FROM attributes AS a INNER JOIN attribute_set_attributes AS sa ON sa.attribute_id = a.id WHERE (sa.set_id = " . $set . ");";

		return $this->_db->query($sql);

	}

	public function deleteAll()
	{
		try{

			if (! $this->truncate($this->_name)) {
				throw new Platon_Exception('Не удалось очистить таблицу атрибутов');
			}

		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}
	
	public function getByName($name, $setId)
	{
		$setIds = (4 == $setId) ? array(4) : array(4, $setId);
		$select = $this->select()
						->from(array('a' => $this->_name))
						->joinInner(array('sa' => $this->_tables['set_attribute']), 'sa.attribute_id = a.id', array())
						->where('sa.set_id IN(' . implode(',', $setIds) . ')')
						->where('a.name = ?', $name);
		if ($result = $this->fetchRow($select)) {
			$result = $result->toArray();
		}
		
		return $result;
	}
}
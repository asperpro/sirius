<?php

class Attribute_SetCategoryController extends Platon_Controller_Action
{

	public function indexAction()
	{
		$grid = $this->_getGrid();

		$this->view->grid = $grid->deploy();
		$this->view->title = 'Категории набора атрибутов';
	}

	public function addAction()
	{
		$form = new Attribute_Form_AttributeSetCategory();

		try {
			if ($this->getRequest()->isPost()) {
				$formData = $this->_request->getPost();

				$attributeSetCategoryModel = new Attribute_Model_Attribute_Set_Category();

				if ($form->isValid($formData)) {
					$data = $form->getValues();
					unset($data['categories']);
					$interfaceId = $attributeSetCategoryModel->add($data);
				}

				$this->_helper->flashMessenger(array('success' => 'Набор атрибутов успешно добавлен'));
				$this->_redirect('/attribute/set-category');
			}
		} catch (Platon_Exception_Form $e) {
			$form->nomenclature->addError($e->getMessage());
		} catch (Platon_Exception $e) {
			$this->_helper->flashMessenger(array('error' => $e->getMessage()));
			$this->_redirect('/attribute/set-category');
		}

		$this->view->form = $form;
		$this->view->title = 'Добавление набора атрибутов';
	}

	public function editAction()
	{
		$attributeSetCategoryId = (int)$this->_request->getParam('id');
		$attributeSetCategoryModel = new Attribute_Model_Attribute_Set_Category();

		$form = new Attribute_Form_AttributeSetCategory();
		try {

			if (!$setCategory = $attributeSetCategoryModel->findById($attributeSetCategoryId)) {
				throw new Platon_Exception('Набор атрибутов не найден');
			}

			if ($this->getRequest()->isPost()) {
				$formData = $this->_request->getPost();

				if ($form->isValid($formData)) {
					$data = $form->getValues();
					unset($data['categories']);
					$attributeSetCategoryModel->update($data, $attributeSetCategoryId);
				}

				$this->_helper->flashMessenger(array('success' => 'Набор атрибутов успешно изменен'));
				$this->_redirect('/attribute/set-category');
			}

			$form->populate($setCategory);
		} catch (Platon_Exception_Form $e) {
			$form->nomenclature->addError($e->getMessage());
		} catch (Platon_Exception $e) {
			$this->_helper->flashMessenger(array('error' => $e->getMessage()));
			$this->_redirect('/attribute/set-category');
		}

		$this->view->form = $form;
		$this->view->id = $attributeSetCategoryId;
		$this->view->title = 'Редактирование набора атрибутов';
	}

    protected function _getGrid()
    {
    	$grid = $this->_initDefaultGrid();
    	
        $attributeSetCategoryModel = new Attribute_Model_Attribute_Set_Category();
        $select = $attributeSetCategoryModel->getAll();
    	$grid->setSource(new Bvb_Grid_Source_Zend_Select($select));
    	
    	$grid->updateColumn('id', array(
    		'title' => 'Номер',
    		'position' => 1,
    		'style' => 'width:10%;text-align:center;',
    		'searchType' => '=',
    	));
    	$grid->updateColumn('attribute_set_id', array(
    		'title' => 'Набор атрибутов',
    		'position' => 2,
    		'style' => 'width:30%;text-align:left;',
			'callback' => array(
				'function' => array($this, 'setNameCallback'),
				'params' => array('{{attribute_set_id}}')
			),

    	));
    	$grid->updateColumn('category_id', array(
    		'title' => 'Категория',
    		'position' => 3,
    		'style' => 'width:40%;text-align:left;',
			'callback' => array(
				'function' => array($this, 'editLinkCallback'),
				'params' => array('{{id}}', '{{category_id}}')
			),

    	));
		$grid->updateColumn('created_date', array(
			'title' => 'Дата создания',
			'position' => 4,
			'style' => 'width:20%;text-align:left;',
			'format' => array(
				'date',
				array('date_format' => 'dd.MM.yyyy H:m')
			)
		));

		$filters = new Bvb_Grid_Filters();
    	$filters->addFilter('created_date', array('render' => 'date'));
		$grid->addFilters($filters);

    	$grid->setTableGridColumns(array('id', 'attribute_set_id', 'category_id', 'created_date'));

		return $grid;
    }

	public function setNameCallback($id)
	{
		$attributeSetModel = new Attribute_Model_Attribute_Set();
		$set = $attributeSetModel->find($id);
		return $set['name'];
	}

	public function editLinkCallback($id, $category_id)
	{
		$categoryModel = new Catalog_Model_Category();
		$category = $categoryModel->find($category_id);
		return "<a href='/attribute/set-category/edit/id/" . $id ."'>" . $category['full_name'] . "</a>";
	}

}
<?php

class Attribute_IndexController extends Platon_Controller_Action
{
    public function indexAction()
    {
    	$grid = $this->_getGrid();
        
		$this->view->grid = $grid->deploy();
        $this->view->title = 'Управление атрибутами';
    }

	public function editAction()
	{
		$attributeId = (int)$this->_request->getParam('id');
		$attributeModel = new Attribute_Model_Attribute();

		$form = $this->_getForm();
		try {

			if (!$attribute = $attributeModel->find($attributeId)) {
				throw new Platon_Exception('Атрибут не найден');
			}

			if ($this->getRequest()->isPost()) {
				$formData = $this->_request->getPost();

				$data = array (
					'is_active' => (int) $formData['is_active'],
					'sort_order' => (int) $formData['sort_order']
				);
				$attributeModel->update($data, $attributeId);

				$this->_helper->flashMessenger(array('success' => 'Атрибут успешно изменён'));
				$this->_redirect('/attribute/index');
			}

			$form->populate($attribute);
		} catch (Platon_Exception_Form $e) {
			$form->nomenclature->addError($e->getMessage());
		} catch (Platon_Exception $e) {
			$this->_helper->flashMessenger(array('error' => $e->getMessage()));
			$this->_redirect('/attribute/index');
		}

		$this->view->form = $form;
		$this->view->title = 'Редактирование атрибута';
	}

	public function loadAttributesAction()
	{
		header('Content-Type: text/html; charset=utf-8');

		$setId = (int) $this->_request->getParam('setid');

		$attributeSetAttributesModel = new Attribute_Model_Attribute_Set_Attributes();
		$attributesModel             = new Attribute_Model_Attribute();

		$attributes = $attributeSetAttributesModel->getBySet($setId);

		$attributeList = array();
		foreach ($attributes as $attribute) {
			$attributeList[] = $attributesModel->find($attribute['attribute_id']);
		}
		$this->_helper->json($attributeList, true);
		exit();
	}

	public function loadValuesAction()
	{
		header('Content-Type: text/html; charset=utf-8');

		$attributeId = (int)$this->_request->getParam('attributeid');

		$attributesModel       = new Attribute_Model_Attribute();
		$attributeOptionsModel = new Attribute_Model_Attribute_Options();

		$attribute = $attributesModel->find($attributeId);
		$options = $attributeOptionsModel->getOptions($attributeId);

		$this->_helper->json($options, true);
		exit();
	}

	protected function _getForm()
	{
		$form = new Attribute_Form_Attribute();
		return $form;
	}

	protected function _getGrid()
	{
		$grid = $this->_initDefaultGrid();

		$categoryModel = new Attribute_Model_Attribute();
		$select = $categoryModel->getAll();
		$grid->setSource(new Bvb_Grid_Source_Zend_Select($select));

		$grid->updateColumn('id', array(
			'title' => 'Номер',
			'position' => 1,
			'style' => 'width:5%;text-align:center;',
			'searchType' => '=',
		));
		$grid->updateColumn('name', array(
			'title' => 'Название',
			'position' => 2,
			'style' => 'width:85%;text-align:left;',
			'callback' => array(
				'function' => array($this, 'editLinkCallback'),
				'params' => array('{{id}}', '{{name}}')
			),
		));
		$grid->updateColumn('is_active', array(
			'title' => 'Активен',
			'position' => 3,
			'style' => 'width:10%;text-align:center;',
			'callback' => array(
				'function' => array($this, 'isActiveCallback'),
				'params' => array('{{is_active}}')
			)
		));

		$grid->setTableGridColumns(array('id', 'name', 'is_active', 'Операции'));

		return $grid;
	}

	public function isActiveCallback($value)
	{
		$label = ($value) ? 'Да': 'Нет';
		$class = ($value) ? 'success': 'danger';

		return '<span class="label label-' . $class . '">' . $label . '</span>';
	}
	public function editLinkCallback($id, $name)
	{
		return "<a href='/attribute/index/edit/id/" . $id ."'>" . $name . "</a>";
	}
}
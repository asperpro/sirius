<?php

class Attribute_SetController extends Platon_Controller_Action
{
    public function indexAction()
    {
    	$grid = $this->_getGrid();

		$this->view->grid = $grid->deploy();
        $this->view->title = 'Управление наборами атрибутов';
    }

	protected function _getGrid()
	{
		$grid = $this->_initDefaultGrid();

		$categoryModel = new Attribute_Model_Attribute_Set();
		$select = $categoryModel->getAll();
		$grid->setSource(new Bvb_Grid_Source_Zend_Select($select));

		$grid->updateColumn('id', array(
			'title' => 'Номер',
			'position' => 1,
			'style' => 'width:5%;text-align:center;',
			'searchType' => '=',
		));
		$grid->updateColumn('name', array(
			'title' => 'Название',
			'position' => 2,
			'style' => 'width:90%;text-align:left;'
		));

		$grid->setTableGridColumns(array('id', 'name', 'Операции'));

		return $grid;
	}
}
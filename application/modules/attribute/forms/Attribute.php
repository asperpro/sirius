<?php

class Attribute_Form_Attribute extends EasyBib_Form
{
    public function init()
    {
        // form config
        $this->setMethod('POST');
        $this->setAttrib('class', 'form-horizontal');
        $this->addPrefixPath('Platon_JQuery_Form','Platon/JQuery/Form');
        $this->addPrefixPath('ZendX_JQuery_View_Helper','ZendX/JQuery/View/Helper');

        // create elements

        $name   = new Zend_Form_Element_Text('name');
        $code   = new Zend_Form_Element_Text('code');
        $type   = new Zend_Form_Element_Text('type');
        $order  = new Zend_Form_Element_Select('sort_order');
        $status = new Zend_Form_Element_Select('is_active');
        $submit = new Zend_Form_Element_Button('submit');

        $priority = array();
        for ($i = 0; $i <= 10; $i++) {
            $priority[] = $i;
        }

        $name->setLabel('Название:')
            ->setRequired(true)
            ->addValidator('NotEmpty')
            ->setAttrib('disabled', 'disabled');

        $code->setLabel('Код:')
            ->setRequired(true)
            ->addValidator('NotEmpty')
            ->setAttrib('disabled', 'disabled');

        $type->setLabel('Тип:')
            ->setRequired(true)
            ->addValidator('NotEmpty')
            ->setAttrib('disabled', 'disabled');

        $order->setLabel('Приоритет:')
            ->setMultiOptions($priority);

        $status->setLabel('Статус:')
            ->setMultiOptions(array(
                0 => 'не активный',
                1 => 'активный'
            ));

        $submit->setLabel('Сохранить')
            ->setAttrib('type', 'submit');

        // add elements
        $this->addElements(array(
            $name, $code, $type, $order, $status, $submit
        ));

        // set decorators
        EasyBib_Form_Decorator::setFormDecorator($this, EasyBib_Form_Decorator::BOOTSTRAP, 'submit');

    }

}
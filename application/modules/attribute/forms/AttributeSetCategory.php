<?php

class Attribute_Form_AttributeSetCategory extends EasyBib_Form
{
    public function init()
    {
        // form config
        $this->setMethod('POST');
        $this->setAttrib('class', 'form-horizontal');
        $this->addPrefixPath('Platon_JQuery_Form','Platon/JQuery/Form');
        $this->addPrefixPath('ZendX_JQuery_View_Helper','ZendX/JQuery/View/Helper');

        $attributeSetModel = new Attribute_Model_Attribute_Set();
        $sets = $attributeSetModel->getAllSets();

        // create elements
        $categories  = new ZendX_JQuery_Form_Element_AutoComplete('categories');
        $categId     = new Zend_Form_Element_Hidden('category_id');
        $attribSet   = new Zend_Form_Element_Select('attribute_set_id');
        $submit      = new Zend_Form_Element_Button('submit');

        $categories
            ->setJQueryParam('url', '/catalog/index/autocomplete')
            ->setJQueryParam('select', new Zend_Json_Expr("function(event, ui){jQuery('#category_id').val(ui.item.id);}"))
            ->setJQueryParam('dataType', 'json')
            ->setJQueryParam('minLength', '2')
            ->setAttrib('placeholder', 'Категория')
            ->setAttrib('class', 'form-control')
            ->addFilter('StripTags')
            ->addFilter('StringTrim');

        $attribSet->setMultiOptions($sets);

        $submit->setLabel('Сохранить')
            ->setAttrib('type', 'submit');

        // add elements
        $this->addElements(array(
           $categories, $categId, $attribSet, $submit
        ));

        // set decorators
        EasyBib_Form_Decorator::setFormDecorator($this, EasyBib_Form_Decorator::BOOTSTRAP, 'submit');

    }

}
<?php

class Catalog_IndexController extends Platon_Controller_Action
{
    public function indexAction()
    {
    	$grid = $this->_getGrid();
        
		$this->view->grid = $grid->deploy();
        $this->view->title = 'Категории';
    }

   	public function autocompleteAction()
	{
		header('Content-Type: text/html; charset=utf-8');
		
		$data = array();
		$term = $this->_request->getParam('term');
		$categoryModel = new Catalog_Model_Category();
		if ($result = $categoryModel->search($term, 'full')) {
			foreach ($result as $value) {
				$data[] = array(
					'id' 	=> $value['id'],
					'label' => $value['name'],
					'value' => $value['name'],
				);
			}
		}
		$this->_helper->json(array_values($data));
		exit();
	}
    
    protected function _getGrid()
    {
    	$grid = $this->_initDefaultGrid();
    	
        $categoryModel = new Catalog_Model_Category();
        $select = $categoryModel->getAll();
    	$grid->setSource(new Bvb_Grid_Source_Zend_Select($select));
    	
    	$grid->updateColumn('id', array(
    		'title' => 'Номер',
    		'position' => 1,
    		'style' => 'width:5%;text-align:center;',
    		'searchType' => '=',
    	));
    	$grid->updateColumn('name', array(
    		'title' => 'Название',
    		'position' => 2,
    		'style' => 'width:65%;text-align:left;',
    	));
		$grid->updateColumn('is_active', array(
			'title' => 'Активна',
			'position' => 3,
			'style' => 'width:10%;text-align:center;',
			'callback' => array(
				'function' => array($this, 'isActiveCallback'),
				'params' => array('{{is_active}}')
			)
		));
		$grid->updateColumn('imported_date', array(
			'title' => 'Дата импорта',
			'position' => 4,
			'style' => 'width:15%;text-align:center;',
			'format' => array(
    			'date',
    			array('date_format' => 'dd.MM.yyyy H:m')
    		)
		));
		
    	$grid->setTableGridColumns(array('id', 'name', 'is_active', 'imported_date'));
        
		return $grid;
    }
    
    public function isActiveCallback($value)
    {
		$label = ($value) ? 'Активна': 'Не активна';
		$class = ($value) ? 'success': 'danger';
    	
		return '<span class="label label-' . $class . '">' . $label . '</span>';
    }
}
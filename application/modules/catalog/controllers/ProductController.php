<?php

class Catalog_ProductController extends Platon_Controller_Action
{
	static $photoUploadDir;
	static $photoUploadUrl;

	static protected $_statusesOptions = array(
		'pending'   => 'Ожидается',
		'prepared'  => 'Подготовлен',
		'completed' =>  'Завершён',
		'archived'  =>  'Архивный'
	);

	function init()
	{
		$this->view->headLink()->appendStylesheet('/public/css/uploadify.css');
		$this->view->headScript()->appendFile('/public/js/product.js');
		$this->view->headScript()->appendFile('/public/js/jquery.uploadify.min.js');

		self::$photoUploadDir = APPLICATION_PATH . '/../media/product/';
		self::$photoUploadUrl = $this->getRequest()->getBaseUrl() . '/media/product/';
	}

	public function indexAction()
    {
    	$grid = $this->_getGrid();
        
		$this->view->grid = $grid->deploy();
        $this->view->title = 'Товары';
    }

	public function addAction()
	{
		$form = $this->_getForm();
		try {
			if ($this->getRequest()->isPost()) {
				$formData = $this->_request->getPost();

				$productModel           = new Catalog_Model_Product();
				$productAttributesModel = new Catalog_Model_Product_Attributes();
				$productCategoriesModel = new Catalog_Model_Category_Products();
				$productMediasModel     = new Catalog_Model_Product_Medias();
				$logEventModel 			= new System_Model_Log();

				if ($form->isValid($formData)) {
					$data = $form->getValues();
					unset($data['categories']);
					unset($data['categ_id']);
					$data['price'] = (int)$data['price'];
					$productId = $productModel->add($data);
				}

				if (isset($productId)) {

					// Log accept revision event
					$eventValues = array(
						'event' => System_Model_Log::EVENT_CREATE_PRODUCT,
						'entity_id' => $productId,
					);
					$logEventModel->add($eventValues);
					
					//save all attributes
					foreach (array_keys($formData) as  $attribute) {
						if (strpos($attribute, 'attributes') !== false) {

							$options = is_array($formData[$attribute]) ? $formData[$attribute] : array($formData[$attribute]);
							foreach ($options as $option) {
								$values = array(
									'product_id'   => $productId,
									'attribute_id' => substr($attribute, 10),
									'value'        => $option
								);
								$productAttributesModel->add($values);
							}
						}
					}

					//save all categories
					foreach (array_keys($formData) as  $category) {
						if (strpos($category, 'category') !== false) {
							$values = array(
								'product_id'  => $productId,
								'category_id' => substr($category, 8),
							);
							$productCategoriesModel->add($values);
						}
					}

					// Save all media
					if (isset($formData['media'])) {
						$mainMediaId = $formData['media']['is_main'];
						unset($formData['media']['is_main']);
						foreach ($formData['media'] as $media) {
							$values = array(
								'product_id'  => $productId,
								'media_id'    => $media['id'],
								'is_main'	  => ($mainMediaId == $media['id']) ? 1 : 0,
							);
							$productMediasModel->add($values);
						}
					}
				}

				$this->_helper->flashMessenger(array('success' => 'Товар успешно добавлен'));
				$this->_redirect('/catalog/product');
			}
		} catch (Platon_Exception_Form $e) {
			$form->nomenclature->addError($e->getMessage());
		} catch (Platon_Exception $e) {
			$this->_helper->flashMessenger(array('error' => $e->getMessage()));
			$this->_redirect('/catalog/product');
		}

		$this->view->form = $form;
		$this->view->title = 'Добавление товара';
	}

	public function editAction()
	{
		$productId = (int)$this->_request->getParam('id');
		$productModel = new Catalog_Model_Product();
		$logEventModel = new System_Model_Log();

		$form = $this->_getForm();
		try {

			if (!$product = $productModel->find($productId)) {
				throw new Platon_Exception('Товар не найден');
			}

			if ($product['status'] == "archived") {
				throw new Platon_Exception('Нельзя редактировать архивный товар');
			}

			if ($this->getRequest()->isPost()) {
				$formData = $this->_request->getPost();

				$productAttributesModel = new Catalog_Model_Product_Attributes();
				$productCategoriesModel = new Catalog_Model_Category_Products();
				$productMediasModel     = new Catalog_Model_Product_Medias();

				if ($form->isValid($formData)) {
					$data = $form->getValues();
					unset($data['categories']);
					unset($data['categ_id']);

					$data['price'] = (int)$data['price'];
					$productModel->update($data, $productId);
				}

				if (isset($productId)) {
					
					// Log accept revision event
					$eventValues = array(
						'event' => System_Model_Log::EVENT_CHANGE_PRODUCT,
						'entity_id' => $productId,
					);
					$logEventModel->add($eventValues);

					//delete previews categories and attributes
					$productAttributesModel->deleteByProdId($productId);
					$productCategoriesModel->deleteByProdId($productId);
					$productMediasModel->deleteByProdId($productId);

					//save all attributes
					foreach (array_keys($formData) as  $attribute) {
						if (strpos($attribute, 'attributes') !== false) {

							$options = is_array($formData[$attribute]) ? $formData[$attribute] : array($formData[$attribute]);
							foreach ($options as $option) {

								if ($option !== '') {
									$values = array(
										'product_id'   => $productId,
										'attribute_id' => substr($attribute, 10),
										'value'        => $option
									);
									$productAttributesModel->add($values);
								}
							}
						}
					}

					//save all categories
					foreach (array_keys($formData) as  $category) {
						if (strpos($category, 'category') !== false) {
							$values = array(
								'product_id'  => $productId,
								'category_id' => substr($category, 8),
							);
							if ($formData[$category] == '1') $productCategoriesModel->add($values);
						}
					}

					// Save all media
					if (isset($formData['media'])) {
						$mainMediaId = $formData['media']['is_main'];
						unset($formData['media']['is_main']);
						foreach ($formData['media'] as $media) {
							$values = array(
								'product_id'  => $productId,
								'media_id'    => $media['id'],
								'is_main'	  => ($mainMediaId == $media['id']) ? 1 : 0,
							);
							$productMediasModel->add($values);
						}
					}
				}

				$this->_helper->flashMessenger(array('success' => 'Товар успешно изменен'));
				$this->_redirect('/catalog/product');
			}
			$form->populate($product);
		} catch (Platon_Exception_Form $e) {
			$form->nomenclature->addError($e->getMessage());
		} catch (Platon_Exception $e) {
			$this->_helper->flashMessenger(array('error' => $e->getMessage()));
			$this->_redirect('/catalog/product');
		}

		$this->view->form = $form;
		$this->view->id = $productId;
		$this->view->title = 'Редактирование товара';
	}

	public function loadAction()
	{
		$productModel = new Catalog_Model_Product();
		$logEventModel = new System_Model_Log();
		$form = new Catalog_Form_ProductXls();

		try {
			if ($this->getRequest()->isPost()) {
				$formData = $this->_request->getPost();
				if ($form->isValid($formData)) {
					$data = $form->getValues();

					if (!empty($_FILES['xls_list'])) {

						$file = pathinfo($_FILES['xls_list']['name']);

						if (!in_array($file['extension'], array('xls', 'xlsx'))) {
							throw new Exception('Неверное расширение файла');
						}

						$objReader = PHPExcel_IOFactory::createReaderForFile($_FILES['xls_list']['tmp_name']);
						$objReader->setReadDataOnly(true);
						$objPHPExcel = $objReader->load($_FILES['xls_list']['tmp_name']);

						$list =  $objPHPExcel->setActiveSheetIndex(0);

						$highestRow         = $list->getHighestRow();
						$highestColumn      = $list->getHighestColumn();
						$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);

						for ($row = 1; $row <= $highestRow; ++ $row) {
							$dataExel = array();
							for ($col = 0; $col < $highestColumnIndex; ++ $col) {
								$cell   = $list->getCellByColumnAndRow($col, $row);
								$dataExel[] = $cell->getValue();
							}
							$values = array(
								'sku'         => $dataExel[0],
								'name'        => $dataExel[1],
								'alias'       => $dataExel[1],
								'price'       => $dataExel[2],
								'supplier_id' => $data['supplier_id']
							);
							if ($values['sku'] && $values['alias']) {
								if ($productId = $productModel->import($values)) {
									// Log accept revision event
									$eventValues = array(
										'event' => System_Model_Log::EVENT_CREATE_PRODUCT,
										'entity_id' => $productId,
									);
									$logEventModel->add($eventValues);	
								}
							}
						}

						$objPHPExcel->disconnectWorksheets();
						unset($objPHPExcel);
					}

				}
				$this->_helper->flashMessenger(array('success' => 'Список товаров успешно загружен'));
				$this->_redirect('/catalog/product');
			}
		} catch (Platon_Exception_Form $e) {
			$form->nomenclature->addError($e->getMessage());
		} catch (Platon_Exception $e) {
			$this->_helper->flashMessenger(array('error' => $e->getMessage()));
			$this->_redirect('/catalog/product');
		}

		$this->view->form = $form;
		$this->view->title = 'Загрузить список товаров';
	}

	public function getSetAttributesAction()
	{
		header('Content-Type: text/html; charset=utf-8');
		
		$fields = '<div class="list-group">';
		$prodId = (int)$this->_request->getParam('prodid');
		$setId = (int)$this->_request->getParam('setid');

		$ajaxContext = $this->_helper->getHelper('AjaxContext');
		$ajaxContext->addActionContext('getSetAttributes', 'html')->initContext();

		$attributeSetAttributesModel = new Attribute_Model_Attribute_Set_Attributes();
		$attributesModel             = new Attribute_Model_Attribute();
		$attributeOptionsModel       = new Attribute_Model_Attribute_Options();
		$productAttributeModel       = new Catalog_Model_Product_Attributes();

		$attributes = $attributeSetAttributesModel->getBySet($setId);

		foreach ($attributes as $attribute) {

			if ($productAttribute = $productAttributeModel->getByAttribIdAndProdId($prodId, $attribute['attribute_id'])) {
				if (count($productAttribute) == 1) $productAttribute = array_shift($productAttribute);
			}

			$attrInfo = $attributesModel->find($attribute['attribute_id']);

			switch ($attrInfo['type']) {
				case 'select':
					$options = $attributeOptionsModel->getOptions($attribute['attribute_id']);
					$options[""] = '-Выберите значение';
					asort($options);
					$element = new Zend_Form_Element_Select('attributes-' . $attribute['attribute_id']);
					$element->setLabel($attrInfo['name'])
						->setMultiOptions($options);
					if ($productAttribute) {
						$element
							->setValue($productAttribute);
					}
					$element->setAttrib('class', 'form-control input-sm')
						->setDecorators(array(
							'ViewHelper',
							array('Label', array('class' => 'control-label')),
							array(array('list-group-item' => 'HtmlTag'), array('tag' => 'div', 'class' => 'list-group-item')),
						));
					$fields .= $element->__toString();
					break;
				case 'multiselect':

					$options = $attributeOptionsModel->getOptions($attribute['attribute_id']);
					$element = new Zend_Form_Element_Multiselect('attributes-' . $attribute['attribute_id']);
					$element->setLabel($attrInfo['name'])
						->setMultiOptions($options);
					if ($productAttribute) {
						$element
							->setValue($productAttribute);
					}
					$element->setAttrib('class', 'form-control input-sm')
						->setDecorators(array(
							'ViewHelper',
							array('Label', array('class' => 'control-label')),
							array(array('list-group-item' => 'HtmlTag'), array('tag' => 'div', 'class' => 'list-group-item')),
						));
					$fields .= $element->__toString();

					break;
				case 'boolean':
					$options = array(
						0 => 'нет',
						1 => 'да'
					);
					$element = new Zend_Form_Element_Select('attributes-' . $attribute['attribute_id']);
					$element->setLabel($attrInfo['name'])
							->setMultiOptions($options);
					if ($productAttribute) {
						$element
							->setValue($productAttribute);
					}
					$element->setAttrib('class', 'form-control input-sm')
						->setDecorators(array(
							'ViewHelper',
							array('Label', array('class' => 'control-label')),
							array(array('list-group-item' => 'HtmlTag'), array('tag' => 'div', 'class' => 'list-group-item')),
						));
					$fields .= $element->__toString();

					break;
				case 'text':

					$element = new Zend_Form_Element_Text('attributes-' . $attribute['attribute_id']);
					$element->setLabel($attrInfo['name']);
					if ($productAttribute) {
						$element
							->setValue($productAttribute);
					}
					$element->setAttrib('class', 'form-control input-sm')
						->setDecorators(array(
							'ViewHelper',
							array('Label', array('class' => 'control-label')),
							array(array('list-group-item' => 'HtmlTag'), array('tag' => 'div', 'class' => 'list-group-item')),
						));
					$fields .= $element->__toString();

					break;
			}
		}
		$fields .= '</div>';
		echo $fields; die;
	}

	public function getCategoryAction()
	{
		header('Content-Type: text/html; charset=utf-8');
		
		$fields = '';
		$prodId  = (int)$this->_request->getParam('prodid');

		$ajaxContext = $this->_helper->getHelper('AjaxContext');
		$ajaxContext->addActionContext('getsetattributes', 'html')->initContext();

		$categoryModel        = new Catalog_Model_Category();
		$categoryProductModel = new Catalog_Model_Category_Products();

		if ($prodId) {

			$productCategories = $categoryProductModel->getByProdId($prodId);
			foreach ($productCategories as $categoryId) {
				$category = $categoryModel->find($categoryId);
				$element = new Zend_Form_Element_Checkbox('category-' . $category['id']);
				$element->setLabel($category['full_name'])
					->setAttrib('checked', 'checked')
					->setDecorators(array(
						'ViewHelper',
						array(array('checkbox' => 'HtmlTag'), array('tag' => 'div', 'style' => 'float:right;')),
						array('Label', array('class' => 'control-label')),
						array(array('list-group-item' => 'HtmlTag'), array('tag' => 'div', 'class' => 'list-group-item')),
					));
				$fields .= $element->__toString();
			}
		}

		echo $fields;
		die;
	}

	public function getMediaAction()
	{
		$mediaId = $this->_request->getParam('media_id');
		$mediaModel = new Catalog_Model_Media();

		if ($mediaData = $mediaModel->find($mediaId)) {
			$field = $this->_createMediaElement($mediaData);
			echo $field;
			exit();
		}
	}

	public function loadMediaAction()
	{
		$productId = (int) $this->_request->getParam('product_id');

		$mediaModel         = new Catalog_Model_Media();
		$productMediasModel = new Catalog_Model_Product_Medias();

		$productMedia = $productMediasModel->getByProdId($productId);

		$field = '<table class="table">';
		if ($productMedia) {
			foreach ($productMedia as $key => $media) {
				$field .= $this->_createMediaElement($media);
			}
		}
		$field .= '</table>';
		echo $field;
		exit();
	}

	public function uploadifyAction()
	{
		$productId = $this->_request->getParam('id');

		$mediaModel = new Catalog_Model_Media();

		if (!empty($_FILES)) {
			try {
				$file = pathinfo($_FILES['Filedata']['name']);
				if (!in_array(strtolower($file['extension']), array('jpeg', 'jpg', 'png'))) {
					throw new Exception('Неверное расширение файла');
				}

				//Set file name
				$fileName = strtolower(str_replace(array(' ', '.', '/', '-', '&'), '_', $file['filename']));
				$fileName = md5($fileName . uniqid() . @mktime()) . '.' . $file['extension'];

				// Create file path

				$mediaPath =  self::$photoUploadDir;
				$path = '';
				$chars = str_split(substr($fileName, 0, 2));

				foreach ($chars as $char) {
					$path .= $char . '/';
					if (!is_dir($mediaPath . $path)) {
						mkdir($mediaPath . $path);
					}
				}

				//Save file
				$uploadFile = $mediaPath . $path . $fileName;
				move_uploaded_file($_FILES['Filedata']['tmp_name'], $uploadFile);

				switch ($file['extension']) {
					case 'png':
						$mimeType = 'image/png';
						break;
					case 'jpg':
					case 'jpeg':
					default:
						$mimeType = 'image/jpeg';
				}
				
				// Trim the image
				$imagick = new Imagick($uploadFile);
				$imagick->trimImage(0);
				$imagick->writeImage($uploadFile);
				
				$values = array(
					'filename' => $path . $fileName,
					'mime_type' => $mimeType,
				);

				$mediaId = $mediaModel->add($values);

				$result['status'] = 'success';
				$result['id'] = $mediaId;

			} catch (Exception $e) {
				$result['status'] = 'error';
				$result['message'] = $e->getMessage();
			}

			$this->_helper->json($result, true);
		}
		exit();
	}

	protected function _createMediaElement($data)
	{
		$result = '<tr>' .
			'<td><input type="checkbox" name="media[' . $data['id'] . '][id]" id="media' . $data['id'] . '" value="' . $data['id'] . '" checked="checked"></td>' .
			'<td>
				<label for="media' . $data['id'] . '">' .
					'<a class="fancybox-thumbs" data-fancybox-group="gallery" href="' . self::$photoUploadUrl . $data['filename'] . '">
						<img class="thumb-image" src="' . self::$photoUploadUrl . $data['filename'] . '">
					</a>' .
				'</label>
			</td>' .
			'<td><input type="radio" name="media[is_main]" value="' . $data['id'] . '" ' . (!empty($data['is_main']) ? ' checked="checked"' : '') . ' /></td>' .
			'</tr>';

		return $result;
	}

	protected function _getForm()
	{
		$form = new Catalog_Form_Product();

		return $form;
	}
    
    protected function _getGrid()
    {
    	$grid = $this->_initDefaultGrid();
    	
        $categoryModel = new Catalog_Model_Product();
        $select = $categoryModel->getAll();
    	$grid->setSource(new Bvb_Grid_Source_Zend_Select($select));
    	
    	$grid->updateColumn('id', array(
    		'title' => 'Номер',
    		'position' => 1,
    		'style' => 'width:5%;text-align:center;',
    		'searchType' => '=',
    	));
    	$grid->updateColumn('name', array(
    		'title' => 'Название',
    		'position' => 2,
    		'style' => 'width:30%;text-align:left;',
			'callback' => array(
				'function' => array($this, 'editLinkCallback'),
				'params' => array('{{id}}', '{{name}}', '{{status}}')
			),
    	));
    	$grid->updateColumn('sku', array(
    		'title' => 'Артикул',
    		'position' => 3,
    		'style' => 'width:5%;text-align:left;',
    	));
    	$grid->updateColumn('supplier_name', array(
    		'title' => 'Поставщик',
    		'position' => 4,
    		'style' => 'width:10%;text-align:left;',
    		'searchType' => 'sqlExp',
    		'searchSqlExp' => 'supplier_id = {{value}}',
    	));
    	$grid->updateColumn('set_name', array(
    		'title' => 'Набор атрибутов',
    		'position' => 5,
    		'style' => 'width:10%;text-align:left;',
    		'searchType' => 'sqlExp',
    		'searchSqlExp' => 'set_id = {{value}}',
    	));
		$grid->updateColumn('status', array(
			'title' => 'Статус',
			'position' => 6,
			'style' => 'width:10%;text-align:left;',
			'callback' => array(
				'function' => array($this, 'statusesCallback'),
				'params' => array('{{status}}')
			),
		));
		$grid->updateColumn('username', array(
			'title' => 'Пользователь',
			'position' => 7,
			'style' => 'width:10%;text-align:left;',
			'searchType' => 'sqlExp',
    		'searchSqlExp' => 'user_id = {{value}}',
		));
		$grid->updateColumn('attempts', array(
			'title' => 'Кол-во итераций',
			'position' => 8,
			'style' => 'width:5%;text-align:center;',
		));
		$grid->updateColumn('created_date', array(
    		'title' => 'Дата создания',
    		'position' => 9,
    		'style' => 'width:15%;text-align:left;',
    		'format' => array(
    			'date',
    			array('date_format' => 'dd.MM.yyyy H:m')
    		)
    	));

    	$supplierModel = new Reference_Model_Supplier();
    	$suppliers = $supplierModel->getAllSuppliers();
    	
    	$attributeSetModel = new Attribute_Model_Attribute_Set();
    	$attributeSets = $attributeSetModel->getAllSets();
    	
    	$userModel = new Auth_Model_User();
    	$users = $userModel->getAllUsers();
    	
		$filters = new Bvb_Grid_Filters();
		$filters->addFilter('status', array('values' => self::$_statusesOptions));
    	$filters->addFilter('supplier_name', array('values' => $suppliers));
    	$filters->addFilter('set_name', array('values' => $attributeSets));
    	$filters->addFilter('username', array('values' => $users));
    	$filters->addFilter('created_date', array('render' => 'date'));
		$grid->addFilters($filters);

    	$grid->setTableGridColumns(array('id', 'name', 'sku', 'supplier_name', 'set_name', 'status', 'username', 'attempts', 'created_date'));
    	if (in_array(Zend_Auth::getInstance()->getIdentity()->role, array('admin', 'moderator'))) {
			$massActions = array(
	    		array(
			        'url' => '/catalog/product/delete',
			        'caption' => 'Удалить',
		        ),
		        array(
			        'url' => '/catalog/product/update/status/pending',
			        'caption' => 'Статус: Ожидается',
		        ),
		        array(
			        'url' => '/catalog/product/update/status/prepared',
			        'caption' => 'Статус: Подготовлен',
		        ),
		        array(
			        'url' => '/catalog/product/update/status/completed',
			        'caption' => 'Статус: Завершен',
		        ),
		        array(
			        'url' => '/catalog/product/update/status/archived',
			        'caption' => 'Статус: Архивный',
		        ),
                array(
                    'url' => '/catalog/product/revision/',
                    'caption' => 'Создать ревизию товара',
                ),
	    	);
	    	$actions = new Bvb_Grid_Mass_Actions();
	    	$actions->setMassActions($massActions);
	    	$grid->setMassActions($actions);
    	}

		return $grid;
    }

	public function editLinkCallback($id, $name, $status)
	{
		if ('archived' == $status) {
			return $name;
		}
		
		return "<a href='/catalog/product/edit/id/" . $id ."'>" . $name . "</a>";
	}

	public function statusesCallback($value)
	{
		if (isset(self::$_statusesOptions[$value])) {
			
			switch ($value) {
				case 'prepared':
					$class = 'success';
					break;
				case 'completed':
					$class = 'primary';
					break;
				case 'archived':
					$class = 'default';
					break;
				default:
					$class = 'warning';
					break;
			}
			
			return '<span class="label label-' . $class . '">' . self::$_statusesOptions[$value] . '</span>';
		}
	}
	
	public function deleteAction()
    {
    	$productModel = new Catalog_Model_Product();
    	try {
    		if (!in_array(Zend_Auth::getInstance()->getIdentity()->role, array('admin', 'moderator'))) {
    			throw new Platon_Exception('Ваши права ограничены');	
    		}
	    	if ($this->_request->isPost()) {
	            $productIds = $this->_request->getPost('postMassIdsgrid');
	            if (empty($productIds)) {
	            	throw new Platon_Exception('Нет выбранных записей');
	            }
	            $productIds = explode(',', $productIds);
            	$counter = 0;
            	foreach ($productIds as $id) {
            		if ($productModel->delete($id)) {
            			$counter ++;	
            		}
            	}
            	$this->_helper->flashMessenger(array('success' => 'Успешно удалено ' . $counter . ' товаров'));
	        } else {
	   			if (!$productId = (int)$this->_request->getParam('id')) {
	   				throw new Platon_Exception('Товар не найден');
	   			}
	   			$productModel->delete($productId);
	   			$this->_helper->flashMessenger(array('success' => 'Товар успешно удален'));
	        }
        } catch (Platon_Exception $e) {
        	$this->_helper->flashMessenger(array('error' => $e->getMessage()));
        }
		$this->_redirect('/catalog/product');
    }
    
    public function updateAction()
	{
		$status = $this->_request->getParam('status');
		$productModel = new Catalog_Model_Product();
		$values = array('status' => $status);
		
		try {
    		if (!in_array(Zend_Auth::getInstance()->getIdentity()->role, array('admin', 'moderator'))) {
    			throw new Platon_Exception('Ваши права ограничены');	
    		}
	    	if ($this->_request->isPost()) {
	            $productIds = $this->_request->getPost('postMassIdsgrid');
	            if (empty($productIds)) {
	            	throw new Platon_Exception('Нет выбранных записей');
	            }
	            $productIds = explode(',', $productIds);
            	$counter = 0;
            	foreach ($productIds as $id) {
            		if ($productModel->update($values, $id)) {
            			$counter ++;	
            		}
            	}
            	$this->_helper->flashMessenger(array('success' => 'Успешно обновлено ' . $counter . ' товаров'));
	        } else {
	   			if (!$productId = (int)$this->_request->getParam('id')) {
	   				throw new Platon_Exception('Товар не найден');
	   			}
	   			$productModel->update($values, $productId);
	   			$this->_helper->flashMessenger(array('success' => 'Товар успешно обновлен'));
	        }
        } catch (Platon_Exception $e) {
        	$this->_helper->flashMessenger(array('error' => $e->getMessage()));
        }		
		$this->_redirect('/catalog/product');
	}

	public function revisionAction()
    {
        // Init models
        $revisionModel = new Catalog_Model_Revision();
        $productModel = new Catalog_Model_Product();

        try {
            if (!in_array(Zend_Auth::getInstance()->getIdentity()->role, array('admin', 'moderator'))) {
                throw new Platon_Exception('Ваши права ограничены');
            }
            if ($this->_request->isPost()) {
                $productIds = $this->_request->getPost('postMassIdsgrid');
                if (empty($productIds)) {
                    throw new Platon_Exception('Нет выбранных записей');
                }
                $productIds = explode(',', $productIds);
                $counter = 0;
                foreach ($productIds as $id) {
                    // Get product data
                    $product = $productModel->find($id);
                    // Save revision
                    $values = array(
                        'product_id' => $id,
                        'interface_id' => 0,
                        'set_id' => 0,
                        'weight' => $product['weight'],
                        'name' => $product['name'],
                        'price' => $product['price'],
                        'user_id' => $revisionModel->getRevisionUser(),
                    );
                    $revisionModel->add($values);
                    // Update product status
                    $values = array(
                        'status' => 'prepared',
                    );
                    if ($productModel->update($values, $id)) {
                        $counter++;
                    }
                }
                $this->_helper->flashMessenger(array('success' => 'Успешно создано ' . $counter . ' ревизий товаров'));
            } else {
                if (!$productId = (int)$this->_request->getParam('id')) {
                    throw new Platon_Exception('Товар не найден');
                }
                $productModel->update($values, $productId);
                $this->_helper->flashMessenger(array('success' => 'Ревизия товара успешно создана'));
            }
        } catch (Platon_Exception $e) {
            $this->_helper->flashMessenger(array('error' => $e->getMessage()));
        }
        $this->_redirect('/catalog/product');
    }
}
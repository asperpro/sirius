<?php

class Catalog_RevisionController extends Platon_Controller_Action
{

	static $photoUploadDir;
	static $photoUploadUrl;

	function init()
	{
		$this->view->headLink()->appendStylesheet('/public/css/uploadify.css');
		$this->view->headScript()->appendFile('/public/js/revision.js');
		$this->view->headScript()->appendFile('/public/js/jquery.uploadify.min.js');

		self::$photoUploadDir = APPLICATION_PATH . '/../media/product/';
		self::$photoUploadUrl = $this->getRequest()->getBaseUrl() . '/media/product/';
	}

	public function indexAction()
    {
        $this->view->title = 'Модерация ревизий товаров';

        if ($this->getRequest()->isXmlHttpRequest()) {
        	header('Content-Type: text/html; charset=utf-8');
	        $revisionModel          = new Catalog_Model_Revision();
			$revisionCategoryModel  = new Catalog_Model_Revision_Category();
			$revisionMediaModel     = new Catalog_Model_Revision_Media();
			$attributeSetModel      = new Attribute_Model_Attribute_Set();

			$productId = $revisionModel->getLastRevisionProduct();
			$data = $revisionModel->getUserRevisionByProduct($productId);
			$revisions = $data;
			foreach ($data as $key => $revision) {
				$revisions[$key]['categories'] = $revisionCategoryModel->getByRevisionId($revision['id']);
				$revisions[$key]['sets']       = $attributeSetModel->getAllSets();
				$revisions[$key]['media']      = $revisionMediaModel->getByRevisionId($revision['id']);
				$revisions[$key]['attributes'] = $this->_getRevisionAttributes($revision['set_id'], $revision['id']);
			}
			unset($data);

			$this->view->data = $revisions;
			$this->_helper->layout()->disableLayout();
			$template = (count($revisions) > 1) ? 'items.phtml' : 'item.phtml';
			echo $this->view->render('revision/' . $template);
			exit();
        }
    }

	public function saveAction()
	{
		$result = array();
		try {
			if ($this->getRequest()->isPost()) {
				$formData = $this->_request->getPost();

				$revisionModel          = new Catalog_Model_Revision();
				$productModel           = new Catalog_Model_Product();
				$productAttributesModel = new Catalog_Model_Product_Attributes();
				$productCategoriesModel = new Catalog_Model_Category_Products();
				$productMediasModel     = new Catalog_Model_Product_Medias();
				$logEventModel			= new System_Model_Log();

				//Save product data
				$productData = array(
					'status' => 'completed'
				);

				$revisionId = (int)$formData['id'];
				if ($revisionId) {
					if (isset($formData['set'][$revisionId]['checked'])) $productData['set_id'] = $formData['set'][$revisionId]['value'];
					if (isset($formData['name'][$revisionId]['checked'])) $productData['name'] = $formData['name'][$revisionId]['value'];
					if (isset($formData['price'][$revisionId]['checked'])) $productData['price'] = $formData['price'][$revisionId]['value'];
					if (isset($formData['weight'][$revisionId]['checked'])) $productData['weight'] = $formData['weight'][$revisionId]['value'];
					if (isset($formData['description'][$revisionId]['checked'])) $productData['description'] = trim($formData['description'][$revisionId]['value']);
				}

				$productModel->update($productData, $formData['product_id']);
				unset($productData);
				
				// Log accept revision event
				$eventValues = array(
					'event' => System_Model_Log::EVENT_ACCEPT_REVISION,
					'entity_id' => $formData['product_id'],
				);
				$logEventModel->add($eventValues);

				//Save all categories
				if (is_array($formData['category'])) {
					foreach ($formData['category'] as  $categories) {
						foreach ($categories as $category) {
							if (isset($category['checked'])) {
								$values = array(
									'product_id'  => $formData['product_id'],
									'category_id' => $category['value'],
								);
								$productCategoriesModel->add($values);
							}
						}
					}
				}

				//Save all attributes
				if (is_array($formData['attribute'])) {
					foreach($formData['attribute'] as $attributes) {
						foreach ($attributes as $attribute) {
							if (isset($attribute['checked']) && isset($attribute['value']) && !empty($attribute['value'])) {
								$options = is_array($attribute['value']) ? $attribute['value'] : array($attribute['value']);
								foreach ($options as $option) {
									$values = array(
										'product_id' => $formData['product_id'],
										'attribute_id' => $attribute['id'],
										'value' => $option,
									);
									$productAttributesModel->add($values);
								}
							}
						}
					}
				}

				// Save all media
				if (is_array($formData['media'])) {
					$mainImage= explode('-', $formData['media']['main']);
					unset($formData['media']['main']);

					foreach ($formData['media'] as $revision => $medias) {
						foreach ($medias as $key => $media) {
							if (isset($media['checked'])) {
								$values = array(
									'product_id' => $formData['product_id'],
									'media_id'   => $media['value'],
									'is_main'    => (($revision == $mainImage[0]) && ($key == $mainImage[1])) ? 1 : 0,
								);

								$productMediasModel->add($values);
							}
						}
					}
				}
				$revisionModel->deleteById($formData['id']);

				$result['status'] = 'success';
			}

		} catch (Platon_Exception $e) {
			$result['status'] = 'error';
			$result['message'] = $e->getMessage();
		}

		$this->_helper->json($result, true);
		exit();
	}

	public function saveMultiplyAction()
	{
		$result = array();
		try {
			if ($this->getRequest()->isPost()) {
				$formData = $this->_request->getPost();

				$revisionModel          = new Catalog_Model_Revision();
				$productModel           = new Catalog_Model_Product();
				$productAttributesModel = new Catalog_Model_Product_Attributes();
				$productCategoriesModel = new Catalog_Model_Category_Products();
				$productMediasModel     = new Catalog_Model_Product_Medias();
				$logEventModel			= new System_Model_Log();

				$productData = array(
					'status' => 'completed'
				);

				//Get name
				if (isset($formData['name']) && is_array($formData['name'])) {
					if (isset($formData['name']['checked'])) {
						$productData['name'] = $formData['name'][$formData['name']['checked']]['value'];
					}
				}

				//Get price
				if (isset($formData['price']) && is_array($formData['price'])) {
					if (isset($formData['price']['checked'])) {
						$productData['price'] = $formData['price'][$formData['price']['checked']]['value'];
					}
				}

				//Get weight
				if (isset($formData['weight']) && is_array($formData['weight'])) {
					if (isset($formData['weight']['checked'])) {
						$productData['weight'] = $formData['weight'][$formData['weight']['checked']]['value'];
					}
				}

				//Get description
				if (isset($formData['description']) && is_array($formData['description'])) {
					if (isset($formData['description']['checked'])) {
						$productData['description'] = $formData['description'][$formData['description']['checked']]['value'];
					}
				}

				//Get set
				if (isset($formData['set']) && is_array($formData['set'])) {
					if (isset($formData['set']['checked'])) {
						$productData['set_id'] = $formData['set'][$formData['set']['checked']]['value'];
					}
				}

				//Save product data
				$productModel->update($productData, $formData['product_id']);
				unset($productData);

				// Log accept revision event
				$eventValues = array(
					'event' => System_Model_Log::EVENT_ACCEPT_REVISION,
					'entity_id' => $formData['product_id'],
				);
				$logEventModel->add($eventValues);

				//Save all categories
				if (is_array($formData['category'])) {
					foreach ($formData['category'] as  $categories) {
						foreach ($categories as $category) {
							if (isset($category['checked'])) {
								$values = array(
									'product_id'  => $formData['product_id'],
									'category_id' => $category['value'],
								);
								$productCategoriesModel->add($values);
							}
						}
					}
				}

				//Save all attributes
				if (is_array($formData['attribute'])) {
					foreach($formData['attribute'] as $attributes) {
						foreach ($attributes as $attribute) {
							if (isset($attribute['checked']) && isset($attribute['value']) && !empty($attribute['value'])) {
								$options = is_array($attribute['value']) ? $attribute['value'] : array($attribute['value']);
								foreach ($options as $option) {
									$values = array(
										'product_id' => $formData['product_id'],
										'attribute_id' => $attribute['id'],
										'value' => $option,
									);
									$productAttributesModel->add($values);
								}
							}
						}
					}
				}

				// Save all media
				if (is_array($formData['media'])) {
					$mainImage= explode('-', $formData['media']['main']);
					unset($formData['media']['main']);

					foreach ($formData['media'] as $revision => $medias) {
						foreach ($medias as $key => $media) {
							if (isset($media['checked'])) {
								$values = array(
									'product_id' => $formData['product_id'],
									'media_id'   => $media['value'],
									'is_main'    => (($revision == $mainImage[0]) && ($key == $mainImage[1])) ? 1 : 0,
								);

								$productMediasModel->add($values);
							}
						}
					}
				}

				$revisionModel->deleteByProductId($formData['product_id']);

				$result['status'] = 'success';
			}

		} catch (Platon_Exception $e) {
			$result['status'] = 'error';
			$result['message'] = $e->getMessage();
		}

		$this->_helper->json($result, true);
		exit();
	}

	public function cancelAction()
	{
		$result = array();
		try {
			if ($this->getRequest()->isPost()) {
				$formData = $this->_request->getPost();

				$revisionModel = new Catalog_Model_Revision();
				$productModel  = new Catalog_Model_Product();
				$logEventModel = new System_Model_Log();

				$data = array('status' => 'pending');
				$productModel->update($data, $formData['product_id']);
				$revisionModel->deleteById($formData['id']);
				
				// Log accept revision event
				$eventValues = array(
					'event' => System_Model_Log::EVENT_DECLINE_REVISION,
					'entity_id' => $formData['product_id'],
				);
				$logEventModel->add($eventValues);

				$result['status'] = 'success';
			}

		} catch (Platon_Exception $e) {
			$result['status'] = 'error';
			$result['message'] = $e->getMessage();
		}

		$this->_helper->json($result, true);
		exit();
	}

	public function getAttributesAction()
	{
		header('Content-Type: text/html; charset=utf-8');

		$revisionId = (int)$this->_request->getParam('revisid');
		$setId = (int)$this->_request->getParam('setid');
		$revision['attributes'] = $this->_getRevisionAttributes($setId, $revisionId);
		$revision['id'] = $revisionId;

		$this->view->revision = $revision;
		$this->_helper->layout()->disableLayout();
		echo $this->view->render('revision/attributes.phtml');
		exit();
	}

	protected function _getRevisionAttributes($setId, $revisionId)
	{
		$revisionModel          = new Catalog_Model_Revision();
		$revisionAttributeModel = new Catalog_Model_Revision_Attribute();
		$attributesModel        = new Attribute_Model_Attribute();
		$attributeValueModel    = new Attribute_Model_Attribute_Options();
		$attributeSetAttributesModel = new Attribute_Model_Attribute_Set_Attributes();

		$revision = $revisionModel->find($revisionId);
		$attributes = $attributeSetAttributesModel->getBySet($setId);
		$revisionAttributes = $attributes;

		foreach ($attributes as $num => $attribute) {
			if ($revisionAttribute = $revisionAttributeModel->getByAttribIdAndRevisionId($revisionId, $attribute['attribute_id'])) {
				$revisionAttributes[$num]['value'] = (count($revisionAttribute) == 1) ? array_shift($revisionAttribute) : $revisionAttribute;
			}
			$attrInfo = $attributesModel->find($attribute['attribute_id']);
			if (($attrInfo['type'] == 'select') || ($attrInfo['type'] == 'multiselect')) {
				$revisionAttributes[$num]['values'] = $attributeValueModel->getOptions($attribute['attribute_id']);
			}

			$revisionAttributes[$num] = array_merge($revisionAttributes[$num], $attrInfo);
		}
		unset($attributes);
		
		return $revisionAttributes;
	}

	public function getMediaAction()
	{
		$mediaId = $this->_request->getParam('media_id');
		$mediaModel = new Catalog_Model_Media();
		$data = array();

		if ($mediaData = $mediaModel->find($mediaId)) {
			$data = $mediaData;
		}
		$this->_helper->json($data, true);
		exit();
	}

	public function uploadifyAction()
	{
		$mediaModel = new Catalog_Model_Media();

		if (!empty($_FILES)) {
			try {
				$file = pathinfo($_FILES['Filedata']['name']);
				if (!in_array(strtolower($file['extension']), array('jpeg', 'jpg', 'png'))) {
					throw new Exception('Неверное расширение файла');
				}

				//Set file name
				$fileName = strtolower(str_replace(array(' ', '.', '/', '-', '&'), '_', $file['filename']));
				$fileName = md5($fileName . uniqid() . @mktime()) . '.' . $file['extension'];

				// Create file path

				$mediaPath =  self::$photoUploadDir;
				$path = '';
				$chars = str_split(substr($fileName, 0, 2));

				foreach ($chars as $char) {
					$path .= $char . '/';
					if (!is_dir($mediaPath . $path)) {
						mkdir($mediaPath . $path);
					}
				}

				//Save file
				$uploadFile = $mediaPath . $path . $fileName;
				move_uploaded_file($_FILES['Filedata']['tmp_name'], $uploadFile);

				switch ($file['extension']) {
					case 'png':
						$mimeType = 'image/png';
						break;
					case 'jpg':
					case 'jpeg':
					default:
						$mimeType = 'image/jpeg';
				}

				// Trim the image
				$imagick = new Imagick($uploadFile);
				$imagick->trimImage(0);
				$imagick->writeImage($uploadFile);

				$values = array(
					'filename' => $path . $fileName,
					'mime_type' => $mimeType,
				);

				$mediaId = $mediaModel->add($values);

				$result['status'] = 'success';
				$result['id'] = $mediaId;

			} catch (Exception $e) {
				$result['status'] = 'error';
				$result['message'] = $e->getMessage();
			}

			$this->_helper->json($result, true);
		}
		exit();
	}
}
<?php

class Catalog_Form_Product extends EasyBib_Form
{
    public function init()
    {
        // form config
        $this->setMethod('POST');
        $this->setAttrib('class', 'form-horizontal');
        $this->addPrefixPath('Platon_JQuery_Form','Platon/JQuery/Form');
        $this->addPrefixPath('ZendX_JQuery_View_Helper','ZendX/JQuery/View/Helper');

        $referenceModel = new Reference_Model_Supplier();
        $attributeSetModel = new Attribute_Model_Attribute_Set();

        // create elements
        $supplierId  = new Zend_Form_Element_Select('supplier_id');
        $sku         = new Zend_Form_Element_Text('sku');
        $price       = new Zend_Form_Element_Text('price');
        $weight      = new Zend_Form_Element_Text('weight');
        $name        = new Zend_Form_Element_Text('name');
        $alias       = new Zend_Form_Element_Text('alias');
        $description = new Zend_Form_Element_Textarea('description');
        $status      = new Zend_Form_Element_Select('status');
        $attribSet   = new Zend_Form_Element_Select('set_id');
        $categories  = new ZendX_JQuery_Form_Element_AutoComplete('categories');
        $hiddenCategId = new Zend_Form_Element_Hidden('categ_id');
        $images        = new Zend_Form_Element_Button('images');
        $submit        = new Zend_Form_Element_Button('submit');

        $suppliers = $referenceModel->getAllSuppliers(true);
        $sets = $attributeSetModel->getAllSets();

        $statusOptions = array(
            'pending'   => PHP_EOL . 'Ожидается',
            'prepared'  => PHP_EOL . 'Подготовлен',
            'completed' => PHP_EOL . 'Завершён',
            'archived'  => PHP_EOL . 'Архивный'
        );

        $name->setLabel('Название:')
            ->setAttrib('required', 'required')
            ->setRequired(true)
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
            ->addValidator('NotEmpty');

        $alias->setLabel('Псевдоним:')
            ->setAttrib('required', 'required')
            ->setRequired(true)
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
            ->addValidator('NotEmpty');

        $sku->setLabel('Артикул:')
            ->setRequired(true)
            ->setAttrib('required', 'required')
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
            ->addValidator('NotEmpty');

        $supplierId->setLabel('Поставщик:')
            ->setAttrib('required', 'required')
            ->setMultiOptions($suppliers)
            ->addValidator('NotEmpty')
            ->setRequired(true);
            
        $price->setLabel('Цена:')
            ->setRequired(false)
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
            ->addValidator('Digits');

        $weight->setLabel('Вес:')
            ->setRequired(false)
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
            ->addValidator('float', false, array('locale' => 'en_US'));

        $description->setLabel('Описание:')
            ->setAttrib('rows', 4)
            ->addValidator('NotEmpty')
            ->addFilter('StringTrim');

        $status->setLabel('Статус:')
            ->setMultiOptions($statusOptions);

        $attribSet->setLabel('Набор:')
            ->setMultiOptions($sets)
            ->setAttrib('onchange', 'product.showAttributes(this)');

        $categories
            ->setJQueryParam('url', '/catalog/index/autocomplete')
            ->setJQueryParam('select', new Zend_Json_Expr("function(event, ui){product.addCategory(ui.item);}"))
            ->setJQueryParam('dataType', 'json')
            ->setJQueryParam('minLength', '2')
            ->setAttrib('placeholder', 'Введите название категории...')
            ->setAttrib('class', 'form-control')
            ->addFilter('StripTags')
            ->addFilter('StringTrim');

        $images->setLabel('Загрузить изображения')
            ->removeDecorator('Label');

        $submit->setLabel('Сохранить')
            ->setAttrib('type', 'submit');

        // add elements
        $this->addElements(array(
            $supplierId, $sku, $name, $alias, $price, $weight, $description, $status, $attribSet, $categories, $hiddenCategId, $images, $submit
        ));

        //add main group
        $this->addDisplayGroup(array(
            'name',
            'alias',
            'sku',
            'supplier_id',
            'price',
            'weight',
            'description',
            'status',

        ),'main', array(
            'displayGroupClass' => 'Platon_Form_DisplayGroup'
        ));

        $mainTab = $this->getDisplayGroup('main');
        $mainTab->setLabel(' Описание')
            ->setDecorators(array(
            'FormElements',
            array(array('panel-body' => 'HtmlTag'),array('tag'=>'div','class'=>'panel-body')),

            array(array('panel-heading-open' => 'HtmlTag'),
                array('tag' => 'div', 'closeOnly' => true, 'placement' => 'prepend')),

            array ('Label'),

            array(array('fa-list-ol-open' => 'HtmlTag'),
                array('tag' => 'i', 'closeOnly' => true, 'placement' => 'prepend')),

            array(array('fa-list-ol-close' => 'HtmlTag'),
                array('tag' => 'i', 'openOnly' => true, 'placement' => 'prepend', 'class' => 'fa fa-list-ol fa-fw')),

            array(array('panel-heading-close' => 'HtmlTag'),
                array('tag' => 'div', 'openOnly' => true, 'placement' => 'prepend', 'class' => 'panel-heading')),

            array(array('panel' => 'HtmlTag'),array('tag'=>'div','class'=>'panel panel-info')),

            array(array('col-lg-8' => 'HtmlTag'),array('tag'=>'div', 'openOnly'=>true, 'class'=>'col-lg-8')),
        ));

        //add pictures group
        $this->addDisplayGroup(array(
            'images',
        ),
            'pictures',
            array('displayGroupClass' => 'Platon_Form_DisplayGroup'));

        $picturesTab = $this->getDisplayGroup('pictures');
        $picturesTab->setLabel(' Изображения')
            ->setDecorators(array(
            'FormElements',

            array(array('panel-body' => 'HtmlTag'),array('tag'=>'div', 'id'=>'media-panel', 'class'=>'panel-body')),

            array(array('panel-heading-open' => 'HtmlTag'),
                array('tag' => 'div', 'closeOnly' => true, 'placement' => 'prepend')),

            array ('Label'),

            array(array('fa-file-image-o-open' => 'HtmlTag'),
                array('tag' => 'i', 'closeOnly' => true, 'placement' => 'prepend')),

            array(array('fa-file-image-o-close' => 'HtmlTag'),
                array('tag' => 'i', 'openOnly' => true, 'placement' => 'prepend', 'class' => 'fa fa-file-image-o fa-fw')),

            array(array('panel-heading-close' => 'HtmlTag'),
                array('tag' => 'div', 'openOnly' => true, 'placement' => 'prepend', 'class' => 'panel-heading')),

            array(array('panel' => 'HtmlTag'),array('tag'=>'div','class'=>'panel panel-info')),
            array('HtmlTag',array('tag'=>'div','closeOnly'=>true))
        ));

        //add attribute group
        $this->addDisplayGroup(array(

            'set_id',

        ),'attrib', array('displayGroupClass' => 'Platon_Form_DisplayGroup'));

        $attribTab = $this->getDisplayGroup('attrib');
        $attribTab->setLabel(' Атрибуты')
            ->setDecorators(array(
                'FormElements',

                array(array('panel-body' => 'HtmlTag'),array('tag'=>'div','id'=>'attrib-panel', 'class'=>'panel-body')),

                array(array('panel-heading-open' => 'HtmlTag'),
                    array('tag' => 'div', 'closeOnly' => true, 'placement' => 'prepend')),

                array ('Label'),

                array(array('fa-pencil-square-o-open' => 'HtmlTag'),
                    array('tag' => 'i', 'closeOnly' => true, 'placement' => 'prepend')),

                array(array('fa-pencil-square-o-close' => 'HtmlTag'),
                    array('tag' => 'i', 'openOnly' => true, 'placement' => 'prepend', 'class' => 'fa fa-pencil-square-o fa-fw')),

                array(array('panel-heading-close' => 'HtmlTag'),
                    array('tag' => 'div', 'openOnly' => true, 'placement' => 'prepend', 'class' => 'panel-heading')),

                array(array('panel' => 'HtmlTag'),array('tag'=>'div','class'=>'panel panel-info')),
                array('HtmlTag', array('tag'=>'div', 'openOnly'=>true, 'class'=>'col-lg-4'))
        ));

        //add category group
        $this->addDisplayGroup(array(

            'categories',
            'categ_id'

        ),'category', array('displayGroupClass' => 'Platon_Form_DisplayGroup'));

        $categoryTab = $this->getDisplayGroup('category');
        $categoryTab->setLabel(' Категории')
            ->setDecorators(array(
                'FormElements',

                array(array('panel-footer' => 'HtmlTag'),array('tag'=>'div','class'=>'panel-footer')),

                array(array('panel-body-open' => 'HtmlTag'),
                    array('tag' => 'div', 'closeOnly' => true, 'placement' => 'prepend')),

                array(array('panel-body-close' => 'HtmlTag'),
                    array('tag' => 'div', 'openOnly' => true, 'placement' => 'prepend', 'id'=>'categ-panel', 'class' => 'panel-body')),


                array(array('panel-heading-open' => 'HtmlTag'),
                    array('tag' => 'div', 'closeOnly' => true, 'placement' => 'prepend')),

                array ('Label'),

                array(array('fa-tasks-open' => 'HtmlTag'),
                    array('tag' => 'i', 'closeOnly' => true, 'placement' => 'prepend')),

                array(array('fa-tasks-close' => 'HtmlTag'),
                    array('tag' => 'i', 'openOnly' => true, 'placement' => 'prepend', 'class' => 'fa fa-tasks fa-fw')),

                array(array('panel-heading-close' => 'HtmlTag'),
                    array('tag' => 'div', 'openOnly' => true, 'placement' => 'prepend', 'class' => 'panel-heading')),


                array(array('panel' => 'HtmlTag'),array('tag'=>'div','class'=>'panel panel-info')),
                    array('HtmlTag', array('tag'=>'div','closeOnly'=>true))
        ));


        //add submitting group
        $this->addDisplayGroup(array(
            'submit',
        ),'submitting', array('displayGroupClass' => 'Platon_Form_DisplayGroup'));

        $submittingTab = $this->getDisplayGroup('submitting');
        $submittingTab
            ->setDecorators(array(
                'FormElements',
                array(array('col-lg-12' => 'HtmlTag'),array('tag'=>'div','class'=>'col-lg-12')),
            ));


        // set decorators
        EasyBib_Form_Decorator::setFormDecorator($this, EasyBib_Form_Decorator::BOOTSTRAP, 'submit', 'apply_categ');

    }

    /**
     * Validate the form
     *
     * @param  array $data
     * @return boolean
     */
    public function isValid($data)
    {
        if (!is_array($data)) {
            require_once 'Zend/Form/Exception.php';
            throw new Zend_Form_Exception(__METHOD__ . ' expects an array');
        }

        return parent::isValid($data);
    }

    /**
     * After post, pre validation hook
     *
     * Finds all fields where name includes 'newName' and uses addNewField to add
     * them to the form object
     *
     * @param array $data $_GET or $_POST
     */
    public function preValidation(array $data) {
        // array_filter callback
        function findFields($field) {
            // return field names that include 'attributes'
            if (strpos($field, 'attributes')) {
                return $field;
            }
        }

        // Search $data for dynamically added fields using findFields callback
        $newFields = array_filter(array_keys($data), 'findFields');

        foreach ($newFields as $fieldName) {
            $this->addNewField($fieldName, $data[$fieldName]);
        }
    }

    /**
     * Adds new fields to form
     *
     * @param string $name
     * @param string $value
     */
    public function addNewField($name, $value) {

        $this->addElement('text', $name, array(
            'label'          => 'Name',
            'value'          => $value,
        ));
    }

}
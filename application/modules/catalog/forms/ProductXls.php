<?php

class Catalog_Form_ProductXls extends EasyBib_Form
{

    public function init()
    {
        // form config
        $this->setMethod('POST');
        $this->setAttrib('class', 'form-horizontal');

        $referenceModel = new Reference_Model_Supplier();

        // create elements
        $xlsList    = new Zend_Form_Element_File('xls_list');
        $supplierId = new Zend_Form_Element_Select('supplier_id');
        $submit     = new Zend_Form_Element_Button('submit');

        $suppliers = $referenceModel->getAllSuppliers(true);

        $xlsList = new Zend_Form_Element_File('xls_list');
        $xlsList->setLabel('Загрузить xls-фаил товаров:')
            ->setValueDisabled(true)
            ->setRequired(true)
            ->addValidator('NotEmpty')
            ->setAttrib('required', 'required')
            ->setAttrib('style', 'margin-bottom:15px');

        $supplierId->setLabel('Поставщик:')
            ->setAttrib('required', 'required')
            ->setMultiOptions($suppliers)
            ->addValidator('NotEmpty')
            ->setRequired(true);

        $submit->setLabel('Сохранить')
            ->setAttrib('type', 'submit');

        // add elements
        $this->addElements(array(
            $xlsList, $supplierId, $submit
        ));

        // set decorators
        EasyBib_Form_Decorator::setFormDecorator($this, EasyBib_Form_Decorator::BOOTSTRAP, 'submit');

    }

    /**
     * Validate the form
     *
     * @param  array $data
     * @return boolean
     */
    public function isValid($data)
    {
        if (!is_array($data)) {
            require_once 'Zend/Form/Exception.php';
            throw new Zend_Form_Exception(__METHOD__ . ' expects an array');
        }

        return parent::isValid($data);
    }
}
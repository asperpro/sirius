<?php

class Catalog_Form_Category extends Zend_Dojo_Form
{
    public function init()
    {
        $this->setName('categoryForm');
        $this->setMethod('POST');
        
        $this->setDecorators(array(
			"FormElements",
			array("TabContainer", array(
				"id"          => "tabContainer",
				"style"       => "width:100%;height:700px;",
				"dijitParams" => array(
					"tabPosition" => "top"
				),
			)),
			"DijitForm",
		));
		
		$mainSubForm = new Zend_Dojo_Form_SubForm();
		$mainSubForm->setAttribs(array(
			"name"   => "textboxtab",
			"legend" => "Описание",
			"dijitParams" => array(
				"title" => "Описание",
			),
		));
		
		$seoSubForm = new Zend_Dojo_Form_SubForm();
		$seoSubForm->setAttribs(array(
			"name"   => "textboxtab",
			"legend" => "МЕТА информация",
			"dijitParams" => array(
				"title" => "МЕТА информация",
			),
		));
		
		$mainSubFormUa = new Zend_Dojo_Form_SubForm();
		$mainSubFormUa->setAttribs(array(
			"name"   => "textboxtab",
			"legend" => "Описание (укр.)",
			"dijitParams" => array(
				"title" => "Описание (укр.)",
			),
		));
		
		$seoSubFormUa = new Zend_Dojo_Form_SubForm();
		$seoSubFormUa->setAttribs(array(
			"name"   => "textboxtab",
			"legend" => "МЕТА информация (укр.)",
			"dijitParams" => array(
				"title" => "МЕТА информация (укр.)",
			),
		));
        
		$title = new Zend_Form_Element_Text('title');
        $title->setLabel('Название')
        		->setRequired(true)
				->setAttrib('style', 'width:250px')
				->setAttrib('class', 'form-control')
				->addFilter('StripTags')
				->addFilter('StringTrim')
				->addValidator('NotEmpty');
				
		$slug = new Zend_Form_Element_Text('slug');
        $slug->setLabel('Ссылка на категорию')
        		->setDescription('Должна быть уникальной на всю систему')
        		->setRequired(true)
				->setAttrib('style', 'width:250px')
				->setAttrib('class', 'form-control')
				->addFilter('StripTags')
				->addFilter('StringTrim')
				->addValidator('NotEmpty');
				
		$titleUa = new Zend_Form_Element_Text('title_ua');
        $titleUa->setLabel('Название (укр.)')
        		->setRequired(true)
				->setAttrib('style', 'width:250px')
				->setAttrib('class', 'form-control')
				->addFilter('StripTags')
				->addFilter('StringTrim')
				->addValidator('NotEmpty');
        
        $metaTitle = new Zend_Form_Element_Textarea('meta_title');
		$metaTitle->setLabel('МЕТА Заголовок')
				->setAttrib('rows', 3)
				->setAttrib('cols', 80)
				->setAttrib('class', 'form-control')
				->addFilter('StripTags')
				->addFilter('StringTrim')
				->addValidator('NotEmpty');
				
		$metaTitleUa = new Zend_Form_Element_Textarea('meta_title_ua');
		$metaTitleUa->setLabel('МЕТА Заголовок (укр.)')
				->setAttrib('rows', 3)
				->setAttrib('cols', 80)
				->setAttrib('class', 'form-control')
				->addFilter('StripTags')
				->addFilter('StringTrim')
				->addValidator('NotEmpty');
				
		$metaKeywords = new Zend_Form_Element_Textarea('meta_keywords');
		$metaKeywords->setLabel('Ключевые слова')
				->setAttrib('rows', 3)
				->setAttrib('cols', 80)
				->setAttrib('class', 'form-control')
				->addFilter('StripTags')
				->addFilter('StringTrim')
				->addValidator('NotEmpty');
				
		$metaKeywordsUa = new Zend_Form_Element_Textarea('meta_keywords_ua');
		$metaKeywordsUa->setLabel('Ключевые слова (укр.)')
				->setAttrib('rows', 3)
				->setAttrib('cols', 80)
				->setAttrib('class', 'form-control')
				->addFilter('StripTags')
				->addFilter('StringTrim')
				->addValidator('NotEmpty');
				
		$metaDescription = new Zend_Form_Element_Textarea('meta_description');
		$metaDescription->setLabel('МЕТА Описания')
				->setAttrib('rows', 3)
				->setAttrib('cols', 80)
				->setAttrib('class', 'form-control')
				->addFilter('StripTags')
				->addFilter('StringTrim')
				->addValidator('NotEmpty');
				
		$metaDescriptionUa = new Zend_Form_Element_Textarea('meta_description_ua');
		$metaDescriptionUa->setLabel('МЕТА Описания (укр.)')
				->setAttrib('rows', 3)
				->setAttrib('cols', 80)
				->setAttrib('class', 'form-control')
				->addFilter('StripTags')
				->addFilter('StringTrim')
				->addValidator('NotEmpty');
		
		$content = new Zend_Form_Element_Textarea('content');
		$content->setLabel('Описание')
				->setAttrib('rows', 10)
				->setAttrib('cols', 100)
				->setAttrib('class', 'form-control')
				->addFilter('StringTrim')
				->addValidator('NotEmpty');
				
		$contentUa = new Zend_Form_Element_Textarea('content_ua');
		$contentUa->setLabel('Описание (укр.)')
				->setAttrib('rows', 10)
				->setAttrib('cols', 100)
				->setAttrib('class', 'form-control')
				->addFilter('StringTrim')
				->addValidator('NotEmpty');

        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setLabel('Сохранить')
        		->setValue('save')
        		->setAttrib('class', 'btn btn-primary btn-sm')
        		->removeDecorator('Label');
 
    	$mainSubForm->addElements(array($title, $content, $slug, $submit));
    	$seoSubForm->addElements(array($metaTitle, $metaKeywords, $metaDescription, $submit));
    	$mainSubFormUa->addElements(array($titleUa, $contentUa, $submit));
    	$seoSubFormUa->addElements(array($metaTitleUa, $metaKeywordsUa, $metaDescriptionUa, $submit));
    	
    	$this->addSubForm($mainSubForm, 'main');
    	$this->addSubForm($seoSubForm, 'seo');
    	$this->addSubForm($mainSubFormUa, 'mainUa');
    	$this->addSubForm($seoSubFormUa, 'seoUa');
    }
}
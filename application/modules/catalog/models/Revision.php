<?php

class Catalog_Model_Revision extends Platon_Db_Table_Abstract
{
	protected $_name = 'revisions';

	public function getNotUserPendingCount()
	{
		$select = $this->select()
			->setIntegrityCheck(false)
			->from(array('r' => $this->_name), array('total' => new Zend_Db_Expr('COUNT(*)')))
			->joinInner(array('u' => 'users'), 'u.id = r.user_id', array())
			->where('u.role = ?', 'moderator')
			->limit(1);
		$result = $this->fetchRow($select);

		return ($result) ? $result->total : 0;
	}

	public function getUserRevision()
	{
		$select = $this->select()
						->setIntegrityCheck(false)
						->from(array('r' => $this->_name), array('id', 'name', 'price', 'weight', 'description', 'product_id', 'interface_id', 'set_id', 'request_url', 'created_date'))
						->joinLeft(array('as' => 'attribute_sets'), 'as.id = r.set_id', array('set_name' => 'name'))
						->joinLeft(array('p' => 'products'), 'p.id = r.product_id', array('product_name' => 'name'))
						->joinLeft(array('i' => 'interfaces'), 'i.id = r.interface_id', array('interface_name' => 'name'))
						->where('r.user_id = ?', Zend_Auth::getInstance()->getIdentity()->id)
						->order('r.created_date ASC')
						->limit(1);
						
		if ($result = $this->fetchAll($select)) {
			$result = $result->toArray();
		}
						
		return $result;
	}

	public function getUserRevisionByProduct($productId)
	{
		$select = $this->select()
			->setIntegrityCheck(false)
			->from(array('r' => $this->_name), array('id', 'name', 'price', 'weight', 'description', 'product_id', 'interface_id', 'set_id', 'request_url', 'created_date'))
			->joinLeft(array('as' => 'attribute_sets'), 'as.id = r.set_id', array('set_name' => 'name'))
			->joinLeft(array('p' => 'products'), 'p.id = r.product_id', array('product_name' => 'name', 'product_code' => 'sku',))
			->joinLeft(array('i' => 'interfaces'), 'i.id = r.interface_id', array('interface_name' => 'name'))
			->where('r.user_id = ?', Zend_Auth::getInstance()->getIdentity()->id)
			->where('r.product_id = ?', $productId)
			->order('r.created_date ASC');


		if ($result = $this->fetchAll($select)) {
			$result = $result->toArray();
		}

		return $result;
	}

	public function getRevisionUser()
	{
		$select = $this->select()
			->setIntegrityCheck(false)
			->from(array('u' => 'users'), array('id'))
			->joinLeft(array('r' => $this->_name), 'u.id = r.user_id', array('total' => new Zend_Db_Expr('COUNT(*)')))
			->where('u.is_active = ?', 1)
			->where('u.role = ?', 'moderator')
			->group('u.id')
			->order('total ASC')
			->limit(1);
		if ($result = $this->fetchRow($select)) {
			$result = $result->id;
		}

		return $result;
	}

	public function getUserPendingCount($userId)
	{
		$select = $this->select()
			->setIntegrityCheck(false)
			->from(array('r' => $this->_name), array('total' => new Zend_Db_Expr('COUNT(*)')))
			->where('r.user_id = ?', $userId)
			->limit(1);
		$result = $this->fetchRow($select);

		return ($result) ? $result->total : 0;
	}

	public function getLastRevisionProduct()
	{
		$select = $this->select()
						->setIntegrityCheck(false)
						->from(array('r' => $this->_name), array('product_id'))
						->where('user_id = ?', Zend_Auth::getInstance()->getIdentity()->id)
						->order('created_date ASC')
						->limit(1);
		if ($result = $this->fetchRow($select)) {
			$result = $result->product_id;
		}

		return (int)$result;
	}

	public function getRevisionsForReorder($id, $limit)
	{
		$select = $this->select()
			->setIntegrityCheck(false)
			->from(array('r' => $this->_name), array('id'))
			->joinInner(array('u' => 'users'), 'u.id = r.user_id', array())
			->where('u.role = ?', 'moderator')
			->order('r.created_date ASC')
			->limit($limit);

		$data = array();
		if ($result = $this->fetchAll($select)) {
			$result = $result->toArray();

			foreach ($result as $revision) {
				$data[] = $revision['id'];
			}
		}

		return $data;
	}

	public function reorderUserRevision($userId, $id)
	{
		try {
			$this->getAdapter()->beginTransaction();
			$data = array(
				'user_id' => $userId
			);
			$where = $this->getAdapter()->quoteInto('id = ?', $id);
			if (null === parent::update($data, $where)) {
				throw new Platon_Exception('Не удалось переназначить ревизию для пользователя');
			}
			$this->getAdapter()->commit();
		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}

	public function add(array $data)
	{
		$data['created_date'] = new Zend_Db_Expr('NOW()');
		$data['description'] = strip_tags($data['description'], '<b><span><p>');
		try {
			$this->getAdapter()->beginTransaction();
			if (!$id = $this->insert($data)) {
				throw new Platon_Exception('Не удалось добавить ревизию товара');
			}
			$this->getAdapter()->commit();
			return $id;
		} catch (Platon_Exception_Form $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}

	public function update(array $data, $id)
	{
		$data['description'] = strip_tags($data['description'], '<b><span><p>');
		try {
			$this->getAdapter()->beginTransaction();
			$where = $this->getAdapter()->quoteInto('id = ?', $id);

			if (null === parent::update($data, $where)) {
				throw new Platon_Exception('Не удалось обновить ревизию товара');
			}

			$this->getAdapter()->commit();
		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}

	public function deleteById($id)
	{
		try {
			$this->getAdapter()->beginTransaction();
			$where = $this->getAdapter()->quoteInto('id = ?', $id);

			if (null === parent::delete($where)) {
				throw new Platon_Exception('Не удалось удалить ревизию товара');
			}
			$this->getAdapter()->commit();
		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}

	public function deleteByProductId($id)
	{
		try {
			$this->getAdapter()->beginTransaction();
			$where = $this->getAdapter()->quoteInto('product_id = ?', $id);

			if (null === parent::delete($where)) {
				throw new Platon_Exception('Не удалось удалить ревизии товара');
			}
			$this->getAdapter()->commit();
		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}

	public function deleteAll()
	{
		try {
			if (!$this->truncate($this->_name)) {
				throw new Platon_Exception('Не удалось очистить таблицу ревизий товаров');
			}
		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}
}

<?php

class Catalog_Model_Product_Attributes extends Platon_Db_Table_Abstract
{
	protected $_name = 'product_attributes';

	public function getAll($returnType = false)
	{
		$select = $this->select()
			->from(array('pa' => $this->_name), array('id', 'product_id', 'attribute_id', 'value'));
		if ($returnType) {
			return $this->fetchAll($select)->toArray();
			}

		return $select;
	}

	public function getByProdId($prodId)
	{
		$select = $this->select()
			->setIntegrityCheck(false)
			->from(array('pa' => $this->_name), array('value'))
			->joinInner(array('a' => 'attributes'), 'a.id = pa.attribute_id', array('code', 'type'))
			->where('pa.product_id = ?', $prodId);

		if ($result = $this->fetchAll($select)) {
			$result = $result->toArray();
		}

		return $result;

	}

	public function getByAttribIdAndProdId($prodId, $attribId)
	{
		$data = array();
		$select = $this->select()
			->from(array('pa' => $this->_name), array('value'))
			->where('product_id = ?', $prodId)
			->where('attribute_id = ?', $attribId);

		if ($result = $this->fetchAll($select)) {
			foreach ($result as $value) {
				$data[] = $value->value;
			}
		}

		return $data;

	}

	public function add(array $data)
	{
		try {
			if (!$id = $this->insert($data)) {
				throw new Platon_Exception('Не удалось добавить атрибуты товаров');
			}
			return $id;
		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}

	public function replace(array $data)
	{
		try {

			if (!parent::replace($this->_name, $data)) {
				throw new Platon_Exception('Не удалось заменить атрибуты товаров');
			}

		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}

	public function deleteAll() {

		try {
			if (!$this->truncate($this->_name)) {
				throw new Platon_Exception('Не удалось очистить таблицу атрибутов товаров');
			}
		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}

	public function deleteByProdId($id)
	{
		try {
			$this->getAdapter()->beginTransaction();
			$where = $this->getAdapter()->quoteInto('product_id = ?', $id);

			if (null === parent::delete($where)) {
				throw new Platon_Exception('Не удалось удалить атрибуты товаров');
			}
			$this->getAdapter()->commit();
		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}
}
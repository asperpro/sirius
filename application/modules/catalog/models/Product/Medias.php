<?php

class Catalog_Model_Product_Medias extends Platon_Db_Table_Abstract
{
	protected $_name = 'product_media';

	public function add(array $data)
	{
		try {
			$this->getAdapter()->beginTransaction();
			if (!$id = $this->insert($data)) {
				throw new Platon_Exception('Не удалось добавить связь товар - медиа');
			}
			$this->getAdapter()->commit();
			return $id;
		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}

	public function getByProdId($id)
	{
		$select = $this->select()
			->setIntegrityCheck(false)
			->from(array('pm' => $this->_name), array('is_main'))
			->joinInner(array('m' => 'media'), 'm.id = pm.media_id', array('id', 'filename', 'mime_type'))
			->where('pm.product_id = ?', $id);

		if ($result = $this->fetchAll($select)) {
			$result = $result->toArray();
		}

		return $result;
	}

	public function deleteByProdId($id)
	{
		try {
			$this->getAdapter()->beginTransaction();
			$where = $this->getAdapter()->quoteInto('product_id = ?', $id);

			if (null === parent::delete($where)) {
				throw new Platon_Exception('Не удалось удалить медиа-файлы товаров');
			}
			$this->getAdapter()->commit();
		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}



	/*public function getAll($returnType = false)
	{
		$select = $this->select()
			->from(array('pa' => $this->_name), array('id', 'product_id', 'attribute_id', 'value'));
		if ($returnType) {
			return $this->fetchAll($select)->toArray();
			}

		return $select;
	}

	public function replace(array $data)
	{
		try {

			if (!parent::replace($this->_name, $data)) {
				throw new Platon_Exception('Не удалось заменить атрибуты товаров');
			}

		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}

	public function deleteAll() {

		try {
			if (!$this->truncate($this->_name)) {
				throw new Platon_Exception('Не удалось очистить таблицу атрибутов товаров');
			}
		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}
*/
}
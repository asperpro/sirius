<?php

class Catalog_Model_Media extends Platon_Db_Table_Abstract
{
	protected $_name = 'media';

	public function add(array $data)
	{
		$this->getAdapter()->beginTransaction();
		$data['created_date'] = new Zend_Db_Expr('NOW()');
		try {
			if (!$id = $this->insert($data)) {
				throw new Platon_Exception('Не удалось добавить категории');
			}
			$this->getAdapter()->commit();
			return $id;
		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}
	
	/*public function getAll()
	{
		$select = $this->select()
						->setIntegrityCheck(false)
						->from(array('c' => $this->_name), array('id', 'name', 'is_active', 'imported_date'))
						->where('level > ?', 1);
		
		return $select;
	}

	public function search($term, $limit = 10)
	{
		$term = trim($term);
		$select = $this->select()
			->from(array('c' => $this->_name), array('id', 'name'))
			->where('c.name LIKE \'%' . $term . '%\'')
			->limit($limit);

		$result = $this->fetchAll($select);
		return $result->toArray();
	}

	public function replace(array $data)
	{
		$data['imported_date'] = new Zend_Db_Expr('NOW()');
		try {
			if (!parent::replace($this->_name, $data)) {
				throw new Platon_Exception('Не удалось добавить категории');
			}

		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}

	public function deleteAll() {

		try{
			if (! $this->truncate($this->_name)) {
				throw new Platon_Exception('Не удалось очистить таблицу категорий');
			}

		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}*/
}
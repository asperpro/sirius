<?php

class Catalog_Model_Revision_Media extends Platon_Db_Table_Abstract
{
	protected $_name = 'revision_media';

	public function getAll($returnType = false)
	{
		$select = $this->select()
			->from(array('rm' => $this->_name), array('id', 'revision_id', 'media_id', 'is_main'));
		if ($returnType) {
			return $this->fetchAll($select)->toArray();
		}

		return $select;
	}

	public function getByRevisionId($id)
	{
		$select = $this->select()
			->setIntegrityCheck(false)
			->from(array('rm' => $this->_name))
			->joinInner(array('m' => 'media'), 'm.id = rm.media_id', array('filename'))
			->where('revision_id = ?', $id);
		if ($result = $this->fetchAll($select)) {
			$result = $result->toArray();
		}
		return $result;
	}

	public function add(array $data)
	{
		try {
			$this->getAdapter()->beginTransaction();
			if (!$id = $this->insert($data)) {
				throw new Platon_Exception('Не удалось добавить медиа-файлы ревизии');
			}
			$this->getAdapter()->commit();
			return $id;
		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}

	public function update(array $data, $id)
	{
		try {
			$this->getAdapter()->beginTransaction();
			$where = $this->getAdapter()->quoteInto('id = ?', $id);

			if (null === parent::update($data, $where)) {
				throw new Platon_Exception('Не удалось обновить медиа-файлы ревизии');
			}

			$this->getAdapter()->commit();
		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}

	public function deleteById($id)
	{
		try {
			$this->getAdapter()->beginTransaction();
			$where = $this->getAdapter()->quoteInto('id = ?', $id);

			if (null === parent::delete($where)) {
				throw new Platon_Exception('Не удалось удалить медиа-файлы ревизии');
			}
			$this->getAdapter()->commit();
		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}

	public function deleteAll() {

		try {
			if (!$this->truncate($this->_name)) {
				throw new Platon_Exception('Не удалось очистить таблицу медиа ревизий');
			}
		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}

}
<?php

class Catalog_Model_Revision_Attribute extends Platon_Db_Table_Abstract
{
	protected $_name = 'revision_attributes';

	public function getAll($returnType = false)
	{
		$select = $this->select()
			->from(array('ra' => $this->_name), array('id', 'revision_id', 'attribute_id', 'value'));
		if ($returnType) {
			return $this->fetchAll($select)->toArray();
			}

		return $select;
	}

	public function getByRevisionId($id)
	{
		$items = array();
		$select = $this->select()
			->setIntegrityCheck(false)
			->from(array('ra' => $this->_name))
			->joinInner(array('a' => 'attributes'), 'a.id = ra.attribute_id', array('name', 'type'))
			->where('revision_id = ?', $id);
		if ($result = $this->fetchAll($select)) {
			$result = $result->toArray();

			foreach ($result as $key => $atr) {
				if (isset($items[$atr['attribute_id']])) {
					$values = (!is_array($items[$atr['attribute_id']]['value'])) ? (array)$items[$atr['attribute_id']]['value'] : $items[$atr['attribute_id']]['value'];
					$values[] = $atr['value'];
					$items[$atr['attribute_id']]['value'] = $values;
				} else {
					$items[$atr['attribute_id']] = $atr;
				}
			}
		}

		return $items;
	}

	public function getByAttribIdAndRevisionId($revisionId, $attribId)
	{
		$data = array();
		$select = $this->select()
			->from(array('pa' => $this->_name), array('value'))
			->where('revision_id = ?', $revisionId)
			->where('attribute_id = ?', $attribId);

		if ($result = $this->fetchAll($select)) {
			foreach ($result as $value) {
				$data[] = $value->value;
			}
		}

		return $data;

	}

	public function add(array $data)
	{
		try {
			$this->getAdapter()->beginTransaction();
			if (!$id = $this->insert($data)) {
				throw new Platon_Exception('Не удалось добавить атрибуты ревизии');
			}
			$this->getAdapter()->commit();
			return $id;
		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}

	public function update(array $data, $id)
	{
		try {
			$this->getAdapter()->beginTransaction();
			$where = $this->getAdapter()->quoteInto('id = ?', $id);

			if (null === parent::update($data, $where)) {
				throw new Platon_Exception('Не удалось обновить атрибуты ревизии');
			}

			$this->getAdapter()->commit();
		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}

	public function deleteById($id)
	{
		try {
			$this->getAdapter()->beginTransaction();
			$where = $this->getAdapter()->quoteInto('id = ?', $id);

			if (null === parent::delete($where)) {
				throw new Platon_Exception('Не удалось удалить атрибуты ревизии');
			}
			$this->getAdapter()->commit();
		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}

	public function deleteAll() {

		try {
			if (!$this->truncate($this->_name)) {
				throw new Platon_Exception('Не удалось очистить таблицу атрибутов ревизий');
			}
		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}
}
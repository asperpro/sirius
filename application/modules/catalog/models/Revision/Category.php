<?php

class Catalog_Model_Revision_Category extends Platon_Db_Table_Abstract
{
	protected $_name = 'revision_categories';

	public function getAll($returnType = false)
	{
		$select = $this->select()
			->from(array('rc' => $this->_name), array('id', 'category_id', 'revision_id'));
		if ($returnType) {
			return $this->fetchAll($select)->toArray();
			}

		return $select;
	}

	public function getByRevisionId($id)
	{
		$select = $this->select()
			->setIntegrityCheck(false)
			->from(array('rc' => $this->_name))
			->joinInner(array('c' => 'categories'), 'c.id = rc.category_id', array('full_name'))
			->where('revision_id = ?', $id);
		if ($result = $this->fetchAll($select)) {
			$result = $result->toArray();
		}
		return $result;
	}
	
	public function add(array $data)
	{
		try {
			$this->getAdapter()->beginTransaction();
			if (!$id = $this->insert($data)) {
				throw new Platon_Exception('Не удалось добавить категории ревизии');
			}
			$this->getAdapter()->commit();
			return $id;
		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}

	public function update(array $data, $id)
	{
		try {
			$this->getAdapter()->beginTransaction();
			$where = $this->getAdapter()->quoteInto('id = ?', $id);

			if (null === parent::update($data, $where)) {
				throw new Platon_Exception('Не удалось обновить категории ревизии');
			}

			$this->getAdapter()->commit();
		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}

	public function deleteById($id)
	{
		try {
			$this->getAdapter()->beginTransaction();
			$where = $this->getAdapter()->quoteInto('id = ?', $id);

			if (null === parent::delete($where)) {
				throw new Platon_Exception('Не удалось удалить категории ревизии');
			}
			$this->getAdapter()->commit();
		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}

	public function deleteAll() {

		try {
			if (!$this->truncate($this->_name)) {
				throw new Platon_Exception('Не удалось очистить таблицу категорий ревизий');
			}
		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}
}
<?php

class Catalog_Model_Product extends Platon_Db_Table_Abstract
{
	protected $_name = 'products';
	
	public function getAll()
	{
		$select = $this->select()
						->setIntegrityCheck(false)
						->from(array('p' => $this->_name), array('id', 'name', 'sku', 'status', 'attempts', 'created_date'))
						->joinInner(array('rs' => 'reference_suppliers'), 'rs.id = p.supplier_id', array('supplier_name' => 'name'))
						->joinLeft(array('as' => 'attribute_sets'), 'as.id = p.set_id', array('set_name' => 'name'))
						->joinLeft(array('u' => 'users'), 'u.id = p.user_id', array('username' => 'name'))
						->group('p.id')
						->order('p.created_date DESC');

		return $select;
	}

	public function add(array $data)
	{
		$data['created_date'] = new Zend_Db_Expr('NOW()');
		$data['parsed_date'] = new Zend_Db_Expr('NOW()');
		$data['user_id'] = Zend_Auth::getInstance()->getIdentity()->id;
		try {
			$this->getAdapter()->beginTransaction();
			if ($this->findDuplicate($data['sku'], $data['supplier_id'])) {
				throw new Platon_Exception_Product_Duplicate('Товар уже существует');
			}
			if (!$id = $this->insert($data)) {
				throw new Platon_Exception('Не удалось добавить товар');
			}
			$this->getAdapter()->commit();
			return $id;
		} catch (Platon_Exception_Product_Duplicate $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		} catch (Platon_Exception_Form $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}
	
	public function import(array $data)
	{
		$data['created_date'] = new Zend_Db_Expr('NOW()');
		try {
			$id = $this->add($data);
			return $id;
		} catch (Platon_Exception_Product_Duplicate $e) {
		} catch (Platon_Exception $e) {
			throw $e;
		}
		
		return false;
	}
	
	public function getAllByStatus($status = 'pending', $limit = null, $userIds = array())
	{
		$select = $this->select()
						->setIntegrityCheck(false)
						->from(array('p' => $this->_name))
						->where('status = ?', $status);
		if ($userIds) {
			$select->where('p.user_id IN (' . implode(',', $userIds) . ')');
		}
		if ($limit) {
			$select->limit($limit);
		}
		if ($result = $this->fetchAll($select)) {
			$result = $result->toArray();
		}
		
		return $result;
	}

	public function update(array $data, $id)
	{
		$data['user_id'] = (isset(Zend_Auth::getInstance()->getIdentity()->id)) ? Zend_Auth::getInstance()->getIdentity()->id : 0;
		try {
			$this->getAdapter()->beginTransaction();
			$where = $this->getAdapter()->quoteInto('id = ?', $id);
			if (isset($data['sku']) && $data['supplier_id']) {
				$duplicateId = $this->findDuplicate($data['sku'], $data['supplier_id']);
				if ($duplicateId && $duplicateId != $id) {
					throw new Platon_Exception_Product_Duplicate('Товар уже существует');
				}	
			}
			if (null === parent::update($data, $where)) {
				throw new Platon_Exception('Не удалось обновить товар');
			}

			$this->getAdapter()->commit();
		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
		
		return true;
	}
	
	public function findDuplicate($sku, $supplierId)
	{
		$select = $this->select()
						->from(array('p' => $this->_name), array('id'))
						->where('sku = ?', trim($sku))
						->where('supplier_id = ?', (int)$supplierId);
						
		if ($result = $this->fetchRow($select)) {
			$result = $result->id;
		}
		
		return $result;
	}
	
	public function getQueue($limit = 10)
	{
		$select = $this->select()
						->setIntegrityCheck(false)
						->from(array('p' => $this->_name))
						->where('status = ?', 'pending')
						->where('parsed_date <> ?', 'NULL')
						->order('parsed_date ASC')
						->limit($limit);

		if ($result = $this->fetchAll($select)) {
			$result = $result->toArray();
		}
		
		return $result;
	}
	
	public function updateQueue($productId)
	{
		$data = array(
			'parsed_date' => new Zend_Db_Expr('NOW()'),
			'attempts' => new Zend_Db_Expr('attempts + 1')
		);
		$this->update($data, $productId);
	}
	
	public function delete($productId)
	{
		try {
			$this->getAdapter()->beginTransaction();
			$where = $this->getAdapter()->quoteInto('id = ?', $productId);
			if (!parent::delete($where)) {
				throw new Platon_Exception('Не удалось удалить товар');
			}
			$this->getAdapter()->commit();
		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
		
		return true;
	}
}
<?php

class Catalog_Model_Category extends Platon_Db_Table_Abstract
{
	protected $_name = 'categories';
	
	public function getAll()
	{
		$select = $this->select()
						->setIntegrityCheck(false)
						->from(array('c' => $this->_name), array('id', 'name', 'is_active', 'imported_date'))
						->where('level > ?', 1);
		
		return $select;
	}

	public function search($term, $viewMode = 'short', $limit = 10)
	{
		$term = trim($term);
		$name = ('full' == $viewMode) ? 'full_name' : 'name';
		$select = $this->select()
			->from(array('c' => $this->_name), array('id', 'name' => $name))
			->where('c.name LIKE \'%' . $term . '%\'')
			->limit($limit);
			
		if ($result = $this->fetchAll($select)) {
			$result = $result->toArray();
		}
		
		return $result;
	}

	public function add(array $data)
	{
		$data['imported_date'] = new Zend_Db_Expr('NOW()');
		try {
			if (!$id = $this->insert($data)) {
				throw new Platon_Exception('Не удалось добавить категории');
			}

			return $id;
		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}

	public function replace(array $data)
	{
		$data['imported_date'] = new Zend_Db_Expr('NOW()');
		try {
			if (!parent::replace($this->_name, $data)) {
				throw new Platon_Exception('Не удалось добавить категории');
			}

		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}

	public function deleteAll() {

		try{
			if (! $this->truncate($this->_name)) {
				throw new Platon_Exception('Не удалось очистить таблицу категорий');
			}

		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}
}
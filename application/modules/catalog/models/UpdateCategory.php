<?php

class Catalog_Model_UpdateCategory extends Sirius_Soap
{
	protected $categoryModel;

	public function update()
	{
		$this->categoryModel = new Catalog_Model_Category();
		$this->categoryModel->deleteAll();

		$apiResponses = array();
		$apiResponses[] = $this->client->call($this->session, 'catalog_category.tree');
		$this->getCategories($apiResponses);
		unset($apiResponses);
	}

	function getCategories($apiCatalogs)
	{
		global $crumbs;

		foreach ($apiCatalogs as $catalog) {
			if (2 >= $catalog['level']) {
				$crumbs = array();
			}
			$crumbs = array_slice($crumbs, 0, $catalog['level'] - 2);
			$crumbs[] = $catalog['name'];
			$catalogList = array(
				'id'        => $catalog['category_id'],
				'parent_id' => $catalog['parent_id'],
				'level'     => $catalog['level'],
				'name'      => $catalog['name'],
				'full_name' => implode(' / ', $crumbs),
				'is_active' => (int)$catalog['is_active']
			);
			$this->categoryModel->add($catalogList);

			if (!empty($catalog['children'])) {
				$this->getCategories($catalog['children']);
			}
		}
	}

}
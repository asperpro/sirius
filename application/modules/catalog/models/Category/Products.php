<?php

class Catalog_Model_Category_Products extends Platon_Db_Table_Abstract
{
	protected $_name = 'category_products';

	public function getAll($returnType = false)
	{
		$select = $this->select()
			->from(array('cp' => $this->_name), array('id', 'category_id', 'product_id'));
		if ($returnType) {
			return $this->fetchAll($select)->toArray();
			}

		return $select;
	}

	public function getByProdId($prodId)
	{
		$data = array();
		$select = $this->select()
			->from(array('cp' => $this->_name), array('category_id'))
			->where('product_id = ?', $prodId);

		if ($result = $this->fetchAll($select)) {
			foreach ($result as $value) {
				$data[] = $value->category_id;
			}
		}

		return $data;

	}

	public function add(array $data)
	{
		try {
			if (!$id = $this->insert($data)) {
				throw new Platon_Exception('Не удалось добавить категории товарам');
			}
			return $id;
		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}

	public function replace(array $data)
	{
		try {

			if (!parent::replace($this->_name, $data)) {
				throw new Platon_Exception('Не удалось заменить категории товарам');
			}

		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}

	public function deleteAll() {

		try {
			if (!$this->truncate($this->_name)) {
				throw new Platon_Exception('Не удалось очистить таблицу категорий товаров');
			}
		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}

	public function deleteByProdId($id)
	{
		try {
			$this->getAdapter()->beginTransaction();
			$where = $this->getAdapter()->quoteInto('product_id = ?', $id);

			if (null === parent::delete($where)) {
				throw new Platon_Exception('Не удалось удалить категории товаров');
			}
			$this->getAdapter()->commit();
		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}
}
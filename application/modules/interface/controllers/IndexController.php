<?php

class Interface_IndexController extends Platon_Controller_Action
{
	static protected $_statusesOptions = array(
		1 => 'Активный',
		0 => 'Неактивный',
	);

	function init()
	{
		$this->view->headScript()->appendFile('/public/js/interface.js');
	}

	public function indexAction()
	{
		$grid = $this->_getGrid();

		$this->view->grid = $grid->deploy();
		$this->view->title = 'Интерфейсы';
	}

	public function addAction()
	{
		$form = $this->_getForm();
		try {
			if ($this->getRequest()->isPost()) {
				$formData = $this->_request->getPost();

				$interfaceModel = new Interface_Model_Interface();

				if ($form->isValid($formData)) {
					$data = $form->getValues();
					unset($data['attrib_value_set_id']);
					unset($data['categories']);
					unset($data['categ_id']);
					unset($data['supplier_id']);
					unset($data['set_id']);
					unset($data['alias']);
					$interfaceId = $interfaceModel->add($data);
				}

				if (isset($interfaceId)) {

					$supplierInterfaceModel       = new Interface_Model_Interface_Supplier();
					$categoryInterfaceModel       = new Interface_Model_Interface_Category();
					$attributeInterfaceModel      = new Interface_Model_Interface_Attribute();
					$attributeValueInterfaceModel = new Interface_Model_Interface_Attribute_Value();

					//delete previews categories and attributes

					$supplierInterfaceModel->deleteByInterfaceId($interfaceId);
					$categoryInterfaceModel->deleteByInterfaceId($interfaceId);
					$attributeInterfaceModel->deleteByInterfaceId($interfaceId);
					$attributeValueInterfaceModel->deleteByInterfaceId($interfaceId);

					if (isset($formData['data'])) {
						//save all supplier
						foreach ($formData['data']['suppliers'] as  $supplier) {
							$supplier['interface_id'] = $interfaceId;
							$supplierInterfaceModel->add($supplier);
						}

						//save all categories
						foreach ($formData['data']['categories'] as  $category) {
							$category['interface_id'] = $interfaceId;
							$categoryInterfaceModel->add($category);
						}

						//save all attributes
						foreach ($formData['data']['attributes'] as  $attribute) {
							$attribute['interface_id'] = $interfaceId;
							$attributeInterfaceModel->add($attribute);
						}

						//save all attribute values
						foreach ($formData['data']['attribute_values'] as  $attrValue) {
							$attrValue['interface_id'] = $interfaceId;
							$attributeValueInterfaceModel->add($attrValue);
						}
					}
				}

				$this->_helper->flashMessenger(array('success' => 'Интерфейс успешно добавлен'));
				$this->_redirect('/interface/index');
			}
		} catch (Platon_Exception_Form $e) {
			$form->nomenclature->addError($e->getMessage());
		} catch (Platon_Exception $e) {
			$this->_helper->flashMessenger(array('error' => $e->getMessage()));
			$this->_redirect('/interface/index');
		}

		$this->view->form = $form;
		$this->view->title = 'Добавление интерфейса';
	}

	public function editAction()
	{
		$interfaceId = (int)$this->_request->getParam('id');
		$interfaceModel = new Interface_Model_Interface();

		$form = $this->_getForm();
		try {

			if (!$interface = $interfaceModel->find($interfaceId)) {
				throw new Platon_Exception('Интерфейс не найден');
			}

			if ($this->getRequest()->isPost()) {
				$formData = $this->_request->getPost();

				if ($form->isValid($formData)) {
					$data = $form->getValues();

					unset($data['attrib_value_set_id']);
					unset($data['categories']);
					unset($data['categ_id']);
					unset($data['supplier_id']);
					unset($data['set_id']);
					unset($data['alias']);
					$interfaceModel->update($data, $interfaceId);
				}

				if (isset($interfaceId)) {

					$supplierInterfaceModel       = new Interface_Model_Interface_Supplier();
					$categoryInterfaceModel       = new Interface_Model_Interface_Category();
					$attributeInterfaceModel      = new Interface_Model_Interface_Attribute();
					$attributeValueInterfaceModel = new Interface_Model_Interface_Attribute_Value();

					//delete previews categories and attributes

					$supplierInterfaceModel->deleteByInterfaceId($interfaceId);
					$categoryInterfaceModel->deleteByInterfaceId($interfaceId);
					$attributeInterfaceModel->deleteByInterfaceId($interfaceId);
					$attributeValueInterfaceModel->deleteByInterfaceId($interfaceId);

					//save all supplier
					foreach ($formData['data']['suppliers'] as  $supplier) {
						$supplier['interface_id'] = $interfaceId;
						$supplierInterfaceModel->add($supplier);
					}

					//save all categories
					foreach ($formData['data']['categories'] as  $category) {
						$category['interface_id'] = $interfaceId;
						$categoryInterfaceModel->add($category);
					}

					//save all attributes
					foreach ($formData['data']['attributes'] as  $attribute) {
						$attribute['interface_id'] = $interfaceId;
						$attributeInterfaceModel->add($attribute);
					}

					//save all attribute values
					foreach ($formData['data']['attribute_values'] as  $attrValue) {
						$attrValue['interface_id'] = $interfaceId;
						$attributeValueInterfaceModel->add($attrValue);
					}
				}

				$this->_helper->flashMessenger(array('success' => 'Интерфейс успешно изменен'));
				$this->_redirect('/interface/index');
			}

			$form->populate($interface);
		} catch (Platon_Exception_Form $e) {
			$form->nomenclature->addError($e->getMessage());
		} catch (Platon_Exception $e) {
			$this->_helper->flashMessenger(array('error' => $e->getMessage()));
			$this->_redirect('/interface/index');
		}

		$this->view->form = $form;
		$this->view->id = $interfaceId;
		$this->view->title = 'Редактирование интерфейса';
	}

	public function getSupplierAction()
	{
		header('Content-Type: text/html; charset=utf-8');

		$interfaceId = (int) $this->_request->getParam('interfaceid');
		$supplierInterfaceModel = new Interface_Model_Interface_Supplier();
		$suppliers = array();
		if ($interfaceId) {
			$suppliers = $supplierInterfaceModel->getByInterfaceId($interfaceId);
		}
		$this->_helper->json($suppliers, true);
		exit();
	}

	public function getCategoryAction()
	{
		header('Content-Type: text/html; charset=utf-8');

		$interfaceId  = (int) $this->_request->getParam('interfaceid');

		$categoryInterfaceModel = new Interface_Model_Interface_Category();

		$categories = array();
		$categories = $categoryInterfaceModel->getByInterfaceId($interfaceId);

		$this->_helper->json($categories, true);
		exit();
	}

	public function getAttributesAction()
	{
		header('Content-Type: text/html; charset=utf-8');

		$interfaceId = (int) $this->_request->getParam('interfaceid');
		$attributeInterfaceModel = new Interface_Model_Interface_Attribute();
		$attributes = array();

		if ($interfaceId) {
			$attributes = $attributeInterfaceModel->getByInterfaceId($interfaceId);
		}

		$this->_helper->json($attributes, true);
		exit();

	}

	public function getAttributeValuesAction()
	{
		header('Content-Type: text/html; charset=utf-8');

		$interfaceId = (int) $this->_request->getParam('interfaceid');
		$attributeValueInterfaceModel = new Interface_Model_Interface_Attribute_Value();
		$attributes = array();

		if ($interfaceId) {
			$attributes = $attributeValueInterfaceModel->getByInterfaceId($interfaceId);
		}

		$this->_helper->json($attributes, true);
		exit();

	}

	protected function _getForm()
	{
		$form = new Interface_Form_Interface();
		return $form;
	}
    
    protected function _getGrid()
    {
    	$grid = $this->_initDefaultGrid();
    	
        $interfaceModel = new Interface_Model_Interface();
        $select = $interfaceModel->getAll();

    	$grid->setSource(new Bvb_Grid_Source_Zend_Select($select));
    	
    	$grid->updateColumn('id', array(
    		'title' => 'Номер',
    		'position' => 1,
    		'style' => 'width:5%;text-align:center;',
    		'searchType' => '=',
    	));
    	$grid->updateColumn('name', array(
    		'title' => 'Название',
    		'position' => 2,
    		'style' => 'width:20%;text-align:left;',
			'callback' => array(
				'function' => array($this, 'editLinkCallback'),
				'params' => array('{{id}}', '{{name}}')
			),
    	));
    	$grid->updateColumn('url', array(
    		'title' => 'Ссылка',
    		'position' => 3,
    		'style' => 'width:10%;text-align:left;',
			'callback' => array(
				'function' => array($this, 'viewSiteCallback'),
				'params' => array('{{url}}')
			),
    	));
		$grid->updateColumn('is_active', array(
			'title' => 'Статус',
			'position' => 4,
			'style' => 'width:10%;text-align:left;',
			'callback' => array(
				'function' => array($this, 'statusesCallback'),
				'params' => array('{{is_active}}')
			),
		));
		$grid->updateColumn('categories', array(
			'title' => 'Кол-во категорий',
			'position' => 5,
			'style' => 'width:10%;text-align:center;',
			'callback' => array(
				'function' => array($this, 'categoryCountCallback'),
				'params' => array('{{id}}')
			),
		));
		$grid->updateColumn('attributes', array(
			'title' => 'Кол-во атрибутов',
			'position' => 6,
			'style' => 'width:10%;text-align:center;',
		));
		$grid->updateColumn('atr_values', array(
			'title' => 'Кол-во значений атрибутов',
			'position' => 7,
			'style' => 'width:10%;text-align:center;',
		));
		$grid->updateColumn('created_date', array(
			'title' => 'Дата создания',
			'position' => 8,
			'style' => 'width:15%;text-align:left;',
			'format' => array(
				'date',
				array('date_format' => 'dd.MM.yyyy H:m')
			)
		));

		$filters = new Bvb_Grid_Filters();
		$filters->addFilter('is_active', array('values' => self::$_statusesOptions));
    	$filters->addFilter('created_date', array('render' => 'date'));
		$grid->addFilters($filters);

    	$grid->setTableGridColumns(array('id', 'name', 'url', 'is_active', 'categories', 'attributes', 'atr_values', 'created_date'));

		return $grid;
    }

	public function editLinkCallback($id, $name)
	{
		return "<a href='/interface/index/edit/id/" . $id ."'>" . $name . "</a>";
	}
	
	public function viewSiteCallback($url)
	{
		return "<a href='" . $url . "' target='a_blank'>Перейти</a>";
	}
	
	public function categoryCountCallback($id)
	{
		$interfaceCategoryModel = new Interface_Model_Interface_Category();
		return count($interfaceCategoryModel->getByInterfaceId($id));
	}
	
	public function statusesCallback($value)
	{
		if (isset(self::$_statusesOptions[$value])) {
			
			switch ($value) {
				case 1:
					$class = 'success';
					break;
				case 0:
				default:
					$class = 'danger';
					break;
			}
			
			return '<span class="label label-' . $class . '">' . self::$_statusesOptions[$value] . '</span>';
		}
	}
}
<?php

class Interface_Form_Interface extends Zend_Form
{
    public function init()
    {
        // form config
        $this->setMethod('POST');
        $this->setAttrib('class', 'form-horizontal');
        $this->setAttrib('id', 'form-interface');
        $this->addPrefixPath('Platon_JQuery_Form','Platon/JQuery/Form');
        $this->addPrefixPath('ZendX_JQuery_View_Helper','ZendX/JQuery/View/Helper');

        $attributeSetModel = new Attribute_Model_Attribute_Set();
        $sets = $attributeSetModel->getAllSets();

        $referenceModel = new Reference_Model_Supplier();
        $suppliers = $referenceModel->getAllSuppliers(true);

        $statusOptions = array(
            1   => PHP_EOL . 'Активный',
            0   => PHP_EOL . 'Неактивный',
        );

        // create elements
        $name        = new Zend_Form_Element_Text('name');
        $url         = new Zend_Form_Element_Text('url');
        $description = new Zend_Form_Element_Textarea('description');
        $status      = new Zend_Form_Element_Select('is_active');

        $alias       = new Zend_Form_Element_Text('alias');

        $supplier_id = new Zend_Form_Element_Select('supplier_id');
        $supplierAdd = new Zend_Form_Element_Button('supplier_add');

        $categories  = new ZendX_JQuery_Form_Element_AutoComplete('categories');
        $categAdd    = new Zend_Form_Element_Button('categ_add');

        $attribSet   = new Zend_Form_Element_Select('set_id');
        $attribAdd   = new Zend_Form_Element_Button('attrib_add');

        $attrValSet   = new Zend_Form_Element_Select('attrib_value_set_id');
        $attrValAdd  = new Zend_Form_Element_Button('attrib_value_add');

        $submit      = new Zend_Form_Element_Button('submit_btn');

        $name->setLabel('Название:')
            ->setAttrib('required', 'required')
            ->setRequired(true)
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
            ->addValidator('NotEmpty')
            ->setAttrib('class', 'form-control input-sm')
            ->setDecorators(array(
                'ViewHelper',
                array(array('col-xs-10' => 'HtmlTag'),array('tag'=>'div','class'=>'col-xs-10')),
                array('Label', array('class' => 'control-label col-xs-2')),
                array(array('form-group' => 'HtmlTag'),array('tag'=>'div','class'=>'form-group')),
            ));

        $url->setLabel('Ссылка:')
            ->setAttrib('required', 'required')
            ->setRequired(true)
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
            ->addValidator('NotEmpty')
            ->setAttrib('class', 'form-control input-sm')
            ->setDecorators(array(
                'ViewHelper',
                array(array('col-xs-10' => 'HtmlTag'),array('tag'=>'div','class'=>'col-xs-10')),
                array('Label', array('class' => 'control-label col-xs-2')),
                array(array('form-group' => 'HtmlTag'),array('tag'=>'div','class'=>'form-group')),
            ));

        $description->setLabel('Описание:')
            ->setAttrib('rows', 4)
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
            ->setAttrib('class', 'form-control input-sm')
            ->setDecorators(array(
                'ViewHelper',
                array(array('col-xs-10' => 'HtmlTag'),array('tag'=>'div','class'=>'col-xs-10')),
                array('Label', array('class' => 'control-label col-xs-2')),
                array(array('form-group' => 'HtmlTag'),array('tag'=>'div','class'=>'form-group')),
            ));

        $status->setLabel('Статус:')
            ->setMultiOptions($statusOptions)
            ->setAttrib('class', 'form-control input-sm')
            ->setDecorators(array(
                'ViewHelper',
                array(array('col-xs-10' => 'HtmlTag'),array('tag'=>'div','class'=>'col-xs-10')),
                array('Label', array('class' => 'control-label col-xs-2')),
                array(array('form-group' => 'HtmlTag'),array('tag'=>'div','class'=>'form-group')),
            ));

        $alias->setAttrib('placeholder', 'Псевдоним')
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
            ->setAttrib('class', 'form-control input-sm')
            ->setDecorators(array(
                'ViewHelper',
                array(array('col-xs-3' => 'HtmlTag'),array('tag'=>'div','class'=>'col-xs-3')),
            ));

        $supplier_id->setLabel('Поставщик:')
            ->setAttrib('required', 'required')
            ->setMultiOptions($suppliers)
            ->addValidator('NotEmpty')
            ->setRequired(true)
            ->setAttrib('class', 'form-control input-sm')
            ->setDecorators(array(
                'ViewHelper',
                array(array('col-xs-8' => 'HtmlTag'),array('tag'=>'div','class'=>'col-xs-8')),
                array('Label', array('class' => 'control-label col-xs-2')),
            ));

        $supplierAdd->setLabel('Добавить')
            ->setAttrib('onclick', 'interfaces.addSuppliers()')
            ->setAttrib('class', 'btn btn-primary')
            ->setDecorators(array(
                'ViewHelper',
                array(array('col-xs-2' => 'HtmlTag'),array('tag'=>'div','class'=>'col-xs-2')),
            ));

        $categories
            ->setJQueryParam('url', '/catalog/index/autocomplete')
            ->setJQueryParam('select', new Zend_Json_Expr("function(event, ui){interfaces.currentCategory(ui.item);}"))
            ->setJQueryParam('dataType', 'json')
            ->setJQueryParam('minLength', '2')
            ->setAttrib('placeholder', 'Категория')
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
            ->setAttrib('class', 'form-control input-sm')
            ->setDecorators(array(
                array('UiWidgetElement', array('tag' => '')),
                array('Errors'),
                array(array('col-xs-6' => 'HtmlTag'),array('tag'=>'div','class'=>'col-xs-6')),
            ));


        $categAdd->setLabel('Добавить')
            ->setAttrib('onclick', 'interfaces.addCategory()')
            ->setAttrib('class', 'btn btn-primary')
            ->setDecorators(array(
                'ViewHelper',
                array(array('col-xs-1' => 'HtmlTag'),array('tag'=>'div','class'=>'col-xs-1')),
            ));

        $attribSet->setMultiOptions($sets)
            ->setAttrib('onchange', 'interfaces.loadAttributes()')
            ->setAttrib('class', 'form-control input-sm')
            ->setDecorators(array(
                'ViewHelper',
                array(array('col-xs-3' => 'HtmlTag'),array('tag'=>'div','class'=>'col-xs-3')),
            ));

        $attribAdd->setLabel('Добавить')
            ->setAttrib('onclick', 'interfaces.addAttributes()')
            ->setAttrib('class', 'btn btn-primary')
            ->setDecorators(array(
                'ViewHelper',
                array(array('col-xs-2' => 'HtmlTag'),array('tag'=>'div','class'=>'col-xs-2')),
            ));

        $attrValSet->setMultiOptions($sets)
            ->setAttrib('onchange', 'interfaces.loadAttrValAttributes()')
            ->setAttrib('class', 'form-control input-sm')
            ->setDecorators(array(
                'ViewHelper',
                array(array('col-xs-3' => 'HtmlTag'),array('tag'=>'div','class'=>'col-xs-3')),
            ));

        $attrValAdd->setLabel('Добавить')
            ->setAttrib('onclick', 'interfaces.addAttrValAttributes()')
            ->setAttrib('class', 'btn btn-primary')
            ->setDecorators(array(
                'ViewHelper',
                array(array('col-xs-2' => 'HtmlTag'),array('tag'=>'div','class'=>'col-xs-2')),
            ));

        $submit->setLabel('Сохранить')
            ->setAttrib('type', 'submit')
            ->setAttrib('onclick', 'document.getElementById("form-interface").submit()')
            ->setAttrib('class', 'btn btn-primary')
            ->setDecorators(array(
                'ViewHelper',
            ));

        // add elements
        $this->addElements(array(
            $name, $url, $description, $status, $alias, $supplier_id, $supplierAdd, $categories, $categAdd, $attribSet, $attribAdd, $attrValSet, $attrValAdd, $submit
        ));

        //add main group
        $this->addDisplayGroup(array(
            'name',
            'url',
            'description',
            'is_active',

        ),'main', array(
            'displayGroupClass' => 'Platon_Form_DisplayGroup'
        ));

        $mainTab = $this->getDisplayGroup('main');
        $mainTab
            ->setDecorators(array(
                'FormElements',
                array(array('tab-pane' => 'HtmlTag'),array('tag'=>'div','class'=>'tab-pane active', 'id' => 'main_tab')),
                array('HtmlTag', array('tag'=>'div', 'openOnly'=>true, 'class'=>'tab-content', 'id' => 'interface'))
        ));

        //add supplier group
        $this->addDisplayGroup(array(

            'supplier_id',
            'supplier_add',
        ),'supplier', array('displayGroupClass' => 'Platon_Form_DisplayGroup'));

        $supplierTab = $this->getDisplayGroup('supplier');
        $supplierTab
            ->setDecorators(array(
                'FormElements',
                array(array('tab-pane' => 'HtmlTag'),array('tag'=>'div','class'=>'tab-pane', 'id' => 'supplier_tab')),

            ));

        //add category group
        $this->addDisplayGroup(array(
            'alias',
            'categories',
            'categ_add'

        ),'category', array('displayGroupClass' => 'Platon_Form_DisplayGroup'));

        $categoryTab = $this->getDisplayGroup('category');
        $categoryTab
            ->setDecorators(array(
                'FormElements',
                array(array('tab-pane' => 'HtmlTag'),array('tag'=>'div','class'=>'tab-pane', 'id' => 'category_tab')),

            ));

        //add attribute group
        $this->addDisplayGroup(array(
            'alias',
            'set_id',
            'attrib_add'

        ),'attrib', array('displayGroupClass' => 'Platon_Form_DisplayGroup'));

        $attribTab = $this->getDisplayGroup('attrib');
        $attribTab
            ->setDecorators(array(
                'FormElements',
                array(array('tab-pane' => 'HtmlTag'),array('tag'=>'div','class'=>'tab-pane', 'id' => 'attribute_tab')),
        ));

        //add pictures group
        $this->addDisplayGroup(array(
            'alias',
            'attrib_value_set_id',
            'attrib_value_add',
        ),
            'pictures',
            array('displayGroupClass' => 'Platon_Form_DisplayGroup'));

        $picturesTab = $this->getDisplayGroup('pictures');
        $picturesTab
            ->setDecorators(array(
                'FormElements',
                array(array('tab-pane' => 'HtmlTag'),array('tag'=>'div','class'=>'tab-pane', 'id' => 'attr_value_tab')),
                array('HtmlTag', array('tag'=>'div','closeOnly'=>true))
            ));

        //add submitting group
        $this->addDisplayGroup(array(
            'submit_btn',
        ),'submitting', array('displayGroupClass' => 'Platon_Form_DisplayGroup'));

        $submittingTab = $this->getDisplayGroup('submitting');
        $submittingTab
            ->setDecorators(array(
                'FormElements',
                array(array('col-lg-12' => 'HtmlTag'),array('tag'=>'div','class'=>'col-lg-12')),
            ));

        // set decorators
        //EasyBib_Form_Decorator::setFormDecorator($this, EasyBib_Form_Decorator::BOOTSTRAP, 'submit');

    }

}
<?php

class Interface_Model_Interface_Attribute extends Platon_Db_Table_Abstract
{
	protected $_name = 'interface_attributes';

	public function getByInterfaceId($interfaceId)
	{
		$data = array();
		$select = $this->select()
			->setIntegrityCheck(false)
			->from(array('ia' => $this->_name), array('attribute_set_id', 'attribute_id', 'alias'))
			->joinInner(array('a' => 'attributes'), 'a.id = ia.attribute_id', array('attribute_name' => 'a.name'))
			->joinInner(array('as' => 'attribute_sets'), 'as.id = ia.attribute_set_id', array('set_name' => 'as.name'))
			->where('interface_id = ?', $interfaceId)
			->order('a.sort_order DESC');

		$result = $this->fetchAll($select)->toArray();
		
		return $result;
	}

	public function deleteByInterfaceId($id)
	{
		try {
			$this->getAdapter()->beginTransaction();
			$where = $this->getAdapter()->quoteInto('interface_id = ?', $id);

			if (null === parent::delete($where)) {
				throw new Platon_Exception('Не удалось удалить атрибуты у интерфейсов');
			}
			$this->getAdapter()->commit();
		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}

	public function add(array $data)
	{
		$data['created_date'] = new Zend_Db_Expr('NOW()');
		try {
			if (!$id = $this->insert($data)) {
				throw new Platon_Exception('Не удалось добавить атрибуты интерфейсу');
			}
			return $id;
		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}

//	public function getAll($returnType = false)
//	{
//		$select = $this->select()
//			->from(array('pa' => $this->_name), array('id', 'product_id', 'attribute_id', 'value'));
//		if ($returnType) {
//			return $this->fetchAll($select)->toArray();
//		}
//
//		return $select;
//	}
//

//	public function add(array $data)
//	{
//		try {
//			if (!$id = $this->insert($data)) {
//				throw new Platon_Exception('Не удалось добавить атрибуты товаров');
//			}
//			return $id;
//		} catch (Platon_Exception $e) {
//			$this->getAdapter()->rollBack();
//			throw $e;
//		}
//	}
//

//	public function deleteAll() {
//
//		try {
//			if (!$this->truncate($this->_name)) {
//				throw new Platon_Exception('Не удалось очистить таблицу атрибутов товаров');
//			}
//		} catch (Platon_Exception $e) {
//			$this->getAdapter()->rollBack();
//			throw $e;
//		}
//	}
//

	public function getAttributeBySet($setId, $interfaceId)
	{
		$setIds = (4 == $setId) ? array(4) : array(4, $setId);
		$select = $this->select()
						->setIntegrityCheck(false)
						->from(array('ia' => $this->_name), array('id' => 'attribute_id', 'alias'))
						->joinInner(array('a' => 'attributes'), 'a.id = ia.attribute_id', array('type'))
						->where('interface_id = ?', $interfaceId)
						->where('attribute_set_id IN (' . implode(',', $setIds) . ')');
						
		if ($result = $this->fetchAll($select)) {
			$result = $result->toArray();
		}
		
		return $result;
	}
}
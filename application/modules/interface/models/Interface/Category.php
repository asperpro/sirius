<?php

class Interface_Model_Interface_Category extends Platon_Db_Table_Abstract
{
	protected $_name = 'interface_categories';

	public function getByInterfaceId($interfaceId)
	{
		$data = array();
		$select = $this->select()
			->setIntegrityCheck(false)
			->from(array('ic' => $this->_name), array('category_id', 'alias'))
			->joinInner(array('c' => 'categories'), 'c.id = ic.category_id', array('name', 'full_name'))
			->where('interface_id = ?', $interfaceId);

		$result = $this->fetchAll($select)->toArray();
		return $result;

	}

	public function add(array $data)
	{
		$data['created_date'] = new Zend_Db_Expr('NOW()');
		try {
			if (!$id = $this->insert($data)) {
				throw new Platon_Exception('Не удалось добавить категории интерфейсу');
			}
			return $id;
		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}

	public function deleteByInterfaceId($id)
	{
		try {
			$this->getAdapter()->beginTransaction();
			$where = $this->getAdapter()->quoteInto('interface_id = ?', $id);

			if (null === parent::delete($where)) {
				throw new Platon_Exception('Не удалось удалить категории интерфейсу');
			}
			$this->getAdapter()->commit();
		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}

/*

	public function getAll($returnType = false)
	{
		$select = $this->select()
			->from(array('cp' => $this->_name), array('id', 'category_id', 'product_id'));
		if ($returnType) {
			return $this->fetchAll($select)->toArray();
			}

		return $select;
	}


	public function deleteAll() {

		try {
			if (!$this->truncate($this->_name)) {
				throw new Platon_Exception('Не удалось очистить таблицу категорий товаров');
			}
		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}*/

	public function findByAlias($alias, $interfaceId)
	{
		$items = array();
		$alias = str_replace(' ', '', $alias);
		$select = $this->select()
						->from(array('ic' => 'interface_categories'), array('category_id'))
						->where('interface_id = ?', $interfaceId)
						->where('REPLACE(alias, " ", "") = ?', $alias);
		if ($result = $this->fetchAll($select)) {
			$result = $result->toArray();
			foreach ($result as $_result) {
				$items[] = $_result['category_id'];
			}
		}
		
		return $items;
	}
}
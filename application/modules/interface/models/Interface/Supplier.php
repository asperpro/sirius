<?php

class Interface_Model_Interface_Supplier extends Platon_Db_Table_Abstract
{
	protected $_name = 'interface_suppliers';

//	public function getAll($returnType = false)
//	{
//		$select = $this->select()
//			->from(array('cp' => $this->_name), array('id', 'category_id', 'product_id'));
//		if ($returnType) {
//			return $this->fetchAll($select)->toArray();
//			}
//
//		return $select;
//	}

	public function getByInterfaceId($interfaceId)
	{
		$data = array();
		$select = $this->select()
			->setIntegrityCheck(false)
			->from(array('is' => $this->_name), array('supplier_id'))
			->joinInner(array('rs' => 'reference_suppliers'), 'rs.id = is.supplier_id', array('name'))
			->where('interface_id = ?', $interfaceId);

		$result = $this->fetchAll($select)->toArray();
		return $result;

	}

	public function add(array $data)
	{
		try {
			if (!$id = $this->insert($data)) {
				throw new Platon_Exception('Не удалось добавить поставщиков интерфейсу');
			}
			return $id;
		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}
//
//	public function replace(array $data)
//	{
//		try {
//
//			if (!parent::replace($this->_name, $data)) {
//				throw new Platon_Exception('Не удалось заменить категории товарам');
//			}
//
//		} catch (Platon_Exception $e) {
//			$this->getAdapter()->rollBack();
//			throw $e;
//		}
//	}
//
//	public function deleteAll() {
//
//		try {
//			if (!$this->truncate($this->_name)) {
//				throw new Platon_Exception('Не удалось очистить таблицу категорий товаров');
//			}
//		} catch (Platon_Exception $e) {
//			$this->getAdapter()->rollBack();
//			throw $e;
//		}
//	}
//
	public function deleteByInterfaceId($id)
	{
		try {
			$this->getAdapter()->beginTransaction();
			$where = $this->getAdapter()->quoteInto('interface_id = ?', $id);

			if (null === parent::delete($where)) {
				throw new Platon_Exception('Не удалось удалить поставщиков интерфейсу');
			}
			$this->getAdapter()->commit();
		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}
}
<?php

class Interface_Model_Interface extends Platon_Db_Table_Abstract
{
	protected $_name = 'interfaces';
	
	public function getAll($returnType = false)
	{
		$select = $this->select()
						->setIntegrityCheck(false)
						->from(array('i' => $this->_name), array('id', 'name', 'url', 'is_active', 'created_date'))
						->joinLeft(array('ic' => 'interface_categories'), 'i.id = ic.interface_id', array('categories' => new Zend_Db_Expr('COUNT("ic.category_id")')))
						->group('i.id')
						->order('i.created_date DESC');
		if ($returnType) {
			return $this->fetchAll($select)->toArray();
		}
		return $select;
	}

	public function update(array $data, $id)
	{
		try {
			$this->getAdapter()->beginTransaction();
			$where = $this->getAdapter()->quoteInto('id = ?', $id);

			if (null === parent::update($data, $where)) {
				throw new Platon_Exception('Не удалось обновить интерфейс');
			}

			$this->getAdapter()->commit();
		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}

	public function add(array $data)
	{
		$data['created_date'] = new Zend_Db_Expr('NOW()');
		try {
			$this->getAdapter()->beginTransaction();
			if (!$id = $this->insert($data)) {
				throw new Platon_Exception('Не удалось добавить интерфейс');
			}
			$this->getAdapter()->commit();
			return $id;
		} catch (Platon_Exception_Form $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}
	
	public function getBySupplierId($supplierId)
	{
		$select = $this->select()
						->setIntegrityCheck(false)
						->from(array('i' => $this->_name))
						->join(array('is' => 'interface_suppliers') , 'is.interface_id = i.id', array())
						->where('i.is_active = ?', 1)
						->where('is.supplier_id = ?', $supplierId);
		if ($result = $this->fetchAll($select)) {
			$result = $result->toArray();
		}
		
		return $result;
	}
	
	public function getResourceModel($resourceName)
	{
		$filename = str_replace(array('-', ' '), '', $resourceName);
		$filename = ucfirst($filename);
		$resourceModel = 'Sirius_Resource_' . ucfirst($filename);
		if (class_exists($resourceModel)) {
			return new $resourceModel();
		}
		
		return null;
	}
}
<?php

class Reference_Model_UpdateSupplier extends Sirius_Soap
{

	public function update()
	{
		$supplierModel = new Reference_Model_Supplier();

		$endPoint = '/api/supplier';
		$client = new Zend_Rest_Client($this->configs['crm/domain']);
		$response = $client->restGet($endPoint);
		if (200 == $response->getStatus()) {
			// Delete all suppliers
			$supplierModel->deleteAll();
			// Set all suppliers
			$responseData = Zend_Json::decode($response->getBody());
			foreach ($responseData['data'] as $id => $value) {
				$data = array(
					'id' => $id,
					'name' => $value,
				);
				$supplierModel->add($data);
			}
		}
	}
}
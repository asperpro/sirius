<?php

class Reference_Model_Supplier extends Platon_Db_Table_Abstract 
{
	protected $_name = 'reference_suppliers';

	public function getAll($returnType = false)
	{
		$select = $this->select()
						->from(array('c' => $this->_name), array('id', 'name', 'imported_date'));

		return $select;
	}

	public function getAllSuppliers()
	{
		$data = array();
		$select = $this->select()
						->from($this->_name, array('id', 'name'));
		if ($result = $this->fetchAll($select)) {
			foreach ($result as $value) {
				$data[$value->id] = $value->name;
			}
		}

		return $data;
	}

	public function add(array $data)
	{
		$data['imported_date'] = new Zend_Db_Expr('NOW()');
		try {
			if (!$id = $this->insert($data)) {
				throw new Platon_Exception('Не удалось добавить поставщика');
			}

			return $id;
		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}

	public function deleteAll()
	{
		try{
			if (! $this->truncate($this->_name)) {
				throw new Platon_Exception('Не удалось очистить таблицу поставщиков');
			}

		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}
}
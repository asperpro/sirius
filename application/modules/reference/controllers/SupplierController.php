<?php

class Reference_SupplierController extends Platon_Controller_Action
{
    public function indexAction()
    {
    	$grid = $this->_getGrid();
        
		$this->view->grid = $grid->deploy();
        $this->view->title = 'Поставщики';
    }
    
    protected function _getGrid()
    {
    	$grid = $this->_initDefaultGrid();
    	
        $supplierModel = new Reference_Model_Supplier();
        $select = $supplierModel->getAll();
    	$grid->setSource(new Bvb_Grid_Source_Zend_Select($select));
    	
    	$grid->updateColumn('id', array(
    		'title' => 'Номер',
    		'position' => 1,
    		'style' => 'width:5%;text-align:center;',
    		'searchType' => '=',
    	));
    	$grid->updateColumn('name', array(
    		'title' => 'Название',
    		'position' => 2,
    		'style' => 'width:65%;text-align:left;',
    	));
		$grid->updateColumn('imported_date', array(
			'title' => 'Дата импорта',
			'position' => 3,
			'style' => 'width:15%;text-align:center;',
			'format' => array(
    			'date',
    			array('date_format' => 'dd.MM.yyyy H:m')
    		)
		));
		
    	$grid->setTableGridColumns(array('id', 'name', 'imported_date'));
        
		return $grid;
    }
}
<?php

class User_Model_User extends Platon_Db_Table_Abstract
{
	protected $_name = 'users';
	
	public function getAll()
	{
		$select = $this->select()
						->setIntegrityCheck(false)
						->from(array('u' => $this->_name), array('id', 'login', 'password', 'name', 'is_active', 'role', 'created_date', 'changed_date'))
						->order('u.created_date DESC');

		return $select;
	}

	public function add(array $data)
	{
		$data['created_date'] = new Zend_Db_Expr('NOW()');
		$data['changed_date'] = new Zend_Db_Expr('NOW()');

		try {
			$this->getAdapter()->beginTransaction();
			if (!$id = $this->insert($data)) {
				throw new Platon_Exception('Не удалось добавить Пользователя');
			}
			$this->getAdapter()->commit();
			return $id;
		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}

	public function update(array $data, $id)
	{
		$data['changed_date'] = new Zend_Db_Expr('NOW()');

		try {
			$this->getAdapter()->beginTransaction();
			$where = $this->getAdapter()->quoteInto('id = ?', $id);

			if (null === parent::update($data, $where)) {
				throw new Platon_Exception('Не удалось обновить пользователя');
			}

			$this->getAdapter()->commit();
		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}

}
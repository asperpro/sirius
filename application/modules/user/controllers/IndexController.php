<?php

class User_IndexController extends Platon_Controller_Action
{
    static protected $_userRoles = array(
        'editor'    => 'Редактор',
        'moderator' => 'Модератор',
        'admin'     => 'Администратор',
    );
    protected $_passwordSalt;

    static $weekdays = array(
        1 => 'Пн',
        2 => 'Вт',
        3 => 'Ср',
        4 => 'Чт',
        5 => 'Пт',
        6 => 'Сб',
        7 => 'Вс',
    );

    static $periodLabel = array(
        'day' => 'За день',
        'week' => 'За неделю',
        'month' => 'За месяц',
        'year' => 'За год',
        'total' => 'За все время',
        'datepicker' => 'Интервал дат',
    );

    function init()
    {
    	if (!in_array(Zend_Auth::getInstance()->getIdentity()->role, array('admin', 'moderator'))) {
    		$this->redirect('/');
    	}
    	
        $this->view->headScript()->appendFile('/public/js/user.js');
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini');
        $this->_passwordSalt = $config->production->auth->salt;
    }

    public function indexAction()
    {
        $grid = $this->_getGrid();

        $this->view->grid = $grid->deploy();
        $this->view->title = 'Пользователи';
    }

    public function addAction()
    {
        $form = new User_Form_User_Add();

        try {
            if ($this->getRequest()->isPost()) {
                $formData = $this->_request->getPost();

                $userModel = new User_Model_User();

                if ($form->isValid($formData)) {
                    $data = $form->getValues();
                    $data['password'] = (sha1(md5($data['password'] . $this->_passwordSalt)));
                    $userModel->add($data);
                }

                $this->_helper->flashMessenger(array('success' => 'Пользователь успешно добавлен'));
                $this->_redirect('/user/index');
            }
        } catch (Platon_Exception_Form $e) {
            $form->nomenclature->addError($e->getMessage());
        } catch (Platon_Exception $e) {
            $this->_helper->flashMessenger(array('error' => $e->getMessage()));
            $this->_redirect('/user/index');
        }

        $this->view->form = $form;
        $this->view->title = 'Добавление пользователя';
    }

    public function editAction()
    {
        $userId = (int)$this->_request->getParam('id');
        $period = $this->_request->getParam('period', 'total');

        $userModel = new User_Model_User();
        $logEventModel = new System_Model_Log();
        $revisionModel = new Catalog_Model_Revision();

        $acceptedRevisions  = $logEventModel->getUserEventLogSummary($userId, System_Model_Log::EVENT_ACCEPT_REVISION, $period);
        $availableRevisions = $revisionModel->getNotUserPendingCount($userId);
        $pendingRevisions   = $revisionModel->getUserPendingCount($userId);

        $totalEventLogs = $this->_getAllUserEventLogs($userId, $period);
        $summaryData = $this->_getSummaryData($userId, $period);

        $form = $this->_getForm();
        try {

            if (!$user = $userModel->find($userId)) {
                throw new Platon_Exception('Пользователь не найден');
            }

            if ($this->getRequest()->isPost()) {
                $formData = $this->_request->getPost();

                if ($form->isValid($formData)) {
                    $data = $form->getValues();
                    if (!empty($data['password'])) {
			   $data['password'] = (sha1(md5($data['password'] . $this->_passwordSalt)));
                    } else {
			   unset($data['password']);
		      }
                    $userModel->update($data, $userId);
                }

                $this->_helper->flashMessenger(array('success' => 'Пользователь успешно изменен'));
                $this->_redirect('/user/index');
            }

            $form->populate($user);
        } catch (Platon_Exception_Form $e) {
            $form->nomenclature->addError($e->getMessage());
        } catch (Platon_Exception $e) {
            $this->_helper->flashMessenger(array('error' => $e->getMessage()));
            $this->_redirect('/user/index');
        }

        $this->view->form = $form;
        $this->view->title = 'Редактирование пользователя';
        $this->view->id = $userId;

        $this->view->summary = $summaryData;
        $this->view->periodLabel = self::$periodLabel[$period];
        
        $this->view->total_event_logs        = $totalEventLogs;
        $this->view->accepted_revision_count = $acceptedRevisions;
        $this->view->pending_revision_count  = $pendingRevisions;
        $this->view->total_revision_count    = $availableRevisions;
    }

    public function addRevisionAction()
    {
        $userId = (int) $this->_request->getParam('id');
        $limit  = $this->_request->getParam('count');

        $revisionModel = new Catalog_Model_Revision();
        $logEventModel = new System_Model_Log();

        $result = array();

        try {
            if ($limit) {

                $limit = ($limit > $revisionModel->getNotUserPendingCount()) ? $revisionModel->getNotUserPendingCount() : $limit;

                $orderedRevisions = $revisionModel->getRevisionsForReorder($userId, $limit);
                foreach ($orderedRevisions as $revision) {
                    $revisionModel->reorderUserRevision($userId, $revision);
                }
                $availableRevisions = $revisionModel->getNotUserPendingCount($userId);
            }

        } catch (Platon_Exception $e) {
            $result['message'] = 'Ошибка запроса';
        }

        if (isset($availableRevisions)) {
            $result['logs'] = $availableRevisions;
            $result['message'] = 'Успешно начисленно ' . $limit . ' ревизий';
        }
        $donutData = $this->_getDonutData($userId);
        $result['data'] = $donutData;

        $this->_helper->json($result, true);
        exit();
    }

    public function getByPeriodAction()
    {
        $userId = (int) $this->_request->getParam('id');
        $period  = $this->_request->getParam('period');

        $result = array();
        $result['periodName'] = self::$periodLabel[$period];

        if ($period == 'datepicker') {
            $period = array (
                'from' => $this->_request->getParam('from'),
                'to' => $this->_request->getParam('to')
            );
        }

        try {
            $summaryData = $this->_getSummaryData($userId, $period);
            $totalEventLogs = $this->_getAllUserEventLogs($userId, $period);

            $result['status'] = 'success';
            $result['summary'] = $summaryData;
            $result['eventLogs'] = $totalEventLogs;


        } catch (Platon_Exception $e) {
            $result['message'] = 'Ошибка запроса';
        }

        $this->_helper->json($result, true);
        exit();
    }

    private function _getDonutData($userId)
    {
        $revisionModel = new Catalog_Model_Revision();
        $logEventModel = new System_Model_Log();

        $acceptedRevisions = $logEventModel->getUserEventLogSummary($userId, System_Model_Log::EVENT_ACCEPT_REVISION);
        $pendingRevisions  = $revisionModel->getUserPendingCount($userId);
        
        return array(
        	'arc' => $acceptedRevisions,
        	'prc' => $pendingRevisions,
        );
    }

    private function _getSummaryData($id, $period = null)
    {
        $logEventModel = new System_Model_Log();

        $acceptRevision  = $logEventModel->getUserEventLogs($id, System_Model_Log::EVENT_ACCEPT_REVISION, $period);
        $declineRevision = $logEventModel->getUserEventLogs($id, System_Model_Log::EVENT_DECLINE_REVISION, $period);
        $createProduct  = $logEventModel->getUserEventLogs($id, System_Model_Log::EVENT_CREATE_PRODUCT, $period);
        $changeProduct  = $logEventModel->getUserEventLogs($id, System_Model_Log::EVENT_CHANGE_PRODUCT, $period);

        if (is_array($period)) {
            $period = 'year';
        }

        $data = array(
            'accept_revision'  => $this->_formatSummaryData($acceptRevision, $period),
            'decline_revision' => $this->_formatSummaryData($declineRevision, $period),
            'create_product'   => $this->_formatSummaryData($createProduct, $period),
            'change_product'   => $this->_formatSummaryData($changeProduct, $period),
        );

        return $data;
    }

    protected function _formatSummaryData($data, $period)
    {
        $values = array();

        switch ($period) {
            case 'week':
                $sv = 1;
                $fv = 7;
                break;
            case 'month':
                $sv = 1;
                $fv = date('d');
                break;
            case 'year':
                $sv = 1;
                $fv = 12;
                break;
            case 'total':
                $sv = 2014;
                $fv = date('Y');
                break;
            case 'day':
            default:
                $sv = 1;
                $fv = 24;
        }
        for ($i = $sv; $i <= $fv; $i++) {
            $label = ('week'==$period) ? self::$weekdays[$i] : $i;
            $values[$label] = (isset($data[$i])) ? $data[$i] : 0;
        }

        return $values;
    }

    protected function _getAllUserEventLogs($id, $period = null)
    {
        $logEventModel = new System_Model_Log();

        $data = array(
            'accept_revision'  => $logEventModel->getUserEventLogSummary($id, System_Model_Log::EVENT_ACCEPT_REVISION, $period),
            'decline_revision' => $logEventModel->getUserEventLogSummary($id, System_Model_Log::EVENT_DECLINE_REVISION, $period),
            'create_product'   => $logEventModel->getUserEventLogSummary($id, System_Model_Log::EVENT_CREATE_PRODUCT, $period),
            'change_product'   => $logEventModel->getUserEventLogSummary($id, System_Model_Log::EVENT_CHANGE_PRODUCT, $period),
        );

        return $data;
    }

    protected function _getForm()
    {
        $form = new User_Form_User();
        return $form;
    }

    protected function _getGrid()
    {
        $grid = $this->_initDefaultGrid();

        $userModel = new User_Model_User();

        $select = $userModel->getAll();
        $grid->setSource(new Bvb_Grid_Source_Zend_Select($select));

        $grid->updateColumn('id', array(
            'title' => 'Номер',
            'position' => 1,
            'style' => 'width:5%;text-align:center;',
            'searchType' => '=',
        ));
        $grid->updateColumn('name', array(
            'title' => 'Название',
            'position' => 2,
            'style' => 'width:30%;text-align:left;',
            'callback' => array(
                'function' => array($this, 'editLinkCallback'),
                'params' => array('{{id}}', '{{name}}')
            ),
        ));
        $grid->updateColumn('role', array(
            'title' => 'Роль',
            'position' => 3,
            'style' => 'width:15%;text-align:center;',
            'callback' => array(
                'function' => array($this, 'rolesCallback'),
                'params' => array('{{role}}')
            )
        ));
        $grid->updateColumn('is_active', array(
            'title' => 'Статус',
            'position' => 4,
            'style' => 'width:15%;text-align:center;',
            'callback' => array(
                'function' => array($this, 'isActiveCallback'),
                'params' => array('{{is_active}}')
            )
        ));
        $grid->updateColumn('created_date', array(
            'title' => 'Дата создания',
            'position' => 8,
            'style' => 'width:15%;text-align:center;',
            'format' => array(
                'date',
                array('date_format' => 'dd.MM.yyyy H:m')
            )
        ));

        $filters = new Bvb_Grid_Filters();
        $filters->addFilter('role', array('values' => self::$_userRoles));
        $filters->addFilter('created_date', array('render' => 'date'));
        $grid->addFilters($filters);

        $grid->setTableGridColumns(array('id', 'name', 'role', 'is_active', 'created_date'));

        return $grid;
    }

    public function isActiveCallback($value)
    {
        $label = ($value) ? 'Активный': 'Неактивный';
        $class = ($value) ? 'success': 'danger';

        return '<span class="label label-' . $class . '">' . $label . '</span>';
    }

    public function editLinkCallback($id, $name)
    {
        return "<a href='/user/index/edit/id/" . $id ."'>" . $name . "</a>";
    }

    public function rolesCallback($value)
    {
        if (isset(self::$_userRoles[$value])) {

            $roles = array(
                'editor'    => 'info',
                'moderator' => 'primary',
                'admin'     => 'danger',
            );

            return '<span class="label label-' . $roles[$value] . '">' . self::$_userRoles[$value] . '</span>';
        }
    }
}
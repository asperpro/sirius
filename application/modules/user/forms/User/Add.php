<?php

class User_Form_User_Add extends EasyBib_Form
{
    public function init()
    {
        // form config
        $this->setMethod('POST');
        $this->setAttrib('class', 'form-horizontal');
        $this->addPrefixPath('Platon_JQuery_Form','Platon/JQuery/Form');
        $this->addPrefixPath('ZendX_JQuery_View_Helper','ZendX/JQuery/View/Helper');

        // create elements

        $name     = new Zend_Form_Element_Text('name');
        $login     = new Zend_Form_Element_Text('login');
        $role     = new Zend_Form_Element_Select('role');
        $status   = new Zend_Form_Element_Select('is_active');
        $password = new Zend_Form_Element_Password('password');
        $submit   = new Zend_Form_Element_Button('submit');

        $_userRoles = array(
            'editor'    => 'Редактор',
            'moderator' => 'Модератор',
            'admin'     => 'Администратор',
        );

        $name->setLabel('Имя:')
            ->setRequired(true)
            ->addValidator('NotEmpty')
            ->setAttrib('required', 'required');
            
		$login->setLabel('Логин:')
            ->setRequired(true)
            ->addValidator('NotEmpty')
            ->setAttrib('required', 'required');

        $password->setLabel('Пароль:')
            ->setRequired(true)
            ->addValidator('NotEmpty')
            ->setAttrib('class', 'form-control input-sm')
            ->setAttrib('required', 'required');

        $role->setLabel('Роль:')
            ->setMultiOptions($_userRoles);

        $status->setLabel('Статус:')
            ->setMultiOptions(array(
                0 => 'неактивный',
                1 => 'активный'
            ));

        $submit->setLabel('Сохранить')
            ->setAttrib('type', 'submit');

        // add elements
        $this->addElements(array(
            $name, $login, $role, $status, $password, $submit
        ));

        // set decorators
        EasyBib_Form_Decorator::setFormDecorator($this, EasyBib_Form_Decorator::BOOTSTRAP, 'submit');

    }

}
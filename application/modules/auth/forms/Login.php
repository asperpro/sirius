<?php

class Auth_Form_Login extends Zend_Form
{
    public $inputDecorators = array(
        'ViewHelper',
        'Label'
    );

    public $checkboxDecorators = array(
        'ViewHelper',
        'Label',
        'Description',
        array(array('label' => 'HtmlTag'), array('tag' => 'label')),
        array(array('div' => 'HtmlTag'), array('tag' => 'div', 'class' => 'checkbox')),
    );

    public function init()
    {
        $this->setMethod('POST');
        //$this->setAction('/auth');
        $this->setAttrib('id', 'authForm');
        $this->setName('authForm');

        $this->setDecorators(array(
            array ('Description', array('class' => 'alert alert-danger')),
            'FormElements',
            'Form'
        ));

        $login = new Zend_Form_Element_Text('login');
        $login->setRequired(true)
				->addFilter('StringTrim')
				->addValidator('Alpha')
				->addValidator('NotEmpty')
				->setAttrib('autofocus', 'autofocus')
				->setAttrib('required', 'required')
				->setAttrib('class', 'form-control')
				->setAttrib('placeholder', 'Логин')
                ->setDecorators($this->inputDecorators);


        $password = new Zend_Form_Element_Password('password');
        $password->setRequired(true)
                ->addValidator('NotEmpty')
                ->setAttrib('class', 'form-control')
                ->setAttrib('placeholder', 'Пароль')
                ->setAttrib('required', 'required')
                ->setDecorators($this->inputDecorators);
		
		$rememberMe = new Zend_Form_Element_Checkbox('remember-me');
        $rememberMe->setDescription('Запомнить меня')
                ->setDecorators($this->checkboxDecorators);
        
        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setLabel('Войти')
                ->setAttrib('class', 'btn btn-lg btn-primary btn-block')
                ->setDecorators(array(
                    'ViewHelper',
                ));
 
        $this->addElements(array($login, $password, $rememberMe, $submit));

    }

    public function isValid($data)
    {
        if (!is_array($data)) {
            require_once 'Zend/Form/Exception.php';
            throw new Zend_Form_Exception(__METHOD__ . ' expects an array');
        }

        return parent::isValid($data);
    }


}
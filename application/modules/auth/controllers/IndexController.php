<?php

class Auth_IndexController extends Zend_Controller_Action
{	

	public function init()
	{
		$this->_helper->layout->setLayout('auth');
	}

	public function indexAction()
	{
		if (Zend_Auth::getInstance()->hasIdentity()) {
			$this->_redirect('/');
		}

		$userModel = new Auth_Model_User();
		$form = new Auth_Form_Login();

		if ($this->_request->isPost()) {
			$formData = $this->_request->getPost();
			if ($form->isValid($formData)) {
				$auth  = Zend_Auth::getInstance();
				$authAdapter = $this->_getAuthAdapter($formData['login'], $formData['password']);
				$result = $auth->authenticate($authAdapter);
				try {
					if ($userModel->isBlocked($formData['login'])) {
						throw new Platon_Exception_AccessDenied('Учетная запись заблокирована');
					}
					if (!$result->isValid()) {
						throw new Platon_Exception('Неправильные имя или пароль');
					}
					/* Remember me */
					if ($formData['rememberme']) {
						Zend_Session::rememberMe(1209600); //14 days
					}
					$currentUser = $authAdapter->getResultRowObject();
					$auth->getStorage()->write($currentUser);
					$userModel->update(array('failed_log_num' => 0), $currentUser->id);
					// add access log
					$data = array(
						'user_id' => $currentUser->id,
						'ip' => self::_getUserIp(),
						'user_agent' => self::_getUserAgent(),
					);
					$accessLogModel = new Auth_Model_Access_Log();
					$accessLogModel->add($data);
					$this->_redirect('/');
				} catch (Platon_Exception $e) {
					$userModel->setFailedLog($formData['login']);
					$auth->clearIdentity();
					$form->setDescription($e->getMessage());
				} catch (Platon_Exception_AccessDenied $e) {
					$auth->clearIdentity();
					$form->setDescription($e->getMessage());
				}
			}
			$form->populate($formData);
		}
		$this->view->form = $form;
	}

	public function logoutAction()
	{	
		Zend_Auth::getInstance()->clearIdentity();
		Zend_Session::forgetMe();
		Zend_Session::expireSessionCookie();
		
		$this->_redirect('/');
	}
	
	protected function _getAuthAdapter($login, $password)
	{
		$config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini');
		$salt = $config->production->auth->salt;

		$authAdapter = new Zend_Auth_Adapter_DbTable(Zend_Db_Table::getDefaultAdapter());
		$authAdapter->setTableName('users');
		$authAdapter->setIdentityColumn('login');
		$authAdapter->setCredentialColumn('password');
		$authAdapter->setIdentity($login);
		$authAdapter->setCredential(sha1(md5($password . $salt)));
		
		return $authAdapter;
	}
	
	static protected function _getUserIp()
	{
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
	      $ip = $_SERVER['HTTP_CLIENT_IP'];
	    }
	    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
	      $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	    }
	    else {
	      $ip = $_SERVER['REMOTE_ADDR'];
	    }
	    
	    return $ip;
	}	
	
	static protected function _getUserAgent()
	{
		$userAgent = $_SERVER['HTTP_USER_AGENT'];
		
		return $userAgent;
	}	
}
<?php

class Auth_Model_Role extends Platon_Db_Table_Abstract 
{
	protected $_name = 'acl_roles';
	protected $_primary = 'id';
	
	
	public function add(array $data)
	{
		$data['created_date'] = new Zend_Db_Expr('NOW()');
		if (!$roleId = $this->insert($data)) {
			throw new Exception('Не удалось добавить должность');
		}
		
		return $roleId;
	}
	
	public function getRoles()
	{
		$data = array();
		$select = $this->select()
						->from($this->_name, array('id', 'name'));
		if ($result = $this->fetchAll($select)) {
			foreach ($result as $value) {
				$data[$value->id] = $value->name;
			}
		}
		
		return $data;
	}
}
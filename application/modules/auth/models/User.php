<?php

class Auth_Model_User extends Zend_Db_Table_Abstract
{
	const FAILED_LOG_NUM = 5;
	
	protected $_name = 'users';
	protected $_tables = array(
		'access_storages' => 'user_access_storages',
		'access_cash' => 'user_access_cash',
		'user_units' => 'unit_users',
		'units' => 'units',
	);
	
	public function add(array $data)
	{
		$data['password'] = md5($data['password']);
		$data['created_date'] = new Zend_Db_Expr('NOW()');
		
		$units = $data['units'];
		unset($data['units']);
		
		try {
			$this->getAdapter()->beginTransaction();
			if (!$id = $this->insert($data)) {
				throw new Platon_Exception('Не удалось добавить пользователя');
			}
			$this->_addUnits($units, $id);
			$this->getAdapter()->commit();
		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}
	
	public function update(array $data, $id)
	{
		if (!empty($data['password'])) {
			$data['password'] = md5($data['password']);	
		} else {
			if (isset($data['password'])) {
				unset($data['password']);
			}
		}
		$data['changed_date'] = new Zend_Db_Expr('NOW()');
		if (isset($data['units'])) {
			$units = $data['units'];
			unset($data['units']);
		}
		try {
			$this->getAdapter()->beginTransaction();
			$where = $this->getAdapter()->quoteInto('id = ?', $id);
			if (null === parent::update($data, $where)) {
				throw new Platon_Exception('Не удалось изменить пользователя');
			}
			if (isset($units)) {
				$this->_removeUnits($id);
				$this->_addUnits($units, $id);
			}
			$this->getAdapter()->commit();
		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}
	
	public function delete($id)
	{
		try {
			$this->getAdapter()->beginTransaction();
			$where = $this->getAdapter()->quoteInto('id = ?', $id);
			if (!parent::delete($where)) {
				throw new Platon_Exception('Не удалось удалить пользователя');
			}
			$this->getAdapter()->commit();
		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
		
		return true;
	}
	
	public function getAllUsers($setEmpty = false)
	{
		$data = array();
		$select = $this->select()
						->from($this->_name, array('id', 'name'));
		if ($result = $this->fetchAll($select)) {
			if ($setEmpty) {
				$data[0] = '';
			}
			foreach ($result as $value) {
				$data[$value->id] = $value->name;
			}
		}
		
		return $data;
	}

	public function getAllActiveUsers($setEmpty = false)
	{
		$data = array();
		$select = $this->select()
			->from($this->_name, array('id', 'name'))
			->where('is_active = ?', 1);
		if ($result = $this->fetchAll($select)) {
			if ($setEmpty) {
				$data[0] = '';
			}
			foreach ($result as $value) {
				$data[$value->id] = $value->name;
			}
		}
		
		return $data;
	}
	
	public function find($id)
	{
		$data = parent::find($id);
		$accessStorages = $this->getUserAccessStorages($id);
		$accessCash = $this->getUserAccessCash($id);
		$userUnits = $this->_getUnits($id);
		
		$data['storages'] = $accessStorages['list'];
		$data['cash'] = $accessCash['list'];
		$data['units'] = $userUnits;
		
		return $data;
	}
	
	public function findByLogin($login)
	{
		$select = $this->select()
						->from(array('u' => $this->_name))
						->where('login = ?', $login);
						
		return $this->fetchRow($select);
	}
	
	public function setFailedLog($login)
	{
		$user = $this->findByLogin($login);
		if ($user) {
			$data = array(
				'failed_log_num' => new Zend_Db_Expr('failed_log_num + 1')
			);
			if ($user->failed_log_num > self::FAILED_LOG_NUM) {
				$data['is_active'] = 0;
			}
			$this->update($data, $user['id']);	
		}
	}
	
	public function isBlocked($login)
	{
		if ($user = $this->findByLogin($login)) {
			return (bool)!$user->is_active;
		}
		
		return false;
	}
	
	public function getUserAccessStorages($userId)
	{
		$list = array();
		$flat = '';
		$select = $this->select()
						->setIntegrityCheck(false)
						->from(array('uac' => $this->_tables['access_storages']), array('storage_id'))
						->where('user_id = ?', $userId);
		if ($result = $this->fetchAll($select)) {
			foreach ($result as $value) {
				$list[] = $value['storage_id'];
				if (0 == $value['storage_id']) {
					$flat = 'all';
				}
			}
		}
		if (!$flat && $list) {
			$flat = implode(', ', $list);
		}
		
		return array(
			'list' => $list,
			'flat' => $flat
		);
	}
	
	public function getUserAccessCash($userId)
	{
		$list = array();
		$flat = '';
		$select = $this->select()
						->setIntegrityCheck(false)
						->from(array('uac' => $this->_tables['access_cash']), array('cash_id'))
						->where('user_id = ?', $userId);
		if ($result = $this->fetchAll($select)) {
			foreach ($result as $value) {
				$list[] = $value['cash_id'];
				if (0 == $value['cash_id']) {
					$flat = 'all';
				}
			}
		}
		if (!$flat && $list) {
			$flat = implode(', ', $list);
		}
		
		return array(
			'list' => $list,
			'flat' => $flat
		);
	}
	
	protected function _setAccessRights($data, $userId)
	{
		$where = $this->getAdapter()->quoteInto('user_id = ?', $userId);
		
		// access storages
		if (null === $this->getAdapter()->delete($this->_tables['access_storages'], $where)) {
			throw new Platon_Exception('Не удалось удалить права доступа к складам');
		}
		if ($storages = $data['storages']) {
			foreach ($storages as $storageId) {
				$values = array(
					'user_id' => $userId,
					'storage_id' => $storageId
				);
				if (!$this->getAdapter()->insert($this->_tables['access_storages'], $values)) {
					throw new Platon_Exception('Не удалось сохранить права доступа к складам');
				}
			}
		}
		
		// access cash
		if (null === $this->getAdapter()->delete($this->_tables['access_cash'], $where)) {
			throw new Platon_Exception('Не удалось удалить права доступа к кассам');
		}
		if ($cash = $data['cash']) {
			foreach ($cash as $cashId) {
				$values = array(
					'user_id' => $userId,
					'cash_id' => $cashId
				);
				if (!$this->getAdapter()->insert($this->_tables['access_cash'], $values)) {
					throw new Platon_Exception('Не удалось сохранить права доступа к кассам');
				}
			}
		}
	}
	
	public function addCashPermissions($data, $cashId, $userId)
	{
		$where = array(
			'user_id = ?' => $userId,
			'cash_id = ?' => $cashId,
		);
		$this->getAdapter()->delete($this->_tables['cash_permissions'], $where);
		if ($data) {
			foreach ($data as $value) {
				
			}
		}
	}
	
	protected function _addUnits($units, $id)
	{
		if ($units) {
			foreach ($units as $value) {
				$data = array(
					'user_id' => $id,
					'unit_id' => $value,
				);
				if (!$this->getAdapter()->insert($this->_tables['user_units'], $data)) {
					throw new Platon_Exception('Не удалось добавить подразделения');
				}
			}
		}
	}
	
	protected function _removeUnits($id)
	{
		$where = $this->getAdapter()->quoteInto('user_id = ?', $id);
		$this->getAdapter()->delete($this->_tables['user_units'], $where);
	}
	
	protected function _getUnits($id)
	{
		$items = array();
		$select = $this->select()
						->setIntegrityCheck(false)
						->from($this->_tables['user_units'], array('unit_id'))
						->where('user_id = ?', $id);
		
		if ($result = $this->fetchAll($select)) {
			foreach ($result as $value) {
				$items[] = $value['unit_id'];
			}
		}
		
		return $items;
	}
	
	public function getUserUnits($id)
	{
		$items = array();
		$select = $this->select()
						->setIntegrityCheck(false)
						->from(array('un' => $this->_tables['units']), array('id', 'name'))
						->joinInner(array('uu' => $this->_tables['user_units']), 'uu.unit_id = un.id', '')
						->where('uu.user_id = ?', $id);
		
		
		
		if ($result = $this->fetchAll($select)) {
			foreach ($result as $value) {
				$items[$value['id']] = $value['name'];
			}
		}
		
		return $items;
	}
}
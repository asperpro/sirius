<?php

class Auth_Model_Resource extends Zend_Db_Table_Abstract 
{
	protected $_name = 'acl_resources';
	protected $_primary = 'id';
	
	public function getAllResources()
	{
		$items = array();
		$select = $this->select()
						->from(array('ar' => $this->_name), array('id', 'name'))
						->order('resource');
		if ($result = $this->fetchAll($select)) {
			foreach ($result as $value) {
				$items[$value->id] = $value->name;
			}
		}
		
		return $items;
	}
	
	public function getByModule($module)
	{
		$select = $this->select()
						->setIntegrityCheck(false)
						->from(array('ar' => $this->_name), array('id', 'name'))
						->where('resource LIKE "' . $module . '%"')
						->order('resource');

		return $this->fetchAll($select);
	}
}
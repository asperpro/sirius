<?php

class Auth_Model_Privilege extends Zend_Db_Table_Abstract 
{
	protected $_name = 'privileges';
	protected $_tables = array(
		'user_privileges' => 'user_privileges',
	);
	
	public function getPrivilegeByGroup($group)
	{
		$select = $this->select()
						->setIntegrityCheck(false)
						->from(array('p' => $this->_name), array('id', 'name'))
						->where('group_type = ?', $group);
						
		return $this->fetchAll($select);
	}
	
	public function setUserPrivileges($data, $userId)
	{
		// delete privileges
		$where = $this->getAdapter()->quoteInto('user_id = ?', $userId);
		$this->getAdapter()->delete($this->_tables['user_privileges'], $where);
		
		// set privileges
		if ($data) {
			if ($data['cash']) {
				foreach ($data['cash'] as $cashId => $value) {
					foreach ($value as $privilegeId) {
						$values = array(
							'user_id' => $userId,
							'resource_id' => $cashId,
							'privilege_id' => $privilegeId,
						);
						if (!$this->getAdapter()->insert($this->_tables['user_privileges'], $values)) {
							throw new Platon_Exception('Не удалось сохранить права доступа к кассам');
						}
					}
				}
			}
			if ($data['storage']) {
				foreach ($data['storage'] as $storageId => $value) {
					foreach ($value as $privilegeId) {
						$values = array(
							'user_id' => $userId,
							'resource_id' => $storageId,
							'privilege_id' => $privilegeId,
						);
						if (!$this->getAdapter()->insert($this->_tables['user_privileges'], $values)) {
							throw new Platon_Exception('Не удалось сохранить права доступа к складам');
						}
					}
				}
			}
		}
	}
	
	public function getUserPrivileges($userId)
	{
		$items = array();		
		$select = $this->select()
						->setIntegrityCheck(false)
						->from(array('up' => $this->_tables['user_privileges']), array('resource_id', 'privilege_id'))
						->joinInner(array('p' => $this->_name), 'p.id = up.privilege_id', array('group_type'))
						->where('user_id = ?', $userId);
		if ($result = $this->fetchAll($select)) {
			foreach ($result as $value) {
				if (!isset($items[$value['group_type']])) {
					$items[$value['group_type']] = array();
				}
				if (!isset($items[$value['group_type']][$value['resource_id']])) {
					$items[$value['group_type']][$value['resource_id']] = array();
				}
				$items[$value['group_type']][$value['resource_id']][] = $value['privilege_id'];
			}
		}
		
		return $items;
	}
	
	public function getUserPrivilegeByCode($userId)
	{
		$items = array();		
		$select = $this->select()
						->setIntegrityCheck(false)
						->from(array('up' => $this->_tables['user_privileges']), array('resource_id'))
						->joinInner(array('p' => $this->_name), 'p.id = up.privilege_id', array('code'))
						->where('user_id = ?', $userId);
		if ($result = $this->fetchAll($select)) {
			foreach ($result as $value) {
				if (!isset($items[$value['code']])) {
					$items[$value['code']] = array();
				}
				$items[$value['code']][] = $value['resource_id'];
			}
		}
		
		return $items;
	}
	
	static public function getUserResources($code)
	{
		$user = Zend_Auth::getInstance()->getStorage()->read();
		$privileges = $user->privileges;

		return (isset($privileges[$code])) ? $privileges[$code] : null;
	}
	
	static public function checkUserPrivilege($code, $resourceId)
	{
		$privileges = self::getUserResources($code);
		
		return in_array($resourceId, (array)$privileges);
	}
}
<?php

class Auth_Model_Permission extends Zend_Db_Table_Abstract 
{
	protected $_name = 'acl_permissions';
	
	public function getAllPermissions()
	{
		$select = $this->select()
						->setIntegrityCheck(false)
						->from(array('ap' => $this->_name))
						->joinInner(array('arl' => 'acl_roles'), 'arl.id = ap.role_id', array('id'))
						->joinInner(array('ars' => 'acl_resources'), 'ars.id = ap.resource_id', array('resource'));
		$result = $this->fetchAll($select);
		
		return $result;
	}
}
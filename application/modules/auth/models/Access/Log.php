<?php

class Auth_Model_Access_Log extends Platon_Db_Table_Abstract 
{
	protected $_name = 'access_log';
	protected $_tables = array(
		'users' => 'users',
	);
	
	public function add(array $data)
	{
		$data['ip'] = new Zend_Db_Expr('INET_ATON("' . $data['ip'] . '")');
		$data['log_time'] = new Zend_Db_Expr('NOW()');
		$this->insert($data);
	}
	
	public function getGridSelect()
	{
		$select = $this->select()
						->setIntegrityCheck(false)
						->from(array('al' => $this->_name), array('ip' => new Zend_Db_Expr('INET_NTOA(ip)'), 'user_agent', 'log_time'))
						->joinInner(array('u' => $this->_tables['users']), 'al.user_id = u.id', array('name'))
						->order('al.log_time DESC');
	
		return $select;
	}
}
<?php

class Default_Form_Sirius extends EasyBib_Form
{

    /**
     * Configure user form.
     *
     * @return void
     */
    public function init()
    {
        // form config
        $this->setMethod('POST');
        //$this->setAction('/test/add');
        $this->setAttrib('id', 'testForm');
        $this->setAttrib('class', 'form-horizontal');

        // create elements
        $userId      = new Zend_Form_Element_Hidden('id');
        $mail        = new Zend_Form_Element_Text('email');
        $name        = new Zend_Form_Element_Text('name');
        $radio       = new Zend_Form_Element_Radio('radio');
        $select      = new Zend_Form_Element_Select('select');
        $multi       = new Zend_Form_Element_MultiCheckbox('multi');
        $submit      = new Zend_Form_Element_Button('submit');

        $multiOptions = array(
            'view'    => PHP_EOL . 'view',
            'edit'    => PHP_EOL . 'edit',
            'comment' => PHP_EOL . 'comment'
        );

        // config elements
        $userId->addValidator('digits');

        $mail->setLabel('Mail:')
            ->setRequired(true)

            ->addValidator('emailAddress');

        $name->setLabel('Name:')
            ->setRequired(true);

        $radio->setLabel('Radio:')
            ->setMultiOptions(array(
                '1' => PHP_EOL . 'test1',
                '2' => PHP_EOL . 'test2'
            ))
            ->setRequired(true);

        $select->setLabel('Select:')
            ->addValidator('Alpha')
            ->setMultiOptions($multiOptions)
            ->setRequired(true);

        $multi->setLabel('Multi:')
            ->addValidator('Alpha')
            ->setMultiOptions($multiOptions)
            ->setRequired(true);

        $submit->setLabel('Save');

        // add elements
        $this->addElements(array(
            $userId, $mail, $name, $radio, $select, $multi, $submit
        ));

        // add display group
        $this->addDisplayGroup(
            array('email', 'name', 'radio', 'select', 'multi', 'submit'),
            'users'
        );
        $this->getDisplayGroup('users')->setLegend('Add User');

        // set decorators
        EasyBib_Form_Decorator::setFormDecorator($this, EasyBib_Form_Decorator::BOOTSTRAP, 'submit', 'cancel');

    }

    /**
     * Validate the form
     *
     * @param  array $data
     * @return boolean
     */
    public function isValid($data)
    {
        if (!is_array($data)) {
            require_once 'Zend/Form/Exception.php';
            throw new Zend_Form_Exception(__METHOD__ . ' expects an array');
        }

        return parent::isValid($data);
    }
}
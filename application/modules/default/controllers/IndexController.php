<?php 

class IndexController extends Platon_Controller_Action
{
    static $weekdays = array(
    	1 => 'Пн',
    	2 => 'Вт',
    	3 => 'Ср',
    	4 => 'Чт',
    	5 => 'Пт',
    	6 => 'Сб',
    	7 => 'Вс',
     );
     static $periodLabel = array(
		'day' => 'За день',
		'week' => 'За неделю',
		'month' => 'За месяц',
		'year' => 'За год',
		'total' => 'За все время',
	);

    public function indexAction()
    {
    	$period = $this->_request->getParam('period', 'day');
    	
    	$logEventModel = new System_Model_Log();
    	$revisionModel = new Catalog_Model_Revision();
    	
    	$userId = Zend_Auth::getInstance()->getIdentity()->id;
		$acceptedRevisions = $logEventModel->getUserEventLogSummary($userId, System_Model_Log::EVENT_ACCEPT_REVISION);
		$pendingRevisions = $revisionModel->getUserPendingCount($userId);
		
		$summary = $logEventModel->getUserEventLogs($userId, System_Model_Log::EVENT_ACCEPT_REVISION, $period);
		$summary = $this->formatSummaryData($summary, $period);
		
		$this->view->accepted_revision_count = $acceptedRevisions;
		$this->view->pending_revision_count = $pendingRevisions;
		$this->view->summary = $summary;
		$this->view->periodLabel = self::$periodLabel[$period];
    }

	protected function formatSummaryData($data, $period)
	{
		$values = array();
		
		switch ($period) {
			case 'week':
				$sv = 1;
				$fv = 7;
				break;
			case 'month':
				$sv = 1;
				$fv = date('d');
				break;
			case 'year':
				$sv = 1;
				$fv = 12;
				break;
			case 'total':
				$sv = 2014;
				$fv = date('Y');
				break;
			default:
				$sv = 1;
				$fv = 24;
		}
		for ($i = $sv; $i <= $fv; $i++) {
			$label = ('week'==$period) ? self::$weekdays[$i] : $i;
			$values[$label] = (isset($data[$i])) ? $data[$i] : 0;
		}

		return $values;
	}
} 
<?php

class System_ConfigController extends Platon_Controller_Action
{
    public function indexAction()
    {
        $grid = $this->_getGrid();

        $this->view->grid = $grid->deploy();
        $this->view->title = 'Настройки';
    }

    public function addAction()
    {
        $form = $this->_getForm();

        try {
            if ($this->getRequest()->isPost()) {
                $formData = $this->_request->getPost();

                $configModel = new System_Model_Config();

                if ($form->isValid($formData)) {
                    $data = $form->getValues();
                    $configModel->add($data);
                }

                $this->_helper->flashMessenger(array('success' => 'Конфигурация успешно добавлена'));
                $this->_redirect('/system/config');
            }
        } catch (Platon_Exception_Form $e) {
            $form->nomenclature->addError($e->getMessage());
        } catch (Platon_Exception $e) {
            $this->_helper->flashMessenger(array('error' => $e->getMessage()));
            $this->_redirect('/system/config');
        }

        $this->view->form = $form;
        $this->view->title = 'Добавление конфигурации';
    }

    public function editAction()
    {
        $configId = (int)$this->_request->getParam('id');
        $configModel = new System_Model_Config();

        $form = $this->_getForm();
        try {

            if (!$config = $configModel->find($configId)) {
                throw new Platon_Exception('Конфигурация не найдена');
            }

            if ($this->getRequest()->isPost()) {
                $formData = $this->_request->getPost();

                if ($form->isValid($formData)) {
                    $data = $form->getValues();
                    $configModel->update($data, $configId);
                }

                $this->_helper->flashMessenger(array('success' => 'Конфигурация успешно изменена'));
                $this->_redirect('/system/config');
            }

            $form->populate($config);
        } catch (Platon_Exception_Form $e) {
            $form->nomenclature->addError($e->getMessage());
        } catch (Platon_Exception $e) {
            $this->_helper->flashMessenger(array('error' => $e->getMessage()));
            $this->_redirect('/system/config');
        }

        $this->view->form = $form;
        $this->view->title = 'Редактирование конфигурации';
    }

    protected function _getForm()
    {
        $form = new System_Form_Config();
        return $form;
    }

    protected function _getGrid()
    {
        $grid = $this->_initDefaultGrid();

        $configModel = new System_Model_Config();

        $select = $configModel->getAll();
        $grid->setSource(new Bvb_Grid_Source_Zend_Select($select));

        $grid->updateColumn('id', array(
            'title' => 'Номер',
            'position' => 1,
            'style' => 'width:5%;text-align:center;',
            'searchType' => '=',
        ));
        $grid->updateColumn('name', array(
            'title' => 'Название',
            'position' => 2,
            'style' => 'width:30%;text-align:left;',
            'callback' => array(
                'function' => array($this, 'editLinkCallback'),
                'params' => array('{{id}}', '{{name}}')
            ),
        ));
        $grid->updateColumn('value', array(
            'title' => 'Значение',
            'position' => 3,
            'style' => 'width:65%;text-align:left;',
        ));


        $grid->setTableGridColumns(array('id', 'name', 'value'));

        return $grid;
    }

    public function editLinkCallback($id, $name)
    {
        return "<a href='/system/config/edit/id/" . $id ."'>" . $name . "</a>";
    }
}
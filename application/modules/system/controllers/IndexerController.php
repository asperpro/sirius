<?php

class System_IndexerController extends Platon_Controller_Action
{
    function init()
    {
        $this->view->headScript()->appendFile('/public/js/indexer.js');
    }

    public function indexAction()
    {
        $indexerModel      = new System_Model_Indexer();
        $attributeSetModel = new Attribute_Model_Attribute_Set();

        $indices = $indexerModel->getAll(true);
        $sets = $attributeSetModel->getAllSets();

        $this->view->indices = $indices;
        $this->view->sets = $sets;
        $this->view->title = 'Обновление индексов';
    }

    public function updateAttributeAction()
    {
        try {
            if ($this->getRequest()->isPost()) {
                $formData = $this->_request->getPost();

                $UpdateAttributeModel = new Attribute_Model_UpdateAttribute();
                $UpdateAttributeModel->update($formData);

                $result['status'] = 'success';
            }

        } catch (Platon_Exception $e) {
            $result['status'] = 'error';
            $result['message'] = $e->getMessage();
        }

        $this->_helper->json($result, true);
        exit();
    }

    public function updateSetsAction()
    {
        try {
            $id = $this->_request->getParam('id');

            if ($this->getRequest()->isPost()) {

                $indexerModel = new System_Model_Indexer();
                $updateAttributeSetModel = new Attribute_Model_Attribute_UpdateSet();

                $updateAttributeSetModel->update();

                $date = array(
                    'last_update' => new Zend_Db_Expr('NOW()')
                );

                $indexerModel->update($date, $id);
                $index = $indexerModel->find($id);
                $result['status'] = 'success';
                $result['date'] = $index['last_update'];
            }

        } catch (Platon_Exception $e) {
            $result['status'] = 'error';
            $result['message'] = $e->getMessage();
        }

        $this->_helper->json($result, true);
        exit();
    }

    public function updateCategoriesAction()
    {
        try {
            $id = $this->_request->getParam('id');

            if ($this->getRequest()->isPost()) {

                $indexerModel = new System_Model_Indexer();
                $updateCategoryModel = new Catalog_Model_UpdateCategory();

                $updateCategoryModel->update();

                $date = array(
                    'last_update' => new Zend_Db_Expr('NOW()')
                );

                $indexerModel->update($date, $id);
                $index = $indexerModel->find($id);
                $result['status'] = 'success';
                $result['date'] = $index['last_update'];
            }

        } catch (Platon_Exception $e) {
            $result['status'] = 'error';
            $result['message'] = $e->getMessage();
        }

        $this->_helper->json($result, true);
        exit();
    }

    public function updateSuppliersAction()
    {
        try {
            $id = $this->_request->getParam('id');

            if ($this->getRequest()->isPost()) {

                $indexerModel = new System_Model_Indexer();
                $updateSupplierModel = new Reference_Model_UpdateSupplier();

                $updateSupplierModel->update();

                $date = array(
                    'last_update' => new Zend_Db_Expr('NOW()')
                );

                $indexerModel->update($date, $id);
                $index = $indexerModel->find($id);
                $result['status'] = 'success';
                $result['date'] = $index['last_update'];
            }

        } catch (Platon_Exception $e) {
            $result['status'] = 'error';
            $result['message'] = $e->getMessage();
        }

        $this->_helper->json($result, true);
        exit();
    }

}
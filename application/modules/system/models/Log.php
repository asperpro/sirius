<?php

class System_Model_Log extends Platon_Db_Table_Abstract
{
	protected $_name = 'log_events';
	
	const EVENT_ACCEPT_REVISION = 'accept_revision';
	const EVENT_DECLINE_REVISION = 'decline_revision';
	const EVENT_CREATE_PRODUCT = 'create_product';
	const EVENT_CHANGE_PRODUCT = 'change_product';

	public function getAll($returnType = false)
	{
		$select = $this->select()
						->from(array('c' => $this->_name), array('id', 'name', 'value'));

		return $select;
	}

	public function add(array $data)
	{
		$data['user_id'] = Zend_Auth::getInstance()->getIdentity()->id;
		$data['log_time'] = new Zend_Db_Expr('NOW()');
		if (!$this->insert($data)) {
			return false;
		}
		
		return true;
	}
	
	public function getUserEventLogSummary($userId, $event, $period = 'total')
	{
		$select = $this->select()
						->where('user_id = ?', $userId)
						->where('event = ?', $event);

		if (!is_array($period)) {
			switch ($period) {
				case 'week':
					$select->from(array('l' => $this->_name), array(
						'total' => new Zend_Db_Expr('COUNT(*)'),
					));
					$select->where('YEARWEEK(log_time) = YEARWEEK(CURDATE())');
					break;
				case 'month':
					$select->from(array('l' => $this->_name), array(
						'total' => new Zend_Db_Expr('COUNT(*)'),
					));
					$select->where('MONTH(log_time) = MONTH(CURDATE())');
					break;
				case 'year':
					$select->from(array('l' => $this->_name), array(
						'total' => new Zend_Db_Expr('COUNT(*)'),
					));
					$select->where('YEAR(log_time) = YEAR(CURDATE())');
					break;
				case 'day':
					$select->from(array('l' => $this->_name), array(
						'total' => new Zend_Db_Expr('COUNT(*)'),
					));
					$select->where('DAY(log_time) = DAYOFMONTH(NOW())');
					break;
				case 'total':
				default:
					$select->from(array('l' => $this->_name), array(
						'total' => new Zend_Db_Expr('COUNT(*)'),
					));
					break;
			}
		} else {

			if (isset($period['from']) || (isset($period['to']))) {

				$select->from(array('l' => $this->_name), array(
					'total' => new Zend_Db_Expr('COUNT(*)'),
				));

				if (isset($period['to'])) {
					$select->where('DATE(log_time) <= ' . new Zend_Db_Expr('"' . $period['to'] . '"'));
				}

				if (isset($period['from'])) {
					$select->where('DATE(log_time) >= ' . new Zend_Db_Expr('"' . $period['from'] . '"'));
				}

			}
		}

		$result = $this->fetchRow($select);
		
		return ($result) ? $result->total : 0;
	}
	
	public function getUserEventLogs($userId, $event, $period = 'total')
	{

		$items = array();
		$select = $this->select()
						->where('user_id = ?', $userId)
						->where('event = ?', $event)
						->group('period');
		if (!is_array($period)) {
			switch ($period) {
				case 'week':
					$select->from(array('l' => $this->_name), array(
						'total' => new Zend_Db_Expr('COUNT(*)'),
						'period' => new Zend_Db_Expr('(WEEKDAY(log_time))+1')
					));
					$select->where('YEARWEEK(log_time) = YEARWEEK(CURDATE())');
					break;
				case 'month':
					$select->from(array('l' => $this->_name), array(
						'total' => new Zend_Db_Expr('COUNT(*)'),
						'period' => new Zend_Db_Expr('DAY(log_time)')
					));
					$select->where('MONTH(log_time) = MONTH(CURDATE())');
					break;
				case 'year':
					$select->from(array('l' => $this->_name), array(
							'total' => new Zend_Db_Expr('COUNT(*)'),
							'period' => new Zend_Db_Expr('MONTH(log_time)'))
					);
					$select->where('YEAR(log_time) = YEAR(CURDATE())');
					break;
				case 'day':
					$select->from(array('l' => $this->_name), array(
						'total' => new Zend_Db_Expr('COUNT(*)'),
						'period' => new Zend_Db_Expr('HOUR(log_time)')
					));
					$select->where('DAY(log_time) = DAYOFMONTH(NOW())');
					break;
				case 'total':

					$select->from(array('l' => $this->_name), array(
							'total' => new Zend_Db_Expr('COUNT(*)'),
							'period' => new Zend_Db_Expr('YEAR(log_time)'))
					);
					break;
			}
		} else {

			if (isset($period['from']) ||(isset($period['to']))) {

				$select->from(array('l' => $this->_name), array(
					'total' => new Zend_Db_Expr('COUNT(*)'),
					'period' => new Zend_Db_Expr('DATE(log_time)')
				));

				if (isset($period['to'])) {
					$select->where('DATE(log_time) <= ' . new Zend_Db_Expr('"' . $period['to'] . '"'));
				}

				if (isset($period['from'])) {
					$select->where('DATE(log_time) >= ' . new Zend_Db_Expr('"' . $period['from'] . '"'));
				}

			}

		}

		if ($result = $this->fetchAll($select)) {
			$result = $result->toArray();
			foreach ($result as $value) {
				$items[$value['period']] = $value['total'];
			}
		}
		
		return $items;
	}
}
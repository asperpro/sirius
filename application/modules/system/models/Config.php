<?php

class System_Model_Config extends Platon_Db_Table_Abstract
{
	protected $_name = 'configs';

	public function getAll($returnType = false)
	{
		$select = $this->select()
			->from(array('c' => $this->_name), array('id', 'name', 'value'));

		return $select;
	}

	public function getAllConfigs()
	{
		$data = array();
		$select = $this->select()
						->from($this->_name, array('value', 'name'));
		if ($result = $this->fetchAll($select)) {
			foreach ($result as $value) {
				$data[$value->name] = $value->value;
			}
		}

		return $data;
	}

	public function add(array $data)
	{
		try {
			$this->getAdapter()->beginTransaction();
			if (!$id = $this->insert($data)) {
				throw new Platon_Exception('Не удалось добавить конфигурацию');
			}
			$this->getAdapter()->commit();
			return $id;
		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}

	public function update(array $data, $id)
	{
		try {
			$this->getAdapter()->beginTransaction();
			$where = $this->getAdapter()->quoteInto('id = ?', $id);

			if (null === parent::update($data, $where)) {
				throw new Platon_Exception('Не удалось обновить конфигурацию');
			}

			$this->getAdapter()->commit();
		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}
	}
}
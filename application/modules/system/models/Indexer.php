<?php

class System_Model_Indexer extends Platon_Db_Table_Abstract
{
	protected $_name = 'indices';

	public function getAll($returnType = false)
	{
		$select = $this->select()
			->from(array('i' => $this->_name), array('id', 'name', 'code', 'last_update'));

		if ($returnType) {
			if ($result = $this->fetchAll($select)) {
				return $result->toArray();
			}
		}

		return $select;
	}

	public function update(array $data, $id)
	{
		try {
			$this->getAdapter()->beginTransaction();
			$where = $this->getAdapter()->quoteInto('id = ?', $id);

			if (null === parent::update($data, $where)) {
				throw new Platon_Exception('Не удалось обновить конфигурацию');
			}

			$this->getAdapter()->commit();
		} catch (Platon_Exception $e) {
			$this->getAdapter()->rollBack();
			throw $e;
		}

	}
}
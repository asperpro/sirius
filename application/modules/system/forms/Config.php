<?php

class System_Form_Config extends EasyBib_Form
{
    public function init()
    {
        // form config
        $this->setMethod('POST');
        $this->setAttrib('class', 'form-horizontal');
        $this->addPrefixPath('Platon_JQuery_Form','Platon/JQuery/Form');
        $this->addPrefixPath('ZendX_JQuery_View_Helper','ZendX/JQuery/View/Helper');


        // create elements

        $name   = new Zend_Form_Element_Text('name');
        $value  = new Zend_Form_Element_Text('value');
        $submit = new Zend_Form_Element_Button('submit');

        $name->setLabel('Название:')
            ->setAttrib('required', 'required')
            ->setRequired(true)
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
            ->addValidator('NotEmpty');

        $value->setLabel('Значение:')
            ->setAttrib('required', 'required')
            ->setRequired(true)
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
            ->addValidator('NotEmpty');

        $submit->setLabel('Сохранить')
            ->setAttrib('type', 'submit');

        // add elements
        $this->addElements(array(
            $name, $value, $submit
        ));

        // set decorators
        EasyBib_Form_Decorator::setFormDecorator($this, EasyBib_Form_Decorator::BOOTSTRAP, 'submit');

    }

}
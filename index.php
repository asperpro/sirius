<?php

// Указание пути к директории приложения
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH',
              realpath(dirname(__FILE__) . '/application'));
 
// Определение текущего режима работы приложения
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV',
              (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV')
                                         : 'production'));

set_include_path(
	APPLICATION_PATH . '/../library' . PATH_SEPARATOR .
	APPLICATION_PATH . '/../library/Zend' . PATH_SEPARATOR.
    APPLICATION_PATH.  '/../library/PHPExcel'
);

/** Zend_Application */
require_once 'Zend/Application.php';
 
// Создание объекта приложения, начальная загрузка, запуск
$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);
$application->bootstrap()
            ->run();
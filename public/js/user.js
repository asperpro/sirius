function User() {}

User.prototype.addRevision = function(id)
{
	var count = jQuery('#revision').val();
	var _this = this;

	jQuery.ajax({
		url: '/user/index/add-revision/id/' + id + '/count/' + count,
		dataType: 'json',
		success: function (result) {
			jQuery('#revision-count strong').html(result.logs);
			jQuery('#revision-message strong').html(result.message);
			jQuery('#revision').val('');
			_this.printDonut(result.data);
		},
		error: function (result) {
			jQuery('#revision-message strong').html(result.message);
		}
	});

};

User.prototype.setPeriod = function(type, id)
{
	event.preventDefault();

	var url = '/user/index/get-by-period/id/' + id + '/period/' + type;

	if (type == 'datepicker') {
		var from = jQuery('#start_date').val();
		var to = jQuery('#finish_date').val();

		url += '/from/' + from + '/to/' + to;
	} else {
		jQuery('#date-period').css('display', 'none');
	}

	var _this = this;
	jQuery.ajax({
		url: url,
		dataType: 'json',
		beforeSend: function() {
		},
		success: function (result) {

			if ('success' == result.status) {
				_this.printLogs(result.eventLogs);
				_this.showPeriodHead(result.periodName);
				_this.printChart(result.summary);
			}
		}
	});

};

User.prototype.openDatepicker = function()
{
	event.preventDefault();
	jQuery('#date-period').css('display', 'block');
};

User.prototype.showPeriodHead = function(buttonName)
{
	jQuery('#button-caret-name').html(buttonName);
};

User.prototype.printLogs = function(params)
{
	jQuery('#event-logs td.accept_revision').html(params.accept_revision);
	jQuery('#event-logs td.change_product').html(params.change_product);
	jQuery('#event-logs td.create_product').html(params.create_product);
	jQuery('#event-logs td.decline_revision').html(params.decline_revision);
};

User.prototype.printChart = function(data)
{
	lineChartData.removeRows(0, 50);

	for (var i in data.accept_revision) {
		lineChartData.addRow([i, parseInt(data.accept_revision[i], 10), parseInt(data.decline_revision[i], 10), parseInt(data.create_product[i], 10), parseInt(data.change_product[i], 10)]);
	}

	drawLineChart();
};

User.prototype.printDonut = function(data)
{
	donutChartParams.arc = parseInt(data.arc, 10);
	donutChartParams.prc = parseInt(data.prc, 10);
	drawDonutChart();
};

var user = new User();

jQuery(document).ready(function() {

	jQuery(".integer").numeric({ negative: false });

	jQuery("#start_date").datepicker({
		dateFormat: 'yy-mm-dd',
		onSelect: function(selected) {
			jQuery("#finish_date").datepicker("option","minDate", selected)
		}
	});
	jQuery("#finish_date").datepicker({
		dateFormat: 'yy-mm-dd',
		onSelect: function(selected) {
			jQuery("#start_date").datepicker("option","maxDate", selected)
		}
	});
});

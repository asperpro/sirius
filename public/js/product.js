function Product() {}

Product.prototype.showAttributes = function()
{
	var setId = jQuery('#set_id').val();
	var prodId = this.urlParam('id');

	jQuery.ajax({
		url: '/catalog/product/get-set-attributes/setid/' + setId + '/prodid/' + prodId,
		contentType: "text/html; charset=utf-8",
		dataType: 'html',
		success: function (result) {
			jQuery('#attrib-panel .list-group').remove();
			jQuery('#attrib-panel').append(result);
		}
	});
};

Product.prototype.loadCategory = function()
{
	var prodId = this.urlParam('id');

	jQuery.ajax({
		url: '/catalog/product/get-category/prodid/' + prodId,
		contentType: "text/html; charset=utf-8",
		dataType: 'html',
		success: function (result) {
			jQuery('#categ-panel').append(result);
			jQuery('#categories').val('');
		}
	});
};

Product.prototype.addCategory = function(data)
{
	var template = '<div class="list-group-item">'
						+ '<label for="category' + data.id + '" class="control-label optional">' + data.label + '</label>'
						+ '<div style="float:left;">'
							+ '<input type="hidden" name="category' + data.id + '" value="0">'
							+ '<input type="checkbox" name="category' + data.id + '" id="category' + data.id + '" value="1" checked="checked">'
						+ '</div>'
					+ '</div>';
	jQuery('#categ-panel').append(template);
};

Product.prototype.loadMedia = function(id)
{
	jQuery.ajax({
		url: '/catalog/product/load-media/product_id/' + id,
		dataType: 'html',
		success: function (result) {
			jQuery('#media-panel').append(result);
		}

	});
};

Product.prototype.addMedia = function(id)
{
	jQuery.ajax({
		url: '/catalog/product/get-media/media_id/' + id,
		dataType: 'html',
		success: function (result) {
			jQuery('#media-panel table').append(result);
		}

	});
};

Product.prototype.uploadifyMedia = function(productId)
{
	jQuery('#images').uploadify({
		'swf'      : '/public/swf/uploadify.swf',
		'uploader' : '/catalog/product/uploadify/__tkn/35fbc5c646f61cd0ab19b72dd542e34b/id/'+ productId,
		'folder'   : '/tmp/',
		'buttonText' : 'Выберете файл(ы)...',
		'cancelImg': '/public/img/uploadify-cancel.png',
		'auto'     : true,
		'multi'	   : true,
		'width'    : 200,
		'fileExt'  : '*.jpg;*.gif;*.png',
		'fileDesc' : 'Файлы изображений',
		'method'   : 'post',
		'onCancel' : function(file) {
			alert('Предупреждение: Вы отменили загрузку файла ' + file.name);
		},
		'onUploadError' : function(file, errorCode, errorMsg, errorString) {
			alert('The file ' + file.name + ' could not be uploaded: ' + errorString);
		},
		'onUploadSuccess' : function(file, data, response) {
			var data = jQuery.parseJSON(data);
			if (data.status == 'success') {
				product.addMedia(data.id);
			}
		}
	});
};

Product.prototype.urlParam = function(name){
	var href = jQuery(location).attr('href');
	var position = href.indexOf(name);
	if (position != -1) {
		return href.substring(href.indexOf(name) + name.length + 1);
	}
	return false;
};

var product = new Product();

jQuery(document).ready(function() {
	product.showAttributes();
	product.loadCategory();
	product.uploadifyMedia(productId);
	product.loadMedia(productId);
});
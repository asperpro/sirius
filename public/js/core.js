function Core(){}

Core.prototype.initTinyMce = function() {

	tinymce.init({
        selector: "textarea",
        theme: "modern",
        height: 100,
        relative_urls: false,
        language: "ru",
        fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
        plugins: [
                "advlist autolink link image lists charmap preview hr anchor pagebreak spellchecker",
                "searchreplace visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                "save table directionality emoticons template paste textcolor"
        ],
        toolbar: "bold italic underline | bullist numlist | link preview code",
        style_formats: [],
        menubar: false,
        statusbar: false,
       	setup: function (editor) {
        	editor.on('change', function () {
            		editor.save();
          	});
        }
    });
};
Core.prototype.initFancyBox = function() {
    jQuery('.fancybox-thumbs').fancybox({
        prevEffect : 'none',
        nextEffect : 'none',

        closeBtn  : true,
        arrows    : true,
        nextClick : true,

        helpers : {
            thumbs : {
                width  : 50,
                height : 50
            },
            title : {
                type : 'inside'
            }
        }
    });
};

var core = new Core();
jQuery(document).ready(function(){
	core.initTinyMce();
	core.initFancyBox();
});
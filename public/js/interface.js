function Interfaces() {
	this.attr_index = 0;
	this.attr_val_index = 0;
	this.category_index = 0;
	this.supplier_index = 0;
	this.currentCategoryData = '';
}

Interfaces.prototype.loadCategoryTable = function()
{
	var table = "<table class='table table-bordered'>" +
					"<thead>" +
						"<tr>" +
						"<th>Псевдоним</th>" +
						"<th>Категория</th>" +
						"<th>Действия</th>" +
						"</tr>" +
					"</thead>" +
					"<tbody>" +
					"</tbody>" +
				"</table>";
	jQuery('#category_tab').append(table);
};

Interfaces.prototype.loadSupplierTable = function()
{
	var table = "<table class='table table-bordered'>" +
		"<thead>" +
		"<tr>" +
		"<th>Поставщик</th>" +
		"<th>Действия</th>" +
		"</tr>" +
		"</thead>" +
		"<tbody>" +
		"</tbody>" +
		"</table>";
	jQuery('#supplier_tab').append(table);
};

Interfaces.prototype.loadAttributeTable = function()
{
	var table = "<table class='table table-bordered'>" +
		"<thead>" +
		"<tr>" +
		"<th>Псевдоним</th>" +
		"<th>Набор атрибутов</th>" +
		"<th>Атрибут</th>" +
		"<th>Действия</th>" +
		"</tr>" +
		"</thead>" +
		"<tbody>" +
		"</tbody>" +
		"</table>";
	jQuery('#attribute_tab').append(table);
};

Interfaces.prototype.loadAttributeValueTable = function()
{
	var table = "<table class='table table-bordered'>" +
		"<thead>" +
		"<tr>" +
		"<th>Псевдоним</th>" +
		"<th>Набор атрибутов</th>" +
		"<th>Атрибут</th>" +
		"<th>Значение атрибута</th>" +
		"<th>Действия</th>" +
		"</tr>" +
		"</thead>" +
		"<tbody>" +
		"</tbody>" +
		"</table>";
	jQuery('#attr_value_tab').append(table);
};

Interfaces.prototype.loadDefaultAttributes = function()
{
	var _this = this;
	jQuery.ajax({
		url: '/interface/index/get-attributes/interfaceid/' + interfaceId,
		contentType: "text/html; charset=utf-8",
		dataType: 'html',
		success: function (result) {
			var values = jQuery.parseJSON(result);
			var tr = '';

			if (values.length > 0) {

				for(var i = 0; i < values.length; i++) {
					tr += "<tr>" +
					"<td>" + values[i]['alias'] + "<input type='hidden' name='data[attributes][" + _this.attr_index + "][alias]' value='" + values[i]['alias'] + "'>" + "</td>" +
					"<td>" + values[i]['set_name'] + "<input type='hidden' name='data[attributes][" + _this.attr_index + "][attribute_set_id]' value='" + values[i]['attribute_set_id'] + "'>" + "</td>"  +
					"<td>" + values[i]['attribute_name'] + "<input type='hidden' name='data[attributes][" + _this.attr_index + "][attribute_id]' value='" + values[i]['attribute_id'] + "'>" + "</td>"  +
					"<td><i class='fa fa-remove fa-fw' onclick='interfaces.removeElementFromTable(this)'></i></td>"  +
					"</tr>";
					++_this.attr_index;
				}
			}

			jQuery('#attribute_tab tbody').append(tr);
		}
	});
};

Interfaces.prototype.loadDefaultAttributeValues = function()
{
	var _this = this;
	jQuery.ajax({
		url: '/interface/index/get-attribute-values/interfaceid/' + interfaceId,
		contentType: "text/html; charset=utf-8",
		dataType: 'html',
		success: function (result) {
			var values = jQuery.parseJSON(result);
			var tr = '';

			if (values.length > 0) {

				for(var i = 0; i < values.length; i++) {
					tr += "<tr>" +
					"<td>" + values[i]['alias'] + "<input type='hidden' name='data[attribute_values][" + _this.attr_val_index + "][alias]' value='" + values[i]['alias'] + "'>" + "</td>" +
					"<td>" + values[i]['set_name'] + "<input type='hidden' name='data[attribute_values][" + _this.attr_val_index + "][attribute_set_id]' value='" + values[i]['attribute_set_id'] + "'>" + "</td>"  +
					"<td>" + values[i]['attribute_name'] + "<input type='hidden' name='data[attribute_values][" + _this.attr_val_index + "][attribute_id]' value='" + values[i]['attribute_id'] + "'>" + "</td>"  +
					"<td>" + values[i]['value_name'] + "<input type='hidden' name='data[attribute_values][" + _this.attr_val_index + "][value]' value='" + values[i]['value'] + "'>" + "</td>"  +
					"<td><i class='fa fa-remove fa-fw' onclick='interfaces.removeElementFromTable(this)'></i></td>"  +
					"</tr>";
					++_this.attr_val_index;

				}
			}

			jQuery('#attr_value_tab tbody').append(tr);
		}
	});

};

Interfaces.prototype.loadDefaultCategories = function()
{
	var _this = this;
	jQuery.ajax({
		url: '/interface/index/get-category/interfaceid/' + interfaceId,
		contentType: "text/html; charset=utf-8",
		dataType: 'html',
		success: function (result) {
			var values = jQuery.parseJSON(result);
			var tr = '';

			if (values.length > 0) {

				for(var i = 0; i < values.length; i++) {
					tr += "<tr>" +
					"<td>" + values[i]['alias'] + "<input type='hidden' name='data[categories][" + _this.category_index + "][alias]' value='" + values[i]['alias'] + "'>" + "</td>" +
					"<td>" + values[i]['full_name'] + "<input type='hidden' name='data[categories][" + _this.category_index + "][category_id]' value='" + values[i]['category_id'] + "'>" + "</td>"  +
					"<td><i class='fa fa-remove fa-fw' onclick='interfaces.removeElementFromTable(this)'></i></td>"  +
					"</tr>";
					++_this.category_index;
				}
			}
			jQuery('#category_tab tbody').append(tr);
		}

	});

};

Interfaces.prototype.loadDefaultSuppliers = function()
{
	var _this = this;
	jQuery.ajax({
		url: '/interface/index/get-supplier/interfaceid/' + interfaceId,
		contentType: "text/html; charset=utf-8",
		dataType: 'html',
		success: function (result) {
			var values = jQuery.parseJSON(result);
			var tr = '';

			if (values.length > 0) {

				for(var i = 0; i < values.length; i++) {
					tr += "<tr>" +
					"<td>" + values[i]['name'] + "<input type='hidden' name='data[suppliers][" + _this.supplier_index + "][supplier_id]' value='" + values[i]['supplier_id'] + "'>" + "</td>"  +
					"<td><i class='fa fa-remove fa-fw' onclick='interfaces.removeElementFromTable(this)'></i></td>"  +
					"</tr>";
					++_this.supplier_index;
				}
			}

			jQuery('#supplier_tab tbody').append(tr);
		}

	});

};

Interfaces.prototype.loadAttributes = function()
{
	jQuery('#attribute_tab .load-attribute').remove();
	var setId = jQuery('#set_id').val();

	jQuery.ajax({
		url: '/attribute/index/load-attributes/setid/' + setId,
		contentType: "text/html; charset=utf-8",
		dataType: 'html',
		success: function (result) {
			var values = jQuery.parseJSON(result);
			if (values.length > 0) {
				var element = "<div class='load-attribute col-xs-3'>" +
					"<select name='loaded_attribute' id='attribute_id' class='form-control input-sm'>";

				for(var i = 0; i < values.length; i++) {
					if (values[i] != null) {
						element += "<option value='" + values[i]['id'] + "'>" + values[i]['name'] + "</option>";	
					}
				}

				element += "</select>" +
				"</div>";

				jQuery(element).insertBefore('#attribute_tab .col-xs-2');
			}
		}
	});
};

Interfaces.prototype.loadAttrValAttributes = function()
{
	jQuery('#attr_value_tab .load-attribute').remove();
	jQuery('#attr_value_tab .load-value').remove();
	var setId = jQuery('#attrib_value_set_id').val();

	jQuery.ajax({
		url: '/attribute/index/load-attributes/setid/' + setId,
		contentType: "text/html; charset=utf-8",
		dataType: 'html',
		success: function (result) {
			var values = jQuery.parseJSON(result);
			if (values.length > 0) {
				var element = "<div class='load-attribute col-xs-2'>" +
					"<select name='loaded_attribute' id='attr_val_attribute_id' onchange='interfaces.loadAttributeValues()' class='form-control input-sm'>";

				for(var i = 0; i < values.length; i++) {
					if (values[i] != null) {
						if ((values[i]['type'] == 'select') || (values[i]['type'] == 'multiselect')) {
							element += "<option value='" + values[i]['id'] + "'>" + values[i]['name'] + "</option>";
						}	
					}
				}

				element += "</select>" +
				"</div>";

				jQuery(element).insertBefore('#attr_value_tab .col-xs-2');

				interfaces.loadAttributeValues();
			}
		}
	});
};

Interfaces.prototype.loadAttributeValues = function()
{
	jQuery('#attr_value_tab .load-value').remove();
	var attrId = jQuery('#attr_val_attribute_id').val();

	jQuery.ajax({
		url: '/attribute/index/load-values/attributeid/' + attrId,
		contentType: "text/html; charset=utf-8",
		dataType: 'html',
		success: function (result) {
			var values = jQuery.parseJSON(result);

			if (! jQuery.isEmptyObject(values)) {
				var element = "<div class='col-xs-2 load-value'>" +
					"<select name='loaded_attribute-value' id='attr_val_attribute_value_id' class='form-control input-sm'>";

				for(var id in values) {
					element += "<option value='" + id + "'>" + values[id] + "</option>";
				}

				element += "</select>" +
				"</div>";

				jQuery(element).insertAfter('#attr_value_tab .load-attribute');
			}
		}
	});
};

Interfaces.prototype.removeElementFromTable = function(element)
{
	if (confirm('Подтвердите удаление')) {
		jQuery(element).parents('tr').remove();
	}
};

Interfaces.prototype.currentCategory = function(data)
{
	this.currentCategoryData = data;
};

Interfaces.prototype.addSuppliers = function()
{
	var tr = "<tr>" +
		"<td>" + jQuery('#supplier_id option:selected').text() + "<input type='hidden' name='data[suppliers][" + this.supplier_index + "][supplier_id]' value='" + jQuery('#supplier_id').val() + "'>" + "</td>" +
		"<td><i class='fa fa-remove fa-fw' onclick='interfaces.removeElementFromTable(this)'></i></td>"  +
		"</tr>";

	this.supplier_index++;
	jQuery('#supplier_tab tbody').append(tr);
};

Interfaces.prototype.addCategory = function()
{
	var tr = "<tr>" +
	"<td>" + jQuery('#category_tab #alias').val() + "<input type='hidden' name='data[categories][" + this.category_index + "][alias]' value='" + jQuery('#category_tab #alias').val() + "'>" + "</td>" +
	"<td>" + this.currentCategoryData['value'] + "<input type='hidden' name='data[categories][" + this.category_index + "][category_id]' value='" + this.currentCategoryData['id'] + "'>" + "</td>" +
	"<td><i class='fa fa-remove fa-fw' onclick='interfaces.removeElementFromTable(this)'></i></td>"  +
	"</tr>";

	this.category_index++;
	jQuery('#category_tab tbody').append(tr);
	jQuery('#category_tab #alias').val('');
	jQuery('#category_tab #categories').val('');
};

Interfaces.prototype.addAttributes = function()
{
	var tr = "<tr>" +
		"<td>" + jQuery('#attribute_tab #alias').val() + "<input type='hidden' name='data[attributes][" + this.attr_index + "][alias]' value='" + jQuery('#attribute_tab #alias').val() + "'>" + "</td>" +
		"<td>" + jQuery('#set_id option:selected').text() + "<input type='hidden' name='data[attributes][" + this.attr_index + "][attribute_set_id]' value='" + jQuery('#set_id').val() + "'>" + "</td>"  +
		"<td>" + jQuery('#attribute_id option:selected').text() + "<input type='hidden' name='data[attributes][" + this.attr_index + "][attribute_id]' value='" + jQuery('#attribute_id').val() + "'>" + "</td>"  +
		"<td><i class='fa fa-remove fa-fw' onclick='interfaces.removeElementFromTable(this)'></i></td>"  +
		"</tr>";
	this.attr_index++;
	jQuery('#attribute_tab tbody').append(tr);
	jQuery('#attribute_tab #alias').val('');
};

Interfaces.prototype.addAttrValAttributes = function()
{
	var tr = "<tr>" +
		"<td>" + jQuery('#attr_value_tab #alias').val() + "<input type='hidden' name='data[attribute_values][" + this.attr_val_index + "][alias]' value='" + jQuery('#attr_value_tab #alias').val() + "'>" + "</td>" +
		"<td>" + jQuery('#attrib_value_set_id option:selected').text() + "<input type='hidden' name='data[attribute_values][" + this.attr_val_index + "][attribute_set_id]' value='" + jQuery('#attrib_value_set_id').val() + "'>" + "</td>"  +
		"<td>" + jQuery('#attr_val_attribute_id option:selected').text() + "<input type='hidden' name='data[attribute_values][" + this.attr_val_index + "][attribute_id]' value='" + jQuery('#attr_val_attribute_id').val() + "'>" + "</td>"  +
		"<td>" + jQuery('#attr_val_attribute_value_id option:selected').text() + "<input type='hidden' name='data[attribute_values][" + this.attr_val_index + "][value]' value='" + jQuery('#attr_val_attribute_value_id').val() + "'>" + "</td>"  +
		"<td><i class='fa fa-remove fa-fw' onclick='interfaces.removeElementFromTable(this)'></i></td>"  +
		"</tr>";
	this.attr_val_index++;

	jQuery('#attr_value_tab tbody').append(tr);
	jQuery('#attr_value_tab #alias').val('');
};

var interfaces = new Interfaces();

jQuery(document).ready(function() {
	interfaces.loadCategoryTable();
	interfaces.loadSupplierTable();
	interfaces.loadAttributeTable();
	interfaces.loadAttributeValueTable();
	interfaces.loadDefaultSuppliers();
	interfaces.loadDefaultCategories();
	interfaces.loadDefaultAttributes();
	interfaces.loadDefaultAttributeValues();
	interfaces.loadAttributes();
	interfaces.loadAttrValAttributes();

	jQuery('#submit_btn').prependTo('.page-header-buttons');

});
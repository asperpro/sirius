function Revision() {}

Revision.prototype.initRevision = function()
{
	var _this = this;
	jQuery.ajax({
		url: '/catalog/revision/',
		dataType: 'html',
		beforeSend: function() {
		},
		success: function (result) {
			jQuery('#revision-content').html(result);
			_this.uploadifyMedia();
			core.initTinyMce();
			core.initFancyBox();
		},
		error: function () {
			jQuery('#revision-content').append('Не удалось подгрузить ревизии');
		}
	});
};

Revision.prototype.validateRevision = function(form)
{
	if (!form.valid()) return false;
	if (!jQuery('.category input[type=checkbox]:checked').size()) {
		alert('Выберите одну или несколько категорий');
		return false;
	}
	if (!jQuery('.image-group input[type=checkbox]:checked').size()) {
		alert('Загрузите одно или несколько изображений');
		return false;
	}

	return true;
}

Revision.prototype.submitForm = function(element)
{
	var _this = this;
	var form  = jQuery(element).parents('form');
	var data  = form.serialize();
	var url   = (jQuery('.revision-item').length > 1) ? 'save-multiply' : 'save';

	if (!this.validateRevision(form)) return false;

	jQuery('#submit, #cancel').attr("disabled", "disabled").off('click');
 	jQuery(form).find('.ajax-loader').css("display", "inline-block");

	jQuery.ajax({
		type: "POST",
		url: "/catalog/revision/" + url,
		dataType: 'json',
		data: data,
		success: function(info) {
			if (info.status == 'success') {
	 			jQuery('#revision-content').html('');
				 _this.initRevision();
			} else {
				jQuery(form).find('.ajax-loader').css("display", "none");
	 			jQuery('#submit, #cancel').removeAttr("disabled");
	 			jQuery('#revision-content').html(info.message);
			}
		}
	});
};

Revision.prototype.cancelForm = function(element)
{
	var _this = this;
	var form = jQuery(element).parents('form');
	var data = form.serialize();

	jQuery('#submit, #cancel').attr("disabled", "disabled").off('click');
	jQuery(form).find('.ajax-loader').css("display", "inline-block");

	jQuery.ajax({
		type: "POST",
		url: "/catalog/revision/cancel",
		dataType: 'json',
		data: data,
		success: function(info){
			if (info.status == 'success') {
				jQuery('#revision-content').html('');
				_this.initRevision();
			} else {
				jQuery(form).find('.ajax-loader').css("display", "none");
				jQuery('#submit, #cancel').removeAttr("disabled");
				jQuery('#revision-content').html(info.message);
			}
		}
	});
};

Revision.prototype.addCategory = function(data, _this)
{
	var item = jQuery(_this).parents('.revision-item');
	var revision = jQuery(item).find('.revision_id').val();
	var index = jQuery(item).find('.form-group.category').length;
	var categoryForm = jQuery(item).find('.form-auto-complete');

	var template = '<div class="form-group category">'

		+ '<div class="col-lg-11">'
		+ '<label for="category-' + data.id + '" class="control-label optional">' + data.label + '</label>'
		+ '<input type="hidden" name="category[' + revision + '][' + index + '][value]"  value="' + data.id + '">'
		+ '</div>'

		+ '<div class="col-lg-1">'
		+ '<input type="checkbox" name="category[' + revision + '][' + index + '][checked]" id="category-' + data.id + '" checked="checked">'
		+ '</div>'

		+ '</div>';

	jQuery(template).insertBefore(categoryForm);
	jQuery('.auto-categ').val('');
};

Revision.prototype.getAttributes = function(element)
{
	var setId      = jQuery(element).val();
	var revisionId = jQuery(element).parents('.revision-item').find('.revision_id').val();

	jQuery.ajax({
		url: '/catalog/revision/get-attributes/setid/' + setId + '/revisid/' + revisionId,
		contentType: "text/html; charset=utf-8",
		dataType: 'html',
		success: function(result) {
			jQuery(element).parents('.revision-item').find('.attribute-list').html(result);
		}
	});
};

Revision.prototype.addMedia = function(id, selector)
{
	jQuery.ajax({
		type: "POST",
		dataType: 'json',
		url: '/catalog/revision/get-media/media_id/' + id,

		success: function (result) {
			var out = '';

			var item = jQuery('#' + selector).parents('.revision-item');
			var revision = jQuery(item).find('.revision_id').val();
			var index = jQuery(item).find('.form-group.media').length;

			if ( ! jQuery.isEmptyObject(result)) {

				out += "<div class='row'>" +
						"<div class='form-group media'>" +
							"<div class='col-lg-3'>" +
								"<input type='checkbox' name='media[" + revision + "][" + index + "][checked]' checked>" +
							"</div>" +
							"<div class='col-lg-6'>" +
								"<a class='fancybox-thumbs' data-fancybox-group='gallery' href='/media/product/" + result.filename + "'>" +
									"<img class='thumb-image' src='/media/product/" + result.filename + "'>" +
								"</a>" +
								"<input type='hidden' name='media[" + revision + "][" + index + "][value]'  value='" + result.id + "' class='form-control input-sm'>" +
							"</div>" +
							"<div class='col-lg-3'>" +
								"<input type='radio' name='media[main]' value='" + revision + "-" + index + "'>" +
							"</div>" +
						"</div>" +
				"</div>";
			}
			jQuery('#' + selector).append(out);
		}
	});
};

Revision.prototype.uploadifyMedia = function()
{
	var _this = this;

	jQuery(".images").each(function(index, element){
		var selector = jQuery(element).parents('.revision-item').find('.loaded-images').attr('id');
		jQuery(element).uploadify({
			'swf'      : '/public/swf/uploadify.swf',
			'uploader' : '/catalog/revision/uploadify/__tkn/35fbc5c646f61cd0ab19b72dd542e34b/id/',
			'folder'   : '/tmp/',
			'buttonText' : 'Выберете файл(ы)...',
			'cancelImg': '/public/img/uploadify-cancel.png',
			'auto'     : true,
			'multi'	   : true,
			'width'    : 200,
			'fileExt'  : '*.jpg;*.gif;*.png',
			'fileDesc' : 'Файлы изображений',
			'method'   : 'post',
			'onCancel' : function(file) {
				alert('Предупреждение: Вы отменили загрузку файла ' + file.name);
			},
			'onUploadError' : function(file, errorCode, errorMsg, errorString) {
				alert('The file ' + file.name + ' could not be uploaded: ' + errorString);
			},
			'onUploadSuccess' : function(file, data, response) {
				var data = jQuery.parseJSON(data);
				if (data.status == 'success') {
					_this.addMedia(data.id, selector);
				}
			}
		});
	});
};

var revision = new Revision();

jQuery(document).ready(function() {
	revision.initRevision();

	jQuery(window).on("scroll", function(e) {
		if (jQuery(document).scrollTop() > 140) {
			jQuery("#revision-header").addClass("fixed");
			jQuery(".revisions").css("margin-top", 130);
		} else {
			jQuery("#revision-header").removeClass("fixed");
			jQuery(".revisions").css("margin-top", 0);
		}
	});

});

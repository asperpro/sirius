function Indices() {}

Indices.prototype.submitForm = function(element)
{
	if (confirm('Подтвердите действие')) {

		var form = jQuery(element).parents('form');
		var data = form.serialize();

		jQuery(form).find('.ajax-loader').css("display", "inline-block");
		jQuery(form).find('*').attr("disabled", "disabled").off('click');

		jQuery.ajax({
			type: "POST",
			url: "/system/indexer/update-attribute",
			dataType: 'json',
			data: data,
			success: function(info){
				jQuery(form).find('.ajax-loader').css("display", "none");
				jQuery(form).find('*').removeAttr("disabled");

				if (info.status == 'error') {
					jQuery(form).removeAttr("disabled").append(info.message);
				}
			}
		});

	}
};

Indices.prototype.update = function(code)
{
	if (confirm('Подтвердите действие')) {

		var tr = jQuery('#update-' + code);
		var id = jQuery('#' + code).val();
		jQuery(tr).find('.ajax-loader').css("display", "inline-block");
		jQuery(tr).find('*').attr("disabled", "disabled").off('click');

		jQuery.ajax({
			type: "POST",
			url: "/system/indexer/update-" + code + "/id/" + id,
			dataType: 'json',
			success: function(info){

				jQuery(tr).find('.ajax-loader').css("display", "none");
				jQuery(tr).find('*').removeAttr("disabled");

				if (info.status == 'success') {
					jQuery('#date-' + code).text(info.date);
				} else {
					jQuery(tr).removeAttr("disabled").append(info.message);
				}
			}
		});

	}
};

Indices.prototype.loadAttributes = function()
{
	var setId = jQuery('#set_id').val();

	jQuery.ajax({
		url: '/attribute/index/load-attributes/setid/' + setId,
		contentType: "text/html; charset=utf-8",
		dataType: 'html',
		success: function (result) {
			var values = jQuery.parseJSON(result);

			var element = "<div class='form-group load-attribute'><div>" +
				"<select name='attribute_id' id='attribute_id' class='form-control input-sm'>" +
				"<option value=''> -- выбрать атрибут </option>";

			if (values.length > 0) {
				for (var i = 0; i < values.length; i++) {
					if (values[i] !== null) {
						element += "<option value='" + values[i]['id'] + "'>" + values[i]['name'] + "</option>";
					}
				}
			}

			element += "</select>" +
			"</div></div>";

			jQuery('#attribute_div').html(element);

		}
	});
};



var indices = new Indices();

jQuery(document).ready(function() {
	indices.loadAttributes();
});
